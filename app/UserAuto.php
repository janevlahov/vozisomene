<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAuto extends Model
{
    public function user(){
    	return $this->belongsTo('App/User','id');
    }

    public function getLicencePlateAttribute($value)
    {
        return strtoupper($value);
    }
}
