<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function Auto() {
        return  $this->hasOne('App\UserAuto','id');
    }
    public function Userpreferences() {
        return  $this->hasOne('App\UserPreferences','id');
    }
    public function Userdetails() {
        return  $this->hasOne('App\UserDetails','id');
    }
    public function Rides(){
        return $this->hasMany('App\Rides');
    }
    public function Notificationsettings(){
        return $this->hasOne('App\NotificationSettings','id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
