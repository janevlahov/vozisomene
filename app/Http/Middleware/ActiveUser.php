<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) 
        {
            if(!Auth::user()->active){
                Auth::logout();
                return redirect('/login');
            }
        }

        return $next($request);
    }
}
