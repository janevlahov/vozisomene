<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $notification = auth()->user()->notifications()->where('id', $id)->first();

        if ($notification) {
            $notification->markAsRead();
            if(isset($notification->data['ride_id'])){
            	return redirect($notification->data['url'].$notification->data['ride_id']);
            }else{
            	return redirect($notification->data['url']);
            } 
        }
    }

    public function markAllRead()
    {
        $markupdated = auth()->user()->unreadNotifications()->update(['read_at' => now()]);
        return response()->json();
    }
}
