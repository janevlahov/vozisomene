<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Rides;
use App\RidesAdds;
use App\User;
use App\Bookings;
use Auth;
use Carbon\Carbon;
use Notification;
use App\Http\Middleware\ActiveUser;
use App\Notifications\NewRideCreation;
use App\Notifications\NewRideRequest;
use App\Notifications\RideRequestAccepted;
use App\Notifications\RideRequestCanceled;
use App\Notifications\RideRequestChanged;
use App\Notifications\RideRequestRejected;
use App\Notifications\RideUpdated;
use App\Notifications\RideStatusCanceled;

class RidesController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['search_ride']]);
        $this->middleware(ActiveUser::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentuserid = Auth::user()->id;
        $user = User::find($currentuserid);
        $userRides = $user->Rides()
        ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
        ->where('removed','!=','1')->orderBy('created_at', 'desc')->paginate(10);

        return view('rides.index')->with('userRides', $userRides);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$currentuserid = Auth::user()->id;

        $ride = new Rides;
        $ride->user_id = $currentuserid;
        $ride->d_from = $request->input('d_from');
        $ride->d_to = $request->input('d_to');
        $ride->d_stop_1 = $request->input('d_stop_1');
        $ride->d_stop_2 = $request->input('d_stop_2');

        $roundtrip = $request->input('roundtrip');
        if(!isset($roundtrip)){
            $ride->roundtrip = 0;
        }else{
            $ride->roundtrip = $request->input('roundtrip');
        }

        $back_s_guarantee = $request->input('back_s_guarantee');
        if(!isset($back_s_guarantee)){
            $ride->back_s_guarantee = 0;
        }else{
            $ride->back_s_guarantee = $request->input('back_s_guarantee');
        }

        $terms_cond = $request->input('terms_cond');
        if(!isset($terms_cond)){
            $ride->terms_cond = 0;
        }else{
            $ride->terms_cond = $request->input('terms_cond');
        }

       if($request->input('d_date') != ''){
            $ride->d_date = date('Y-m-d',strtotime($request->input('d_date')));
            $ride->d_time = $request->input('d_time');
        }

        if($request->input('d_rdate') != ''){
            $ride->d_rdate = date('Y-m-d',strtotime($request->input('d_rdate')));
            $ride->d_rtime = $request->input('d_rtime');
        }

        $ride->d_cost1 = $request->input('costShare1');
        $ride->d_cost2 = $request->input('costShare2');
        $ride->d_cost3 = $request->input('costShare3');
        $ride->d_cost4 = $request->input('costShare4');
        $ride->d_cost5 = $request->input('costShare5');
        $ride->d_cost6 = $request->input('costShare6');

        $ride->d_stop1_time = $request->input('d_stop1_time');
        $ride->d_stop2_time = $request->input('d_stop2_time');
        $ride->d_stop3_time = $request->input('d_stop3_time');

        $ride->free_places = $request->input('free_places');
        $ride->d_description = $request->input('ride_comment');
        $ride->distance = $request->input('distance');
        $ride->est_d_time = $request->input('est_d_time');
        $ride->status = "in_progress";

        $ride->save();
        $ride_id = $ride->id;

        $ride_adds = new RidesAdds;
        $ride_adds->ride_id = $ride_id;
        $ride_adds->r_qty_places = $request->input('free_places');
        $ride_adds->save();

        //Email Notification to User
        $user = User::find($currentuserid);

        //Get Notification setting for receiving user
        $mailNotification = $user->NotificationSettings->new_ride;

        //Mail to Ride Owner
        $user->notify(new NewRideCreation($ride,$mailNotification));

        return redirect('/rides/'.$ride_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ride = Rides::where('rides.id' ,$id)->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')->first();

        //Metatags for crawlers
        if(!Auth::check()){

            $urlId = $id;
            $rideFrom = $ride->d_from;
            $rideTo = $ride->d_to;
            $rideRQtyPlaces = $ride->r_qty_places;

            return view('layouts.app')->with('urlId',$urlId)->with('rideFrom', $rideFrom)->with('rideTo', $rideTo)->with('rideRQtyPlaces', $rideRQtyPlaces);
        }

        //Redirect if attempted to access nonexistent record
        if(empty($ride)){
            return redirect('/rides');
        }
        //Prevent accessing nonowned records
        if(auth()->user()->id != $ride->user_id){
            return redirect('/rides');
        }

        $users_booked = DB::table('bookings')->select('bookings.id','bookings.b_from','bookings.b_to', 'bookings.status', 'bookings.b_cost' , 'bookings.qty_places', 'bookings.user_id', 'rides.d_stop_1', 'rides.d_stop_2', 'user_details.avatar', 'users.firstname', 'users.lastname', 'users.email', 'users.phone')
        ->where('bookings.ride_id', '=', $id)->Where(function ($users_booked) { $users_booked->where('bookings.status', '=', 'Pending')
        ->orWhere('bookings.status', '=', 'Accepted');})
        ->leftJoin('rides', 'bookings.ride_id', '=', 'rides.id')
        ->leftJoin('user_details', 'bookings.user_id', '=', 'user_details.id')
        ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->get();

        $disable_inputs = array();
        $date_today = Carbon::now()->format('Y-m-d');
        $time_now = Carbon::now()->format('H:i');
        if($ride->status == 'completed'){

            $disable_inputs = array('in_progress','canceled','completed','save_changes');

        }else{

            if($ride->d_date < $date_today){
                $disable_inputs = array('d_date','in_progress','save_changes');

            }elseif($ride->d_date == $date_today){
                if($ride->d_time < $time_now){
                    $disable_inputs = array('d_date','in_progress');
                }
            }
        }

        return view('rides.show')->with('ride', $ride)->with('users_booked', $users_booked)->with('disable_inputs',$disable_inputs);
    }

    public function show_booked_rides()
    {
        $currentuserid = Auth::user()->id;

        $booked_rides = DB::table('bookings')->select('bookings.ride_id', 'bookings.b_from', 'bookings.user_id','bookings.b_to','bookings.b_time','bookings.b_cost', 'bookings.status AS book_status', 'rides.d_from','rides.d_to','rides.d_stop_1','rides.d_stop_2','rides.d_cost1','rides.d_cost2','rides.d_cost3','rides.d_cost4','rides.d_cost5','rides.d_date','rides.d_time','rides.d_stop1_time','rides.d_stop2_time','rides.d_stop3_time','rides.status','rides_adds.r_qty_places')
        ->where('bookings.user_id', '=', $currentuserid)
        ->leftJoin('rides', 'bookings.ride_id', '=', 'rides.id')
        ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
        ->orderBy('bookings.created_at','desc')
        ->orderBy('rides.d_date','desc')
        ->paginate(10);

        return view('rides.my-booked')->with('booked_rides', $booked_rides);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentuserid = Auth::user()->id;

        $ride = Rides::find($id);
        $ride->user_id = $currentuserid;
        $ride->d_from = $request->input('d_from');
        $ride->d_to = $request->input('d_to');
        $ride->d_stop_1 = $request->input('d_stop_1');
        $ride->d_stop_2 = $request->input('d_stop_2');

        $roundtrip = $request->input('roundtrip');
        if(!isset($roundtrip)){
            $ride->roundtrip = 0;
        }else{
            $ride->roundtrip = $request->input('roundtrip');
        }

        $back_s_guarantee = $request->input('back_s_guarantee');
        if(!isset($back_s_guarantee)){
            $ride->back_s_guarantee = 0;
        }else{
            $ride->back_s_guarantee = $request->input('back_s_guarantee');
        }

        if($request->input('d_date') != ''){
            $ride->d_date = date('Y-m-d',strtotime($request->input('d_date')));
            $ride->d_time = $request->input('d_time');
        }

        if($request->input('d_rdate') != ''){
            $ride->d_rdate = date('Y-m-d',strtotime($request->input('d_rdate')));
            $ride->d_rtime = $request->input('d_rtime');
        }else{
            $ride->d_rdate = NULL;
            $ride->d_rtime = NULL;
        }

        $ride->d_cost1 = $request->input('costShare1');
        $ride->d_cost2 = $request->input('costShare2');
        $ride->d_cost3 = $request->input('costShare3');
        $ride->d_cost4 = $request->input('costShare4');
        $ride->d_cost5 = $request->input('costShare5');
        $ride->d_cost6 = $request->input('costShare6');

        $ride->d_stop1_time = $request->input('d_stop1_time');
        $ride->d_stop2_time = $request->input('d_stop2_time');
        $ride->d_stop3_time = $request->input('d_stop3_time');

        //Check if input is eligibe for update
        $free_places = $request->input('free_places');
        $booked_places = DB::table('bookings')->select('qty_places')->where('ride_id', '=', $id)->sum('qty_places');
        if($booked_places <= $free_places){
            $ride->free_places = $request->input('free_places');
        }

        $ride->d_description = $request->input('ride_comment');
        $ride->distance = $request->input('distance');
        $ride->est_d_time = $request->input('est_d_time');
        $ride->save();

        if($booked_places <= $free_places){

            $remain_places = $free_places - $booked_places;

            $update_places = new RidesAdds;
            $update_places->where('ride_id', $id)->update(['r_qty_places' => $remain_places]);
        }

        //Get all users that have booking for this trip
        $booked_users = Bookings::select('user_id')->where('ride_id', $id)->where('status', '!=' ,'Rejected')->get();
        foreach($booked_users as $buser){
            //$user = User::find(Bookings::find($id)->user->id);
            $user = User::find($buser);
            $r_firstname = Auth::user()->firstname;

            //Get Notification setting for receiving user
            $mailNotification = $user[0]->NotificationSettings->update_ride;
            //Notification to booked user
            $user[0]->notify(new RideUpdated($ride,$user,$r_firstname,$mailNotification));
        }

        return redirect('/rides/'.$id)->with('success','Измените се успешно зачувани');
    }


    public function search_ride(Request $request)
    {
        $fromI = $request->fp;
        $toI = $request->tp;
        $dsort = $request->dsort;
        $plsort = $request->plsort;
        $ratesort = $request->ratesort;

        if ($request->rd != '') {
            $dateI = date('Y-m-d',strtotime($request->rd));
        }else{
            $dateI = $request->rd;
        }

        if($dsort != ''){
            $dsort_bool = true;
        }else{
            $dsort_bool = false;
        }

        if($plsort != ''){
            $freeplaces_bool = true;
        }else{
            $freeplaces_bool = false;
        }

        if($ratesort != ''){
            $ratesort_bool = true;
        }else{
            $ratesort_bool = false;
        }

        $dateToday = date("Y-m-d");
        $timeNow = date('H:m');
        $dateNow = array('dateToday' => $dateToday, 'timeNow' => $timeNow);

        //From Input in array trimmed and unique
        $arrfromI = explode(" ", $fromI);
        $trimmedArrfromI = array();

        foreach ($arrfromI as $fromIval) {
            $trimmedfromIval = trim($fromIval, ',');
            if($trimmedfromIval != 'Македонија'){
                $trimmedArrfromI[] = $trimmedfromIval;
            }
        }

        $trimmedArrfromI = array_unique($trimmedArrfromI);

        $arrtoI = explode(" ", $toI);
        $trimmedArrtoI = array();

        foreach ($arrtoI as $toIval) {
            $trimmedtoIval = trim($toIval, ',');
            if($trimmedtoIval != 'Македонија'){
                $trimmedArrtoI[] = $trimmedtoIval;
            }
        }

        $trimmedArrtoI= array_unique($trimmedArrtoI);

        if($fromI != '' && $toI == '' && $dateI == ''){
            $searchRides = Rides::select('rides.*','users.id as driver_id','users.firstname','users.lastname','user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.car_photo','rides_adds.r_qty_places', DB::raw("(SELECT FORMAT(avg(user_reviews.rating), 1) FROM user_reviews WHERE user_reviews.r_user_id = rides.user_id) as driver_rating"))
           /* ->where('d_from', 'LIKE', '%'. $fromI .'%')*/
            ->where('rides.status', '=', 'in_progress')
            ->where(function ($query) use($dateToday, $timeNow) {
                $query->where('d_date' , '=' , $dateToday)
                    ->where('d_time', '>', $timeNow)
                    ->orwhere('d_date', '>', $dateToday);
            })

            ->where('rides_adds.r_qty_places', '>', 0)
            ->where(function ($query) use($trimmedArrfromI) {
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_from', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_stop_1', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_stop_2', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
            })
            ->leftJoin('users', 'rides.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'rides.user_id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'rides.user_id', '=', 'user_autos.id')
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->when($dsort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('d_date', $dsort);
                })
            ->when($freeplaces_bool, function ($query) use ($plsort) {
                    return $query->orderBy('rides_adds.r_qty_places', $plsort);
                })
            ->when($ratesort_bool, function ($query) use ($ratesort) {
                    return $query->orderBy('driver_rating', $ratesort);
                })
            ->paginate(5);

        }elseif ($fromI == '' && $toI != '' && $dateI == ''){
            $searchRides = Rides::select('rides.*','users.id as driver_id','users.firstname','users.lastname','user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.car_photo','rides_adds.r_qty_places', DB::raw("(SELECT FORMAT(avg(user_reviews.rating), 1) FROM user_reviews WHERE user_reviews.r_user_id = rides.user_id) as driver_rating"))
            ->where('rides.status', '=', 'in_progress')
            ->where(function ($query) use($dateToday, $timeNow) {
                $query->where('d_date' , '=' , $dateToday)
                    ->where('d_time', '>', $timeNow)
                    ->orwhere('d_date', '>', $dateToday);
            })
            ->where('rides_adds.r_qty_places', '>', 0)
            ->where(function ($query) use($trimmedArrtoI) {
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_to', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_stop_1', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_stop_2', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
            })
            ->leftJoin('users', 'rides.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'rides.user_id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'rides.user_id', '=', 'user_autos.id')
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->when($dsort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('d_date', $dsort);
                })
            ->when($freeplaces_bool, function ($query) use ($plsort) {
                    return $query->orderBy('rides_adds.r_qty_places', $plsort);
                })
            ->when($ratesort_bool, function ($query) use ($ratesort) {
                    return $query->orderBy('driver_rating', $ratesort);
                })
            ->paginate(5);

        }elseif ($fromI == '' && $toI == '' && $dateI != ''){
            $searchRides = Rides::select('rides.*','users.id as driver_id','users.firstname','users.lastname','user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.car_photo','rides_adds.r_qty_places', DB::raw("(SELECT FORMAT(avg(user_reviews.rating), 1) FROM user_reviews WHERE user_reviews.r_user_id = rides.user_id) as driver_rating"))
            ->where('d_date' , '=' , $dateI)
            ->where('rides.status', '=', 'in_progress')
            ->where('rides_adds.r_qty_places', '>', 0)
            ->leftJoin('users', 'rides.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'rides.user_id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'rides.user_id', '=', 'user_autos.id')
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->when($dsort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('d_date', $dsort);
                })
            ->when($freeplaces_bool, function ($query) use ($plsort) {
                    return $query->orderBy('rides_adds.r_qty_places', $plsort);
                })
            ->when($ratesort_bool, function ($query) use ($ratesort) {
                    return $query->orderBy('driver_rating', $ratesort);
                })
            ->paginate(5);

        }elseif($fromI != '' && $toI != '' && $dateI == ''){
            $searchRides = Rides::select('rides.*','users.id as driver_id','users.firstname','users.lastname','user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.car_photo','rides_adds.r_qty_places', DB::raw("(SELECT FORMAT(avg(user_reviews.rating), 1) FROM user_reviews WHERE user_reviews.r_user_id = rides.user_id) as driver_rating"))
            ->where('rides.status', '=', 'in_progress')
            ->where(function ($query) use($dateToday, $timeNow) {
                $query->where('d_date' , '=' , $dateToday)
                    ->where('d_time', '>', $timeNow)
                    ->orwhere('d_date', '>', $dateToday);
            })
            ->where('rides_adds.r_qty_places', '>', 0)
            ->where(function ($query) use($trimmedArrfromI) {
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_from', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_stop_1', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_stop_2', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
            })

            ->where(function ($query) use($trimmedArrtoI) {
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_to', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_stop_1', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_stop_2', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
            })
            ->leftJoin('users', 'rides.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'rides.user_id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'rides.user_id', '=', 'user_autos.id')
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->when($dsort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('d_date', $dsort);
                })
            ->when($freeplaces_bool, function ($query) use ($plsort) {
                    return $query->orderBy('rides_adds.r_qty_places', $plsort);
                })
            ->when($ratesort_bool, function ($query) use ($ratesort) {
                    return $query->orderBy('driver_rating', $ratesort);
                })
            ->paginate(5);

        }elseif($fromI != '' && $toI == '' && $dateI != ''){
            $searchRides = Rides::select('rides.*','users.id as driver_id','users.firstname','users.lastname','user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.car_photo','rides_adds.r_qty_places', DB::raw("(SELECT FORMAT(avg(user_reviews.rating), 1) FROM user_reviews WHERE user_reviews.r_user_id = rides.user_id) as driver_rating"))
            ->where('d_date' , '=' , $dateI)
            ->where('rides.status', '=', 'in_progress')
            ->where('rides_adds.r_qty_places', '>', 0)
            ->where(function ($query) use($trimmedArrfromI) {
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_from', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_stop_1', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_stop_2', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
            })

            ->leftJoin('users', 'rides.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'rides.user_id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'rides.user_id', '=', 'user_autos.id')
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->when($dsort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('d_date', $dsort);
                })
            ->when($freeplaces_bool, function ($query) use ($plsort) {
                    return $query->orderBy('rides_adds.r_qty_places', $plsort);
                })
            ->when($ratesort_bool, function ($query) use ($ratesort) {
                    return $query->orderBy('driver_rating', $ratesort);
                })
            ->paginate(5);

        }elseif($fromI == '' && $toI != '' && $dateI != ''){
            $searchRides = Rides::select('rides.*','users.id as driver_id','users.firstname','users.lastname','user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.car_photo','rides_adds.r_qty_places', DB::raw("(SELECT FORMAT(avg(user_reviews.rating), 1) FROM user_reviews WHERE user_reviews.r_user_id = rides.user_id) as driver_rating"))
            ->where('d_date' , '=' , $dateI)
            ->where('rides.status', '=', 'in_progress')
            ->where('rides_adds.r_qty_places', '>', 0)
            ->where(function ($query) use($trimmedArrtoI) {
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_to', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_stop_1', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_stop_2', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
            })

            ->leftJoin('users', 'rides.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'rides.user_id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'rides.user_id', '=', 'user_autos.id')
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->when($dsort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('d_date', $dsort);
                })
            ->when($freeplaces_bool, function ($query) use ($plsort) {
                    return $query->orderBy('rides_adds.r_qty_places', $plsort);
                })
            ->when($ratesort_bool, function ($query) use ($ratesort) {
                    return $query->orderBy('driver_rating', $ratesort);
                })
            ->paginate(5);

        }elseif($fromI != '' && $toI != '' && $dateI != ''){
            $searchRides = Rides::select('rides.*','users.id as driver_id','users.firstname','users.lastname','user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.car_photo','rides_adds.r_qty_places', DB::raw("(SELECT FORMAT(avg(user_reviews.rating), 1) FROM user_reviews WHERE user_reviews.r_user_id = rides.user_id) as driver_rating"))
            ->where('d_date' , '=' , $dateI)
            ->where('status', '=', 'in_progress')
            ->where('rides_adds.r_qty_places', '>', 0)
            ->where(function ($query) use($trimmedArrfromI) {
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_from', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_stop_1', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrfromI); $i++){
                    $query->orwhere('d_stop_2', 'LIKE',  '%' . $trimmedArrfromI[$i] .'%');
                }
            })
            /*->where('d_to', 'LIKE', '%'. $toI .'%')*/
            ->where(function ($query) use($trimmedArrtoI) {
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_to', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_stop_1', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
                for ($i = 0; $i < count($trimmedArrtoI); $i++){
                    $query->orwhere('d_stop_2', 'LIKE',  '%' . $trimmedArrtoI[$i] .'%');
                }
            })

            ->leftJoin('users', 'rides.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'rides.user_id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'rides.user_id', '=', 'user_autos.id')
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->when($dsort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('d_date', $dsort);
                })
            ->when($freeplaces_bool, function ($query) use ($plsort) {
                    return $query->orderBy('rides_adds.r_qty_places', $plsort);
                })
            ->when($ratesort_bool, function ($query) use ($ratesort) {
                    return $query->orderBy('driver_rating', $ratesort);
                })
            ->paginate(5);

        }elseif($request->dest != ''){
            $destination = filter_var($request->dest, FILTER_SANITIZE_STRING);

            $searchRides = Rides::select('rides.*','users.id as driver_id','users.firstname','users.lastname','user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.car_photo','rides_adds.r_qty_places', DB::raw("(SELECT FORMAT(avg(user_reviews.rating), 1) FROM user_reviews WHERE user_reviews.r_user_id = rides.user_id) as driver_rating"))
            ->where('rides.status', '=', 'in_progress')
            ->where(function ($query) use($dateToday, $timeNow) {
                $query->where('d_date' , '=' , $dateToday)
                    ->where('d_time', '>', $timeNow)
                    ->orwhere('d_date', '>', $dateToday);
            })
            ->where('d_to' , 'LIKE' , '%' . $destination . '%')
            ->where('rides_adds.r_qty_places', '>', 0)
            ->leftJoin('users', 'rides.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'rides.user_id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'rides.user_id', '=', 'user_autos.id')
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->when($dsort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('d_date', $dsort);
                })
            ->when($freeplaces_bool, function ($query) use ($plsort) {
                    return $query->orderBy('rides_adds.r_qty_places', $plsort);
                })
            ->when($ratesort_bool, function ($query) use ($ratesort) {
                    return $query->orderBy('driver_rating', $ratesort);
                })
            ->paginate(5);

        }else{

            $searchRides = Rides::select('rides.*','users.id as driver_id','users.firstname','users.lastname','user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.car_photo','rides_adds.r_qty_places', DB::raw("(SELECT FORMAT(avg(user_reviews.rating), 1) FROM user_reviews WHERE user_reviews.r_user_id = rides.user_id) as driver_rating"))
            ->where(function ($query) use($dateToday, $timeNow) {
                $query->where('d_date' , '=' , $dateToday)
                    ->where('d_time', '>', $timeNow)
                    ->orwhere('d_date', '>', $dateToday);
            })
            ->where('status', '=', 'in_progress')
            ->where('rides_adds.r_qty_places', '>', 0)
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->leftJoin('users', 'rides.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'rides.user_id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'rides.user_id', '=', 'user_autos.id')
            ->when(!$dsort_bool && !$freeplaces_bool && !$ratesort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('rides.created_at', 'desc');
                })
            ->when($dsort_bool, function ($query) use ($dsort) {
                    return $query->orderBy('d_date', $dsort);
                })
            ->when($freeplaces_bool, function ($query) use ($plsort) {
                    return $query->orderBy('rides_adds.r_qty_places', $plsort);
                })
            ->when($ratesort_bool, function ($query) use ($ratesort) {
                    return $query->orderBy('driver_rating', $ratesort);
                })
            ->paginate(5);
        }

        $searchInputs = array("fromI"=>$fromI, "toI"=>$toI, "dateI"=>$dateI, "plsort"=>$plsort, "dsort"=>$dsort, "ratesort"=>$ratesort);

        return view('rides.search-ride')->with('userRides', $searchRides)->with('searchInputs', $searchInputs)->with('dateNow', $dateNow);
    }

    public function driver_rating($user_id){
        $driver_rating = DB::table('user_reviews')->where('r_user_id', '=', $user_id)->avg('rating');
        if(!empty($driver_rating)){
            $driver_rating = number_format($driver_rating, 1, '.', '');
        }

        return $driver_rating;
    }

    public function detailed_ride_view(Request $request,$id)
    {
        $ride = DB::table('rides')->select('rides.*','rides_adds.ride_id','rides_adds.r_qty_places')
            ->where('rides.id', '=', $id)
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->first();

        //Metatags for crawlers
        if(!Auth::check()){

            $urlId = $id;
            $rideFrom = $ride->d_from;
            $rideTo = $ride->d_to;
            $rideRQtyPlaces = $ride->r_qty_places;

            return view('layouts.app')->with('urlId',$urlId)->with('rideFrom', $rideFrom)->with('rideTo', $rideTo)->with('rideRQtyPlaces', $rideRQtyPlaces);
        }

        $mode = $request->input('mode');
        $currentuserid = Auth::user()->id;

        if($mode == 'requested_new'){
            DB::table('bookings')->where('user_id', '=', $currentuserid)->where('ride_id', '=', $id)->delete();
        }


        if(empty($ride)){ return redirect('/');}

        $user = DB::table('users')->select('users.firstname','users.lastname','users.phone','users.email','users.created_at' ,'user_details.avatar','user_autos.car_brand','user_autos.car_model','user_autos.year_production','user_autos.color','user_autos.type','user_autos.car_photo','user_preferences.talkative','user_preferences.smoker','user_preferences.pets','user_preferences.music')
            ->where('users.id', '=', $ride->user_id)
            ->leftJoin('user_details', 'users.id', '=', 'user_details.id')
            ->leftJoin('user_autos', 'users.id', '=', 'user_autos.id')
            ->leftJoin('user_preferences', 'users.id', '=', 'user_preferences.id')
            ->first();

        $driver_rating = $this->driver_rating($ride->user_id);

        $booked_places = DB::table('bookings')->select('qty_places')->where('ride_id', '=', $id)->where('user_id', '!=', $currentuserid)->sum('qty_places');

        $users_booked = DB::table('bookings')->select('bookings.id','bookings.b_from','bookings.b_to','bookings.user_id', 'rides.d_stop_1','rides.d_stop_2','user_details.avatar','users.firstname','users.lastname')
            ->where('bookings.ride_id', '=', $id)->where(function ($users_booked) { $users_booked->where('bookings.status', '=', 'Pending')
            ->orWhere('bookings.status', '=', 'Accepted');})
            ->leftJoin('rides', 'bookings.ride_id', '=', 'rides.id')
            ->leftJoin('user_details', 'bookings.user_id', '=', 'user_details.id')
            ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
            ->get();

        $chk_booked = $users_booked->contains('user_id',$currentuserid);
        $cur_booked_info = DB::table('bookings')->select('status','qty_places','b_from','b_to', 'b_cost')->where('user_id', '=', $currentuserid)->where('ride_id','=', $id)->first();

        $current_user_phone = Auth::user()->phone;

        $review = DB::table('user_reviews')->where('s_user_id', '=', $currentuserid)->where('ride_id', '=', $id)->first();

        //check permissions for modifying reservation
        $ride_permissions = array();
        $date_today = Carbon::now()->format('Y-m-d');
        $time_now = Carbon::now()->format('H:i');
        if($date_today == $ride->d_date){
            if($time_now < $ride->d_time && $ride->status == 'in_progress'){
                $ride_permissions = array('Change', 'Cancel');
            }else{
                $ride_permissions = array();
            }

        }elseif($date_today > $ride->d_date){
            $ride_permissions = array();

        }elseif($date_today < $ride->d_date && $ride->status == 'in_progress'){
            $ride_permissions = array('Change', 'Cancel');
        }

        return view('rides.detailed-view', compact('ride','user','booked_places','users_booked','chk_booked','mode','currentuserid','cur_booked_info','current_user_phone','ride_permissions','review','driver_rating'));
    }

    public function book_ride(Request $request)
    {
        $ride_id = $request->ride_id;
        $currentuserid = Auth::user()->id;
        $option = explode(" - ",$request->options);
        $qty_places = $request->qty_places;

        $mode = $request->mode;
        $ride_book = new Bookings;

        //For the Notification
        $user = User::find(Rides::find($ride_id)->user->id);
        //Get Notification setting for receiving user
        $mailNotification = $user->NotificationSettings->ride_request;

        $curruser_firstname = Auth::user()->firstname;
        $ride_user_firstname = User::find(Rides::find($ride_id)->user->id)['firstname'];
        $ride = Rides::find($ride_id);

        if($mode != 'cancel_ride'){
            //Update Contact Phone
            if($request->input('phone') != ''){
                $this->validate($request, [
                    'phone' => 'digits_between:6,15'
                ]);

                $UserData = User::find($currentuserid);
                $UserData->phone = $request->input('phone');
                $UserData->save();
            }else{
                return redirect('/rides/detailed-view/'.$ride_id);
            }
        }

        if($mode == 'edit'){
            $ride_book->where('user_id', $currentuserid)->where('ride_id', $ride_id)->update([
                'b_from' => $option[0],
                'b_to' => $option[1],
                'b_cost' => $option[2],
                'b_time' => $option[3],
                'qty_places' => $qty_places,
                'status' => 'Pending'
            ]);

            //Ride Request Changed Notification to Ride Owner
            $user->notify(new RideRequestChanged($curruser_firstname,$ride_user_firstname,$ride_id,$ride,$mailNotification));

        }elseif($mode == 'cancel_ride'){

            $booked_places = DB::table('bookings')->select('qty_places')->where('ride_id', '=', $ride_id)->where('user_id', '=', $currentuserid)->sum('qty_places');

            $ride_book->where('user_id', $currentuserid)->where('ride_id', $ride_id)->update([
                'status' => 'Canceled',
                'qty_places' => '0',
            ]);

            //New Ride Request Canceled Notification to Ride Owner
            $user->notify(new RideRequestCanceled($curruser_firstname,$ride_user_firstname,$ride_id,$ride,$mailNotification));

        }else{

            $get_places = new RidesAdds;
            $check_places = $get_places->select('r_qty_places')->where('ride_id', $ride_id)->first();

            if($check_places->r_qty_places <= 0){
                return redirect('/rides/detailed-view/'.$ride_id);
            }

            if ($ride_book->where('user_id', $currentuserid)->where('ride_id', $ride_id)->exists()) {

                $ride_book->where('user_id', $currentuserid)->where('ride_id', $ride_id)->update([
                    'b_from' => $option[0],
                    'b_to' => $option[1],
                    'b_cost' => $option[2],
                    'b_time' => $option[3],
                    'qty_places' => $qty_places,
                    'status' => 'Pending'
                ]);

            }else{

                $ride_book->ride_id = $ride_id;
                $ride_book->user_id = $currentuserid;
                $ride_book->b_from = $option[0];
                $ride_book->b_to = $option[1];
                $ride_book->b_cost = $option[2];
                $ride_book->b_time = $option[3];
                $ride_book->qty_places = $qty_places;
                $ride_book->status = 'Pending';
                $ride_book->save();
            }

            //New Ride Request Notification to Ride Owner
            $user->notify(new NewRideRequest($curruser_firstname,$ride_user_firstname,$qty_places,$ride,$mailNotification));
        }

        $get_total_places = DB::table('rides')->select('free_places')->where('id', '=', $ride_id)->first();
        $booked_places = DB::table('bookings')->select('qty_places')->where('ride_id', '=', $ride_id)->sum('qty_places');

        $total_places = $get_total_places->free_places;
        $remain_places = $total_places - $booked_places;

        $update_places = new RidesAdds;
        $update_places->where('ride_id', $ride_id)->update(['r_qty_places' => $remain_places]);

        return redirect('/rides/detailed-view/'.$ride_id);
    }

    public function book_approval(Request $request){

        $formArrKey = array_keys($request->post());

        $booking_id = $request->post('booking_id');
        $user_id = $request->post('user_id');
        $ride_id = $request->post('ride_id');
        $formRequest = $formArrKey[0];
        $book_action = new Bookings;

        //For the Notification
        $user = User::find(Bookings::find($booking_id)->user->id);

        //Get Notification setting for receiving user
        $mailNotification = $user->NotificationSettings->ride_status;

        $curr_user = User::find(Auth::user()->id);
        $ride = Rides::find($ride_id);
        $book = Bookings::find($booking_id);
        $from = $book->b_from;
        $to = $book->b_to;
        $bFrom = $ride->$from;
        $bTo = $ride->$to;

        $ride_date = $ride->d_date;

        if($formRequest == 'accept'){

            $book_action->where('user_id', $user_id)->where('id', $booking_id)->update([
                'status' => 'Accepted'
            ]);

            //Ride Request Accepted Notification to Booking User
            $user->notify(new RideRequestAccepted($ride_id,$ride_date,$bFrom,$bTo,$curr_user,$user,$mailNotification));

        }elseif($formRequest == 'deny'){

            $book_action->where('user_id', $user_id)->where('id', $booking_id)->update([
                'status' => 'Rejected',
                'qty_places' => '0'
            ]);

            //Update Real Qty Places
            $get_total_places = DB::table('rides')->select('free_places')->where('id', '=', $ride_id)->first();
            $booked_places = DB::table('bookings')->select('qty_places')->where('ride_id', '=', $ride_id)->sum('qty_places');

            $total_places = $get_total_places->free_places;
            $remain_places = $total_places - $booked_places;

            $update_places = new RidesAdds;
            $update_places->where('ride_id', $ride_id)->update(['r_qty_places' => $remain_places]);

            //Ride Request Rejected Notification to Booking User
            $user->notify(new RideRequestRejected($ride_id,$ride_date,$bFrom,$bTo,$curr_user,$user,$mailNotification));

        }elseif($formRequest == 'modify'){

            $book_action->where('user_id', $user_id)->where('id', $booking_id)->update([
                'status' => 'Pending'
            ]);

        }

        return back();
    }

    public function update_ride_status(Request $request){

        if(isset($request->status_options)){

            $new_status = $request->status_options;
            $ride_id = $request->ride_id;
            $ride_update = new Rides;

            $ride_update->where('id', $ride_id)->update([
                'status' => $new_status
            ]);

            if($new_status == 'canceled'){

                //Get all users that have booking for this trip
                $booked_users = Bookings::select('user_id')->where('ride_id', $ride_id)->where('status', '!=' ,'Rejected')->get();
                foreach($booked_users as $buser){

                    $user = User::find($buser);
                    $userFirstname = $user[0]->firstname;
                    $r_firstname = Auth::user()->firstname;

                    $ride = Rides::find($ride_id);

                    //Notification to booked user
                    $user[0]->notify(new RideStatusCanceled($ride,$userFirstname,$r_firstname));
                }

            }
        }

        return back();
    }

    public function show_user($id){

        $user_data = DB::table('users')->select('users.id','firstname', 'lastname', 'users.created_at','avatar','birthday','bio','car_brand','car_model','year_production','type','color','car_photo','talkative', 'smoker','smoker','pets','music', 'active')
            ->where('users.id', '=', $id)
            ->leftJoin('user_autos', 'users.id', '=', 'user_autos.id')
            ->leftJoin('user_details', 'users.id', '=', 'user_details.id')
            ->leftJoin('user_preferences', 'users.id', '=', 'user_preferences.id')
            ->first();

        $user_reviews = DB::table('user_reviews')->select('user_reviews.*','users.firstname','user_details.avatar')
            ->where('r_user_id', '=', $id)
            ->leftJoin('users', 'user_reviews.s_user_id', '=', 'users.id')
            ->leftJoin('user_details', 'user_reviews.s_user_id', '=', 'user_details.id')
            ->get();

        $currentuserid = Auth::user()->id;

        $authUserData = DB::table('user_reports')
        ->where('reported_by', '=', $currentuserid)
        ->where('user_id', '=', $id)
        ->where('status', '!=', 'completed')
        ->exists();

        $avg_rating = $this->driver_rating($id);

        return view('users.show')->with('user', $user_data)->with('avg_rating',$avg_rating)->with('user_reviews',$user_reviews)->with('authUserData',$authUserData);
    }

    public function remove_ride(Request $request){

        $ride = Rides::find($request->id);
        $ride->removed = 1;
        $ride->save();

        return back();
    }

    public function remove_booking(Request $request){

        $currentuserid = Auth::user()->id;
        $book_update = new Bookings;
        $book_update->where('ride_id',$request->id)->where('user_id',$currentuserid)->update([
            'removed' => 1
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
