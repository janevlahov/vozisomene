<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserAuto;
use App\UserReport;
use Auth;
use Image;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Http\Middleware\ActiveUser;
use Notification;
use App\Notifications\NewUserReport;

class UsersController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
        $this->middleware(ActiveUser::class);
    }

    public function index(){

        $dateToday = date("Y-m-d");
        $timeNow = date('H:m');

        $getFrequnetRides = DB::table('rides')
            ->select('d_from', 'd_to', DB::raw('COUNT(*) AS `count`'))
            ->groupBy('d_from', 'd_to')
            ->where('rides.status', '=', 'in_progress')
            ->where(function ($query) use($dateToday, $timeNow) {
                $query->where('d_date' , '=' , $dateToday)
                    ->where('d_time', '>', $timeNow)
                    ->orwhere('d_date', '>', $dateToday);
            })
            ->where('rides_adds.r_qty_places', '>', 0)
            ->leftJoin('rides_adds', 'rides.id', '=', 'rides_adds.ride_id')
            ->orderBy('count', 'desc')
            ->limit(3)
            ->get();

    	return view('users.index')->with('getFrequnetRides', $getFrequnetRides);
    }

    public function dashboard(){

        $currentuserid = Auth::user()->id;
        $user = User::find($currentuserid);
        $userRides = $user->Rides()->select('id','d_from','d_to')->where('removed','!=','1')->orderBy('created_at', 'desc')->take(4)->get();

        $booked_rides = DB::table('bookings')->select('bookings.ride_id', 'bookings.b_from', 'bookings.user_id', 'bookings.b_to', 'bookings.status AS book_status', 'rides.d_from','rides.d_to', 'rides.d_stop_1','rides.d_stop_2','rides.status')
        ->where('bookings.user_id', '=', $currentuserid)
        ->where('bookings.removed','!=','1')
        ->leftJoin('rides', 'bookings.ride_id', '=', 'rides.id')
        ->orderBy('bookings.created_at','desc')
        ->take(4)->get();

        $UserInfo = array(
            'UserCity' => Auth::user()->Userdetails->city,
            'UserCarBrand' => Auth::user()->Auto->car_brand,
            'UserCarModel' => Auth::user()->Auto->car_model
        );
        
        return view('users.dashboard')->with('userRides', $userRides)->with('booked_rides',$booked_rides)->with('UserInfo',$UserInfo);
    }


    public function index_search(Request $request){

        $from = $request->input('fp');
        $to = $request->input('tp');
        $date = $request->input('rd');

        return redirect('rides.search-ride?fp='.$from.'&tp='.$to.'&rd='.$date);
    }

    public function general(){
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);

        $curYear = date('Y');
        $MinAdultYear = $curYear - 18;
        $MaxAdultYear = $MinAdultYear - 60;
        $BirthdayDates =  array();

        for($i = $MinAdultYear; $i >= $MaxAdultYear; $i--){
            array_push($BirthdayDates,$i);
        }

    	return view('users.profile.general', compact('UserData','BirthdayDates'));
    }

    public function update_general_details(Request $request){
        
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);

        $email = $request->input('email');

        if($UserData->email == $email){

            $this->validate($request, [
                'firstname' => 'required|string|max:20',
                'lastname' => 'required|string|max:25',
                'phone' => 'nullable|digits_between:6,15',
                'bio' => 'nullable|max:500'
            ]);

        }else{

            $this->validate($request, [
                'firstname' => 'required|string|max:20',
                'lastname' => 'required|string|max:25',
                'phone' => 'nullable|digits_between:6,15',
                'bio' => 'nullable|max:500',
                'email' => 'required|email|max:191|unique:users'
            ]);
        }

        $UserData->firstname = $request->input('firstname');
        $UserData->lastname = $request->input('lastname');
        $UserData->email = $request->input('email');
        $UserData->phone = $request->input('phone');
        
        $UserData->Userdetails()->update([
            'bio' => $request->input('bio'),
            'sex' => $request->input('sex'),
            'birthday' => $request->input('birthday')
        ]);

        $UserData->save();

        return back();
    }

    public function avatar(){
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);

    	return view('users.profile.photo',compact('UserData'));
    }

    public function imageCropPost(Request $request){

        if (!empty($request->avatar_img)) {
    
            $data = $request->image;

            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);

            $data = base64_decode($data);

            $image_name = time().'.png';
            $jpgfileName = str_replace('png','jpg',$image_name);
            $path = public_path() . "/storage/upload/users/" . $image_name;

            file_put_contents($path, $data);

            // encode png image as jpg
            $jpg = (string) Image::make($path)->encode('jpg', 75)->save(public_path() . "/storage/upload/users/" . $jpgfileName);
            
            Storage::delete('public/upload/users/'.$image_name);

            $currentuserid = Auth::user()->id;
            $UserData = User::find($currentuserid);

            $old_avatar = $UserData->Userdetails->avatar;
            if($old_avatar != ''){
                Storage::delete('public/upload/users/'.$old_avatar);
            }

            $UserData->Userdetails()->update([
                'avatar' => $jpgfileName
            ]);

            return response()->json();
        }
    }
   
    public function preferences(){
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);

    	return view('users.profile.preferences')->with('UserData', $UserData);
    }

    public function update_preferences(Request $request){
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);

        $UserData->Userpreferences()->update([
            'talkative' => $request->input('talkative'),
            'smoker' => $request->input('smoker'),
            'pets' => $request->input('pets'),
            'music' => $request->input('music')
        ]);

        return back();
    }

    public function auto(){

        $currentuserid = Auth::user()->id;
        $user = User::find($currentuserid);
       
        $AutoDetails = $user->Auto;
        $CarBrands = array("Abarth", "Acura", "Aixam", "Alfa Romeo", "Aston Martin", "Audi", "Bentley", "Buick", "BMW", "Cadillac", "Chevrolet", "Chrysler", "Citroen", "Dacia", "Daewoo", "Daihatsu", "Dodge", "DS Automobiles", "Ferrari", "Fiat", "Ford", "Honda", "Hummer", "Hyundai", "Infiniti", "Isuzu", "Jaguar", "Jeep", "Kia", "Lada", "Lamborghini", "Lancia", "Land Rover", "Lexus", "Liger", "Lotus", "Maserati", "Maybach", "Mazda", "Mercedes-Benz",   "MG", "Mini", "Mitsubishi", "Nissan", "Opel", "Peugeot", "Pontiac", "Porsche", "Proton", "Renault", "Rolls-Royce", "Rover", "Saab", "Seat", "Skoda", "Smart", "Ssangyong", "Subaru", "Suzuki", "Toyota", "Volkswagen", "Volvo", "Wartburg", "Yugo", "Zastava", "Други");
        $CountryCodes = array("MK"=>"Македонија", "BG"=>"България", "DE"=>"Deutschland","CH"=>"Switzerland","NL"=>"Netherlands");
        $TypeNames = array('Компактно','Хеџбек','Седан','Караван','Моноволумен','Теренско (SUV)','Кабриолет','Купе','Мало Комерцијално Возило','Големо Комерцијално Возило');
        $ColourNames = array('Црна','Бела','Темно Сива','Светло Сива','Црвена','Сина','Зелена','Кафена','Портокалова','Жолта','Виолетова','Розе');

    	return view('users.profile.auto',compact('AutoDetails','CountryCodes','TypeNames','ColourNames','CarBrands'));
    }

    public function update_auto(Request $request){

        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);

        if (!empty($request->car_img)) {

            $data = $request->image;
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);

            $data = base64_decode($data);
            $fileName = time().'.png';
            $jpgfileName = str_replace('png','jpg',$fileName);
            $path = public_path() . "/storage/upload/cars/" . $fileName;

            file_put_contents($path, $data);
            
            // encode png image as jpg
            $jpg = (string) Image::make($path)->encode('jpg', 75)->save(public_path() . "/storage/upload/cars/" . $jpgfileName);
            
            Storage::delete('public/upload/cars/'.$fileName);

            $old_car = $UserData->Auto->car_photo;
            if($old_car != ''){
                Storage::delete('public/upload/cars/'.$old_car);
            }

            $UserData->Auto()->update([
                'car_photo' => $jpgfileName
            ]);
        }

        $UserData->Auto()->update([
                'year_production' => $request->car_years,
                'car_brand' => $request->car_brand,
                'car_model' => $request->car_model,
                'licence_plate' => $request->licence_plate,
                'licence_state' => $request->licence_state,
                'type' => $request->type,
                'color' => $request->color
            ]);

        return response()->json();
    }

    public function remove_auto(){
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);

        $old_car = $UserData->Auto->car_photo;
        if($old_car != ''){
            Storage::delete('public/upload/cars/'.$old_car);
        }

        $UserData->Auto()->update([
            'year_production' => '',
            'car_brand' => '',
            'car_model' => '',
            'type' => '',
            'color' => '',
            'car_photo' => ''
        ]);

        return back();
    }

    public function address(){
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);
        $UserDataDetails = $UserData->Userdetails;

        $StatesList =  array("Afghanistan", "Åland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia, Plurinational State of", "Bonaire, Sint Eustatius and Saba", "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Curaçao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Македонија", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Réunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthélemy", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten (Dutch part)", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard and Jan Mayen", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela, Bolivarian Republic of", "Viet Nam", "Virgin Islands, British", "Virgin Islands, U.S.", "Wallis and Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");

    	return view('users.profile.address',compact('UserDataDetails','StatesList'));
    }

    public function update_address_details(Request $request){
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);

        $this->validate($request, [
            'address' => 'nullable|string|max:100',
            'zip' => 'nullable|string|max:20',
            'city' => 'nullable|string|max:100'
        ]);

        $UserData->Userdetails()->update([
            'address' => $request->input('address'),
            'zip' => $request->input('zip'),
            'city' => $request->input('city'),
            'state' => $request->input('state')
        ]);

        return back();
    }

    public function notification_settings(){
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);
   
        return view('users.profile.notification_settings')->with('UserData', $UserData);
    }

    public function update_notification_settings(Request $request){
        $currentuserid = Auth::user()->id;
        $UserData = User::find($currentuserid);
       
        if($request->input('value') == 'true'){
            $value = true;
        }else{
           $value = false;
        }

        $UserData->NotificationSettings()->update([
            $request->input('name') => $value
        ]);

        return back();
    }

    public function report_user($id, Request $request){
        $currentuserid = Auth::user()->id;

        $userReport = new UserReport;
        $userReport->user_id = $id;
        $userReport->reported_by = $currentuserid;
        $userReport->subject = $request->input('subject');
        $userReport->description = $request->input('description');
        $userReport->status = 'new';
        $userReport->save();
        
        //Send Email to Admin
        $adminEmail = DB::table('admins')->select('email')->where('id', 1)->get();
        Notification::route('mail', $adminEmail)->notify(new NewUserReport($userReport->id));

        return back();
    }

    public function changepassword(){
        $currentuserid = Auth::user()->id;
   
        return view('users.profile.changepassword')->with('currentuserid', $currentuserid);
    }

    public function updatepassword($id, Request $request){

        $currentuserid = Auth::user()->id;

        if($id != $currentuserid){
            return back();
        }

        $UserData = User::find($currentuserid);

        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        if(!empty($request->input('password')))
        {
            $UserData->password = bcrypt($request->input('password'));
        }

        $UserData->update();

        return redirect('/users/profile/general')->with('success','Измените се зачувани');
    }
}
