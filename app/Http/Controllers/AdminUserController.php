<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;

class AdminUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function viewProfile($id){


    	return view('admin.adminUser.viewProfile');
    }

    public function editProfile($id){


    	return view('admin.adminUser.editProfile');
    }

    public function updateProfile($id, Request $request){

    	$AdminData = Admin::find($id);
      
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|string|max:191|unique:admins,email,'.$id
        ]);

        $AdminData->name = $request->input('name');
        $AdminData->email = $request->input('email');
        $AdminData->update();

    	return redirect('/admin/viewAdminProfile/'.$id);
    }

    public function editPassword($id){

    	
    	return view('admin.adminUser.changePassword');
    }

    public function updatePassword($id, Request $request){

    	$AdminData = Admin::find($id);
      
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        if(!empty($request->input('password')))
        {
            $AdminData->password = bcrypt($request->input('password'));
        }

        $AdminData->update();

    	return redirect('/admin/viewAdminProfile/'.$id);
    }
    
}
