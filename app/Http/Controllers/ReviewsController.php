<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserReviews;
use Auth;
use App\Notifications\NewReviewCreation;
use App\Notifications\ReviewUpdated;

class ReviewsController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentuserid = Auth::user()->id;

        $MyReviews = DB::table('user_reviews')->select('user_reviews.*','users.firstname','user_details.avatar')
        ->where('user_reviews.s_user_id', '=', $currentuserid)
        ->leftJoin('users', 'user_reviews.r_user_id', '=', 'users.id')
        ->leftJoin('user_details', 'user_reviews.r_user_id', '=', 'user_details.id')
        ->orderBy('created_at','desc')
        ->paginate(5);

        return view('reviews.myreviews')->with('MyReviews', $MyReviews);
    }

    public function index_received()
    {
        $currentuserid = Auth::user()->id;

        $ReceivedReviews = DB::table('user_reviews')->select('user_reviews.s_user_id','user_reviews.rating','user_reviews.comment','user_reviews.created_at','rides.d_from','rides.d_to','rides.d_stop_1','rides.d_stop_2','users.firstname','user_details.avatar')
        ->where('user_reviews.r_user_id', '=', $currentuserid)
        ->leftJoin('rides', 'user_reviews.ride_id', '=', 'rides.id')
        ->leftJoin('users', 'user_reviews.s_user_id', '=', 'users.id')
        ->leftJoin('user_details', 'user_reviews.s_user_id', '=', 'user_details.id')
        ->orderBy('created_at','desc')
        ->paginate(5);

        return view('reviews.receivedreviews')->with('ReceivedReviews', $ReceivedReviews);
    }

    public function show_all()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'ride_rating' => 'required|numeric',
            'user_comment' => 'required|string|max:500',
        ]);

        if($request->input('review_id') != '' && $request->input('review_id') > 0){
            $new = false;
            $review = UserReviews::find($request->input('review_id'));
        }else{
            $new = true;
            $review = new UserReviews;
        }

        $currentuserid = Auth::user()->id;
        $review->s_user_id = $currentuserid;
        $review->r_user_id = $request->input('ride_user_id');
        $review->rating = $request->input('ride_rating');
        $review->comment = $request->input('user_comment');
        $review->ride_id = $request->input('ride_id');
        $review->save();

        //For the Notification
        $user = User::find(UserReviews::find($review->id)->r_user->id);
        $r_user_firstname = $user['firstname'];
        $review_created_at = $review->created_at;
       
        //Get Notification setting for receiving user
        $mailNotification = $user->NotificationSettings->comment_added;

        if($new == true){
            //Notification to Ride Owner
            $user->notify(new NewReviewCreation(Auth::user()->firstname,$r_user_firstname,$mailNotification));
        }else{
            //Notification to Ride Owner
            $user->notify(new ReviewUpdated($review_created_at,Auth::user()->firstname,$r_user_firstname,$mailNotification));
        }
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
