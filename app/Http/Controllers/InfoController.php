<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;

class InfoController extends Controller
{
    public function kakoraboti()
    {
        return view('kakoraboti');
    }
    public function zanas()
    {
        return view('zanas');
    }
    public function faq()
    {
        return view('faq');
    }
    public function kontakt()
    {
        return view('kontakt');
    }

    public function sendMail(Request $request)
    {
        if(!empty($request->workemailaddress) || !empty($request->company)){
            return redirect()->back()->with('fail','Пораката во моментот не може да биде испратена. Те молам обиди се подоцна');
        }

        $this->validate($request,[
            'firstname' => 'required|string|max:20',
            'email' => 'required|email',
            'poraka' => 'required|string|max:500'
        ]);

        try{
            Mail::send(new ContactMail($request));
        }catch(\Exception $e){
            
            return redirect()->back()->with('fail','Пораката во моментот не може да биде испратена. Те молам обиди се подоцна');
        }
        
        return redirect()->back()->with('success','Пораката е успешно испратена');
    }

    public function uslovi()
    {
        return view('uslovi');
    }
    public function privatnost()
    {
        return view('privatnost');
    }
}
