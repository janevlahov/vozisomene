<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Notification;
use App\Notifications\SuccessfulUserRegistration;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


      /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($service)
    {
        
        if($service == 'twitter'){
            $user = Socialite::driver($service)->user();

        }else{
            $user = Socialite::driver($service)->stateless()->user();
        }

        $findUser = User::where('email',$user->getEmail())->first();

        if($findUser){

            $isActive = User::where('email',$user->getEmail())->where('active', 1)->first();
            if($isActive){
                Auth::login($findUser);
            }else{
                return redirect('login')->withErrors(['deactivated'=>'Корисничкото име е деактивирано']);
            }
            
         }else{

            $newUser = new User();

            $name = trim($user->getName());
            $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );

            $newUser->email = $user->getEmail();
            $newUser->firstname = $first_name;
            $newUser->lastname = $last_name;
            $newUser->password = bcrypt(123456);
            $newUser->save();

            $newUser->auto()->create();
            $newUser->userpreferences()->create();
            $newUser->userdetails()->create();
            $newUser->Notificationsettings()->create();

            $data = array("firstname"=>$first_name, "lastname"=>$last_name);
            
            Auth::login($newUser);
            Notification::route('mail', $user->getEmail())->notify(new SuccessfulUserRegistration($data));

         }

        return redirect('dashboard');
    }

}
