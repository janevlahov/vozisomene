<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Rides;
use App\RidesAdds;
use App\UserReport;
use Image;
use Illuminate\Support\Facades\Storage;
use Notification;
use App\Notifications\NotificateBlockedUser;

class AdminController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$rides_count = DB::table('rides')->count();
        $users_count = DB::table('users')->where('active','=', 1)->count();
    	$userReports_count = DB::table('user_reports')->where('status','=', 'new')->count();

        return view('admin.dashboard')->with('rides_count', $rides_count)->with('users_count', $users_count)->with('userReports_count', $userReports_count);
    }

    public function show_all_users()
    {
    	$users_count = DB::table('users')->count();
    	$users = DB::table('users')->get();

        return view('admin.users.index')->with('users_count', $users_count)->with('users',$users);
    }

    public function showUser($id)
    {
        $user = DB::table('users')
        ->leftJoin('user_autos', 'users.id', '=', 'user_autos.id')
        ->leftJoin('user_details', 'users.id', '=', 'user_details.id')
        ->leftJoin('user_preferences', 'users.id', '=', 'user_preferences.id')
        ->leftJoin('user_reviews', 'users.id', '=', 'user_reviews.id')
        ->leftJoin('notification_settings', 'users.id', '=', 'notification_settings.id')
        ->where('users.id','=', $id)
        ->first();

        $recieved_r = DB::table('user_reviews','users.firstname','users.lastname')->where('r_user_id','=', $id)->leftJoin('users', 'user_reviews.s_user_id', '=', 'users.id')->get();
        $sent_r = DB::table('user_reviews','users.firstname','users.lastname')->where('s_user_id','=', $id)->leftJoin('users', 'user_reviews.r_user_id', '=', 'users.id')->get();
        $user_rating = $this->user_rating($id);

        $user_rides = DB::table('rides')->where('user_id','=', $id)->get();
        $booked_rides = DB::table('bookings')->select('bookings.ride_id', 'bookings.b_from', 'bookings.b_to','bookings.qty_places', 'bookings.b_cost', 'bookings.status AS book_status', 'bookings.created_at', 'rides.d_from','rides.d_to','rides.d_stop_1','rides.d_stop_2','rides.d_cost1','rides.d_cost2','rides.d_cost3','rides.d_cost4','rides.d_cost5','rides.d_date')
        ->where('bookings.user_id', '=', $id)
        ->leftJoin('rides', 'bookings.ride_id', '=', 'rides.id')
        ->orderBy('rides.d_date','desc')
        ->get();

        $userReports = DB::table('user_reports')->select('user_reports.*', 'users.firstname', 'users.lastname', 'users.id as userid')
        ->where('reported_by','=',$id)
        ->leftJoin('users', 'user_reports.user_id', '=', 'users.id')
        ->get();

        $userReportedBy = DB::table('user_reports')->select('user_reports.*', 'users.firstname', 'users.lastname', 'users.id as userid')
        ->where('user_id','=',$id)
        ->leftJoin('users', 'user_reports.reported_by', '=', 'users.id')
        ->get();
        
        return view('admin.users.user-view')->with('user', $user)->with('recieved_r', $recieved_r)->with('sent_r', $sent_r)->with('user_rating', $user_rating)->with('user_rides', $user_rides)->with('booked_rides', $booked_rides)->with('userReports', $userReports)->with('userReportedBy', $userReportedBy);
    }

    public function editUser($id)
    {
        $user = DB::table('users')
        ->leftJoin('user_autos', 'users.id', '=', 'user_autos.id')
        ->leftJoin('user_details', 'users.id', '=', 'user_details.id')
        ->leftJoin('user_preferences', 'users.id', '=', 'user_preferences.id')
        ->leftJoin('user_reviews', 'users.id', '=', 'user_reviews.id')
        ->leftJoin('notification_settings', 'users.id', '=', 'notification_settings.id')
        ->where('users.id','=', $id)
        ->first();

        $user_rating = $this->user_rating($id);

        $curYear = date('Y');
        $MinAdultYear = $curYear - 18;
        $MaxAdultYear = $MinAdultYear - 60;
        $BirthdayDates =  array();

        array_push($BirthdayDates,$MinAdultYear);

        for($i = $MinAdultYear; $i >= $MaxAdultYear; $i--){
            array_push($BirthdayDates,$i);
        }

        $StatesList =  array("Afghanistan", "Åland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia, Plurinational State of", "Bonaire, Sint Eustatius and Saba", "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Curaçao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Македонија", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Réunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthélemy", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten (Dutch part)", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard and Jan Mayen", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela, Bolivarian Republic of", "Viet Nam", "Virgin Islands, British", "Virgin Islands, U.S.", "Wallis and Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");

        $CarBrands = array("Abarth", "Acura", "Aixam", "Alfa Romeo", "Aston Martin", "Audi", "Bentley", "Buick", "BMW", "Cadillac", "Chevrolet", "Chrysler", "Citroen", "Dacia", "Daewoo", "Daihatsu", "Dodge", "DS Automobiles", "Ferrari", "Fiat", "Ford", "Honda", "Hummer", "Hyundai", "Infiniti", "Isuzu", "Jaguar", "Jeep", "Kia", "Lada", "Lamborghini", "Lancia", "Land Rover", "Lexus", "Liger", "Lotus", "Maserati", "Maybach", "Mazda", "Mercedes-Benz",   "MG", "Mini", "Mitsubishi", "Nissan", "Opel", "Peugeot", "Pontiac", "Porsche", "Proton", "Renault", "Rolls-Royce", "Rover", "Saab", "Seat", "Skoda", "Smart", "Ssangyong", "Subaru", "Suzuki", "Toyota", "Volkswagen", "Volvo", "Wartburg", "Yugo", "Zastava", "Други");

        $CountryCodes = array("MK"=>"Македонија", "BG"=>"България", "DE"=>"Deutschland","CH"=>"Switzerland","NL"=>"Netherlands");
        $TypeNames = array('Компактно','Хеџбек','Седан','Караван','Моноволумен','Теренско (SUV)','Кабриолет','Купе','Мало Комерцијално Возило','Големо Комерцијално Возило');
        $ColourNames = array('Црна','Бела','Темно Сива','Светло Сива','Црвена','Сина','Зелена','Кафена','Портокалова','Жолта','Виолетова','Розе');

        return view('admin.users.user-edit')->with('user',$user)->with('user_rating',$user_rating)->with('BirthdayDates',$BirthdayDates)->with('StatesList',$StatesList)->with('CountryCodes',$CountryCodes)->with('TypeNames',$TypeNames)->with('ColourNames',$ColourNames)->with('CarBrands',$CarBrands);
    }

    public function updateUser($id, Request $request)
    {
        $UserData = User::find($id);
      
        $this->validate($request, [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'email' => 'required|string|max:191|unique:users,email,'.$id,
            'phone' => 'nullable|digits_between:6,15',
            'bio' => 'nullable|max:500',
            'licence_plate' => 'nullable|max:100'
        ]);

        if($request->has('active_user')){$active_user = 1;}else{$active_user = 0;}
        $dbUserActive = $UserData->active;

        $UserData->firstname = $request->input('firstname');
        $UserData->lastname = $request->input('lastname');
        $UserData->email = $request->input('email');
        $UserData->phone = $request->input('phone');
        $UserData->active = $active_user;
        $UserData->reason = $request->input('block_reason');

        $UserData->Userdetails()->update([
            'sex' => $request->input('sex'),
            'birthday' => $request->input('birthday'),
            'address' => $request->input('address'),
            'zip' => $request->input('zip'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'bio' => $request->input('bio'),
        ]);

        $UserData->Auto()->update([
            'year_production' => $request->input('car_years'),
            'car_brand' => $request->input('car_makes'),
            'car_model' => $request->input('car_models'),
            'licence_plate' => $request->input('licence_plate'),
            'licence_state' => $request->input('licence_state'),
            'type' => $request->input('car_type'),
            'color' => $request->input('car_color')
        ]);

        $UserData->Userpreferences()->update([
            'talkative' => $request->input('talkative'),
            'smoker' => $request->input('smoker'),
            'pets' => $request->input('pets'),
            'music' => $request->input('music')
        ]);


        if($request->has('new_ride')){$new_ride = 1;}else{$new_ride = 0;}
        if($request->has('update_ride')){$update_ride = 1;}else{$update_ride = 0;}
        if($request->has('ride_request')){$ride_request = 1;}else{$ride_request = 0;}
        if($request->has('ride_status')){$ride_status = 1;}else{$ride_status = 0;}
        if($request->has('comment_added')){$comment_added = 1;}else{$comment_added = 0;}
        if($request->has('promotions_news')){$promotions_news = 1;}else{$promotions_news = 0;}

        $UserData->NotificationSettings()->update([
            'new_ride' => $new_ride,
            'update_ride' => $update_ride,
            'ride_request' => $ride_request,
            'ride_status' => $ride_status,
            'comment_added' => $comment_added,
            'promotions_news' => $promotions_news
        ]);

        if(!empty($request->input('auto_image_name'))){

            $data = $request->input('auto_image');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);

            $data = base64_decode($data);
            $fileName = time().'.png';
            $jpgfileName = str_replace('png','jpg',$fileName);
            $path = public_path() . "/storage/upload/cars/" . $fileName;

            file_put_contents($path, $data);
            
            // encode png image as jpg
            $jpg = (string) Image::make($path)->encode('jpg', 75)->save(public_path() . "/storage/upload/cars/" . $jpgfileName);
            
            Storage::delete('public/upload/cars/'.$fileName);

            $UserData->Auto()->update([
                'car_photo' => $jpgfileName
            ]);
        }
      
        //Send Notification to the Blocked User
        if($dbUserActive == 1 && $active_user == 0){

            //Update Rides in Progress for the blocked User and Cancel them
            DB::table('rides')
            ->where('user_id', $id)
            ->update(['status' => 'canceled']);
            
            if(!empty($request->input('block_reason'))){
                Notification::route('mail', $UserData->email)->notify(new NotificateBlockedUser($UserData, $request->input('block_reason')));
            }
        }

        $UserData->save();

        return redirect('admin/view-user/'.$id);
    }

    public function updateUserCar(Request $request)
    {
        $UserData = User::find($request->user_id);

        if (!empty($request->car_img)) {

            $data = $request->image;
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);

            $data = base64_decode($data);
            $fileName = time().'.png';
            $jpgfileName = str_replace('png','jpg',$fileName);
            $path = public_path() . "/storage/upload/cars/" . $fileName;

            file_put_contents($path, $data);
            
            // encode png image as jpg
            $jpg = (string) Image::make($path)->encode('jpg', 75)->save(public_path() . "/storage/upload/cars/" . $jpgfileName);
            
            Storage::delete('public/upload/cars/'.$fileName);

            $old_car = $UserData->Auto->car_photo;
            if($old_car != ''){
                Storage::delete('public/upload/cars/'.$old_car);
            }

            $UserData->Auto()->update([
                'car_photo' => $jpgfileName
            ]);

            return response()->json();
        }
    }

    public function user_rating($user_id)
    {
        $user_rating = DB::table('user_reviews')->where('r_user_id', '=', $user_id)->avg('rating');
        if(!empty($user_rating)){
            $user_rating = number_format($user_rating, 1, '.', '');
        }
        
        return $user_rating;
    }

    public function show_all_rides(){
        $rides_count = DB::table('rides')->count();
        $rides = DB::table('rides')->get();

        return view('admin.rides.index')->with('rides_count', $rides_count)->with('rides',$rides);
    }

    public function showRide($id)
    {
        $ride = DB::table('rides')->where('id', '=' , $id)->first();

        if(!empty($ride)){

            $r_qty_places = DB::table('rides_adds')->select('r_qty_places')->where('ride_id', '=' , $id)->value('r_qty_places');
            $booked_users = DB::table('bookings')->select('bookings.*','users.firstname','users.lastname','user_details.avatar')
            ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
            ->leftJoin('user_details', 'bookings.user_id', '=', 'user_details.id')
            ->where('ride_id', '=' , $id)->get();

            $driver_info = DB::table('users')->select('users.id','users.firstname','users.lastname','user_details.avatar')
            ->leftJoin('user_details', 'users.id', '=', 'user_details.id')
            ->where('users.id', '=' , $ride->user_id)->first();

            return view('admin.rides.ride-view')->with('ride', $ride)->with('r_qty_places', $r_qty_places)->with('booked_users', $booked_users)->with('driver_info', $driver_info);
            
        }else{
            return view('admin.rides.ride-view')->with('ride', $ride);
        }
    }

    public function editRide($id)
    {
        $ride = DB::table('rides')->where('id', '=' , $id)->first();
        $r_qty_places = DB::table('rides_adds')->select('r_qty_places')->where('ride_id', '=' , $id)->value('r_qty_places');
        $booked_users = DB::table('bookings')->select('bookings.*','users.firstname','users.lastname','user_details.avatar')
        ->leftJoin('users', 'bookings.user_id', '=', 'users.id')
        ->leftJoin('user_details', 'bookings.user_id', '=', 'user_details.id')
        ->where('ride_id', '=' , $id)->get();

        $driver_info = DB::table('users')->select('users.id','users.firstname','users.lastname','user_details.avatar')
        ->leftJoin('user_details', 'users.id', '=', 'user_details.id')
        ->where('users.id', '=' , $ride->user_id)->first();

        return view('admin.rides.ride-edit')->with('ride', $ride)->with('r_qty_places', $r_qty_places)->with('booked_users', $booked_users)->with('driver_info', $driver_info);
    }

    public function updateRide($id, Request $request)
    {
        $ride = Rides::find($id);
        $ride->d_from = $request->input('d_from');
        $ride->d_to = $request->input('d_to');
        $ride->d_stop_1 = $request->input('d_stop_1');
        $ride->d_stop_2 = $request->input('d_stop_2');

        $roundtrip = $request->input('roundtrip');
        if(!isset($roundtrip)){
            $ride->roundtrip = 0;
        }else{
            $ride->roundtrip = $request->input('roundtrip');
        }

        $back_s_guarantee = $request->input('back_s_guarantee');
        if(!isset($back_s_guarantee)){
            $ride->back_s_guarantee = 0;
        }else{
            $ride->back_s_guarantee = $request->input('back_s_guarantee');
        }
        
        if($request->input('d_date') != ''){
            $ride->d_date = date('Y-m-d',strtotime($request->input('d_date')));
            $ride->d_time = $request->input('d_time');
        }
        
        if($request->input('d_rdate') != ''){
            $ride->d_rdate = date('Y-m-d',strtotime($request->input('d_rdate')));
            $ride->d_rtime = $request->input('d_rtime');
        }else{
            $ride->d_rdate = NULL;
            $ride->d_rtime = NULL;
        }

        $ride->d_cost1 = $request->input('costShare1');
        $ride->d_cost2 = $request->input('costShare2');
        $ride->d_cost3 = $request->input('costShare3');
        $ride->d_cost4 = $request->input('costShare4');
        $ride->d_cost5 = $request->input('costShare5');
        $ride->d_cost6 = $request->input('costShare6');

        $ride->d_stop1_time = $request->input('d_stop1_time');
        $ride->d_stop2_time = $request->input('d_stop2_time');
        $ride->d_stop3_time = $request->input('d_stop3_time');

        //Check if input is eligibe for update
        $free_places = $request->input('free_places');
        $booked_places = DB::table('bookings')->select('qty_places')->where('ride_id', '=', $id)->sum('qty_places');
        if($booked_places <= $free_places){
            $ride->free_places = $request->input('free_places');
        }

        $ride->d_description = $request->input('ride_comment');
        $ride->distance = $request->input('distance');
        $ride->est_d_time = $request->input('est_d_time');
        $ride->save();

        if($booked_places <= $free_places){
       
            $remain_places = $free_places - $booked_places;

            $update_places = new RidesAdds;
            $update_places->where('ride_id', $id)->update(['r_qty_places' => $remain_places]);
        }else{

        }

        return redirect('admin/view-ride/'.$id);
    }

    public function show_all_userReports(){

        $userReports_count = DB::table('user_reports')->count();
        $userReports = DB::table('user_reports')->select('user_reports.*', 'A.firstname as userFirstname', 'A.lastname as userLastname', 'B.firstname as reportedByFirstname', 'B.lastname as reportedByLastname')
        ->leftjoin('users AS A', 'A.id', '=', 'user_reports.user_id')
        ->leftjoin('users AS B', 'B.id', '=', 'user_reports.reported_by')
        ->get();

        return view('admin.users.reports.index')->with('userReports_count', $userReports_count)->with('userReports',$userReports);
    }

    public function getReport($id){

        $userReport = DB::table('user_reports')
        ->select('user_reports.*', 'A.firstname as userFirstname', 'A.lastname as userLastname', 'B.firstname as reportedByFirstname', 'B.lastname as reportedByLastname')
        ->where('user_reports.id', '=' , $id)
        ->leftjoin('users AS A', 'A.id', '=', 'user_reports.user_id')
        ->leftjoin('users AS B', 'B.id', '=', 'user_reports.reported_by')
        ->first();

        return $userReport;
    }

    public function showUserReport($id){

        $userReport = $this->getReport($id);

        return view('admin.users.reports.report-view')->with('userReport', $userReport);
    }

    public function editUserReport($id){

        $userReport = $this->getReport($id);
        $reportStatus = array("new"=>"Нова", "in_progress"=>"Во тек", "completed"=>"Завршена", "canceled"=>"Откажена");

        return view('admin.users.reports.report-edit')->with('userReport', $userReport)->with('reportStatus', $reportStatus);
    }

    public function updateUserReport($id, Request $request){
        
        $report = UserReport::find($id);
        $report->status = $request->input('status');
        $report->action = $request->input('action');
        $report->solution = $request->input('solution');

        $report->save();
        return redirect('admin/users/reports/view-report/'.$id);
    }

}
