<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rides extends Model
{
    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function RidesAdds() {
        return  $this->hasOne('App\RidesAdds','ride_id');
    }
}
