<?php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;

class Helper {

    public static function Short_Destination($place)
    {
       $placeArr = explode(",",$place);
			$placeCountArr= count($placeArr);
			if($placeCountArr == 4){
				$shortCity = $placeArr[2].", ". $placeArr[$placeCountArr - 1];
			}elseif ($placeCountArr == 3) {
				$shortCity = $placeArr[1].", ". $placeArr[$placeCountArr - 1];
			}elseif($placeCountArr == 2){
				$shortCity = $placeArr[0].", ".$placeArr[$placeCountArr - 1];
			}else{
				$shortCity = $placeArr[$placeCountArr - 1];
			}
		return $shortCity;
    }

    public static function rmCountry($place)
    {
       $placeArr = explode(",",$place);
		$placeCountArr= count($placeArr);
		if($placeCountArr > 1){

			/*if($placeArr[count($placeArr)-1] == "Macedonia" || $placeArr[count($placeArr)-1] == "Македонија"){*/
				array_pop($placeArr);
				$shortCity = implode(", ",$placeArr);
			/*}else{
				return $place;
			}*/
		}else{
			return $place;
    }
    return $shortCity;
  }

  public static function pref_talkative($talkative)
  {
    switch ($talkative) {
      case "talk_1":
      $thought = array(
        '<span class="fa-stack fa-2x"><i class="fas fa-comments fa-stack-1x text-muted"></i><i class="fas fa-ban fa-stack-2x text-danger"></i></span>',
        'Јас сум тивок човек'
      );
      break;
      case "talk_2":
      $thought = array(
        '<i class="fas fa-comments fa-3x text-muted"></i>',
        'Разговарам во зависност од расположението'
      );
      break;
      case "talk_3":
      $thought = array(
        '<i class="fas fa-comments fa-3x text-primary"></i>',
        'Возењето неможе да помине без раговор'
      );
      break;
      default:
      $thought = array(
        '<i class="far fa-question-circle fa-3x text-muted"></i>',
        'Корисникот нема поставено преференци'
      );
    }
    return $thought;
  }

  public static function pref_smoker($smoker)
  {
    switch ($smoker) {
      case "smoke_1":
      $thought = array(
        '<span class="fa-stack fa-2x"><i class="fas fa-joint fa-stack-1x text-muted"></i><i class="fas fa-ban fa-stack-2x text-danger"></i></span>',
        'Не дозволувам пушење во автомобилот'
      );
      break;
      case "smoke_2":
      $thought = array(
        '<i class="fas fa-joint fa-3x text-muted"></i>',
        'Понекогаш дозволувам пушење во автомобилот'
      );
      break;
      case "smoke_3":
      $thought = array(
        '<i class="fas fa-joint fa-3x text-primary"></i>',
        'Дозволувам пушење во автомобилот'
      );
      break;
      default:
      $thought = array(
        '<i class="far fa-question-circle fa-3x text-muted"></i>',
        'Корисникот нема поставено преференци'
      );
    }
    return $thought;
  }

  public static function pref_pets($pets)
  {
    switch ($pets) {
      case "pets_1":
      $thought = array(
        '<span class="fa-stack fa-2x"><i class="fas fa-paw fa-stack-1x text-muted"></i><i class="fas fa-ban fa-stack-2x text-danger"></i></span>',
        'Без миленичиња во автомобилот'
      );
      break;
      case "pets_2":
      $thought = array(
        '<i class="fas fa-paw fa-3x text-muted"></i>',
        'Зависи за какво милениче се работи'
      );
      break;
      case "pets_3":
      $thought = array(
        '<i class="fas fa-paw fa-3x text-primary"></i>',
        'Миленичињата се добредојдени'
      );
      break;
      default:
      $thought = array(
        '<i class="far fa-question-circle fa-3x text-muted"></i>',
        'Корисникот нема поставено преференци'
      );
    }
    return $thought;
  }

  public static function pref_music($music)
  {
    switch ($music) {
      case "music_1":
      $thought = array(
        '<span class="fa-stack fa-2x"><i class="fas fa-music fa-stack-1x text-muted"></i><i class="fas fa-ban fa-stack-2x text-danger"></i></span>',
        'Тишината е закон'
      );
      break;
      case "music_2":
      $thought = array(
        '<i class="fas fa-music fa-3x text-muted"></i>',
        'Слушам во зависност од расположението'
      );
      break;
      case "music_3":
      $thought = array(
        '<i class="fas fa-music fa-3x text-primary"></i>',
        'Патувањето неможе да помине без музика'
      );
      break;
      default:
      $thought = array(
        '<i class="far fa-question-circle fa-3x text-muted"></i>',
        'Корисникот нема поставено преференци'
      );
    }
    return $thought;
  }

  public static function check_img($img_name){
    if($img_name == ""){
      return $img_name = 'no_preference';
    }else{
      return $img_name;
    }
  }

  public static function Status_Message($status){
    $message = '';
    if($status == 'Accepted'){
      return $message = 'Прифатена';
    }elseif($status == 'Pending'){
      return $message = 'На Одобрување';
    }elseif($status == 'Rejected'){
      return $message = 'Одбиена';
    }elseif($status == 'Canceled'){
      return $message = 'Откажена';
    }
  }

  public static function Ride_Status($status){
    if($status == 'in_progress'){
      return 'Во тек';
    }elseif($status == 'completed'){
      return 'Завршено';
    }elseif($status == 'canceled'){
      return 'Откажено';
    }
  }

  public static function userAvatar($avatar,$firstname,$lastname,$user_id){
    if(!empty($avatar)){
      return "<div class='media'><div class='media-left'><a href='/rides/user_profile/$user_id'><img class='media-object' src='/storage/upload/users/$avatar' alt='$firstname $lastname'></a></div>
      <div class='media-body'><p class='media-heading lead'>$firstname <br>$lastname</p></div></div>";
    }else{
      return "<div class='media'><div class='media-left'><a href='/rides/user_profile/$user_id'><img class='media-object' src='/storage/upload/web/nouser.jpg' alt='Корисникот нема слика'></a></div>
      <div class='media-body'><p class='media-heading lead'><a href='/rides/user_profile/$user_id'>$firstname <br>$lastname</a></p></div></div>";
    }
  }

  public static function userSlika($avatar,$firstname,$lastname){
    if(!empty($avatar)){
      return "<img class='slika' src='/storage/upload/users/$avatar' alt='$firstname $lastname'>";
    }else{
      return "<img class='slika' src='/storage/upload/web/nouser.jpg' alt='$firstname $lastname'>";
    }
  }

  public static function userKola($car_photo,$car_brand,$car_model){
    if(!empty($car_photo)){
      return "<img class='slika' src='/storage/upload/cars/$car_photo' alt='$car_brand $car_model'>";
    }else{
      return "<img class='slika' src='/storage/upload/web/nocar.jpg' alt='Корисникот нема слика од автомобилот'>";
    }
  }

  public static function userAvatarAdmin($avatar,$firstname,$lastname,$user_id){
    if(!empty($avatar)){

      if(empty($firstname) && empty($lastname)){
        return "<a href='/admin/view-user/$user_id'><img src='/storage/upload/users/$avatar' width='65px' alt='User Image'></a>";
      }

      return "<a href='/admin/view-user/$user_id'><img src='/storage/upload/users/$avatar' width='65px' alt='User Image'>".$firstname." ". $lastname."</a>";

    }else{
      if(empty($firstname) && empty($lastname)){
        return "<a href='/admin/view-user/$user_id'><img src='/storage/upload/web/no_image.png' width='65px' alt='No Image'></a>";
      }

      return "<a href='/admin/view-user/$user_id'><img src='/storage/upload/web/no_image.png' width='65px' alt='No Image'>".$firstname." ". $lastname."</a>";
    }
  }

  public static function sex_translation($sex){
    if(!empty($sex)){

      if($sex == 'male'){
        return 'машки';
      }elseif($sex == 'female'){
        return 'женски';
      }
    }else{
      return "n/a";
    }
  }

  public static function notification_settings($notification){
    if(is_numeric($notification)){

      if($notification == 1){
        return 'да';
      }else{
        return 'не';
      }
    }else{
      return "n/a";
    }
  }

  public static function bool_beautify($bool){
    if(is_numeric($bool)){

      if($bool == 1){
        return 'да';
      }else{
        return 'не';
      }
    }else{
      return "n/a";
    }
  }

  public static function zero_beautify($num){
    if(is_numeric($num)){

        if($num == 0){
            return 'нема';
        }else{
            return $num;
        }

    }else{
        return "n/a";
    }
  }

  public static function free_seats_label($free_seats){
    if($free_seats > 1){
      return $free_seats." слободни седишта за резервирање";
    }elseif($free_seats == 1){
      return $free_seats." слободнo седиштe за резервирање";
    }else{
      return "<span class='text-danger'>Нема слободни седишта</span>";
    }
  }

  public static function ride_place_name($user_id, $ride_id){

    $place_tag = DB::table('bookings')->select('b_from','b_to')
    ->where('bookings.user_id', '=', $user_id)
    ->where('bookings.ride_id', '=', $ride_id)
    ->first();

    $get_place_name = DB::table('rides')->select($place_tag->b_from, $place_tag->b_to)->where('id', '=', $ride_id)->first();

    return array(current($get_place_name), next($get_place_name));
  }

  public static function count_numas_driver($user_id){
    $count_num_drive = DB::table('rides')->where('status', '!=', 'canceled')->where('user_id', '=', $user_id)->get()->count();
    return $count_num_drive;
  }

  public static function count_numas_passanger($user_id){
    $count_num_books = DB::table('bookings')->where('status', '=', 'Accepted')->where('user_id', '=', $user_id)->get()->count();
    return $count_num_books;
  }

  public static function user_rating($user_id){

      $user_rating = DB::table('user_reviews')->where('r_user_id', '=', $user_id)->avg('rating');
      if(!empty($user_rating)){
          $user_rating = number_format($user_rating, 1, '.', '');
      }

      return $user_rating;
  }

  public static function reportStatus_trans($status){
    if(!empty($status)){

      if($status == 'new'){
        return 'Нова';
      }elseif($status == 'in_progress'){
        return 'Во тек';
      }elseif($status == 'completed'){
        return 'Завршена';
      }elseif($status == 'canceled'){
        return 'Откажена';
      }
    }else{
      return "n/a";
    }
  }

}
