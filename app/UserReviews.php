<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReviews extends Model
{
    public function user(){
    	return $this->belongsTo('App\User','s_user_id');
    }

    public function r_user(){
    	return $this->belongsTo('App\User','r_user_id');
    }
}
