<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class RideRequestCanceled extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($curruser_firstname,$ride_user_firstname,$ride_id,$ride,$mailNotification)
    {
        $this->ride_id = $ride_id;
        $this->firstname = $curruser_firstname;
        $this->ride_user_firstname = $ride_user_firstname;
        $this->d_date = Carbon::parse($ride->d_date)->format('d.m.Y');
        $this->mailNotification = $mailNotification;
        $this->notification_channels = ($this->mailNotification == 1 ? ['database','mail'] : ['database']);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->notification_channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Откажена резервација за патувањето на '.$this->d_date)
            ->greeting('Здраво '.$this->ride_user_firstname)
            ->line($this->firstname.' ја откажа неговата резервација.')
            ->action('Види резервација', url('rides/'.$this->ride_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
         return [
            'url' => 'rides/',
            'title' => 'Откажена резервација',
            'ride_id' => $this->ride_id
        ];
    }
}
