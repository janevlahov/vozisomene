<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class RideUpdated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ride,$user,$r_firstname,$mailNotification)
    {
        $this->mailNotification = $mailNotification;
        $this->notification_channels = ($this->mailNotification == 1 ? ['database','mail'] : ['database']);

        $this->d_date = Carbon::parse($ride->d_date)->format('d.m.Y');
        $this->d_time = $ride->d_time;

        $this->d_from = $ride->d_from;
        $this->d_to = $ride->d_to;
        $this->d_stop_1 = $ride->d_stop_1;
        $this->d_stop_2 = $ride->d_stop_2;
        $this->ride_id = $ride->id;

        $this->b_firstname = $user[0]->firstname;
        $this->r_firstname = $r_firstname;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->notification_channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Промена во патувањето')
            ->greeting('Здраво '.$this->b_firstname)
            ->line($this->r_firstname.' направи измена на следното патување на кое имаш резервирано место.')
            ->line('<strong>Поаѓање:</strong> '. $this->d_date.', '.$this->d_time.' часот')
            ->line('<table cellpadding="6" style="margin-bottom:16px;"><tr><td width="35px"><img src="'.asset("/storage/upload/web/location_icon_blue.jpg").'"></td><td><strong>'.$this->d_from.'</strong></td></tr>')
            ->line($this->d_stop_1)
            ->line($this->d_stop_2)
            ->line('<tr><td width="35px"><img src="'.asset("/storage/upload/web/location_icon_blue.jpg").'"></td><td><strong>'.$this->d_to.'</strong></td></tr></table>')
            ->line('За да го видиш патувањето кликни на копчето подолу.')
            ->action('Види го патувањето', url('rides/detailed-view/'.$this->ride_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'url' => 'rides/detailed-view/',
            'title' => 'Промена во патувањето',
            'ride_id' => $this->ride_id
        ];
    }
}
