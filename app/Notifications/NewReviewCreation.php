<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewReviewCreation extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user_firstname,$r_user_firstname,$mailNotification)
    {
        $this->user_firstname = $user_firstname;
        $this->r_user_firstname = $r_user_firstname;
        $this->mailNotification = $mailNotification;
        $this->notification_channels = ($this->mailNotification == 1 ? ['database','mail'] : ['database']);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->notification_channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Нов коментар')
            ->greeting('Здраво '. $this->r_user_firstname)
            ->line('Имаш добиено нов коментар од '.$this->user_firstname.'.')
            ->line('Кликни на копчето подолу за да го видиш коментарот')
            ->action('Види коментар', url('reviews/receivedreviews'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

            'url' => 'reviews/receivedreviews',
            'title' => 'Имаш нов коментар од '. $this->user_firstname
        ];
    }
}
