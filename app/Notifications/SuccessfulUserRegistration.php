<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SuccessfulUserRegistration extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $firstname = $this->data['firstname'];
        $lastname = $this->data['lastname'];

        return (new MailMessage)
            ->subject('Добредојде на '.config('app.name', 'Возисомене'))
            ->greeting('Здраво '. $firstname . " ". $lastname)
            ->line('Благодариме што се регистрира на '.config('app.name', 'Возисомене').'.')
            ->line('Сега можеш да го споделиш твоето патување или да ги пронајдеш луѓето од твоето опкружување што патуваат каде што патуваш и ти.')
            ->line('Во меѓувреме те молам да го разгледаш и да ги пополниш информациите на твојот профил.')
            ->line('Најави се со кликање на копчето подолу')
            ->action('Најави се', url('/users/profile/general'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
