<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class RideStatusCanceled extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ride,$userFirstname,$r_firstname)
    {
        $this->ride_id = $ride->id;
        $this->user_firstname = $userFirstname;
        $this->r_firstname = $r_firstname;
        $this->ride_date = Carbon::parse($ride->d_date)->format('d.m.Y');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Патувaњето на '.$this->ride_date.' е откажено')
            ->greeting('Здраво '.$this->user_firstname)
            ->line($this->r_firstname.' го откажа патувањето на '.$this->ride_date.' за кое имаш резервирано.')
            ->action('Види го патувањето', url('rides/detailed-view/'.$this->ride_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'url' => 'rides/detailed-view/',
            'title' => $this->r_firstname.' го откажа патувањето',
            'ride_id' => $this->ride_id
        ];
    }
}
