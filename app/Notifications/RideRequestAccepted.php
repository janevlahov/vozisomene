<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;

class RideRequestAccepted extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ride_id,$ride_date,$bFrom,$bTo,$curr_user,$user,$mailNotification)
    {
        $this->ride_id = $ride_id;
        $this->user_firstname = $curr_user->firstname;
        $this->user_lastname = $curr_user->lastname;
        $this->user_phone = $curr_user->phone;
        $this->user_email = $curr_user->email;
        $this->b_firstname = $user['firstname'];
        $this->ride_date = Carbon::parse($ride_date)->format('d.m.Y');
        $this->bFrom = $bFrom;
        $this->bTo = $bTo;
        $this->mailNotification = $mailNotification;
        $this->notification_channels = ($this->mailNotification == 1 ? ['database','mail'] : ['database']);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->notification_channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Твојата резервација е прифатена')
            ->greeting('Здраво '.$this->b_firstname)
            ->line($this->user_firstname.' ја прифати твојата резервација за патувањето')
            ->line('<table cellpadding="6" style="margin-bottom:16px;"><tr><td width="35px"><img src="'.asset("/storage/upload/web/location_icon_blue.jpg").'"></td><td><strong>'.$this->bFrom.'</strong></td></tr>')
            ->line('<tr><td width="35px"><img src="'.asset("/storage/upload/web/location_icon_blue.jpg").'"></td><td><strong>'.$this->bTo.'</strong></td></tr></table>')
            ->line('на <strong>'.$this->ride_date.'</strong>')
            ->line('Те молам контактирај го за понатамошен договор.')
            ->line('Контакт податоци:<br>'.$this->user_firstname.' '.$this->user_lastname.'<br>Телефон: '.$this->user_phone.'<br>Е-Mаил: '.$this->user_email)
            ->action('Види го патувањето', url('rides/detailed-view/'.$this->ride_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'url' => 'rides/detailed-view/',
            'title' => 'Твојата резервација е прифатена',
            'ride_id' => $this->ride_id
        ];
    }
}
