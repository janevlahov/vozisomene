<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    use Queueable;

    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Ресетирај ја твојата лозинка')
            ->greeting('Неодамна побара да ја ресетираш твојата лозинка')
            ->line('Доколку ти го поднесе ова барање за промена, кликни на копчето подолу за да поставиш нова лозинка:')
            ->action('Ресетирај лозинка', url(config('app.url').route('password.reset', [$this->token, encrypt($notifiable->email)], false)))
            ->line('Доколку не го поднесе ова барање, игнорирај ја оваа порака и твојата лозинка ќе остане иста.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
