<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Carbon\Carbon;
use Auth;

class NewRideCreation extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ride,$mailNotification)
    {
        $this->ride = $ride;
        $this->mailNotification = $mailNotification;
        $this->notification_channels = ($this->mailNotification == 1 ? ['mail'] : '');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->notification_channels;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $firstname = Auth::user()->firstname;
        $d_date = Carbon::parse($this->ride->d_date)->format('d.m.Y');
        $d_time = $this->ride->d_time;

        $d_from = $this->ride->d_from;
        $d_to = $this->ride->d_to;
        if (!empty($this->ride->d_stop_1)) {
          $d_stop_1 = '<tr><td width="35px"><img src="'.asset("/storage/upload/web/location_icon_orange.jpg").'"></td><td>'.$this->ride->d_stop_1.'</td></tr>';
        } else {
          $d_stop_1 = '';
        }
        if (!empty($this->ride->d_stop_2)) {
          $d_stop_2 = '<tr><td width="35px"><img src="'.asset("/storage/upload/web/location_icon_orange.jpg").'"></td><td>'.$this->ride->d_stop_2.'</td></tr>';
        } else {
          $d_stop_2 = '';
        }
        $ride_id = $this->ride->id;

        return (new MailMessage)
            ->subject('Потврда за ново патување')
            ->greeting('Здраво '. $firstname)
            ->line('Објави ново патување на дата <strong>'.$d_date.'</strong> во <strong>'.$d_time.'</strong> часот на релација:')
            ->line('<table cellpadding="6" style="margin-bottom:16px;"><tr><td width="35px"><img src="'.asset("/storage/upload/web/location_icon_blue.jpg").'"></td><td><strong>'.$d_from.'</strong></td></tr>')
            ->line($d_stop_1)
            ->line($d_stop_2)
            ->line('<tr><td width="35px"><img src="'.asset("/storage/upload/web/location_icon_blue.jpg").'"></td><td><strong>'.$d_to.'</strong></td></tr></table>')
            ->line('За да го видиш или споделиш на Facebook патувањето кликни на копчето подолу.')
            ->action('Види го патувањето', url('/rides/'.$ride_id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
