<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '161310557801912',
        'client_secret' => '90ef7bbf487bc3a3879706a9de424746',
        'redirect' => 'http://localhost.vozisomene.com/login/facebook/callback',
    ],

    'google' => [
        'client_id' => '452072041573-76mc93pu6955gp0cjkcrhe6ski3haohf.apps.googleusercontent.com',
        'client_secret' => '-abmz6ILm3H-82Mau7Ia_jTL',
        'redirect' => 'http://localhost.vozisomene.com/login/google/callback',
    ],

];
