<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_autos', function (Blueprint $table) {
            $table->integer('id')->unique();
            $table->string('licence_plate')->nullable();
            $table->string('licence_state')->nullable();
            $table->string('car_brand')->nullable();
            $table->string('car_model')->nullable();
            $table->string('type')->nullable();
            $table->string('color')->nullable();
            $table->string('year_production')->nullable();
            $table->string('car_photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_autos');
    }
}
