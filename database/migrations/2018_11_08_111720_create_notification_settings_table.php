<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_settings', function (Blueprint $table) {
            $table->integer('id')->unique();
            $table->tinyInteger('new_ride')->default('1');
            $table->tinyInteger('update_ride')->default('1');
            $table->tinyInteger('ride_request')->default('1');
            $table->tinyInteger('ride_status')->default('1');
            $table->tinyInteger('comment_added')->default('1');
            $table->tinyInteger('promotions_news')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_settings');
    }
}
