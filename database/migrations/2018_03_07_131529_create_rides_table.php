<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('d_from');
            $table->text('d_to');

            $table->text('d_stop_1')->nullable();
            $table->text('d_stop_2')->nullable();

            $table->tinyInteger('roundtrip');

            $table->date('d_date');
            $table->string('d_time'); 
            $table->date('d_rdate')->nullable();
            $table->string('d_rtime')->nullable();

            $table->string('d_cost1')->nullable();
            $table->string('d_cost2')->nullable();
            $table->string('d_cost3')->nullable();
            $table->string('d_cost4')->nullable();
            $table->string('d_cost5')->nullable();
            $table->string('d_cost6')->nullable();

            $table->string('d_stop1_time')->nullable();
            $table->string('d_stop2_time')->nullable();
            $table->string('d_stop3_time')->nullable();

            $table->tinyInteger('free_places');
            $table->tinyInteger('back_s_guarantee');
            $table->text('d_description')->nullable();

            $table->float('distance')->nullable();
            $table->string('est_d_time')->nullable();
            $table->tinyInteger('terms_cond');
            $table->string('status')->nullable();
            $table->tinyInteger('removed')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rides');
    }
}
