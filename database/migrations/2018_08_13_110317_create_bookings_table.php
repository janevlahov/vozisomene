<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ride_id');
            $table->integer('user_id');
            $table->text('b_from');
            $table->text('b_to');
            $table->tinyInteger('qty_places');
            $table->string('b_cost')->nullable();
            $table->string('b_time');
            $table->string('status')->nullable();
            $table->tinyInteger('removed')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
