<?php
/*Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('route:cache ');
    return 'DONE'; //Return anything
});*/
Route::get('/', 'UsersController@index');
Route::get('/users/profile/general', 'UsersController@general');
Route::get('/users/profile/avatar', 'UsersController@avatar');
Route::get('/users/profile/preferences', 'UsersController@preferences');
Route::get('/users/profile/auto', 'UsersController@auto');
Route::get('/users/profile/address', 'UsersController@address');
Route::get('/users/profile/changepassword', 'UsersController@changepassword');
Route::get('/users/profile/notification_settings', 'UsersController@notification_settings');
Route::get('/users/profile/actions/removecar', 'UsersController@remove_auto')->name('users.remove_auto');
Route::get('/rides/user_profile/{id}', 'RidesController@show_user');
Route::get('/rides/my-booked', 'RidesController@show_booked_rides');
Route::get('/rides/detailed-view/{id}', 'RidesController@detailed_ride_view');
Route::get('/rides/search-ride', 'RidesController@search_ride');

Route::resource('rides', 'RidesController');
Route::post('/rides/book-ride', 'RidesController@book_ride');
Route::post('/rides/action', 'RidesController@book_action');
Route::post('rides/book_approval', 'RidesController@book_approval');
Route::post('rides/status_change', 'RidesController@update_ride_status');
Route::post('/rides/actions/remove_ride', 'RidesController@remove_ride');
Route::post('/rides/actions/remove_booking', 'RidesController@remove_booking');

Route::put('/users/profile/general', 'UsersController@update_general_details')->name('users.update_general_details');
Route::put('/users/profile/changepassword/{id}', 'UsersController@updatepassword')->name('users.updatepassword');
Route::post('/users/profile/avatar', 'UsersController@imageCropPost');
Route::put('/users/profile/preferences', 'UsersController@update_preferences')->name('users.update_preferences');
Route::post('/users/profile/actions/update_notification_settings', 'UsersController@update_notification_settings');
Route::post('/users/profile/auto', 'UsersController@update_auto')->name('users.update_auto');
Route::put('/users/profile/address', 'UsersController@update_address_details')->name('users.update_address_details');
Route::post('/users/report_user/{id}', 'UsersController@report_user')->name('users.report_user');

Route::resource('reviews/myreviews', 'ReviewsController');
Route::get('reviews/receivedreviews', 'ReviewsController@index_received');

Route::get('/notifications/show/{id}', 'NotificationsController@show');
Route::post('/notifications/markAllRead', 'NotificationsController@markAllRead')->name('markAllRead');

Route::get('logout', function(){return back();});
Auth::routes();

Route::get('/dashboard', 'UsersController@dashboard');
Route::post('/rides/search-ride', 'UsersController@index_search');
Route::get('login/{service}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{service}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/password/reset/{token}/{email}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

Route::prefix('admin')->group(function(){
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	//Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
	
	Route::get('/users', 'AdminController@show_all_users');
	Route::get('/view-user/{id}', 'AdminController@showUser');
	Route::get('/edit-user/{id}', 'AdminController@editUser');
	Route::post('/view-user/{id}', 'AdminController@updateUser')->name('admin.updateUser');
	Route::post('/update-user-car', 'AdminController@updateUserCar');

	Route::get('/users/reports', 'AdminController@show_all_userReports');
	Route::get('/users/reports/view-report/{id}', 'AdminController@showUserReport');
	Route::get('/users/reports/edit-report/{id}', 'AdminController@editUserReport');
	Route::post('/users/reports/update-report/{id}', 'AdminController@updateUserReport')->name('admin.updateUserReport');

	Route::get('/rides', 'AdminController@show_all_rides');
	Route::get('/view-ride/{id}', 'AdminController@showRide');
	Route::get('/edit-ride/{id}', 'AdminController@editRide');
	Route::post('/view-ride/{id}', 'AdminController@updateRide')->name('admin.updateRide');

	//AdminUser Routes
	Route::get('/viewAdminProfile/{id}', 'AdminUserController@viewProfile');
	Route::get('/editAdminProfile/{id}', 'AdminUserController@editProfile');
	Route::post('/updateAdminProfile/{id}', 'AdminUserController@updateProfile')->name('admin.updateAdminProfile');
	Route::get('/changeAdminpassword/{id}', 'AdminUserController@editPassword');
	Route::post('/updateAdminpassword/{id}', 'AdminUserController@updatePassword')->name('admin.updateAdminPassword');
});

Route::get('/kako-raboti', 'InfoController@kakoraboti');
Route::get('/za-nas', 'InfoController@zanas');
Route::get('/najchesto-postavuvani-prasanja', 'InfoController@faq');
Route::get('/kontakt', 'InfoController@kontakt');
Route::post('/kontakt', 'InfoController@sendMail');
Route::get('/uslovi-za-koristenje', 'InfoController@uslovi');
Route::get('/politika-za-privatnost', 'InfoController@privatnost');
