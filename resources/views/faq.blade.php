@extends('layouts.app')
@section('content')
<section class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="mt-0 text-center">Најчесто поставувани прашања</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel-group" id="prasanja">
          <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#prasanja" href="#prasanje1">
              <div class="panel-heading">
                <h3 class="panel-title">Што е Возисомене?</h3>
              </div>
            </a>
            <div id="prasanje1" class="panel-collapse collapse in">
              <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#prasanja" href="#prasanje2">
              <div class="panel-heading">
                <h3 class="panel-title">Како да пронајдам превоз?</h3>
              </div>
            </a>
            <div id="prasanje2" class="panel-collapse collapse">
              <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#prasanja" href="#prasanje3">
              <div class="panel-heading">
                <h3 class="panel-title">Како да објавам патување?</h3>
              </div>
            </a>
            <div id="prasanje3" class="panel-collapse collapse">
              <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#prasanja" href="#prasanje4">
              <div class="panel-heading">
                <h3 class="panel-title">Како се плаќа?</h3>
              </div>
            </a>
            <div id="prasanje4" class="panel-collapse collapse">
              <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#prasanja" href="#prasanje5">
              <div class="panel-heading">
                <h3 class="panel-title">Како се резервира возење?</h3>
              </div>
            </a>
            <div id="prasanje5" class="panel-collapse collapse">
              <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#prasanja" href="#prasanje6">
              <div class="panel-heading">
                <h3 class="panel-title">Како да го споделам моето патување на Фејсбук?</h3>
              </div>
            </a>
            <div id="prasanje6" class="panel-collapse collapse">
              <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <a data-toggle="collapse" data-parent="#prasanja" href="#prasanje7">
              <div class="panel-heading">
                <h3 class="panel-title">Prasanje so reden broj 7?</h3>
              </div>
            </a>
            <div id="prasanje7" class="panel-collapse collapse">
              <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
