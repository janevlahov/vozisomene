@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1 py-5">
      <div class="row row-eq-height shadow">
        <div class="col-md-5 bg-kontakt text-white">
          <div class="kontakt-podatoci">
            <p class="lead text-secondary mb-1">Контакт информации</p>
            <address>
              Моргенстерн дооел <br>Јосиф Ј. Свештарот 9 <br>2400 Струмица <br>Македонија
            </address>
            <p>+389 78 34 59 43 <br>marketing@morgensternmarketing.com</p>
            <p class="lead text-secondary mt-2 mb-1">Следете не</p>
            <ul class="list-inline">
              <li><a href="#"><i class="fab fa-facebook fa-2x text-white"></i></a></li>
              <li><a href="#"><i class="fab fa-instagram fa-2x text-white"></i></a></li>
              <li><a href="#"><i class="fab fa-youtube fa-2x text-white"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-7 bg-white">
          <h1 class="text-center">Контактирајте не</h1>
          <p class="lead text-center text-secondary">Ве молиме исполнете ги полињата</p>
          @if(Session::has('success'))
            <div class="alert alert-success text-center">
								<p class="lead">{{Session::get('success')}}</p>
						</div>
          @endif
          @if(Session::has('fail'))
            <div class="alert alert-danger text-center">
                <p class="lead">{{Session::get('fail')}}</p>
            </div>
          @endif
          <form method="POST" action="/kontakt">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                  <label class="control-label" for="firstname">Име: *</label>
                  <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Име" @guest value="" @else value="{{ Auth::user()->firstname }}" @endguest >
                  @if ($errors->has('firstname'))
                    <span class="help-block">
                      {{ $errors->first('firstname') }}
                    </span>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="lastname">Презиме:</label>
                  <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Презиме" @guest value="" @else value="{{ Auth::user()->lastname }}" @endguest>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label class="control-label" for="email">Е-Маил: *</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Е-маил Адреса" @guest value="" @else value="{{ Auth::user()->email }}" @endguest>
                  @if ($errors->has('email'))
                    <span class="help-block">
                      {{ $errors->first('email') }}
                    </span>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="phone">Телефон:</label>
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Телефон" value="">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group{{ $errors->has('poraka') ? ' has-error' : '' }}">
                  <label class="control-label" for="poraka">Порака: *</label>
                  <textarea name="poraka" id="poraka" class="form-control" rows="5"></textarea>
                  @if ($errors->has('poraka'))
                    <span class="help-block">
                      {{ $errors->first('poraka') }}
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <label class="ohnohoney" for="workemailaddress"></label>
            <input class="ohnohoney" autocomplete="off" type="text" id="workemailaddress" name="workemailaddress" placeholder="Your workemailaddress here">
            <label class="ohnohoney" for="company"></label>
            <input class="ohnohoney" autocomplete="off" type="text" id="company" name="company" placeholder="Your company here">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50"><i class="fas fa-share-square"></i></span>
                <span class="text">Испрати</span>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
