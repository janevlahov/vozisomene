@extends('layouts.app')
@section('content')
<section class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="mt-0 text-center">Како работи вози со мене</h1>
      </div>
    </div>
    <div class="row mb-2">
      <div class="col-md-12">
        <ul class="nav nav-pills nav-justified kakoraboti">
          <li role="presentation" class="active">
            <a href="#vozime" aria-controls="vozime" role="tab" data-toggle="tab">
              <p class="lead">Сакаш да одиш некаде?</p>
              <p>Пронајди превоз со сигурен возач. <br>Заштеди време и пари и вози се удобно.</p>
            </a>
          </li>
          <li role="presentation">
            <a href="#vozam" aria-controls="vozam" role="tab" data-toggle="tab">
              <p class="lead">Возиш до некаде и имаш слободни места?</p>
              <p>Понуди превоз на патниците. <br>Сподели го твоето возење со сигурни патници и поделете ги трошоците.</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="vozime">
        <div class="row mb-1">
          <div class="col-md-12">
            <div class="bg-white p-3">
              <div class="row">
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-2 cekor">
                      <span>1</span>
                    </div>
                    <div class="col-md-10">
                      <h2 class="mt-0 mb-1">Пронајди превоз/возење</h2>
                      <p class="lead text-secondary">Како да пронајдеш превоз?</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-justify">Само напиши каде одиш и од каде. Потоа избери го превозот што најмногу ти одговара. Доколку ти требаат повеќе информации секогаш можеш да го видиш профилот на возачот.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <img src="{{asset('storage/upload/web/najava.jpg')}}" class="img-responsive" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-1">
          <div class="col-md-12">
            <div class="bg-white p-3">
              <div class="row">
                <div class="col-md-4">
                  <img src="{{asset('storage/upload/web/najava.jpg')}}" class="img-responsive" alt="">
                </div>
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-2 cekor">
                      <span>2</span>
                    </div>
                    <div class="col-md-10">
                      <h2 class="mt-0 mb-1">Резервирај</h2>
                      <p class="lead text-secondary">Како да резервираш?</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-justify">Откако ќе го најдеш твојот идеален превоз кликни на копчето резервирај, избери ја твојта деситнација и резервирај седиште. Потоа возачот добива е-маил и се чека на негова потврда за твоето барање за резервација. Доколку возачот ја прифати твојата резервација ќе ги добиеш неговите контакт информации за понатамошен договор.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="bg-white p-3">
              <div class="row">
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-2 cekor">
                      <span>3</span>
                    </div>
                    <div class="col-md-10">
                      <h2 class="mt-0 mb-1">Патувајте заедно</h2>
                      <p class="lead text-secondary">Среќно патување</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-justify">Уживајте во возењето и секако не заборавај да го оцениш своето искуство на крај.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <img src="{{asset('storage/upload/web/najava.jpg')}}" class="img-responsive" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="vozam">
        <div class="row mb-1">
          <div class="col-md-12">
            <div class="bg-white p-3">
              <div class="row">
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-2 cekor">
                      <span>1</span>
                    </div>
                    <div class="col-md-10">
                      <h2 class="mt-0 mb-1">Сподели превоз/возење</h2>
                      <p class="lead text-secondary">Како да го споделиш превозот?</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-justify">За да објавите патување треба да ја пополнете формата на страната „Сподели Патување“. Напишете кога и од каде до каде возете.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <img src="{{asset('storage/upload/web/najava.jpg')}}" class="img-responsive" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-1">
          <div class="col-md-12">
            <div class="bg-white p-3">
              <div class="row">
                <div class="col-md-4">
                  <img src="{{asset('storage/upload/web/najava.jpg')}}" class="img-responsive" alt="">
                </div>
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-2 cekor">
                      <span>2</span>
                    </div>
                    <div class="col-md-10">
                      <h2 class="mt-0 mb-1">Барања за резервација</h2>
                      <p class="lead text-secondary">Патниците ќе резервираат преку интернет</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-justify">Кога патник ќе резервира место за твоето возење, ќе добиеш известување преку е-маил и треба да го прифатиш или одбиеш барањето за резервација. Доколку го прифатиш ќе ги добиеш податоците за контакт на патникот за понатамошен договор.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-1">
          <div class="col-md-12">
            <div class="bg-white p-3">
              <div class="row">
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-2 cekor">
                      <span>3</span>
                    </div>
                    <div class="col-md-10">
                      <h2 class="mt-0 mb-1">Патувајте заедно</h2>
                      <p class="lead text-secondary">Уживајте во возењето</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-justify">Патниците ќе ви платат за превозот во текот или крајот на возењето во зависност од твојот договор со нив. Секако на крајот ќе биде оценети.</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <img src="{{asset('storage/upload/web/najava.jpg')}}" class="img-responsive" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
