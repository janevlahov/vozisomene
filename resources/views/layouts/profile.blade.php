@extends('layouts.app')
@section('content')
  <div class="container profil py-5 py-xs-0 pt-xs-22">
    @if (session('status'))
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      </div>
    </div>
    @endif
    <div class="row">
      <div class="col-md-12 visible-xs">
        <button type="button" class="btn btn-default btn-block btn-meni" data-toggle="collapse" data-target="#profil-meni">Мени <i class="fas fa-plus pull-right smeni-ikonka"></i></button>
      </div>
      <div class="col-md-12 collapse" id="profil-meni">
          <ul class="nav nav-pills">
            <li><a class="{{ request()->is('users/profile/general') ? 'active' : '' }}" href="/users/profile/general"><i class="fas fa-user"></i> <span>Податоци</span></a></li>
            <li><a class="{{ request()->is('users/profile/avatar') ? 'active' : '' }}" href="/users/profile/avatar"><i class="fas fa-image"></i> <span>Профилна Слика</span></a></li>
            <li><a class="{{ request()->is('users/profile/preferences') ? 'active' : '' }}" href="/users/profile/preferences"><i class="fas fa-user-circle"></i> <span>За мене</span></a></li>
            <li><a class="{{ request()->is('users/profile/auto') ? 'active' : '' }}" href="/users/profile/auto"><i class="fas fa-car"></i> <span>Автомобил</span></a></li>
            <li><a class="{{ request()->is('users/profile/address') ? 'active' : '' }}" href="/users/profile/address"><i class="fas fa-map-marker-alt"></i> <span>Адреса</span></a></li>
            <li><a class="{{ request()->is('users/profile/notification_settings') ? 'active' : '' }}" href="/users/profile/notification_settings"><i class="fas fa-bell"></i> <span>Известувања</span></a></li>
            <li class="pull-right"><a class="{{ request()->is('reviews/myreviews') ? 'active' : '' }}" href="/reviews/myreviews"><i class="fas fa-comments"></i> <span>Дадени Коментари</span></a></li>
            <li class="pull-right"><a class="{{ request()->is('reviews/receivedreviews') ? 'active' : '' }}" href="/reviews/receivedreviews"><i class="fas fa-comment-dots"></i> <span>Примени Коментари</span></a></li>
          </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 mt-xs-3">
        @yield('profile_content')
      </div>
    </div>
  </div>
@endsection
@section('includesscripts')
<script>
  $(".btn-menu").click(function(){
    var si = $('.smeni-ikonka');
    if(si.hasClass('fa-plus')){
        si.removeClass('fa-plus')
           .addClass('fa-minus')
    }
    else{
        si.addClass('fa-plus')
           .removeClass('fa-minus')
    }
  });
</script>
@endsection
