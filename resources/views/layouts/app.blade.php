<!DOCTYPE html>
<html lang="mk">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Пронајди превоз со сигурен возач. Заштеди време и пари и вози се удобно."/>
    <meta name="theme-color" content="#fff">
    <link rel="icon" href="{{ asset('/storage/upload/web/favicon.png') }}" type="image/png">
    <title>{{ config('app.name', 'Возисомене') }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Oswald:200,300,400,600&amp;subset=cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="manifest" href="/manifest.json">
    <script>
        if ('serviceWorker' in navigator && 'PushManager' in window) {
            window.addEventListener('load', function() {
                navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
            // Registration was successful
            //console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
            // registration failed :(
            //console.log('ServiceWorker registration failed: ', err);
        });
            });
        }
    </script>
    @yield('includecss')
    <link href="{{ asset('css/vozisomene.css') }}" rel="stylesheet">
    @include('inc.social_meta')
</head>
<body class="{{ request()->is('login') || request()->is('password/*') || request()->is('register') ? 'bg-najava' : 'bg-vozi' }} {{ request()->is('users/profile/*') ? 'bg-profil' : '' }} {{ request()->is('reviews/*') ? 'bg-reviews' : '' }} {{ request()->is('rides/create') ? 'bg-dodadi' : '' }} {{ request()->is('rides*') ? 'bg-ride' : '' }}">
    @include('inc.navbar')
    <div class="" id='app'>
      <div class="overlay">
        @yield('content')
      </div>
    </div>
    @include('inc.footer')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('js/bootstrap-datepicker.mk.js') }}"></script>
    <script src="{{ asset('js/bootstrap-clockpicker.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/vozisomene.js') }}"></script>
    @yield('includesscripts')
</body>
</html>
