@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 py-5 py-xs-0">
      <div class="row row-eq-height">
        <div class="col-xs-12 col-md-6 bg-login hidden-xs"></div>
        <div class="col-xs-12 col-md-6 bg-white">
          <h1 class="text-center">Ресетирај ја лозинката</h1>
          <p class="lead text-center text-secondary">Внеси ја твојата нова лозинка</p>
          <form class="form" method="POST" action="{{ route('password.request') }}">
              {{ csrf_field() }}

              <input type="hidden" name="token" value="{{ $token }}">

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="control-label">Е-Маил адреса</label>

                      <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" readonly>

                      @if ($errors->has('email'))
                          <span class="help-block">
                              {{ $errors->first('email') }}
                          </span>
                      @endif

              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="control-label">Лозинка</label>

                      <input id="password" type="password" class="form-control" name="password" autofocus>

                      @if ($errors->has('password'))
                          <span class="help-block">
                              {{ $errors->first('password') }}
                          </span>
                      @endif
              </div>

              <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  <label for="password-confirm" class="control-label">Потврди лозинка</label>
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                      @if ($errors->has('password_confirmation'))
                          <span class="help-block">
                              {{ $errors->first('password_confirmation') }}
                          </span>
                      @endif
              </div>

              <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-lg btn-block">
                          Ресетирај лозинка
                      </button>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
