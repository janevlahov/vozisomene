@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 py-5 py-xs-0">
      <div class="row row-eq-height">
        <div class="col-xs-12 col-md-6 bg-login hidden-xs"></div>
        <div class="col-xs-12 col-md-6 bg-white">
          <h1 class="text-center">Ресетирај ја лозинката</h1>
          <p>Разбираме, работите се случуваат. Едноставно внеси ја твојата Е-Маил адреса подолу и ние ќе ти испратиме линк за да ја ресетираш твојата лозинка!</p>
          @if (session('status'))
            <div class="alert alert-success">
              <p class="lead">{{ session('status') }}</p>
            </div>
          @endif
          <form class="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="control-label">Е-Маил Адреса</label>

              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

              @if ($errors->has('email'))
                <span class="help-block">
                  {{ $errors->first('email') }}
                </span>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-lg btn-block">Испрати</button>
              <hr>
            </div>
            <div class="col-md-12 pt-0 pb-3 text-center">
              <p class="mb-0"><a href="{{ route('login') }}">Веќе имаш профил? Најави се!</a></p>
              <p class="mb-0"><a href="{{ route('register') }}">Регистрирај се</a></p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
