@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 py-5 py-xs-0">
      <div class="row row-eq-height shadow">
        <div class="col-xs-12 col-md-6 bg-login hidden-xs"></div>
        <div class="col-xs-12 col-md-6 bg-white">
          <h1 class="text-center">Регистрирај профил</h1>
          <p class="lead text-center text-secondary">Те молам пополни ги полињата</p>
          <form class="form" method="POST" action="{{ route('register') }}">
            <div class="row">
              {{ csrf_field() }}

              <div class="col-md-6 form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                <label for="firstname" class="control-label">Име</label>
                <input id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" autofocus>
                @if ($errors->has('firstname'))
                <span class="help-block">
                  {{ $errors->first('firstname') }}
                </span>
                @endif
              </div>

              <div class="col-md-6 form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                <label for="name" class="control-label">Презиме</label>
                <input id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" autofocus>
                @if ($errors->has('lastname'))
                  <span class="help-block">
                    {{ $errors->first('lastname') }}
                  </span>
                @endif
              </div>

              <div class="col-md-12 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">Е-Маил Адреса</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                @if ($errors->has('email'))
                  <span class="help-block">
                    {{ $errors->first('email') }}
                  </span>
                @endif
              </div>

              <div class="col-md-6 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Лозинка</label>
                <input id="password" type="password" class="form-control" name="password">
                @if ($errors->has('password'))
                  <span class="help-block">
                    {{ $errors->first('password') }}
                  </span>
                @endif
              </div>

              <div class="col-md-6 form-group">
                <label for="password-confirm" class="control-label">Потврди Лозинка</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
              </div>

              <label class="ohnohoney" for="workemailaddress"></label>
              <input class="ohnohoney" autocomplete="off" type="text" id="workemailaddress" name="workemailaddress" placeholder="Your workemailaddress here">
              <label class="ohnohoney" for="company"></label>
              <input class="ohnohoney" autocomplete="off" type="text" id="company" name="company" placeholder="Your company here">

              <div class="form-group col-md-12">
                <button type="submit" class="btn btn-primary btn-block btn-lg">
                  Регистрирај се
                </button>
                <hr>
              </div>
              <div class="col-md-12 pt-0 py-3 text-center">
                <p class="mb-0"><a href="{{ route('login') }}">Веќе имаш профил? Најави се!</a></p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
