@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 py-5 py-xs-0">
      <div class="row row-eq-height shadow">
        <div class="col-xs-12 col-md-6 bg-kruska hidden-xs"></div>
        <div class="col-xs-12 col-md-6 bg-white py-4">
          <h1 class="text-center">Само за нашио админ</h1>
          <p class="lead text-center text-secondary">Ти одвонка не чепки тука</p>
          <form class="form" method="POST" action="{{ route('admin.login.submit') }}">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                @if($errors->has('deactivated'))
                <h4 class="text-center">{{$errors->first('deactivated')}}</h4>
                @endif
              </div>
            </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="control-label">Е-Маил Адреса</label>
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                  @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="control-label">Лозинка</label>
                  <input id="password" type="password" class="form-control" name="password" required>
                  @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-lg btn-block">Најава</button>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
