@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1 py-5 py-xs-0">
      <div class="row row-eq-height shadow">
        <div class="col-xs-12 col-md-6 bg-login hidden-xs"></div>
        <div class="col-xs-12 col-md-6 bg-white">
          <h1 class="text-center">Добредојде назад</h1>
          <p class="lead text-center text-secondary">Те молам најави се</p>
          <form class="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-12">
                @if($errors->has('deactivated'))
                  <h4 class="text-center">{{$errors->first('deactivated')}}</h4>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="control-label">Е-Маил Адреса</label>
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                  @if ($errors->has('email'))
                  <span class="help-block">
                    {{ $errors->first('email') }}
                  </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="control-label">Лозинка</label>

                  <input id="password" type="password" class="form-control" name="password">

                  @if ($errors->has('password'))
                  <span class="help-block">
                    {{ $errors->first('password') }}
                  </span>
                  @endif
                </div>

                <div class="form-group">
                  <div class="checkbox checkbox-primary">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label for="remember">Запомни ме</label>
                  </div>
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-lg btn-block">Најави се</button>
                </div>
                <hr>
              </div>
              <div class="col-md-12">
                <a href="{{url('/login/facebook')}}" class="btn btn-facebook btn-lg btn-block"><i class="fab fa-facebook-f"></i> Најави се со Facebook</a>
                <a href="{{url('/login/google')}}" class="btn btn-google btn-lg btn-block"><i class="fab fa-google"></i> Најави се со Google</a>
                <hr>
              </div>
              <div class="col-md-12 pt-0 pb-3 text-center">
                <p class="mb-0"><a href="{{ route('password.request') }}">Си ја заборави лозинката?</a></p>
                <p class="mb-0"><a href="{{ route('register') }}">Регистрирај се</a></p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
