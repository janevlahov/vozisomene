@extends('layouts.app')

@section('content')
<div class="py-5">
<div class="container">
  <div class="row row-eq-height">
    <div class="col-md-6 pr-0 col-xs-12 pr-xs-15">
      <div class="col-md-12 bg-white pb-3">
        <!-- multistep form -->
        <form id="msform" action="{{action('RidesController@store')}}" method="POST" >
          <h1>Додади Патување</h1>
          <ul id="progressbar">
            <li class="active"></li><li></li>
          </ul>
          <!-- fieldsets -->
          <fieldset>
            <div id="mode-selector" class="controls hidden">
              <input type="radio" name="type" id="changemode-driving" checked="checked">
              <label for="changemode-driving">Driving</label>
            </div>

            <div id="right-panel">
              <h3>Места на поаѓање и пристигнување</h3>
              <div id="origin-input-fg" class="form-group">
                <label class="control-label" for="origin-input">Возам од</label>
                <input id="origin-input" name="d_from" class="controls form-control" type="text" placeholder="Додади локација на поаѓање">
              </div>
              <div id="destination-input-fg" class="form-group">
                <label class="control-label" for="destination-input">Возам до</label>
                <input id="destination-input" name="d_to" class="controls form-control" type="text" placeholder="Додади локација на пристигнување">
              </div>

              <h3 class="text-secondary">Попатни места</h3>

              <label class="control-label" for="d_stop_1">Пополни го ова поле доколку застануваш на место кое е помеѓу твојата почетна и крајна дестинација. Така може да те пронајдат повеќе луѓе и да патуват со тебе.</label>
              <div class="form-group">
                <div class="input-group" id="waypoints1inputgroup">
                  <input id="waypoints1" name="d_stop_1" class="controls waypoints form-control " type="text" placeholder="Додади попатна локација">
                  <div class="input-group-addon bg-primary addstop" id='addstop'><i class="fas fa-plus text-white"></i></div>
                </div>
              </div>
              <div class="form-group">
                <input id="waypoints2" name="d_stop_2" class="controls waypoints form-control hide" type="text" placeholder="Додади попатна локација">
              </div>

              <h3>Дата и час на патувањето</h3>
              <div id='s_date_time'>
                <div class="col-md-6">
                  <div id="datepicker1-fg" class="form-group">
                    <label class="control-label" for="datepicker1">Дата на поаѓање</label>
                    <div id="datepicker1-ig" class="input-group">
                      <div class="input-group-addon"><i class="fas fa-calendar-alt"></i></div>
                      <input type="text" id="datepicker1" name="d_date" class="form-control" autocomplete="off">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div id="clockpicker1I-fg" class="form-group">
                    <label class="control-label" for="datepicker1">Час на поаѓање</label>
                    <div id="clockpicker1I-ig" class="input-group clockpicker1" data-align="top" data-autoclose="true">
                      <div class="input-group-addon"><i class="fas fa-clock"></i></div>
                      <input type="text" class="form-control" name="d_time" id='clockpicker1I' autocomplete="off">
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="checkbox checkbox-primary">
                  <input type="checkbox" name="roundtrip" id="roundtrip" onchange="toggleCheckbox(this)" value="0">
                  <label class="control-label" for="roundtrip">Се враќам назад</label>
                </div>
              </div>

              <div id='b_date_time' style="display: none;">
                <div class="col-md-6">
                  <div id="datepicker2-fg" class="form-group">
                    <label class="control-label" for="datepicker2">Дата на враќање</label>
                    <div id="datepicker2-ig" class="input-group">
                      <div class="input-group-addon"><i class="fas fa-calendar-alt"></i></div>
                      <input type="text" id="datepicker2" name="d_rdate" class="form-control" autocomplete="off">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div id="clockpicker2I-fg" class="form-group">
                    <label class="control-label" for="datepicker2">Час на враќање</label>
                    <div id="clockpicker2I-ig" class="input-group clockpicker2" data-align="top" data-autoclose="true">
                      <div class="input-group-addon"><i class="fas fa-clock"></i></div>
                      <input type="text" class="form-control" name="d_rtime" id='clockpicker2I' autocomplete="off">
                    </div>
                  </div>
                </div>
              </div>


              <input type='hidden' id='d_stop1_value'>
              <input type='hidden' id='d_stop1_time' name="d_stop1_time">
              <input type='hidden' id='d_stop2_value'>
              <input type='hidden' id='d_stop2_time' name="d_stop2_time">
              <input type='hidden' id='d_stop3_value'>
              <input type='hidden' id='d_stop3_time' name="d_stop3_time">
              <input type='hidden' id='d_enddest_value'>
              <input type='hidden' id='d_enddest_time' name="d_enddest_time">

              <input type='hidden' id='distance' name='distance'>
              <input type='hidden' id='est_d_time' name='est_d_time'>

              <button type="button" id="get_directions" style="height: 1px; width:1px; visibility: hidden;"></button>
            </div>
            <button type="button" name="next" class="next btn btn-primary btn-lg btn-block" id="next1">Следно <i class="fas fa-angle-double-right"></i></button>

          </fieldset>

          <fieldset>

            <h3>Цена на чинење по патник</h3>

            <div class="row">
              <div id='costsharing'></div>
            </div>

            <hr>
            <div class="form-group">
              <label class="control-label" for="free_places">Слободни места:</label>
              <input type="number" id="free_places" name="free_places" class="controls form-control" value="3" min="1" max="6" >
            </div>

            <div class="form-group">
              <div class="checkbox checkbox-primary">
                <input type="checkbox" name="back_s_guarantee" id="back_s_guarantee" onchange="toggleCheckbox(this)" value="0">
                <label class="control-label" for="back_s_guarantee">Гарантирам дека најмногу две лица ќе седат на задните седишта (патниците го претпочитат ова)</label>
              </div>
            </div>

              <div class="form-group">
                <label class="control-label" for="ride_comment">Напиши повеќе детали за твоето патување</label>
                <textarea name="ride_comment" id="ride_comment" class="form-control controls"></textarea>
              </div>

              <div id="term_conditions-fg" class="form-group">
                <div class="checkbox checkbox-primary">
                  <input type="checkbox" name="terms_cond" id="term_conditions" value="0" onchange="changeVal(this)">
                  <label class="control-label" for="term_conditions" id="LBL_term_conditions">Правила и Услови</label>
                </div>
              </div>

              <button type="button" name="previous" class="previous action-button btn btn-link"><i class="fas fa-angle-double-left"></i> Назад</button>
              <button type="submit" class="btn btn-primary btn-lg btn-block submit">Објави Патување <i class="fas fa-plane-departure"></i></button>
            </fieldset>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </form>
    <!-- /END FORM -->
        </div>
      </div>
      <div class="col-md-6 pl-0 col-xs-12 pl-xs-15">
        <div class="col-md-12 bg-white h-100 pr-0 pl-xs-0">
          <div id="map" class="h-50 mb-xs-3"></div>
          <div class="row pb-xs-3">
            <div class="col-md-12 px-4 py-5">
              <div id="directions-panel"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  </div>
  @endsection

  @section('includesscripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/mk.js"></script>
  <script>

  $('#msform').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      if(document.getElementById("origin-input").value == 'undefined'){
        document.getElementById("origin-input").value = '';
      }
      if(document.getElementById("destination-input").value == 'undefined'){
        document.getElementById("destination-input").value = '';
      }
      if(document.getElementById("waypoints1").value == 'undefined'){
        document.getElementById("waypoints1").value = '';
      }
      if(document.getElementById("waypoints2").value == 'undefined'){
        document.getElementById("waypoints2").value = '';
      }

      e.preventDefault();
      return false;
    }
  });

  window.onload = function() {
    var orig = document.getElementById('origin-input');
    var dest = document.getElementById('destination-input');
    var wp1 = document.getElementById('waypoints1');
    var wp2 = document.getElementById('waypoints2');
     orig.value = dest.value = wp1.value = wp2.value = '';
    orig.onpaste = function(e) {
      e.preventDefault();
    }
    dest.onpaste = function(e) {
      e.preventDefault();
    }
    wp1.onpaste = function(e) {
      e.preventDefault();
    }
    wp2.onpaste = function(e) {
      e.preventDefault();
    }

  }

  $("#addstop").on('click', function() {
    stopIn = document.getElementById("waypoints1").value;
    if(stopIn != ''){
      $("#waypoints2").removeClass("hide");
      $("#waypoints2").focus();
      $("#addstop").addClass("hide");
      $("#waypoints1inputgroup").removeClass("input-group");
    }
  });

  function toggleCheckbox(element)
  {
    if(element.checked){

      $(element).val(1);
      $(element).prop('checked',true);
      $('#b_date_time').css("display","block");

    }else{

      $(element).val(0);
      $(element).prop('checked',false);
      $('#b_date_time').css("display","none");
      $('#clockpicker2I, #datepicker2').val('');
    }
  }

  function changeVal(element)
  {
    if(element.checked){
      $(element).val(1);
    }else{
      $(element).val(0);
    }
  }

  $('.clockpicker1, .clockpicker2').clockpicker().find('input').focusout(function(){
    // regular expression to match required time format
    re = /^\d{1,2}:\d{2}([ap]m)?$/;
    if(!this.value.match(re)){
      this.value = '';
    }
  });

  function checkTimeDiff(){
     var time1I = $('#clockpicker1I').val();
      var time2I = $('#clockpicker2I').val();
      if(time1I >= time2I){
        $('#time2Error').remove();
        $("#b_date_time").after("<div class='row' id='time2Error'><div class='col-md-12'><div class='alert alert-danger'>Времето на враќање треба да е подоцна од времето на поаѓање</div></div></div>");
        $("#clockpicker2I").val('');
        $('#clockpicker2I-fg').addClass('has-error');
      }else{
        $('#time2Error').remove();
        $('#clockpicker2I-fg').removeClass('has-error');
      }
  }

  function checkTime1Validity(){
    d = new Date();
    var datepicker1 = $("#datepicker1").datepicker('getDate');
    var datepicker1 = moment(datepicker1).format('YYYY-MM-DD');
    var dateNow = moment(d).format('YYYY-MM-DD');

    if(datepicker1 == dateNow){
      var time1I = $('#clockpicker1I').val();
      var timeNow = moment(d).add(1, 'hour').format('HH:mm');

      if(timeNow >= time1I){
        $('#time1Error').remove();
        $("#s_date_time").after("<div class='row' id='time1Error'><div class='col-md-12'><div class='alert alert-danger'>Времето на поаѓање треба да е миниум 1 час подоцна од сега</div></div></div>");
        $("#clockpicker1I").val('');
        $('#clockpicker1I-fg').addClass('has-error');
      }else{
        $('#time1Error').remove();
        $('#clockpicker1I-fg').removeClass('has-error');
      }
    }else{
      $('#time1Error').remove();
    }

    if($("#datepicker2").val() != ''){
      checkTimeDiff();
    }
  }

  function checkTime2Validity(){
    var datepicker1 = $("#datepicker1").datepicker('getDate');
    var datepicker2 = $("#datepicker2").datepicker('getDate');
    var datepicker1 = moment(datepicker1).format('YYYY-MM-DD');
    var datepicker2 = moment(datepicker2).format('YYYY-MM-DD');

    if(datepicker1 == datepicker2 ){
      checkTimeDiff();
    }else{
      $('#time2Error').remove();
    }
  }

  $('.clockpicker1').clockpicker().find('input').change(function(){
    checkTime1Validity();
  });

  $('.clockpicker2').clockpicker().find('input').change(function(){
    checkTime2Validity();
  });

  var date = new Date();
  $('#datepicker1,#datepicker2').datepicker({
    format: 'dd.mm.yyyy',
    startDate: date,
    endDate: '+365d',
    autoclose: true,
    language: 'mk',
  });

  $("#datepicker1").change(function(){
    var datepicker1 = $("#datepicker1").datepicker('getDate');
    var datepicker2 = $("#datepicker2").datepicker('getDate');
    $("#datepicker2").datepicker('setStartDate',datepicker1);
    if(datepicker2 < datepicker1){
      $("#datepicker2").datepicker('clearDates');
    }

    if($('#clockpicker1I').val() != ''){
          checkTime1Validity();
    }
  });

  function initMap() {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: {lat: 41.71556, lng: 21.77556},
      streetViewControl: false,
      fullscreenControl: false
    });
    directionsDisplay.setMap(map);


    document.getElementById('get_directions').addEventListener('click', function() {
      calculateAndDisplayRoute(directionsService, directionsDisplay);
    });
    new AutocompleteDirectionsHandler(map);
  }

  function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    var waypts = [];
    var checkboxArray1 = document.getElementById('waypoints1');
    var checkboxArray2 = document.getElementById('waypoints2');

    if(checkboxArray1.value != ""){
      waypts.push({
        location: checkboxArray1.value,
        stopover: true
      });
    }

    if(checkboxArray2.value != ""){
      waypts.push({
        location: checkboxArray2.value,
        stopover: true
      });
    }

    /* for (var i = 0; i < checkboxArray.length; i++) {
    if (checkboxArray.options[i].selected) {
    waypts.push({
    location: checkboxArray[i].value,
    stopover: true
  });
}
}*/
$("#origin-input, #destination-input").removeClass("alert alert-danger");

directionsService.route({
  origin: document.getElementById('origin-input').value,
  destination: document.getElementById('destination-input').value,
  waypoints: waypts,
  optimizeWaypoints: true,
  travelMode: 'DRIVING'
}, function(response, status) {
  if (status === 'OK') {
    directionsDisplay.setDirections(response);
    var route = response.routes[0];
    var summaryPanel = document.getElementById('directions-panel');
    summaryPanel.innerHTML = '';
    var totalDistance = 0;
    var totalDuration = 0;
    var segmentCostshare = [];

    $("#costsharing").empty();
    // console.log(route.legs.length);
    // For each route, display summary information.
    for (var i = 0; i < route.legs.length; i++) {
      var routeSegment = i + 1;
      // summaryPanel.innerHTML += '<p><b>Дел '+ routeSegment +'</b></p>';
      //Format Strings for Part 2
      var startAddress = route.legs[i].start_address.split(',');
      var endAddress = route.legs[i].end_address.split(',');

      if(startAddress.length >= 3){
        startAddress_f = startAddress[0] + ',' + startAddress[startAddress.length - 2];
      }else{
        startAddress_f = startAddress[0];
      }

      if(endAddress.length >= 3){
        endAddress_f = endAddress[0] + ',' + endAddress[endAddress.length - 2];
      }else{
        endAddress_f = endAddress[0];
      }
      var elememID = 'costShare' + routeSegment;
      if (route.legs.length == 1) {
        var WidthClass = 'col-md-12';
      } else {
        var WidthClass = 'col-md-6';
      }
      var appendInputs = "<div class='"+WidthClass+"'><div id='" + elememID + "-fg' class='form-group'><label class='control-label costShareP'><span class='preseci' data-toggle='tooltip' data-placement='top'>" + startAddress_f + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + endAddress_f + "</span></label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' min='10' max='99999' step='10' /></div></div>";
      $("#costsharing").append(appendInputs);

      if(route.legs.length > 1){

        summaryPanel.innerHTML += "<dl class='dl-horizontal'><dt><i class='fas fa-map-marker-alt text-secondary fa-2x'></i></dt><dd><span class='preseci' data-toggle='tooltip' data-placement='top'>" + route.legs[i].start_address + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'> " + route.legs[i].end_address + "</span></dd><dd><span class='dolzina'><i class='fas fa-road'></i>" + parseInt(route.legs[i].distance.text) + " км</span><span class='saat'><i class='far fa-clock'></i> " + secondsToHms(route.legs[i].duration.value) + "</dd></dl>";
      }

      totalDistance = totalDistance + route.legs[i].distance.value;
      totalDuration = totalDuration + route.legs[i].duration.value;

      $('#d_stop' + routeSegment + '_value').val(route.legs[i].duration.value);

      if(route.legs.length >= 2 && i == 1){
        document.getElementById('waypoints1').value = route.legs[1].start_address;
      }

      if(route.legs.length == 3 && i == 2){
        document.getElementById('waypoints2').value = route.legs[2].start_address;
      }
    }

    if(route.legs.length >= 1){

      //Format Strings for Part 2
      var startAddress = route.legs[0].start_address.split(',');
      var endAddress = route.legs[route.legs.length - 1].end_address.split(',');

      if(startAddress.length >= 3){
        startAddress_f = startAddress[0] + ',' + startAddress[startAddress.length - 2];
      }else{
        startAddress_f = startAddress[0];
      }

      if(endAddress.length >= 3){
        endAddress_f = endAddress[0] + ',' + endAddress[endAddress.length - 2];
      }else{
        endAddress_f = endAddress[0];
      }

      if(route.legs.length == 3){
        //First Middle Address
        var middleAddress = route.legs[1].start_address.split(',');
        if(middleAddress.length >= 3){
          middleAddress_f = middleAddress[0] + ',' + middleAddress[middleAddress.length - 2];
        }else{
          middleAddress_f = middleAddress[0];
        }
        //Second Middle Address
        var middleAddress2 = route.legs[2].start_address.split(',');
        if(middleAddress2.length >= 3){
          middleAddress_f2 = middleAddress2[0] + ',' + middleAddress2[middleAddress2.length - 2];
        }else{
          middleAddress_f2 = middleAddress2[0];
        }

        routeSegment = routeSegment + 1;
        var elememID = 'costShare' + routeSegment;

        var appendInputs = "<div class='col-md-6'><div id='" + elememID + "-fg' class='form-group'><label class='control-label costShareP'><span class='preseci' data-toggle='tooltip' data-placement='top'>" + startAddress_f + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + middleAddress_f2 + "</span></label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' min='10' max='99999' step='10' /></div></div>";
        $("#costsharing").append(appendInputs);

        routeSegment = routeSegment + 1;
        var elememID = 'costShare' + routeSegment;

        var appendInputs = "<div class='col-md-6'><div id='" + elememID + "-fg' class='form-group'><label class='control-label costShareP'><span class='preseci' data-toggle='tooltip' data-placement='top'>" + middleAddress_f + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + endAddress_f + "</span></label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' min='10' max='99999' step='10' /></div></div>";
        $("#costsharing").append(appendInputs);

      }

      if(route.legs.length > 1){
        routeSegment = routeSegment + 1;
        var elememID = 'costShare' + routeSegment;
        var appendInputs = "<div class='col-md-6'><div id='" + elememID + "-fg' class='form-group'><label class='control-label costShareP'><span class='preseci' data-toggle='tooltip' data-placement='top'>" + startAddress_f + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + endAddress_f + "</span></label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' min='10' max='99999' step='10' /></div></div>";
        $("#costsharing").append(appendInputs);
      }

      document.getElementById('origin-input').value = route.legs[0].start_address;
      document.getElementById('destination-input').value = route.legs[route.legs.length - 1].end_address;

      totalDistance = totalDistance / 1000;

      summaryPanel.innerHTML += "<dl class='dl-horizontal'><dt><i class='fas fa-map-marker-alt text-primary fa-2x'></i></dt><dd><span class='preseci' data-toggle='tooltip' data-placement='top'>" + startAddress + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + endAddress + "</span></dd><dd><span class='dolzina'><i class='fas fa-road'></i>"+ parseInt(totalDistance) + " км</span><span class='saat'><i class='fas fa-clock'></i> "+ secondsToHms(totalDuration) + "</span></dd></dl>";

      document.getElementById('distance').value = parseInt(totalDistance);
      document.getElementById('est_d_time').value = secondsToHms(totalDuration);

    }

  } else {
    window.alert('Directions request failed due to ' + status);
  }
});
}

function AutocompleteDirectionsHandler(map) {
  this.map = map;
  this.originPlaceId = null;
  this.destinationPlaceId = null;
  this.travelMode = 'DRIVING';
  var originInput = document.getElementById('origin-input');
  var destinationInput = document.getElementById('destination-input');
  var waypointinput1 = document.getElementById('waypoints1');
  var waypointinput2 = document.getElementById('waypoints2');

  var modeSelector = document.getElementById('mode-selector');
  this.directionsService = new google.maps.DirectionsService;
  this.directionsDisplay = new google.maps.DirectionsRenderer;
  this.directionsDisplay.setMap(map);

  var originAutocomplete = new google.maps.places.Autocomplete(
      originInput, {fields: ['place_id']});
  var destinationAutocomplete = new google.maps.places.Autocomplete(
      destinationInput, {fields: ['place_id']});
  var waypointinputcomplete1 = new google.maps.places.Autocomplete(
      waypointinput1, {fields: ['place_id']});
  var waypointinputcomplete2 = new google.maps.places.Autocomplete(
      waypointinput2, {fields: ['place_id']});

          this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
          this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');
          this.setupPlaceChangedListener(waypointinputcomplete1, 'WAYP1');
          this.setupPlaceChangedListener(waypointinputcomplete2, 'WAYP2');
        }

        AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
          var me = this;
          autocomplete.bindTo('bounds', this.map);

          autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();

            var originInput = document.getElementById('origin-input').value;
            var destinationInput = document.getElementById('destination-input').value;

            if (!place.place_id) {
              window.alert("Те молам одбери некоја од понудените опции.");
              return;
            }
            if (mode === 'ORIG') {
              me.originPlaceId = place.place_id;

              if(destinationInput != ''){
                document.getElementById('get_directions').click();

              }    }else if(mode === 'DEST') {
                me.destinationPlaceId = place.place_id;

                if(originInput != ''){
                  document.getElementById('get_directions').click();
                }
              }else if(mode === 'WAYP1'){

                if(originInput != '' && destinationInput != ''){
                  document.getElementById('get_directions').click();
                }
              }else{

                if(originInput != '' && destinationInput != ''){
                  document.getElementById('get_directions').click();
                  $("#origin-input").removeClass("alert alert-danger");
                  $("#destination-input").removeClass("alert alert-danger");
                }
              }

              me.route();
            });

          };

          AutocompleteDirectionsHandler.prototype.route = function() {
            if (!this.originPlaceId || !this.destinationPlaceId) {
              return;
            }
            var me = this;
          };

          $("#waypoints1").focusout(function(){
            if(this.value == ''){
              originI = document.getElementById('origin-input').value;
              destI = document.getElementById('destination-input').value;

              if(originI != '' && destI != ''){
                document.getElementById('get_directions').click();
              }else{
                initMap();
              }
            }
          });

          $("#waypoints2").focusout(function(){
            originI = document.getElementById('origin-input').value;
            destI = document.getElementById('destination-input').value;
            if(this.value == ''){

              $("#waypoints2").addClass("hide");
              $("#addstop").removeClass("hide");
              $("#waypoints1inputgroup").addClass("input-group");

              if(originI != '' && destI != ''){
                document.getElementById('get_directions').click();
              }else{
                initMap();
              }

            }
          });

          $("#destination-input").focusout(function(){
            originI = document.getElementById('origin-input').value;

            if(this.value == '' && originI != ''){
              initMap();
              var summaryPanel = document.getElementById('directions-panel');
              summaryPanel.innerHTML = '';
            }
          });

          $("#origin-input").focusout(function(){
            destI = document.getElementById('destination-input').value;

            if(this.value == '' && destI != ''){
              initMap();
              var summaryPanel = document.getElementById('directions-panel');
              summaryPanel.innerHTML = '';
            }
          });

          function secondsToHms(d) {
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var s = Math.floor(d % 3600 % 60);

            var hDisplay = h > 0 ? h + (h == 1 ? " час и " : " часа и ") : "";
            var mDisplay = m > 0 ? m + (m == 1 ? " минута " : " минути ") : "";

            return hDisplay + mDisplay;
          }

          function timeToSeconds(time) {
            time = time.split(/:/);
            return time[0] * 3600 + time[1] * 60;
          }

          function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
          }


          //Multistep form
          //jQuery time
          var current_fs, next_fs, previous_fs; //fieldsets
          var left, opacity, scale; //fieldset properties which we will animate
          var animating; //flag to prevent quick multi-click glitches

          $(".next").click(function(){

            ClickedElementID = this.id;
            var error1 = error2 = false;

            //CHECK FOR EMPTY INPUTS
            if(ClickedElementID == 'next1'){

              if($('#origin-input').val() == ''){
                error1 = true;
                $("#origin-input-hb").remove();
                $("#origin-input-fg").addClass("has-error");
                $("#origin-input").after("<span class='help-block' id='origin-input-hb'>Полето е задолжително</span>");
              }else{
                $("#origin-input-hb").remove();
                $("#origin-input-fg").removeClass("has-error");
              }
              if($('#destination-input').val() == ''){
                error1 = true;
                $("#destination-input-hb").remove();
                $("#destination-input-fg").addClass("has-error");
                $("#destination-input").after("<span class='help-block' id='destination-input-hb'>Полето е задолжително</span>");
              }else{
                $("#destination-input-hb").remove();
                $("#destination-input-fg").removeClass("has-error");
              }
              if($('#datepicker1').val() == ''){
                error1 = true;
                $("#datepicker1-hb").remove();
                $("#datepicker1-fg").addClass("has-error");
                $("#datepicker1-ig").after("<span class='help-block' id='datepicker1-hb'>Полето е задолжително</span>");
              }else{
                $("#datepicker1-hb").remove();
                $("#datepicker1-fg").removeClass("has-error");
              }
              if($('#clockpicker1I').val() == ''){
                error1 = true;
                $("#clockpicker1I-hb").remove();
                $("#clockpicker1I-fg").addClass("has-error");
                $("#clockpicker1I-ig").after("<span class='help-block' id='clockpicker1I-hb'>Полето е задолжително</span>");
              }else{
                var clockpicker1 = $('#clockpicker1I').val();
                var d_stop1_value = Number(document.getElementById('d_stop1_value').value);

                var h = Math.floor(d_stop1_value / 3600);
                var m = Math.floor(d_stop1_value % 3600 / 60);

                var date = new Date("October 13, 2014 "+clockpicker1);

                if(m > 0){
                  date.setMinutes(date.getMinutes()+m);
                }
                if(h > 0){
                  date.setHours(date.getHours()+h);
                }

                hours = format_two_digits(date.getHours());
                minutes = format_two_digits(date.getMinutes());
                document.getElementById('d_stop1_time').value = hours + ":" + minutes;

                if(document.getElementById('d_stop2_value').value != ''){

                  var d_stop1_time = $('#d_stop1_time').val();
                  var d_stop2_value = Number(document.getElementById('d_stop2_value').value);

                  var h = Math.floor(d_stop2_value / 3600);
                  var m = Math.floor(d_stop2_value % 3600 / 60);

                  var date = new Date("October 13, 2014 "+d_stop1_time);

                  if(m > 0){
                    date.setMinutes(date.getMinutes()+m);
                  }
                  if(h > 0){
                    date.setHours(date.getHours()+h);
                  }

                  hours = format_two_digits(date.getHours());
                  minutes = format_two_digits(date.getMinutes());
                  document.getElementById('d_stop2_time').value = hours + ":" + minutes;

                }

                if(document.getElementById('d_stop3_value').value != ''){

                  var d_stop2_time = $('#d_stop2_time').val();
                  var d_stop3_value = Number(document.getElementById('d_stop3_value').value);

                  var h = Math.floor(d_stop3_value / 3600);
                  var m = Math.floor(d_stop3_value % 3600 / 60);

                  var date = new Date("October 13, 2014 "+d_stop2_time);

                  if(m > 0){
                    date.setMinutes(date.getMinutes()+m);
                  }
                  if(h > 0){
                    date.setHours(date.getHours()+h);
                  }

                  hours = format_two_digits(date.getHours());
                  minutes = format_two_digits(date.getMinutes());
                  document.getElementById('d_stop3_time').value = hours + ":" + minutes;

                }
                $("#clockpicker1I-hb").remove();
                $("#clockpicker1I-fg").removeClass("has-error");
                  if( $('#travel_time').length ) // use this if you are using id to check
                  {
                    $('#start_travel_time').html("<p class='mb-0 text-strong'>Поаѓање:</p><p class='ml-2'><span class='saat'><i class='fas fa-calendar-alt text-primary'></i>" + $("#datepicker1").val() + "</span><br><span class='saat'><i class='fas fa-clock text-primary'></i>" + $("#clockpicker1I").val() + "</span></p>");
                  }else{
                    $("#directions-panel").append("<div class='row' id='travel_time'></div>");
                    $("#travel_time").append("<div class='col-md-6' id='start_travel_time'></div>");

                    $('#start_travel_time').html("<p class='mb-0 text-strong'>Поаѓање:</p><p class='ml-2'><span class='saat'><i class='fas fa-calendar-alt text-primary'></i>" + $("#datepicker1").val() + "</span><br><span class='saat'><i class='fas fa-clock text-primary'></i>" + $("#clockpicker1I").val() + "</span></p>");
                  }
              }

              if($('#roundtrip').is(':checked')){
                if($('#datepicker2').val() == ''){
                  error2 = true;
                  $("#datepicker2-hb").remove();
                  $("#datepicker2-fg").addClass("has-error");
                  $("#datepicker2-ig").after("<span class='help-block' id='datepicker2-hb'>Полето е задолжително</span>");
                }else{
                  $("#datepicker2-hb").remove();
                  $("#datepicker2-fg").removeClass("has-error");
                }
                if($('#clockpicker2I').val() == ''){
                  error2 = true;
                  $("#clockpicker2I-hb").remove();
                  $("#clockpicker2I-fg").addClass("has-error");
                  $("#clockpicker2I-ig").after("<span class='help-block' id='clockpicker2I-hb'>Полето е задолжително</span>");
                }else{
                  $("#clockpicker2I-hb").remove();
                  $("#clockpicker2I-fg").removeClass("has-error");

                  if( $('#travel_time').length ){
                    $("#travel_time").append("<div class='col-md-6' id='ret_travel_time'></div>");
                    $('#ret_travel_time').html("<p class='mb-0 text-strong'>Враќање:</p><p class='ml-2'><span class='saat'><i class='fas fa-calendar-alt text-primary'></i>" + $("#datepicker2").val() + "</span><br><span class='saat'><i class='fas fa-clock text-primary'></i>" + $("#clockpicker2I").val() + "</span></p>");
                  }
                }
              }
            }

            if(error1 == false && error2 == false){

              if(animating) return false;
              animating = true;

              current_fs = $(this).parent();
              next_fs = $(this).parent().next();

              //activate next step on progressbar using the index of next_fs
              $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
              //show the next fieldset
              next_fs.show();
              //hide the current fieldset with style
              current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                  //as the opacity of current_fs reduces to 0 - stored in "now"
                  //1. scale current_fs down to 80%
                  scale = 1 - (1 - now) * 0.2;
                  //2. bring next_fs from the right(50%)
                  left = (now * 50)+"%";
                  //3. increase opacity of next_fs to 1 as it moves in
                  opacity = 1 - now;
                  current_fs.css({
                    'transform': 'scale('+scale+')',
                    'position': 'absolute'
                  });
                  next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 800,
                complete: function(){
                  current_fs.hide();
                  animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
              });

            }

          });

          $(".previous").click(function(){
            if(animating) return false;
            animating = true;

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            //de-activate current step on progressbar
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
              step: function(now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1-now) * 50)+"%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
              },
              duration: 800,
              complete: function(){
                current_fs.hide();
                animating = false;
              },
              //this comes from the custom easing plugin
              easing: 'easeInOutBack'
            });
          });

          $('#free_places').on('keydown keyup', function(e){
            if ($(this).val() > 6
            && e.keyCode !== 46 // keycode for delete
            && e.keyCode !== 8 // keycode for backspace
          ) {
            e.preventDefault();
            $(this).val(3);
          }

          if ($(this).val() <= 0
          && e.keyCode !== 46 // keycode for delete
          && e.keyCode !== 8 // keycode for backspace
        ) {
          e.preventDefault();
          $(this).val(3);
        }

        if(event.keyCode == 69){
          e.preventDefault();
        }
      });

      $(".submit").click(function(event){
        if(!$('#term_conditions').is(':checked')){
          $("#term_conditions-fg").addClass("has-error");
          event.preventDefault();
        }else{
          $("#term_conditions-fg").removeClass("has-error");
        }
        if($('#costShare1').val() == ''){
          $("#costShare1-hb").remove();
          $("#costShare1-fg").addClass("has-error");
          $("#costShare1").after("<span class='help-block' id='costShare1-hb'>Полето е задолжително</span>");
          event.preventDefault();
        }else{
          $("#costShare1-hb").remove();
          $("#costShare1-fg").removeClass("has-error");
        }
        if($('#costShare2').val() == ''){
          $("#costShare2-hb").remove();
          $("#costShare2-fg").addClass("has-error");
          $("#costShare2").after("<span class='help-block' id='costShare2-hb'>Полето е задолжително</span>");
          event.preventDefault();
        }else{
          $("#costShare2-hb").remove();
          $("#costShare2-fg").removeClass("has-error");
        }
        if($('#costShare3').val() == ''){
          $("#costShare3-hb").remove();
          $("#costShare3-fg").addClass("has-error");
          $("#costShare3").after("<span class='help-block' id='costShare3-hb'>Полето е задолжително</span>");
          event.preventDefault();
        }else{
          $("#costShare3-hb").remove();
          $("#costShare3-fg").removeClass("has-error");
        }
        if($('#costShare4').val() == ''){
          $("#costShare4-hb").remove();
          $("#costShare4-fg").addClass("has-error");
          $("#costShare4").after("<span class='help-block' id='costShare4-hb'>Полето е задолжително</span>");
          event.preventDefault();
        }else{
          $("#costShare4-hb").remove();
          $("#costShare4-fg").removeClass("has-error");
        }
        if($('#costShare5').val() == ''){
          $("#costShare5-hb").remove();
          $("#costShare5-fg").addClass("has-error");
          $("#costShare5").after("<span class='help-block' id='costShare5-hb'>Полето е задолжително</span>");
          event.preventDefault();
        }else{
          $("#costShare5-hb").remove();
          $("#costShare5-fg").removeClass("has-error");
        }
        if($('#costShare6').val() == ''){
          $("#costShare6-hb").remove();
          $("#costShare6-fg").addClass("has-error");
          $("#costShare6").after("<span class='help-block' id='costShare6-hb'>Полето е задолжително</span>");
          event.preventDefault();
        }else{
          $("#costShare6-hb").remove();
          $("#costShare6-fg").removeClass("has-error");
        }
      });

      $(document).change(function () {
      function isEllipsisActive(element) {
        return element.offsetWidth < element.scrollWidth;
      }
      Array.from(document.querySelectorAll('.preseci'))
        .forEach(element => {
          if (isEllipsisActive(element)) {
            console.log('dodava title - loadire tooltip');
            // element.title = element.innerText;
            var title = element.title;
            if (element.title == '') {
              element.title = element.innerText;
            } else {
              console.log('ima title');
            }
            $(function () {
              $('[data-toggle="tooltip"]').tooltip()
            })
          }
        });
      });
    </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuvTR6aAp_y_rcc_w1m087uc9jh84V3Qk&libraries=places&callback=initMap&language=mk&region=mk"></script>
@endsection
