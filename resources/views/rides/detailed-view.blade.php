@extends('layouts.app')
@section('content')
<div class="py-5 py-xs-0">
<section>
  <div class="container">
    <div class="row row-eq-height">
      @if(!empty($cur_booked_info))
      <div class="col-xs-12 col-md-4">
        <div class="alert alert-status alert-rezervacija-{{$cur_booked_info->status}}">
          <div class="tekst">
            <p>Твојата резервација е</p>
            <p class="lead text-strong">{{Helper::Status_Message($cur_booked_info->status)}}</p>
            @if(!empty($cur_booked_info) && $cur_booked_info->status == 'Accepted')
              <a class="btn btn-link scroll" href="#kontaktinfo">Контакт <i class="fas fa-caret-down"></i></a>
            @endif
          </div>
        </div>
      </div>
      @endif
      <div class="col-xs-12 col-md-4">
        <div class="alert alert-status alert-status-{{$ride->status}}">
          <div class="tekst">
            <p>Патувањето е</p>
            <p class="lead text-strong">{{Helper::Ride_Status($ride->status)}}</p>
            @if(!empty($cur_booked_info) && $cur_booked_info->status == 'Accepted' && $ride->status == 'completed')
            <a class="btn btn-link scroll" href="#komentar">Дади оценка <i class="fas fa-caret-down"></i></a>
            @endif
          </div>
        </div>
      </div>
      @if($ride->status == 'in_progress')
        <div class="col-xs-12 col-md-4 @if(empty($cur_booked_info)) col-md-offset-4 col-xs-offset-0 @endif text-right col-share">
          <div class="addthis_inline_share_toolbox_vbrd"></div>
        </div>
      @endif
    </div>
    <div class="row row-eq-height">
      <div class="col-md-6 pr-0 col-xs-12 pr-xs-15">
        <div class="col-md-12 bg-white pb-2">
          <div id="right-panel">
            <input id="origin-input" name="d_from" class="controls form-control" type="hidden" value="{{$ride->d_from}}">
            <input id="destination-input" name="d_to" class="controls form-control" type="hidden" value="{{$ride->d_to}}">
            <input id="waypoints1" name="d_stop_1" class="controls waypoints form-control" type="hidden" value="{{$ride->d_stop_1}}">
            <input id="waypoints2" name="d_stop_2" class="controls waypoints form-control" type="hidden" value="{{$ride->d_stop_2}}">
            <button type="button" id="get_directions" style="height: 1px; width:1px; visibility: hidden;"></button>
          </div>
          <div class="row mb-2">
            @if($ride->roundtrip == 1)
              <div class="col-md-2 hidden-xs">
                <span class="fa-stack fa-1x ml-1" data-toggle="tooltip" data-placement="top" title="Се враќам назад">
                  <i class="fas fa-circle fa-stack-2x text-secondary"></i>
                  <i class="fas fa-exchange-alt fa-stack-1x text-primary"></i>
                </span>
              </div>
            @endif
              <div class="col-xs-6 col-md-5">
                <p>Поаѓање:</p>
                <p class="lead mb-xs-0"><i class="fas fa-calendar-alt text-primary"></i> {{ Carbon\Carbon::parse($ride->d_date)->format('d.m.Y') }}</p>
                <p class="lead mb-xs-0"><i class="fas fa-clock text-primary"></i> {{$ride->d_time}}</p>
              </div>
            @if($ride->roundtrip == 1)
              <div class="col-xs-6 col-md-5">
                <p>Враќање</p>
                <p class="lead mb-xs-0"><i class="fas fa-calendar-alt text-primary"></i> {{ Carbon\Carbon::parse($ride->d_rdate)->format('d.m.Y') }}</p>
                <p class="lead mb-xs-0"><i class="fas fa-clock text-primary"></i> {{$ride->d_rtime}}</p>
              </div>
            @endif
          </div>
          <div class="destinacii mb-3">
            <div class="bulet">
              <div class="roundChecked"></div>
              @if($ride->d_stop_1 != '')
                <div class="lineChecked"></div>
                <div class="roundUnChecked"></div>
              @endif
              @if($ride->d_stop_2 != '')
                <div class="lineUnChecked"></div>
                <div class="roundUnChecked"></div>
              @endif
                <div class="lineChecked"></div>
                <div class="roundChecked"></div>
            </div>
            <div class="destinacija destinacija-pocetna">
              <div class="mx-1"></div>
              <div class="col-xs-9 center">
                <p class="lead m-0 preseci" data-toggle="tooltip" data-placement="top">{{ $ride->d_from }}</p>
              </div>
              <div class="col-xs-3 p-0">
                <p class="saat m-0 text-right"><i class="fas fa-clock text-primary"></i> ~ {{$ride->d_time}}</p>
              </div>
            </div>
            @if($ride->d_stop_1 != '')
              <div class="destinacija destinacija-prva">
                <div class="mx-1"></div>
                <div class="col-xs-9 center">
                  <p class="m-0 preseci" data-toggle="tooltip" data-placement="top">{{ $ride->d_stop_1 }}</p>
                </div>
                <div class="col-xs-3 p-0">
                  <p class="saat m-0 text-right"><i class="fas fa-clock text-primary"></i> ~ {{$ride->d_stop1_time}}</p>
                </div>
              </div>
            @endif
            @if($ride->d_stop_2 != '')
            <div class="destinacija destinacija-vtora">
                <div class="mx-1"></div>
                <div class="col-xs-9 center">
                  <p class="m-0 preseci" data-toggle="tooltip" data-placement="top">{{ $ride->d_stop_2 }}</p>
                </div>
                <div class="col-xs-3 p-0">
                  <p class="saat m-0 text-right"><i class="fas fa-clock text-primary"></i> ~ {{$ride->d_stop2_time}}</p>
                </div>
              </div>
            @endif
            <div class="destinacija destinacija-krajna">
              <div class="mx-1"></div>
              <div class="col-xs-9 center">
                <p class="lead m-0 preseci" data-toggle="tooltip" data-placement="top">{{ $ride->d_to }}</p>
              </div>
              <div class="col-xs-3 p-0">
                <p class="saat m-0 text-right"><i class="fas fa-clock text-primary"></i> @if($ride->d_stop_1 != '' && $ride->d_stop_2 == '') ~ {{$ride->d_stop2_time}} @elseif($ride->d_stop_1 != '' && $ride->d_stop_2 != '') ~ {{$ride->d_stop3_time}} @else ~ {{$ride->d_stop1_time}} @endif</p>
              </div>
            </div>
          </div>

          @if($ride->status == 'in_progress')
            <p class="text-strong text-center">{!! Helper::free_seats_label($ride->r_qty_places) !!}</p>
          @endif

          @if($currentuserid != $ride->user_id)

            @if( $ride->r_qty_places > 0 || $chk_booked == 1 )

              @if($chk_booked == 1 && $mode != 'edit')

                <form name="cancel" id='cancel_ride' action="{{action('RidesController@book_ride')}}" method="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="ride_id" value="{{$ride->id}}">
                  <input type="hidden" name="mode" value='cancel_ride'>
                    @php $from = $cur_booked_info->b_from; $to = $cur_booked_info->b_to; $price = $cur_booked_info->b_cost; @endphp
                    <p class="text-strong">Твојата резервација:</p>
                    <div class="panel panel-info panel-rezervacija">
                      <div class="panel-heading">
                        <span class="lead mesto preseci" data-toggle="tooltip" data-placement="top"><i class="fas fa-map-marker-alt text-success"></i> {{$ride->$from}}</span>
                        <span class="lead mesto preseci" data-toggle="tooltip" data-placement="top"><i class="fas fa-map-marker-alt text-danger"></i> {{$ride->$to}}</span>
                      </div>
                      <div class="panel-body">
                        <p class="text-strong">Број на резервирани седишта: {{$cur_booked_info->qty_places}}</p>
                        <p class="text-strong">Цена по седиште: <span class="cena lead">{{$ride->$price}} мкд</span></p>
                        @if(in_array('Change',$ride_permissions))
                          <a href="{{url()->current()}}?mode=edit" class="btn btn-primary btn-icon-split mr-1">
                            <span class="icon text-white-50"><i class="fas fa-edit"></i></span>
                            <span class="text">Промени Резервација</span>
                          </a>
                        @endif
                        @if(in_array('Cancel',$ride_permissions))
                        <a herf='' data-toggle="modal" data-target="#cancel_prompt" class="btn btn-danger btn-icon-split">
                          <span class="icon text-white-50"><i class="fas fa-ban"></i></span>
                          <span class="text">Откажи Резервација</span>
                        </a>
                        @endif
                    </div>
                  </div>
                </form>

              @elseif(!empty($cur_booked_info) && $cur_booked_info->status == 'Rejected' && in_array('Change',$ride_permissions))

                <a href="{{url()->current()}}?mode=requested_new" class="btn btn-primary btn-block" id="">Направи нова резервација</a>

              @else

                @if($mode != 'edit')
                  <button class="btn btn-primary btn-block btn-lg" type="button" data-toggle="collapse" data-target="#rezerviraj-forma" aria-expanded="false" aria-controls="rezerviraj-forma">Резервирај <i class="fas fa-chevron-down"></i></button>
                @endif

              @endif

            @else

              <p>Нема слободни места</p>

            @endif

          @else

            <a href="/rides/{{$ride->id}}" class="btn btn-primary btn-block btn-lg">Види ги резервациите или измени го патвањето <i class="fas fa-angle-double-right"></i></a>

          @endif

            @if(!empty($cur_booked_info) && $cur_booked_info->status == 'Accepted')
              <div id="kontaktinfo">
                <p class="text-strong">Контакт информации:</p>
                <div class="panel panel-info">
                  <div class="panel-body">
                    <p class="lead mb-1">{{$user->firstname}} {{$user->lastname}}</p>
                    <p class="lead mb-0"><i class="fas fa-phone text-primary mr-1"></i> <a href="tel:{{$user->phone}}">{{$user->phone}}</a></p>
                    <p class="lead mb-0"><i class="fas fa-envelope text-primary mr-1"></i> <a href="mailto:{{$user->email}}" target="_blank">{{$user->email}}</a></p>
                  </div>
                </div>
              </div>
            @endif
        </div>
      </div>

      <div class="col-md-6 pl-0 col-xs-12 pl-xs-15">
        <div class="col-md-12 bg-white h-100 pr-0 pl-xs-0">
          <div id="map" class="h-100"></div>
        </div>
      </div>

    </div>

        @if($currentuserid != $ride->user_id)
          @if( $ride->r_qty_places > 0 || $chk_booked == 1 )
            @if(!empty($cur_booked_info) && $cur_booked_info->status == 'Rejected' && in_array('Change',$ride_permissions))
            @else
              @if(in_array('Change',$ride_permissions))
              <div class="row">
                <div class="col-md-12">
                  <div id="rezerviraj-forma" @if($mode == 'edit') @else class="collapse" @endif>
                    <div class="col-md-12 bg-white py-3">
                      <form action="{{action('RidesController@book_ride')}}" method="POST" id="resevation_form">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="btn-group destinations" data-toggle="buttons">

                              @if($ride->d_stop_1 == '' && $ride->d_stop_2 == '')
                              <label class="btn btn-default">
                                <input type="radio" name="options" value="d_from - d_to - d_cost1 - d_time">
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-success"></i> {{ Helper::Short_Destination($ride->d_from)}}
                                </span>
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-danger"></i> {{ Helper::Short_Destination($ride->d_to)}}
                                </span>
                                <span class="cena lead">{{$ride->d_cost1}} мкд</span>
                              </label>
                              @endif

                              @if($ride->d_stop_1 != '')
                              <label class="btn btn-default">
                                <input type="radio" name="options" value="d_from - d_stop_1 - d_cost1 - d_time">
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-success"></i> {{ Helper::Short_Destination($ride->d_from)}}
                                </span>
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-danger"></i> {{ Helper::Short_Destination($ride->d_stop_1)}}
                                </span>
                                <span class="cena lead">{{$ride->d_cost1}} мкд</span>
                              </label>
                              @endif

                              @if($ride->d_stop_2 != '')
                              <label class="btn btn-default">
                                <input type="radio" name="options" value="d_stop_1 - d_stop_2 - d_cost2 - d_stop1_time">
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-success"></i> {{ Helper::Short_Destination($ride->d_stop_1)}}
                                </span>
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-danger"></i> {{ Helper::Short_Destination($ride->d_stop_2)}}
                                </span>
                                <span class="cena lead">{{$ride->d_cost2}} мкд</span>
                              </label>

                              <label class="btn btn-default">
                                <input type="radio" name="options" value="d_stop_2 - d_to - d_cost3 - d_stop2_time">
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-success"></i> {{ Helper::Short_Destination($ride->d_stop_2)}}
                                </span>
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-danger"></i> {{ Helper::Short_Destination($ride->d_to)}}
                                </span>
                                <span class="cena lead">{{$ride->d_cost3}} мкд</span>
                              </label>
                              @endif

                              @if($ride->d_stop_1 != '' && $ride->d_stop_2 == '')
                              <label class="btn btn-default">
                                <input type="radio" name="options" value="d_stop_1 - d_to - d_cost2 - d_stop1_time">
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_stop_1)}}
                                </span>
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_to)}}
                                </span>
                                <span class="cena lead">{{$ride->d_cost2}} мкд</span>
                              </label>

                              <label class="btn btn-default">
                                <input type="radio" name="options" value="d_from - d_to - d_cost3 - d_time">
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_from)}}
                                </span>
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_to)}}
                                </span>
                                <span class="cena lead">{{$ride->d_cost3}} мкд</span>
                              </label>
                              @endif

                              @if($ride->d_stop_1 != '' && $ride->d_stop_2 != '')

                              <label class="btn btn-default">
                                <input type="radio" name="options" value="d_from - d_stop_2 - d_cost4 - d_time">
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_from)}}
                                </span>
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_stop_2)}}
                                </span>
                                <span class="cena lead">{{$ride->d_cost4}} мкд</span>
                              </label>

                              <label class="btn btn-default">
                                <input type="radio" name="options" value="d_stop_1 - d_to - d_cost5 - d_stop1_time">
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_stop_1)}}
                                </span>
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_to)}}
                                </span>
                                <span class="cena lead">{{$ride->d_cost5}} мкд</span>
                              </label>

                              <label class="btn btn-default">
                                <input type="radio" name="options" value="d_from - d_to - d_cost6 - d_time">
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_from)}}
                                </span>
                                <span class="lead mesto">
                                  <i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_to)}}
                                </span>
                                <span class="cena lead">{{$ride->d_cost6}} мкд</span>
                              </label>

                              @endif

                            </div>
                            <p id="option_err" class="lead"></p>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              @if($chk_booked == 1)
                              <label for="qty_places">Избери број на седишта за резервирање</label>
                              <select name="qty_places" id="qty_places" class="form-control">
                                @for($i = 1; $i <= $ride->free_places - $booked_places; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                              </select>
                              @else
                              <label for="qty_places">Избери број на седишта за резервирање</label>
                              <select name="qty_places" id="qty_places" class="form-control">
                                @for($i = 1; $i <= $ride->r_qty_places; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                              </select>
                              @endif
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group" id='phone-input-fg'>
                              @if(!empty($current_user_phone))
                              <label for="phone">Телефон за контакт. Смени доколку не е истиот</label>
                              @else
                              <label for="phone">Телефон за контакт</label>
                              @endif
                              <input type="text" name="phone" id="phone" class="form-control" @if(!empty($current_user_phone)) value="{{$current_user_phone}}" @endif>
                              @if ($errors->has('phone'))
                                <span class="help-block">
                                  {{ $errors->first('phone') }}
                                </span>
                              @endif
                            </div>
                          </div>
                          @if($chk_booked == 1)
                          <div class="col-md-12">
                            <input type="submit" name="" id="" class="btn btn-primary btn-lg" value="Зачувај промена">
                            <a href="/rides/detailed-view/{{$ride->id}}" class="btn btn-lg btn-danger" id="">Откажи промена</a>
                            <input type="hidden" name="mode" value='edit'>
                          </div>
                          @else
                          <div class="col-md-12">
                            <input type="submit" name="" class="btn btn-primary btn-lg btn-block" value="Резервирај седиште">
                          </div>
                          @endif
                        </div>
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="hidden" name="ride_id" value="{{$ride->id}}">
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            @endif
          @endif
        @endif
      @endif
  </div>
</section>

<section class="zasofero mt-5 mt-xs-2">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-info">
          <div class="tekst">
            <p><strong>Повеќе информации:</strong><br>
              @if($ride->d_description != '')
              {{$ride->d_description}}
              @else нема дополнителни информации
              @endif
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-md-4 detali">
        <div class="panel panel-primary @if($ride->back_s_guarantee == 0) panel-profil mt-xs-0 @endif">
          @if($ride->back_s_guarantee != 0)
            <div class="panel-heading">
              <div class="media">
                <div class="media-left media-middle">
                  <i class="fas fa-check-double fa-3x text-white"></i>
                </div>
                <div class="media-body media-middle">
                  <p>Гарантирам дека најмногу двајца ќе седат на задното седиште</p>
                </div>
              </div>
            </div>
          @endif
              <div class="panel-body">
                <div class="media">
      						<div class="media-left media-middle">
      							{!! Helper::pref_talkative($user->talkative)[0] !!}
      						</div>
      						<div class="media-body media-middle">
      							{{ Helper::pref_talkative($user->talkative)[1] }}
      						</div>
      					</div>
      					<div class="media">
      						<div class="media-left media-middle">
      							{!! Helper::pref_smoker($user->smoker)[0] !!}
      						</div>
      						<div class="media-body media-middle">
      							{{ Helper::pref_smoker($user->smoker)[1] }}
      						</div>
      					</div>
      					<div class="media">
      						<div class="media-left media-middle">
      							{!! Helper::pref_pets($user->pets)[0] !!}
      						</div>
      						<div class="media-body media-middle">
      							{{ Helper::pref_pets($user->pets)[1] }}
      						</div>
      					</div>
      					<div class="media">
      						<div class="media-left media-middle">
      							{!! Helper::pref_music($user->music)[0] !!}
      						</div>
      						<div class="media-body media-middle">
      							{{ Helper::pref_music($user->music)[1] }}
      						</div>
      					</div>
              </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-4 kola">
          <div class="panel panel-primary panel-profil">
            @if(!empty($user->car_photo))<img class="slika" src="{{asset('storage/upload/cars/'.$user->car_photo)}}" alt="{{$user->car_brand}} {{$user->car_model}}">
            @else<img class="slika" src="/storage/upload/web/nocar.jpg" alt="Nema slika">
            @endif
            <div class="panel-body text-center">
              @if(!empty($user->car_brand))
                <p class="lead ime mb-1">{{$user->car_brand}} {{$user->car_model}}</p>
                <p class="small">Година {{$user->year_production}}</p>
                <p class="">@if(!empty($user->color))Боја: {{$user->color}} @endif </p>
              @else <p class="lead ime text-danger">Нема податоци за автомобилот</p>
              @endif
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-4 sofer">
          <div class="panel panel-primary panel-profil">
            @if(!empty($user->avatar))<img class="slika" src="{{asset('storage/upload/users/'.$user->avatar)}}" alt="{{$user->firstname}} {{$user->lastname}}">
            @else<img class="slika" src="/storage/upload/web/nouser.jpg" alt="Nema slika">
            @endif
            <div class="panel-body text-center">
              <p class="lead ime mb-1"><a href="/rides/user_profile/{{$ride->user_id}}">{{$user->firstname}} {{$user->lastname}}</a></p>
              <p class="small">Член од {{ Carbon\Carbon::parse($user->created_at)->format('m.Y') }}</p>
              @if(!empty($driver_rating))
                <div class="rejting text-center mb-1">
                  <div class='driver_rating'></div>
                  <span class="text-primary text-strong">{{$driver_rating}}</span>
                </div>
              @else
                <p>Корисникот не е оценет</p>
              @endif
              <a href="/rides/user_profile/{{$ride->user_id}}" class="text-strong pull-right mr-1">Повеќе <i class="fas fa-angle-double-right"></i></a>
            </div>
          </div>
        </div>
    </div>
  </div>
</section>
@if(!empty($cur_booked_info) && $cur_booked_info->status == 'Accepted' && $ride->status == 'completed')
<section id="komentar">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h3><i class="fas fa-comments"></i> Коментари</h3>
          </div>
          <div class="panel-body">
            @if( isset($review->rating))
              <h3 class="mt-0 text-primary">Твоето мислење за ова возење</h3>
              <div class="row" id='comment_block'>
                <div class="col-md-12" >
                  <div class='starrr'></div>
                  <div class="panel panel-default mt-1">
                    <div class="panel-body">
                      <p class="mb-0" id='comment_paragraph'>{{$review->comment}}</p>
                    </div>
                  </div>
                  <a class="btn btn-link" onclick="show_edit_comment()"><i class="fas fa-edit"></i> Измени</a>
                </div>
              </div>
            @endif
            @include('reviews.create_comment')
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endif
<div class="modal fade" tabindex="-1" role="dialog" id="cancel_prompt">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p class="modal-title">Дали сте сигурни дека сакате да ја откажете резервацијата?</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Не</button>
        <button type="button" id='cancel_confirm' class="btn btn-primary">Да</button>
      </div>
    </div>
  </div>
</div>
</div>

@endsection

  @section('includesscripts')
  <script src="{{ asset('js/starrr.js') }}"></script>

  <script>
    var old_riderating = $('#old_riderating');
    var rating = $('#ride_rating');

    if(old_riderating.val() != ''){
      var rideRating_I = old_riderating;
      $('#ride_rating').val(old_riderating.val());
    }else{
      var rideRating_I = rating;
    }

  $('.starrr').starrr({
    rating: rideRating_I.val(),
      change: function(e, value){
      rating.val(value).trigger('input');
    }
  });
  var driver_rating = "<?php echo $driver_rating; ?>";
  if(driver_rating != ''){
    $('.driver_rating').starrr({rating: driver_rating, readOnly: true});
  }

  if(document.getElementById('comment_paragraph')){
    var comment_text = document.getElementById('comment_paragraph').innerHTML;

    if(comment_text.length > 0 || $rating.val() > 0){
      document.getElementById('comment_form').setAttribute("style", "display:none;");
    }
  }

  function show_edit_comment(){
    document.getElementById('comment_form').setAttribute("style", "display:block;");
    document.getElementById('comment_block').setAttribute("style", "display:none;");
  }

  function hide_edit_comment(){
    document.getElementById('comment_form').setAttribute("style", "display:none;");
    document.getElementById('comment_block').setAttribute("style", "display:block;");
  }

  $("#resevation_form").on("submit", function(){
    var ride_options = $("input[name='options']:checked").length;
    if(ride_options == 0){
      $('#option_err').addClass("alert alert-danger");
      $("#option_err").text('Те молам избери од понудените дестинации');
      return false;
    }else{
      $('#option_err').removeClass("alert alert-danger");
      $("#option_err").text('');
    }

    if($('#phone').val() == ''){
      $("#phone-input-hb").remove();
      $("#phone-input-fg").addClass("has-error");
      $("#phone").after("<span class='help-block' id='phone-input-hb'>Телефонски број е задолжителен</span>");
      return false;
    }

  })

  $(function() {
    $('#get_directions').click();
  });

  function initMap() {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: {lat: 41.71556, lng: 21.77556},
      streetViewControl: false,
      fullscreenControl: false
    });
    directionsDisplay.setMap(map);

    document.getElementById('get_directions').addEventListener('click', function() {
      calculateAndDisplayRoute(directionsService, directionsDisplay);
    });
    new AutocompleteDirectionsHandler(map);
  }

  function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    var waypts = [];
    var checkboxArray1 = document.getElementById('waypoints1');
    var checkboxArray2 = document.getElementById('waypoints2');

    if(checkboxArray1.value != ""){
      waypts.push({
        location: checkboxArray1.value,
        stopover: true
      });
    }

    if(checkboxArray2.value != ""){
      waypts.push({
        location: checkboxArray2.value,
        stopover: true
      });
    }

    directionsService.route({
      origin: document.getElementById('origin-input').value,
      destination: document.getElementById('destination-input').value,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
        var route = response.routes[0];

      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  function AutocompleteDirectionsHandler(map) {
    this.map = map;
    this.originPlaceId = null;
    this.destinationPlaceId = null;
    this.travelMode = 'DRIVING';
    var originInput = document.getElementById('origin-input');
    var destinationInput = document.getElementById('destination-input');
    var waypointinput1 = document.getElementById('waypoints1');
    var waypointinput2 = document.getElementById('waypoints2');

    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.directionsDisplay.setMap(map);

    var originAutocomplete = new google.maps.places.Autocomplete(
      originInput, {fields: ['place_id']});
    var destinationAutocomplete = new google.maps.places.Autocomplete(
      destinationInput, {fields: ['place_id']});
    var waypointinputcomplete1 = new google.maps.places.Autocomplete(
      waypointinput1, {fields: ['place_id']});
    var waypointinputcomplete2 = new google.maps.places.Autocomplete(
      waypointinput2, {fields: ['place_id']});
  }

  AutocompleteDirectionsHandler.prototype.route = function() {
    if (!this.originPlaceId || !this.destinationPlaceId) {
      return;
    }
    var me = this;
  };

  //Cancel Reservation
  var form = document.getElementById("cancel_ride");

  document.getElementById("cancel_confirm").addEventListener("click", function () {
    form.submit();
  });
  $('.scroll').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top -100
        });
      }
    });

  $(function() {
    function isEllipsisActive(element) {
      return element.offsetWidth < element.scrollWidth;
    }
    Array.from(document.querySelectorAll('.preseci'))
    .forEach(element => {
      if (isEllipsisActive(element)) {
        element.title = element.innerText;
        // $('[data-toggle="tooltip"]').tooltip()
      }
    });
    $('[data-toggle="tooltip"]').tooltip()
  });

  </script>
  @if ($errors->has('ride_rating') || $errors->has('user_comment'))
    <script>
    $(document).ready(function () {
      $('html, body').animate({
        scrollTop: $('#komentar').offset().top -100
      });
    });
    </script>
  @endif
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c9c87d72b4ed697"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuvTR6aAp_y_rcc_w1m087uc9jh84V3Qk&libraries=places&callback=initMap&language=mk&region=mk"></script>
@endsection
