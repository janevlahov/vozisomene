@extends('layouts.app')
@section('content')
<div class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-md-2">
        <a href="{{ url('/rides') }}" class="btn btn-default mb-2"><i class="fas fa-angle-double-left"></i> Назад</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="alert alert-status alert-status-{{$ride->status}} mb-0">
          <div class="tekst">
            <p>Статус на патувањето:</p>
            <p class="lead text-strong">{{Helper::Ride_Status($ride->status)}}</p>
            @if(strtolower($ride->status) == 'in_progress' || strtolower($ride->status) == 'canceled')
              <a class="btn btn-link" role="button" data-toggle="collapse" href="#smenistatus" aria-expanded="false" aria-controls="smenistatus">Измени <i class="fas fa-caret-down"></i></a>
            @endif
          </div>
        </div>
        @if(strtolower($ride->status) == 'in_progress' || strtolower($ride->status) == 'canceled')
          <div class="collapse bg-primary-70 text-white py-1" id="smenistatus">
            <form id='ride_status' class="form-inline" action="{{action('RidesController@update_ride_status')}}" method="POST">
              <div class="form-group">
                <div class="radio radio-primary pl-0">
                  <input type="radio" name="status_options" data-toggle="modal" data-target="#status_prompt" value="in_progress" id='ride_in_progress' @if(strtolower($ride->status) == 'in_progress') checked @endif  @if(in_array('in_progress', $disable_inputs)) disabled @endif>
                  <label for="ride_in_progress">Во тек</label>
                </div>
              </div>
              <div class="form-group">
                <div class="radio radio-danger">
                  <input type="radio" name="status_options" data-toggle="modal" data-target="#status_prompt" value="canceled" id='ride_canceled' @if(strtolower($ride->status) == 'canceled') checked @endif @if(in_array('canceled', $disable_inputs)) disabled @endif>
                  <label for="ride_canceled">Откажено</label>
                </div>
              </div>
              <div class="form-group">
                <div class="radio radio-success">
                  <input type="radio" name="status_options" data-toggle="modal" data-target="#status_prompt" value="completed" id='ride_completed' @if(strtolower($ride->status) == 'completed') checked @endif @if(in_array('completed', $disable_inputs)) disabled @endif>
                  <label for="ride_completed">Завршено</label>
                </div>
              </div>
              <input type='hidden' name="ride_id" class="" value="{{$ride->id}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
          </div>
        @endif
      </div>
      <div class="col-md-4">
        @if(!$users_booked->isEmpty())
        <div class="alert alert-status alert-rezervacii-ima">
          <div class="tekst">
            <p>Резервации:</p>
            <p>Има пријавени патници</p>
            <a class="btn btn-link scroll" href="#vidirezervacii">Види <i class="fas fa-caret-down"></i></a>
          </div>
        </div>
        @else
        <div class="alert alert-status alert-rezervacii-nema">
          <div class="tekst">
            <p>Резервации:</p>
            <p>Нема пријавени патници</p>
          </div>
        </div>
        @endif
      </div>
      @if(strtolower($ride->status) == 'in_progress')
      <div class="col-md-4">
        <div class="alert alert-status alert-spodeli">
          <div class="tekst">
            <p>Сподели го патување за полесно да биде пронајдено</p>
            <div class="addthis_inline_share_toolbox"></div>
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>

  @if(!$users_booked->isEmpty())
  <section class="pb-5" id="vidirezervacii">
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <h2 class="mt-0">Резервации</h2>
        </div>
      </div>
      <div class="row">
        @foreach($users_booked as $ubook)
        <div class="col-md-4">
          <div class="panel panel-info panel-rezervacija">
            <div class="panel-heading">
              @php $from = $ubook->b_from; $to = $ubook->b_to; $price = $ubook->b_cost; @endphp
              <span class="lead mesto preseci" data-toggle="tooltip" data-placement="top"><i class="fas fa-map-marker-alt text-success"></i> {{$ride->$from }}</span>
              <span class="lead mesto preseci" data-toggle="tooltip" data-placement="top"><i class="fas fa-map-marker-alt text-danger"></i> {{$ride->$to}}</span>
            </div>
            <div class="panel-body">
              {!! Helper::userAvatar($ubook->avatar,$ubook->firstname,$ubook->lastname,$ubook->user_id) !!}
              <p class="text-strong mb-0">Број на седишта за резервирање: {{$ubook->qty_places}}</p>
              <p class="text-strong">Цена по седиште: <span class="cena lead">{{$ride->$price}} мкд</span></p>
              <form class="form-inline book_action" action="{{action('RidesController@book_approval')}}" method="POST">

                @if($ubook->status == 'Accepted')
                <p><i class="fas fa-check-circle text-success fa-2x mr-1"></i> <span class="lead text-success">Резервацијата е прифатена</span></p>
                <p class="text-strong">Контакт информации:</p>
                <p class="mb-0"><i class="fas fa-phone text-primary mr-1"></i> <a href="tel:{{$ubook->phone}}">{{$ubook->phone}}</a></p>
                <p><i class="fas fa-envelope text-primary mr-1"></i> <a href="mailto:{{$ubook->email}}">{{$ubook->email}}</a></p>

                  @if(strtolower($ride->status) != 'completed' && strtolower($ride->status) != 'canceled')
                  <button type="submit" name="modify" class="btn btn-default btn-icon-split">
                    <span class="icon text-white-50"><i class="fas fa-user-edit"></i></span>
                    <span class="text">Измени</span>
                  </button>
                  @endif

                @else
                  @if(strtolower($ride->status) != 'completed' && strtolower($ride->status) != 'canceled')
                    <p><i class="fas fa-user-friends text-secondary fa-2x mr-1"></i> <span class="lead text-secondary">Те молам избери</span></p>
                    <button type="submit" name="accept" class="btn btn-success btn-icon-split">
                      <span class="icon text-white-50"><i class="fas fa-user-check"></i></span>
                      <span class="text">Прифати</span>
                    </button>
                    <button type="submit" name="deny" class="btn btn-danger btn-icon-split">
                      <span class="icon text-white-50"><i class="fas fa-user-times"></i></span>
                      <span class="text">Одбиј</span>
                    </button>
                  @else
                    <p><i class="fas fa-frown-open text-danger fa-2x mr-1"></i> <span class="lead text-danger">Немаш дадено одговор</span></p>
                  @endif

                @endif
                <input type='hidden' name="booking_id" value="{{$ubook->id}}">
                <input type='hidden' name="user_id" value="{{$ubook->user_id}}">
                <input type='hidden' name="ride_id" value="{{$ride->id}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  @endif

  @if(Session::has('success'))
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success text-center">
            <p class="lead text-strong">{{Session::get('success')}}</p>
          </div>
        </div>
      </div>
    </div>
  @endif

    <div class="container">
      <div class="row row-eq-height mt-2">
        <div class="col-md-6 pr-0">
          <div class="col-md-12 bg-white pb-3">
            <form id="msform" action="{!! action('RidesController@update', ['id' => $ride->id]) !!}" method="POST" >
              <h1>Измени Патување</h1>
              <ul id="progressbar">
                <li class="active"></li><li></li>
              </ul>
              <fieldset>

                <div id="mode-selector" class="controls" style="display:none;">
                  <input type="radio" name="type" id="changemode-driving" checked="checked">
                  <label for="changemode-driving">Driving</label>
                </div>

                <div id="right-panel">
                  <h3>Места на поаѓање и пристигнување</h3>
                  <div id="origin-input-fg" class="form-group">
                    <label class="control-label" for="origin-input">Возам од</label>
                    <input id="origin-input" name="d_from" class="controls form-control" type="text" placeholder="Додади локација на поаѓање" value="{{$ride->d_from}}">
                  </div>
                  <div id="destination-input-fg" class="form-group">
                    <label class="control-label" for="destination-input">Возам до</label>
                    <input id="destination-input" name="d_to" class="controls form-control" type="text" placeholder="Додади локација на пристигнување" value="{{$ride->d_to}}">
                  </div>


                 <h3 class="text-secondary">Попатни места</h3>

              <label for="d_stop_1">Пополни го ова поле доколку застануваш на место кое е помеѓу твојата почетна и крајна дестинација. Така може да те пронајдат повеќе луѓе и да патуват со тебе.</label>
              <div class="form-group">
                <div class="input-group" id="waypoints1inputgroup">
                  <input id="waypoints1" name="d_stop_1" class="controls waypoints form-control " type="text" placeholder="Додади попатна локација" value="{{$ride->d_stop_1}}">
                  <div class="input-group-addon bg-primary addstop" id='addstop'><i class="fas fa-plus text-white"></i></div>
                </div>
              </div>
              <div class="form-group">
                <input id="waypoints2" name="d_stop_2" class="controls waypoints form-control hide" type="text" placeholder="Додади попатна локација" value="{{$ride->d_stop_2}}">
              </div>

              <h3>Дата и час на патувањето</h3>
              <div id='s_date_time'>
                <div class="col-md-6">
                  <div id="datepicker1-fg" class="form-group">
                    <label class="control-label" for="datepicker1">Дата на поаѓање</label>
                    <div id="datepicker1-ig" class="input-group">
                      <div class="input-group-addon"><i class="fas fa-calendar-alt"></i></div>
                      <input type="text" id="datepicker1" name="d_date" class="form-control" value="{{ Carbon\Carbon::parse($ride->d_date)->format('d.m.Y') }}" @if(in_array('d_date', $disable_inputs)) disabled @endif autocomplete="off">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div id="clockpicker1I-fg" class="form-group">
                    <label class="control-label" for="datepicker1">Час на поаѓање</label>
                    <div id="clockpicker1I-ig" class="input-group clockpicker1" data-align="top" data-autoclose="true">
                      <div class="input-group-addon"><i class="fas fa-clock"></i></div>
                      <input type="text" class="form-control" name="d_time" id='clockpicker1I' value="{{$ride->d_time}}" autocomplete="off">
                    </div>
                  </div>
                </div>
              </div>

                  <div class="form-group">
                    <div class="checkbox checkbox-primary">
                      <input type="checkbox" name="roundtrip" id="roundtrip" onchange="toggleCheckbox(this)" @if($ride->roundtrip) checked @endif>
                      <label for="roundtrip">Се враќам назад</label>
                    </div>
                  </div>

                  <div id='b_date_time' style="display: none;">
                    <div class="col-md-6">
                      <div id="datepicker2-fg" class="form-group">
                        <label class="control-label" for="datepicker2">Дата на враќање</label>
                        <div id="datepicker2-ig" class="input-group">
                          <div class="input-group-addon"><i class="fas fa-calendar-alt"></i></div>
                          <input type="text" id="datepicker2" name="d_rdate" class="form-control" value="@if($ride->d_rdate != '') {{ Carbon\Carbon::parse($ride->d_rdate)->format('d.m.Y') }} @endif" autocomplete="off">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div id="clockpicker2I-fg" class="form-group">
                        <label class="control-label" for="datepicker2">Час на враќање</label>
                        <div id="clockpicker2I-ig" class="input-group clockpicker2" data-align="top" data-autoclose="true">
                          <div class="input-group-addon"><i class="fas fa-clock"></i></div>
                          <input type="text" class="form-control" name="d_rtime" id='clockpicker2I' value="{{$ride->d_rtime}}" autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>

                  <input type='hidden' id='d_stop1_value'>
                  <input type='hidden' id='d_stop1_time' name="d_stop1_time">
                  <input type='hidden' id='d_stop2_value'>
                  <input type='hidden' id='d_stop2_time' name="d_stop2_time">
                  <input type='hidden' id='d_stop3_value'>
                  <input type='hidden' id='d_stop3_time' name="d_stop3_time">
                  <input type='hidden' id='d_enddest_value'>
                  <input type='hidden' id='d_enddest_time' name="d_enddest_time">

                  <input type='hidden' id='distance' name='distance'>
                  <input type='hidden' id='est_d_time' name='est_d_time'>

                  <button type="button" id="get_directions" style="height: 1px; width:1px; visibility: hidden;"></button>
                </div>
                <button type="button" name="next" class="next btn btn-primary btn-lg btn-block" id="next1">Следно <i class="fas fa-angle-double-right"></i></button>
              </fieldset>

              <fieldset>

                <h3>Цена на чинење по патник</h3>

                <div class="row">
                  <div id='costsharing'></div>
                </div>

                <hr>

                <div class="form-group">
                  <label for="free_places">Слободни места:</label>
                  <input type="number" id="free_places" name="free_places" class="controls form-control" min="1" max="6" value="{{$ride->free_places}}" >
                </div>

                <div class="form-group">
                  <div class="checkbox checkbox-primary">
                    <input type="checkbox" name="back_s_guarantee" id="back_s_guarantee" onchange="changeVal(this)" @if($ride->back_s_guarantee) checked @endif>
                    <label for="back_s_guarantee">Гарантирам дека најмногу две лица ќе седат на ѕадните седишта (патниците го претпочитат ова)</label>
                  </div>
                </div>

                <div class="form-group">
                  <label for="ride_comment">Напишете повеќе детали за вашето патување</label>
                  <textarea name="ride_comment" id="ride_comment" class="form-control controls">{{$ride->d_description}}</textarea>
                </div>

                <button type="button" name="previous" class="previous action-button btn btn-link"><i class="fas fa-angle-double-left"></i> Назад</button>
                @if(!in_array('save_changes', $disable_inputs))
                  <button type="submit" class="btn btn-primary btn-icon-split btn-lg btn-block submit">
                    <span class="icon text-white-50"><i class="fas fa-save"></i></span>
                    <span class="text">Зачувај Промени</span>
                  </button>
                @endif

              </fieldset>

              <input type="hidden" name="_method" value="PUT">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <!-- /END FORM -->
            </form>
          </div>
        </div>
        <div class="col-md-6 pl-0">
          <div class="col-md-12 bg-white h-100 pr-0">
            <div id="map" class="h-50"></div>
            <div class="row">
              <div class="col-md-12 px-4 py-5">
                <div id="directions-panel"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="status_prompt">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Дали сте сигурни дека сакате да го промените статусот?</h3>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" id='status_deny' class="btn btn-default" data-dismiss="modal">Не</button>
            <button type="button" id='status_confirm' class="btn btn-primary">Да</button>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('includesscripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/mk.js"></script>
<script>

  window.onload = function() {
    var orig = document.getElementById('origin-input');
    var dest = document.getElementById('destination-input');
    var wp1 = document.getElementById('waypoints1');
    var wp2 = document.getElementById('waypoints2');

    if(wp2.value != ''){
      $("#waypoints2").removeClass("hide");
      $("#addstop").addClass("hide");
      $("#waypoints1inputgroup").removeClass("input-group");
    }

    orig.onpaste = function(e) {
      e.preventDefault();
    }
    dest.onpaste = function(e) {
      e.preventDefault();
    }
    wp1.onpaste = function(e) {
      e.preventDefault();
    }
    wp2.onpaste = function(e) {
      e.preventDefault();
    }
  }

  $("#addstop").on('click', function() {
    stopIn = document.getElementById("waypoints1").value;
    if(stopIn != ''){
      $("#waypoints2").removeClass("hide");
      $("#waypoints2").focus();
      $("#addstop").addClass("hide");
    }

  });

  // Prevent Submit on Enter
  $('#msform').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });

  roundtrip  = document.getElementById('roundtrip');
  if(roundtrip.checked){
    $(roundtrip).val(1);
    $(roundtrip).prop('checked',true);
    $('#b_date_time').css("display","block");

  }else{
    $(roundtrip).val(0);
    $(roundtrip).prop('checked',false);
    $('#b_date_time').css("display","none");
  }

  back_s_guarantee = document.getElementById('back_s_guarantee');
  if(back_s_guarantee.checked){
    $(back_s_guarantee).val(1);
    $(back_s_guarantee).prop('checked',true);

  }else{
    $(back_s_guarantee).val(0);
    $(back_s_guarantee).prop('checked',false);
  }

  $(function() {
    $('#get_directions').click();
  });

  function toggleCheckbox(element)
  {
    if(element.checked){
      $(element).val(1);
      $(element).prop('checked',true);
      $('#b_date_time').css("display","block");

    }else{
      $(element).val(0);
      $(element).prop('checked',false);
      $('#b_date_time').css("display","none");
      $('#clockpicker2I, #datepicker2').val('');
    }
  }

  function changeVal(element)
  {
    if(element.checked){
      $(element).val(1);
    }else{
      $(element).val(0);
    }
  }

  $('.clockpicker1, .clockpicker2').clockpicker().find('input').focusout(function(){
    // regular expression to match required time format
    re = /^\d{1,2}:\d{2}([ap]m)?$/;
    if(!this.value.match(re)){
      this.value = '';
    }
  });

  function checkTimeDiff(){
     var time1I = $('#clockpicker1I').val();
      var time2I = $('#clockpicker2I').val();
      if(time1I >= time2I){
        $('#time2Error').remove();
        $("#b_date_time").after("<div class='row' id='time2Error'><div class='col-md-12'><div class='alert alert-danger'>Времето на враќање треба да е подоцна од времето на поаѓање</div></div></div>");
        $("#clockpicker2I").val('');
        $('#clockpicker2I-fg').addClass('has-error');
      }else{
        $('#time2Error').remove();
        $('#clockpicker2I-fg').removeClass('has-error');
      }
  }

  function checkTime1Validity(){
    d = new Date();
    var datepicker1 = $("#datepicker1").datepicker('getDate');
    var datepicker1 = moment(datepicker1).format('YYYY-MM-DD');
    var dateNow = moment(d).format('YYYY-MM-DD');

    if(datepicker1 == dateNow){
      var time1I = $('#clockpicker1I').val();
      var timeNow = moment(d).add(1, 'hour').format('HH:mm');

      if(timeNow >= time1I){
        $('#time1Error').remove();
        $("#s_date_time").after("<div class='row' id='time1Error'><div class='col-md-12'><div class='alert alert-danger'>Времето на поаѓање треба да е миниум 1 час подоцна од сега</div></div></div>");
        $("#clockpicker1I").val('');
        $('#clockpicker1I-fg').addClass('has-error');
      }else{
        $('#time1Error').remove();
        $('#clockpicker1I-fg').removeClass('has-error');
      }
    }else{
      $('#time1Error').remove();
    }

    if($("#datepicker2").val() != ''){
      checkTimeDiff();
    }
  }

  function checkTime2Validity(){
    var datepicker1 = $("#datepicker1").datepicker('getDate');
    var datepicker2 = $("#datepicker2").datepicker('getDate');
    var datepicker1 = moment(datepicker1).format('YYYY-MM-DD');
    var datepicker2 = moment(datepicker2).format('YYYY-MM-DD');

    if(datepicker1 == datepicker2 ){
      checkTimeDiff();
    }else{
      $('#time2Error').remove();
    }
  }

  $('.clockpicker1').clockpicker().find('input').change(function(){
    checkTime1Validity();
  });

  $('.clockpicker2').clockpicker().find('input').change(function(){
    checkTime2Validity();
  });

  var date = new Date();
  $('#datepicker1,#datepicker2').datepicker({
    format: 'dd.mm.yyyy',
    startDate: date,
    endDate: '+365d',
    autoclose: true,
    language: 'mk',
  });

  var datepicker1 = $("#datepicker1").datepicker('getDate');
  $("#datepicker2").datepicker('setStartDate',datepicker1);

  $("#datepicker1").change(function(){
    var datepicker1 = $("#datepicker1").datepicker('getDate');
    var datepicker2 = $("#datepicker2").datepicker('getDate');
    $("#datepicker2").datepicker('setStartDate',datepicker1);
    if(datepicker2 < datepicker1){
      $("#datepicker2").datepicker('clearDates');
    }

    if($('#clockpicker1I').val() != ''){
          checkTime1Validity();
    }

  });

  function initMap() {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: {lat: 41.71556, lng: 21.77556},
      streetViewControl: false,
      fullscreenControl: false
    });
    directionsDisplay.setMap(map);

    document.getElementById('get_directions').addEventListener('click', function() {
      calculateAndDisplayRoute(directionsService, directionsDisplay);
    });
    new AutocompleteDirectionsHandler(map);
  }

  function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    var waypts = [];
    var checkboxArray1 = document.getElementById('waypoints1');
    var checkboxArray2 = document.getElementById('waypoints2');

    if(checkboxArray1.value != ""){
      waypts.push({
        location: checkboxArray1.value,
        stopover: true
      });
    }

    if(checkboxArray2.value != ""){
      waypts.push({
        location: checkboxArray2.value,
        stopover: true
      });
    }

    directionsService.route({
      origin: document.getElementById('origin-input').value,
      destination: document.getElementById('destination-input').value,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
        var route = response.routes[0];
        var summaryPanel = document.getElementById('directions-panel');
        summaryPanel.innerHTML = '';
        var totalDistance = 0;
        var totalDuration = 0;
        var segmentCostshare = [];

        $("#costsharing").empty();

        // For each route, display summary information.
        for (var i = 0; i < route.legs.length; i++) {
          var routeSegment = i + 1;

          //Format Strings for Part 2
          var startAddress = route.legs[i].start_address.split(',');
          var endAddress = route.legs[i].end_address.split(',');

          if(startAddress.length >= 3){
            startAddress_f = startAddress[0] + ',' + startAddress[startAddress.length - 2];
          }else{
            startAddress_f = startAddress[0];
          }

          if(endAddress.length >= 3){
            endAddress_f = endAddress[0] + ',' + endAddress[endAddress.length - 2];
          }else{
            endAddress_f = endAddress[0];
          }
          var elememID = 'costShare' + routeSegment;
          if (route.legs.length == 1) {
            var WidthClass = 'col-md-12';
          } else {
            var WidthClass = 'col-md-6';
          }
          var appendInputs = "<div class='"+WidthClass+"'><div id='" + elememID + "-fg' class='form-group'><label class='control-label costShareP'><span class='preseci' data-toggle='tooltip' data-placement='top'>" + startAddress_f + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + endAddress_f + "</span></label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' min='10' max='99999' step='10' /></div></div>"
          $("#costsharing").append(appendInputs);

          if(route.legs.length > 1){
            summaryPanel.innerHTML += "<dl class='dl-horizontal'><dt><i class='fas fa-map-marker-alt text-secondary fa-2x'></i></dt><dd><span class='preseci' data-toggle='tooltip' data-placement='top'>" + route.legs[i].start_address + "</span> <span class='preseci' data-toggle='tooltip' data-placement='top'> <i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'> " + route.legs[i].end_address + "</span></dd><dd><span class='dolzina'><i class='fas fa-road'></i>" + parseInt(route.legs[i].distance.text) + " км</span><span class='saat'><i class='far fa-clock'></i> " + secondsToHms(route.legs[i].duration.value) + "</dd></dl>";
          }
          totalDistance = totalDistance + route.legs[i].distance.value;
          totalDuration = totalDuration + route.legs[i].duration.value;

          $('#d_stop' + routeSegment + '_value').val(route.legs[i].duration.value);

          if(route.legs.length >= 2 && i == 1){
            document.getElementById('waypoints1').value = route.legs[1].start_address;
          }

          if(route.legs.length == 3 && i == 2){
            document.getElementById('waypoints2').value = route.legs[2].start_address;
          }

        }

        if(route.legs.length >= 1){

          //Format Strings for Part 2
          var startAddress = route.legs[0].start_address.split(',');
          var endAddress = route.legs[route.legs.length - 1].end_address.split(',');

          if(startAddress.length >= 3){
            startAddress_f = startAddress[0] + ',' + startAddress[startAddress.length - 2];
          }else{
            startAddress_f = startAddress[0];
          }

          if(endAddress.length >= 3){
            endAddress_f = endAddress[0] + ',' + endAddress[endAddress.length - 2];
          }else{
            endAddress_f = endAddress[0];
          }

          if(route.legs.length == 3){
            //First Middle Address
            var middleAddress = route.legs[1].start_address.split(',');
            if(middleAddress.length >= 3){
              middleAddress_f = middleAddress[0] + ',' + middleAddress[middleAddress.length - 2];
            }else{
              middleAddress_f = middleAddress[0];
            }
            //Second Middle Address
            var middleAddress2 = route.legs[2].start_address.split(',');
            if(middleAddress2.length >= 3){
              middleAddress_f2 = middleAddress2[0] + ',' + middleAddress2[middleAddress2.length - 2];
            }else{
              middleAddress_f2 = middleAddress2[0];
            }

            routeSegment = routeSegment + 1;
            var elememID = 'costShare' + routeSegment;

            var appendInputs = "<div class='col-md-6'><div id='" + elememID + "-fg' class='form-group'><label class='control-label costShareP'><span class='preseci' data-toggle='tooltip' data-placement='top'>" + startAddress_f + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + middleAddress_f2 + "</span></label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' min='10' max='99999' step='10' /></div></div>";
            $("#costsharing").append(appendInputs);

            routeSegment = routeSegment + 1;
            var elememID = 'costShare' + routeSegment;

          var appendInputs = "<div class='col-md-6'><div id='" + elememID + "-fg' class='form-group'><label class='control-label costShareP'><span class='preseci' data-toggle='tooltip' data-placement='top'>" + middleAddress_f + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + endAddress_f + "</span></label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' min='10' max='99999' step='10' /></div></div>";
            $("#costsharing").append(appendInputs);
          }

          if(route.legs.length > 1){

            routeSegment = routeSegment + 1;
            var elememID = 'costShare' + routeSegment;

            var appendInputs = "<div class='col-md-6'><div id='" + elememID + "-fg' class='form-group'><label class='control-label costShareP'><span class='preseci' data-toggle='tooltip' data-placement='top'>" + startAddress_f + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + endAddress_f + "</span></label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' min='10' max='99999' step='10' /></div></div>";
            $("#costsharing").append(appendInputs);
          }

          document.getElementById('origin-input').value = route.legs[0].start_address;
          document.getElementById('destination-input').value = route.legs[route.legs.length - 1].end_address;


          totalDistance = totalDistance / 1000;

          summaryPanel.innerHTML += "<dl class='dl-horizontal'><dt><i class='fas fa-map-marker-alt text-primary fa-2x'></i></dt><dd><span class='preseci' data-toggle='tooltip' data-placement='top'>" + startAddress + "</span> <span class='preseci' data-toggle='tooltip' data-placement='top'><i class='fas fa-angle-double-right'></i></span> <span class='preseci' data-toggle='tooltip' data-placement='top'>" + endAddress + "</span></dd><dd><span class='dolzina'><i class='fas fa-road'></i>"+ parseInt(totalDistance) + " км</span><span class='saat'><i class='fas fa-clock'></i> "+ secondsToHms(totalDuration) + "</span></dd></dl>";


          document.getElementById('distance').value = parseInt(totalDistance);
          document.getElementById('est_d_time').value = secondsToHms(totalDuration);
        }

        var d_from = {!! json_encode($ride->d_from); !!};
        var d_to = {!! json_encode($ride->d_to); !!};
        var d_from_Input = document.getElementById('origin-input').value;
        var d_to_Input = document.getElementById('destination-input').value;

        if(d_from == d_from_Input && d_to == d_to_Input){

          if(document.getElementById('costShare1')){
            var d_cost1 = {!! json_encode($ride->d_cost1); !!};
            document.getElementById('costShare1').value = d_cost1;
          }
          if(document.getElementById('costShare2')){
            var d_cost2 = {!! json_encode($ride->d_cost2); !!};
            if(d_cost2){
              document.getElementById('costShare2').value = d_cost2;
            }else{
              document.getElementById('costShare2').value = 1;
            }
          }
          if(document.getElementById('costShare3')){
            var d_cost3 = {!! json_encode($ride->d_cost3); !!};
            if(d_cost3){
              document.getElementById('costShare3').value = d_cost3;
            }else{
              document.getElementById('costShare3').value = 1;
            }
          }
          if(document.getElementById('costShare4')){
            var d_cost4 = {!! json_encode($ride->d_cost4); !!};
            if(d_cost4){
              document.getElementById('costShare4').value = d_cost4;
            }else{
              document.getElementById('costShare4').value = 1;
            }
          }
          if(document.getElementById('costShare5')){
            var d_cost5 = {!! json_encode($ride->d_cost5); !!};
            if(d_cost5){
              document.getElementById('costShare5').value = d_cost5;
            }else{
              document.getElementById('costShare5').value = 1;
            }
          }
          if(document.getElementById('costShare6')){
            var d_cost6 = {!! json_encode($ride->d_cost6); !!};
            if(d_cost6){
              document.getElementById('costShare6').value = d_cost6;
            }else{
              document.getElementById('costShare6').value = 1;
            }
          }

        }

      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  function AutocompleteDirectionsHandler(map) {
    this.map = map;
    this.originPlaceId = null;
    this.destinationPlaceId = null;
    this.travelMode = 'DRIVING';
    var originInput = document.getElementById('origin-input');
    var destinationInput = document.getElementById('destination-input');
    var waypointinput1 = document.getElementById('waypoints1');
    var waypointinput2 = document.getElementById('waypoints2');

    var modeSelector = document.getElementById('mode-selector');
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.directionsDisplay.setMap(map);

    var originAutocomplete = new google.maps.places.Autocomplete(
      originInput, {fields: ['place_id']});
    var destinationAutocomplete = new google.maps.places.Autocomplete(
      destinationInput, {fields: ['place_id']});
    var waypointinputcomplete1 = new google.maps.places.Autocomplete(
      waypointinput1, {fields: ['place_id']});
    var waypointinputcomplete2 = new google.maps.places.Autocomplete(
      waypointinput2, {fields: ['place_id']});

            this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
            this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');
            this.setupPlaceChangedListener(waypointinputcomplete1, 'WAYP1');
            this.setupPlaceChangedListener(waypointinputcomplete2, 'WAYP2');
  }

  AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
          var me = this;
          autocomplete.bindTo('bounds', this.map);

          autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            var originInput = document.getElementById('origin-input').value;
            var destinationInput = document.getElementById('destination-input').value;

            if (!place.place_id) {
              window.alert("Те молам одбери некоја од понудените опции.");
              return;
            }
            if (mode === 'ORIG') {
              me.originPlaceId = place.place_id;

              if(destinationInput != ''){
                document.getElementById('get_directions').click();
              }
            }else if(mode === 'DEST') {
              me.destinationPlaceId = place.place_id;

              if(originInput != ''){
                document.getElementById('get_directions').click();
              }
            }else if(mode === 'WAYP1'){

              if(originInput != '' && destinationInput != ''){
                document.getElementById('get_directions').click();
              }
            }else{

              if(originInput != '' && destinationInput != ''){
                document.getElementById('get_directions').click();
              }
            }

            me.route();
          });
  };

  AutocompleteDirectionsHandler.prototype.route = function() {
    if (!this.originPlaceId || !this.destinationPlaceId) {
      return;
    }
    var me = this;
  };

  $("#waypoints1").focusout(function(){
    if(this.value == ''){
      originI = document.getElementById('origin-input').value;
      destI = document.getElementById('destination-input').value;

      if(originI != '' && destI != ''){
        document.getElementById('get_directions').click();
      }else{
        initMap();
      }
    }
  });

  $("#waypoints2").focusout(function(){
    originI = document.getElementById('origin-input').value;
    destI = document.getElementById('destination-input').value;
    if(this.value == ''){
      $("#waypoints2").addClass("hide");
      $("#addstop").removeClass("hide");
      $("#waypoints1inputgroup").addClass("input-group");

      if(originI != '' && destI != ''){
        document.getElementById('get_directions').click();
      }else{
        initMap();
      }
    }
  });

  $("#destination-input").focusout(function(){
    originI = document.getElementById('origin-input').value;

    if(this.value == '' && originI != ''){
      initMap();
      var summaryPanel = document.getElementById('directions-panel');
      summaryPanel.innerHTML = '';
    }
  });

  $("#origin-input").focusout(function(){
    destI = document.getElementById('destination-input').value;
    if(this.value == '' && destI != ''){
      initMap();
      var summaryPanel = document.getElementById('directions-panel');
      summaryPanel.innerHTML = '';
    }
  });

  function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " час и " : " часа и ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " минута " : " минути ") : "";

    return hDisplay + mDisplay;
  }

  function timeToSeconds(time) {
    time = time.split(/:/);
    return time[0] * 3600 + time[1] * 60;
  }

  function format_two_digits(n) {
    return n < 10 ? '0' + n : n;
  }

  //Multistep form
  //jQuery time
  var current_fs, next_fs, previous_fs; //fieldsets
  var left, opacity, scale; //fieldset properties which we will animate
  var animating; //flag to prevent quick multi-click glitches

  $(".next").click(function(){

    ClickedElementID = this.id;
    var error1 = error2 = false;

    //CHECK FOR EMPTY INPUTS
    if(ClickedElementID == 'next1'){

      if($('#origin-input').val() == ''){
        error1 = true;
        $("#origin-input-hb").remove();
        $("#origin-input-fg").addClass("has-error");
        $("#origin-input").after("<span class='help-block' id='origin-input-hb'>Полето е задолжително</span>");
      }else{
        $("#origin-input-hb").remove();
        $("#origin-input-fg").removeClass("has-error");
      }
      if($('#destination-input').val() == ''){
        error1 = true;
        $("#destination-input-hb").remove();
        $("#destination-input-fg").addClass("has-error");
        $("#destination-input").after("<span class='help-block' id='destination-input-hb'>Полето е задолжително</span>");
      }else{
        $("#destination-input-hb").remove();
        $("#destination-input-fg").removeClass("has-error");
      }
      if($('#datepicker1').val() == ''){
        error1 = true;
        $("#datepicker1-hb").remove();
        $("#datepicker1-fg").addClass("has-error");
        $("#datepicker1-ig").after("<span class='help-block' id='datepicker1-hb'>Полето е задолжително</span>");
      }else{
        $("#datepicker1-hb").remove();
        $("#datepicker1-fg").removeClass("has-error");
      }
      if($('#clockpicker1I').val() == ''){
        error1 = true;
        $("#clockpicker1I-hb").remove();
        $("#clockpicker1I-fg").addClass("has-error");
        $("#clockpicker1I-ig").after("<span class='help-block' id='clockpicker1I-hb'>Полето е задолжително</span>");
      }else{
        var clockpicker1 = $('#clockpicker1I').val();
        var d_stop1_value = Number(document.getElementById('d_stop1_value').value);

        var h = Math.floor(d_stop1_value / 3600);
        var m = Math.floor(d_stop1_value % 3600 / 60);

        var date = new Date("October 13, 2014 "+clockpicker1);

        if(m > 0){
          date.setMinutes(date.getMinutes()+m);
        }
        if(h > 0){
          date.setHours(date.getHours()+h);
        }

        hours = format_two_digits(date.getHours());
        minutes = format_two_digits(date.getMinutes());
        document.getElementById('d_stop1_time').value = hours + ":" + minutes;

        if(document.getElementById('d_stop2_value').value != ''){

          var d_stop1_time = $('#d_stop1_time').val();
          var d_stop2_value = Number(document.getElementById('d_stop2_value').value);

          var h = Math.floor(d_stop2_value / 3600);
          var m = Math.floor(d_stop2_value % 3600 / 60);

          var date = new Date("October 13, 2014 "+d_stop1_time);

          if(m > 0){
            date.setMinutes(date.getMinutes()+m);
          }
          if(h > 0){
            date.setHours(date.getHours()+h);
          }

          hours = format_two_digits(date.getHours());
          minutes = format_two_digits(date.getMinutes());
          document.getElementById('d_stop2_time').value = hours + ":" + minutes;

        }

        if(document.getElementById('d_stop3_value').value != ''){

          var d_stop2_time = $('#d_stop2_time').val();
          var d_stop3_value = Number(document.getElementById('d_stop3_value').value);

          var h = Math.floor(d_stop3_value / 3600);
          var m = Math.floor(d_stop3_value % 3600 / 60);

          var date = new Date("October 13, 2014 "+d_stop2_time);

          if(m > 0){
            date.setMinutes(date.getMinutes()+m);
          }
          if(h > 0){
            date.setHours(date.getHours()+h);
          }

          hours = format_two_digits(date.getHours());
          minutes = format_two_digits(date.getMinutes());
          document.getElementById('d_stop3_time').value = hours + ":" + minutes;

        }

        $("#clockpicker1I-hb").remove();
        $("#clockpicker1I-fg").removeClass("has-error");
          if( $('#travel_time').length ) // use this if you are using id to check
          {
            $('#start_travel_time').html("<p class='mb-0 text-strong'>Поаѓање:</p><p class='ml-2'><span class='saat'><i class='fas fa-calendar-alt text-primary'></i>" + $("#datepicker1").val() + "</span><br><span class='saat'><i class='fas fa-clock text-primary'></i>" + $("#clockpicker1I").val() + "</span></p>");
          }else{
            $("#directions-panel").append("<div class='row' id='travel_time'></div>");
            $("#travel_time").append("<div class='col-md-6' id='start_travel_time'></div>");

            $('#start_travel_time').html("<p class='mb-0 text-strong'>Поаѓање:</p><p class='ml-2'><span class='saat'><i class='fas fa-calendar-alt text-primary'></i>" + $("#datepicker1").val() + "</span><br><span class='saat'><i class='fas fa-clock text-primary'></i>" + $("#clockpicker1I").val() + "</span></p>");
          }
        }

      if($('#roundtrip').is(':checked')){
        if($('#datepicker2').val() == ''){
          error2 = true;
          $("#datepicker2-hb").remove();
          $("#datepicker2-fg").addClass("has-error");
          $("#datepicker2-ig").after("<span class='help-block' id='datepicker2-hb'>Полето е задолжително</span>");
        }else{
          $("#datepicker2-hb").remove();
          $("#datepicker2-fg").removeClass("has-error");
        }
        if($('#clockpicker2I').val() == ''){
          error2 = true;
          $("#clockpicker2I-hb").remove();
          $("#clockpicker2I-fg").addClass("has-error");
          $("#clockpicker2I-ig").after("<span class='help-block' id='clockpicker2I-hb'>Полето е задолжително</span>");
        }else{
          $("#clockpicker2I-hb").remove();
          $("#clockpicker2I-fg").removeClass("has-error");

          if( $('#travel_time').length ){
            $("#travel_time").append("<div class='col-md-6' id='ret_travel_time'></div>");
            $('#ret_travel_time').html("<p class='mb-0 text-strong'>Враќање:</p><p class='ml-2'><span class='saat'><i class='fas fa-calendar-alt text-primary'></i>" + $("#datepicker2").val() + "</span><br><span class='saat'><i class='fas fa-clock text-primary'></i>" + $("#clockpicker2I").val() + "</span></p>");
          }
        }
      }
    }

    if(error1 == false && error2 == false){

      if(animating) return false;
      animating = true;

      current_fs = $(this).parent();
      next_fs = $(this).parent().next();

      //activate next step on progressbar using the index of next_fs
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
          //as the opacity of current_fs reduces to 0 - stored in "now"
          //1. scale current_fs down to 80%
          scale = 1 - (1 - now) * 0.2;
          //2. bring next_fs from the right(50%)
          left = (now * 50)+"%";
          //3. increase opacity of next_fs to 1 as it moves in
          opacity = 1 - now;
          current_fs.css({
            'transform': 'scale('+scale+')',
            'position': 'absolute'
          });
          next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 800,
        complete: function(){
          current_fs.hide();
          animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
      });
    }

  });

  $(".previous").click(function(){
    if(animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
      step: function(now, mx) {
        //as the opacity of current_fs reduces to 0 - stored in "now"
        //1. scale previous_fs from 80% to 100%
        scale = 0.8 + (1 - now) * 0.2;
        //2. take current_fs to the right(50%) - from 0%
        left = ((1-now) * 50)+"%";
        //3. increase opacity of previous_fs to 1 as it moves in
        opacity = 1 - now;
        current_fs.css({'left': left});
        previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
      },
      duration: 800,
      complete: function(){
        current_fs.hide();
        animating = false;
      },
      //this comes from the custom easing plugin
      easing: 'easeInOutBack'
    });
  });

  $('#free_places').on('keydown keyup', function(e){
      if ($(this).val() > 6
        && e.keyCode !== 46 // keycode for delete
        && e.keyCode !== 8 // keycode for backspace
      ) {
        e.preventDefault();
        $(this).val(3);
      }

      if ($(this).val() <= 0
      && e.keyCode !== 46 // keycode for delete
      && e.keyCode !== 8 // keycode for backspace
    ) {
      e.preventDefault();
      $(this).val(3);
    }

    if(event.keyCode == 69){
      e.preventDefault();
    }
  });

  $(".submit").click(function(event){
    if($('#costShare1').val() == ''){
      $("#costShare1-hb").remove();
      $("#costShare1-fg").addClass("has-error");
      $("#costShare1").after("<span class='help-block' id='costShare1-hb'>Полето е задолжително</span>");
      event.preventDefault();
    }else{
      $("#costShare1-hb").remove();
      $("#costShare1-fg").removeClass("has-error");
    }
    if($('#costShare2').val() == ''){
      $("#costShare2-hb").remove();
      $("#costShare2-fg").addClass("has-error");
      $("#costShare2").after("<span class='help-block' id='costShare2-hb'>Полето е задолжително</span>");
      event.preventDefault();
    }else{
      $("#costShare2-hb").remove();
      $("#costShare2-fg").removeClass("has-error");
    }
    if($('#costShare3').val() == ''){
      $("#costShare3-hb").remove();
      $("#costShare3-fg").addClass("has-error");
      $("#costShare3").after("<span class='help-block' id='costShare3-hb'>Полето е задолжително</span>");
      event.preventDefault();
    }else{
      $("#costShare3-hb").remove();
      $("#costShare3-fg").removeClass("has-error");
    }
    if($('#costShare4').val() == ''){
      $("#costShare4-hb").remove();
      $("#costShare4-fg").addClass("has-error");
      $("#costShare4").after("<span class='help-block' id='costShare4-hb'>Полето е задолжително</span>");
      event.preventDefault();
    }else{
      $("#costShare4-hb").remove();
      $("#costShare4-fg").removeClass("has-error");
    }
    if($('#costShare5').val() == ''){
      $("#costShare5-hb").remove();
      $("#costShare5-fg").addClass("has-error");
      $("#costShare5").after("<span class='help-block' id='costShare5-hb'>Полето е задолжително</span>");
      event.preventDefault();
    }else{
      $("#costShare5-hb").remove();
      $("#costShare5-fg").removeClass("has-error");
    }
    if($('#costShare6').val() == ''){
      $("#costShare6-hb").remove();
      $("#costShare6-fg").addClass("has-error");
      $("#costShare6").after("<span class='help-block' id='costShare6-hb'>Полето е задолжително</span>");
      event.preventDefault();
    }else{
      $("#costShare6-hb").remove();
      $("#costShare6-fg").removeClass("has-error");
    }
  });

  const checkbox = document.getElementById('ride_completed')

  checkbox.addEventListener('change', (event) => {
    if (event.target.checked) {
      if ($('#warning').length > 0) {

      }else{
        $(".modal-body").append("<div class='alert alert-info p-0 m-0' id='warning'><div class='tekst'><p class='lead ml-3 mt-1'>Патувањата со статус <span class='text-danger text-strong'>\"Завршено\"</span> немаат можност за изменување</p></div></div>");
      }
    }
  })

  //Change Status
  var form = document.getElementById("ride_status");

  document.getElementById("status_confirm").addEventListener("click", function () {
    form.submit();
  });

  //Restore Radio Buttons
  document.getElementById("status_deny").addEventListener("click", function () {
    if ($('#warning').length > 0) {
      $("#warning").remove();
    }

    var status = "<?php echo $ride->status ?>";

    if(status == 'in_progress'){
      document.getElementById("ride_in_progress").checked = true;

    }else if(status == 'canceled'){
      document.getElementById("ride_canceled").checked = true;

    }else if(status == 'completed'){
      document.getElementById("ride_completed").checked = true;
    }
  });
  $(function() {
    function isEllipsisActive(element) {
      return element.offsetWidth < element.scrollWidth;
    }
    Array.from(document.querySelectorAll('.preseci'))
    .forEach(element => {
      if (isEllipsisActive(element)) {
        element.title = element.innerText;
      }
    });
    $('[data-toggle="tooltip"]').tooltip()
  });
</script>
@if(strtolower($ride->status) == 'completed')
<script>
  $("input").prop('disabled', true);
</script>
@endif
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuvTR6aAp_y_rcc_w1m087uc9jh84V3Qk&libraries=places&callback=initMap&language=mk&region=mk" ></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c9c87d72b4ed697"></script>
@endsection
