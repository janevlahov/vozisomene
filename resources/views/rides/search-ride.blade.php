@extends('layouts.app')

@section('content')
<div class="pt-5 pt-xs-0">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="mb-1 mt-0">Бараш луѓе кои патуваат на исто место како тебе?</h1>
			</div>
		</div>
		<form action="" id="msform" class="form" action="{{action('RidesController@search_ride')}}" method="GET">
			<div class="panel panel-baraj bg-primary-70">
				<div class="panel-heading">
					<div class="row">
						<div class="col-md-6 col-md-push-6">
							<div class="row">
								<div class="form-group col-xs-12 col-md-4 pr-0 m-0 pr-xs-15 mb-xs-15">
									<select class="form-control input-sm" id='dsort' name="dsort" onchange="sortDate()">
										<option value="" selected>Сорт. по дата</option>
										<option value="asc" @if(isset($searchInputs['dsort']) && $searchInputs['dsort'] == 'asc') selected @endif>Растечки</option>
										<option value="desc" @if(isset($searchInputs['dsort']) && $searchInputs['dsort'] == 'desc') selected @endif>Опаѓачки</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-md-4 m-0 mb-xs-15">
									<select class="form-control input-sm" id='plsort' name="plsort" onchange="sortPlace()">
										<option value="" selected>Сорт. по бр. на места</option>
										<option value="asc" @if(isset($searchInputs['plsort']) && $searchInputs['plsort'] == 'asc') selected @endif>Растечки</option>
										<option value="desc" @if(isset($searchInputs['plsort']) && $searchInputs['plsort'] == 'desc') selected @endif>Опаѓачки</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-md-4 pl-0 m-0 pl-xs-15 mb-xs-15">
									<select class="form-control input-sm" id='ratesort' name="ratesort" onchange="sortRating()">
										<option value="" selected>Сорт. по рејтинг</option>
										<option value="asc" @if(isset($searchInputs['ratesort']) && $searchInputs['ratesort'] == 'asc') selected @endif>Растечки</option>
										<option value="desc" @if(isset($searchInputs['ratesort']) && $searchInputs['ratesort'] == 'desc') selected @endif>Опаѓачки</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-md-pull-6">
							@if(isset($searchInputs['dsort']) || isset($searchInputs['plsort']) || isset($searchInputs['ratesort']) || isset($searchInputs['fromI']) || isset($searchInputs['toI']) || isset($searchInputs['dateI']))
							<!-- <p class="lead m-0 text-white">Филтрирај ги патувањата</p> -->
							<a href="{{ url('/rides/search-ride') }}" class="btn btn-link"><i class="fas fa-sync-alt"></i> Ресетеирај филтри</a>
							@else
							<a class="btn btn-link" role="button" data-toggle="collapse" href="#filterprebaruvanje" aria-expanded="false" aria-controls="filterprebaruvanje">Филтрирај ги патувањата <i class="fas fa-angle-down"></i></a>
							@endif
						</div>
					</div>
				</div>
					@if(isset($searchInputs['dsort']) || isset($searchInputs['plsort']) || isset($searchInputs['ratesort']) || isset($searchInputs['fromI']) || isset($searchInputs['toI']) || isset($searchInputs['dateI']))
					<div>
						@else
						<div class="collapse" id="filterprebaruvanje">
							@endif
							<div class="panel-body">
							<div class="row">
								<div class="form-group form-group-lg col-xs-12 col-md-8">
									<div class="input-group filterprebaruvanje-xs-inputs">
										<input type="text" name="fp" id="autocomplete" class="form-control " placeholder="Локација на поаѓање" @if(isset($searchInputs)) value="{{$searchInputs['fromI']}}" @endif >
										<span class="input-group-addon"><button id="swapbtn" class="btn-link p-0"><i class="fas fa-exchange-alt fa-2x"></i></button></span>
										<input type="text" name="tp" id="autocomplete2" class="form-control " placeholder="Локација на пристигнување" @if(isset($searchInputs)) value="{{$searchInputs['toI']}}" @endif>
									</div>
								</div>
								<div class="form-group form-group-lg col-xs-12 col-md-4">
									<input type="text" name="rd" id="startDate" class="form-control" placeholder="Дата на поаѓање" @if(isset($searchInputs['dateI'])) value="{{ Carbon\Carbon::parse($searchInputs['dateI'])->format('d.m.Y') }}" @endif autocomplete="off">
								</div>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="form-group col-md-12">
									<input type="submit" name="submit" class="submit btn btn-secondary btn-lg btn-block" id="submitbtn" value="Барај патувања" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			@if(count($userRides) > 0)
				@foreach($userRides as $userRide)
					<div class="panel panel-patuvanja my-5 my-xs-2">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-md-5">
									<p> </p>
									<div class="destinacii">
										<div class="bulet">
											<div class="roundChecked"></div>
											@if($userRide->d_stop_1 != '')
												<div class="lineChecked"></div>
												<div class="roundUnChecked"></div>
											@endif
											@if($userRide->d_stop_2 != '')
												<div class="lineUnChecked"></div>
												<div class="roundUnChecked"></div>
											@endif
											<div class="lineChecked"></div>
											<div class="roundChecked"></div>
										</div>
										<div class="destinacija destinacija-pocetna">
											<div class="col-xs-1"></div>
											<div class="col-xs-11 px-xs-0 center">
												<p class="lead m-0 preseci" data-toggle="tooltip" data-placement="top">{{ Helper::rmCountry($userRide->d_from) }}</p>
											</div>
										</div>
										@if($userRide->d_stop_1 != '')
											<div class="destinacija destinacija-prva">
												<div class="col-xs-1"></div>
												<div class="col-xs-11 px-xs-0 center">
													<p class="m-0 preseci" data-toggle="tooltip" data-placement="top">{{ Helper::Short_Destination($userRide->d_stop_1) }}</p>
												</div>
											</div>
										@endif
										@if($userRide->d_stop_2 != '')
											<div class="destinacija destinacija-vtora">
												<div class="col-xs-1"></div>
												<div class="col-xs-11 px-xs-0 center">
													<p class="m-0 preseci" data-toggle="tooltip" data-placement="top">{{ Helper::Short_Destination($userRide->d_stop_2) }}</p>
												</div>
											</div>
										@endif
										<div class="destinacija destinacija-krajna">
											<div class="col-xs-1"></div>
											<div class="col-xs-11 px-xs-0 center">
												<p class="lead m-0 preseci" data-toggle="tooltip" data-placement="top">{{ Helper::rmCountry($userRide->d_to) }}</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-md-7">
									<div class="row mt-xs-15">
										<div class="col-xs-12 col-md-8">
											<div class="col-xs-6 col-md-6">
												<p>Поаѓање:</p>
												<p class="lead mb-xs-0"><i class="fas fa-calendar-alt text-primary"></i> {{ Carbon\Carbon::parse($userRide->d_date)->format('d.m.Y') }}</p>
												<p class="lead mb-xs-0"><i class="fas fa-clock text-primary"></i> {{$userRide->d_time}}</p>
											</div>
											@if($userRide->roundtrip == 1)
												<div class="col-xs-6 col-md-6">
													<p>Враќање:</p>
													<p class="lead mb-xs-0"><i class="fas fa-calendar-alt text-primary"></i> {{ Carbon\Carbon::parse($userRide->d_rdate)->format('d.m.Y') }}</p>
													<p class="lead mb-xs-0"><i class="fas fa-clock text-primary"></i> {{$userRide->d_rtime}}</p>
												</div>
											@endif
										</div>
										<div class="col-xs-12 col-md-4">
											<div class="text-right mb-3 hidden-xs">
												@if($userRide->roundtrip == 1)
													<span class="fa-stack fa-1x ml-1" data-toggle="tooltip" data-placement="top" title="Се враќам назад">
														<i class="fas fa-circle fa-stack-2x text-secondary"></i>
														<i class="fas fa-exchange-alt fa-stack-1x text-primary"></i>
													</span>
												@endif
												<span data-toggle="tooltip" data-placement="top" title="{{$userRide->r_qty_places}} слободни места за резервирање">
													{!!str_repeat('<img src="/storage/upload/web/seat_icon.png" class="sediste" alt="Седиште од автомобил">', $userRide->r_qty_places)!!}
												</span>
												@php $rezsedista = $userRide->free_places - $userRide->r_qty_places @endphp
												<span data-toggle="tooltip" data-placement="top" title="{{$userRide->r_qty_places}} слободни места за резервирање">
												{!!str_repeat('<img src="/storage/upload/web/seat_icon_full.png" class="sediste" alt="Седиште од автомобил">', $rezsedista)!!}
												</span>
											</div>
											<div class="visible-xs mt-xs-1">
							         	<p class="text-strong text-center">{!! Helper::free_seats_label($userRide->r_qty_places) !!}</p>
											</div>
											<div class="media">
												<div class="media-body text-right">
													<p class="lead media-heading"><a href="/rides/user_profile/{{$userRide->driver_id}}">{{$userRide->firstname}}</a></p>
													@if(!empty($userRide->car_brand))<p>{{$userRide->car_brand}} {{$userRide->car_model}}</p>
								          @else<p>Нема податоци за автомобилот</p>
								          @endif
												</div>
												<div class="media-right">
													<a href="/rides/user_profile/{{$userRide->driver_id}}">
													@if(!empty($userRide->avatar))<img class="media-object slika" src="{{asset('storage/upload/users/'.$userRide->avatar)}}" alt="{{$userRide->firstname}}">
								          @else<img class="media-object slika img-circle" src="/storage/upload/web/nouser.jpg" alt="Nema slika">
								          @endif
													</a>
												</div>
											</div>
											@if(!empty($userRide->driver_rating))
												<div class='starrr pull-right'>
													{{$userRide->driver_rating}}
													@for($i = 1; $i<= 5; $i++)
														@if($i <= $userRide->driver_rating)
															<a style="cursor: default;" class="fa-star fa"></a>
														@else
															<a style="cursor: default; "class="fa-star-o fa" ></a>
														@endif
													@endfor
												</div>
											@else
												<p class="text-right">Возачот не е оценет</p>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
						<a href="{{ url('/rides/detailed-view/'. $userRide->id) }}" class="btn btn-primary btn-block btn-lg">Види повеќе <i class="fas fa-angle-double-right"></i></a>
					</div>
				@endforeach
				{{$userRides->appends(['fp' => $searchInputs['fromI'],'tp' => $searchInputs['toI'],'rd' => $searchInputs['dateI'],'plsort' => $searchInputs['plsort'],'dsort' => $searchInputs['dsort'],'ratesort' => $searchInputs['ratesort']])->links()}}
			@else
				@if($searchInputs['dateI'] != '' || $searchInputs['fromI'] != '' || $searchInputs['toI'] != '')
					<div class="alert alert-danger">
						<h2 class="text-danger">Не се пронајдени патувања</h2>
						<p class="lead">Те молам барај со други критериуми</p>
						<a href="{{ url('/rides/search-ride') }}" class="btn btn-link"><i class="fas fa-sync-alt"></i> Ресетеирај филтри</a>
					</div>
				@endif
			@endif
		</div>
	</div>
</div>

</div>
@endsection
@section('includesscripts')

<script>

	var form = document.getElementById("msform");

	function sortDate() {
		$('#submitbtn').click();
	}

	function sortPlace() {
		$('#submitbtn').click();
	}

	function sortRating() {
		$('#submitbtn').click();
	}

	document.getElementById('swapbtn').onclick = function(event) {
		var tmp = document.getElementById('autocomplete').value;
		document.getElementById('autocomplete').value = document.getElementById('autocomplete2').value;
		document.getElementById('autocomplete2').value = tmp;
		event.preventDefault();
	};

	$('#msform').on('keyup keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) {
			if(document.getElementById("autocomplete2").value == 'undefined'){
				document.getElementById("autocomplete2").value = '';
			}
			if(document.getElementById("autocomplete").value == 'undefined'){
				document.getElementById("autocomplete").value = '';
			}
			e.preventDefault();
			return false;
		}
	});

	var date = new Date();
	$('#startDate').datepicker({
		format: 'dd.mm.yyyy',
		startDate: date,
		endDate: '+365d',
		autoclose: true,
		language: 'mk',
	});

	var placeSearch, autocomplete, geocoder;

	function initAutocomplete() {

		geocoder = new google.maps.Geocoder();

		autocomplete = new google.maps.places.Autocomplete(
			(document.getElementById('autocomplete')), {
				types: ['geocode']
		});

		autocomplete.addListener('place_changed', fillInAddress);

		autocomplete2 = new google.maps.places.Autocomplete(
			(document.getElementById('autocomplete2')), {
				types: ['geocode']
			});

			autocomplete2.addListener('place_changed', fillInAddress2);
		}

		function fillInAddress() {
			var place = autocomplete.getPlace();
			document.getElementById("autocomplete").value = place.formatted_address;
		}
		function fillInAddress2() {
			var place = autocomplete2.getPlace();
			document.getElementById("autocomplete2").value = place.formatted_address;
		}
		$(function() {
	    function isEllipsisActive(element) {
	      return element.offsetWidth < element.scrollWidth;
	    }
	    Array.from(document.querySelectorAll('.preseci'))
	    .forEach(element => {
	      if (isEllipsisActive(element)) {
	        element.title = element.innerText;
	        // $('[data-toggle="tooltip"]').tooltip()
	      }
	    });
	  });
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuvTR6aAp_y_rcc_w1m087uc9jh84V3Qk&libraries=places&callback=initAutocomplete&language=mk&region=MKD"></script>
@endsection
