@extends('layouts.app')
@section('content')
<div class="pt-5 pt-xs-0">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1 class="m-0">Мои закажени патувања</h1>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      @foreach ($booked_rides as $ride)
      <?php $bfrom = $ride->b_from; $bto = $ride->b_to; $btime = $ride->b_time;?>
      <div class="panel panel-patuvanja my-5 my-xs-2" id="{{$ride->ride_id}}">
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-12 col-md-5">
              <p> </p>
              <div class="destinacii">
                <div class="bulet">
                  <div class="roundChecked"></div>
                  @if($ride->d_stop_1 != '')
                  <div class="lineChecked"></div>
                  <div class="roundUnChecked"></div>
                  @endif
                  @if($ride->d_stop_2 != '')
                  <div class="lineUnChecked"></div>
                  <div class="roundUnChecked"></div>
                  @endif
                  <div class="lineChecked"></div>
                  <div class="roundChecked"></div>
                </div>
                <div class="destinacija destinacija-pocetna">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-11 px-xs-0 center">
                    <p class="lead m-0 preseci" data-toggle="tooltip" data-placement="top">{{ Helper::rmCountry($ride->d_from) }}</p>
                  </div>
                </div>
                @if($ride->d_stop_1 != '')
                <div class="destinacija destinacija-prva">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-11 px-xs-0 center">
                    <p class="m-0 preseci" data-toggle="tooltip" data-placement="top">{{ Helper::Short_Destination($ride->d_stop_1) }}</p>
                  </div>
                </div>
                @endif
                @if($ride->d_stop_2 != '')
                <div class="destinacija destinacija-vtora">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-11 px-xs-0 center">
                    <p class="m-0 preseci" data-toggle="tooltip" data-placement="top">{{ Helper::Short_Destination($ride->d_stop_2) }}</p>
                  </div>
                </div>
                @endif
                <div class="destinacija destinacija-krajna">
                  <div class="col-xs-1"></div>
                  <div class="col-xs-11 px-xs-0 center">
                    <p class="lead m-0 preseci" data-toggle="tooltip" data-placement="top">{{ Helper::rmCountry($ride->d_to) }}</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-7">
              <div class="row mt-xs-15">
                <div class="col-xs-12 col-md-8">
                  <div class="col-xs-6 col-md-6">
                    <p>Поаѓање:</p>
                    <p class="lead mb-xs-0"><i class="fas fa-calendar-alt text-primary"></i> {{ Carbon\Carbon::parse($ride->d_date)->format('d.m.Y') }}</p>
                    <p class="lead mb-xs-0"><i class="fas fa-clock text-primary"></i> {{$ride->d_time}}</p>
                  </div>
                  {{-- @if($ride->roundtrip == 1)
                    <div class="col-xs-6 col-md-6">
                      <p>Враќање:</p>
                      <p class="lead mb-xs-0"><i class="fas fa-calendar-alt text-primary"></i> {{ Carbon\Carbon::parse($ride->d_rdate)->format('d.m.Y') }}</p>
                      <p class="lead mb-xs-0"><i class="fas fa-clock text-primary"></i> {{$ride->d_rtime}}</p>
                    </div>
                  @endif --}}
                </div>
                <div class="col-xs-12 col-md-4 mt-xs-1">
                  <div class="status">
                    <p>Статус Патување:</p>
                    <span class="label label-primary">{{Helper::Ride_Status($ride->status)}}</span>
                  </div>
                  <div class="status status-rezervacija">
                    <p>Статус резервација:</p>
                    <span class="label label-primary">{{Helper::Status_Message($ride->book_status)}}</span>
                  </div>
                  <p class="text-strong">Слободни Места: {{Helper::zero_beautify($ride->r_qty_places)}}</p>
                </div>
              </div>
            </div>
            @if($ride->status == 'completed')
            <button id='remove_{{$ride->ride_id}}' class="removeBtn btn btn-danger">X</button>
            @endif
          </div>
        </div>
        <a href="/rides/detailed-view/{{$ride->ride_id}}" class="btn btn-primary btn-block btn-lg">Види повеќе <i class="fas fa-angle-double-right"></i></a>
      </div>
      @endforeach
      {{ $booked_rides->links() }}
    </div>
  </div>
</div>
</div>
@endsection

@section('includesscripts')
<script>
$('.removeBtn').click(function(){

  var rowID = $(this).attr('id').replace('remove_','');

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  $.ajax({
    url: "/rides/actions/remove_booking",
    type: 'post',
    data: {
      id: rowID
    },
    success: function(result){
      console.log(result);
    }});

    $('div #'+rowID).fadeOut(300, function(){ $(this).remove();});
  });
  $(function() {
      function isEllipsisActive(element) {
        return element.offsetWidth < element.scrollWidth;
      }
      Array.from(document.querySelectorAll('.preseci'))
      .forEach(element => {
        if (isEllipsisActive(element)) {
          element.title = element.innerText;
        }
      });
    });
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
</script>
@endsection
