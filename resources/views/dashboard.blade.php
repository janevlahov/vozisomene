@extends('layouts.app')

@section('content')
<section class="bg-light-gradient py-5">
<div class="container">
  <div class="row">
    <div class="col-md-7 vcenter">
      <h1 class="mt-0">Успешно се најавивте</h1>
    </div><!--
    --><div class="col-md-5 vcenter">
      <p class="text-muted text-right">Како слушнавте и што мислите за Возисомене? <a href="#" class="btn btn-default">Кажете ни</a></p>
    </div>
  </div>
  <div class="row">
  <div class="col-md-4">
    <div class="panel panel-primary panel-profil">
      @if(!empty($user->avatar))<img class="slika" src="{{asset('storage/upload/users/'.$user->avatar)}}" alt="{{$user->firstname}} {{$user->lastname}}">
      @else<img class="slika" src="/storage/upload/web/nouser.jpg" alt="Nema slika">
      @endif
      <!-- <img class="slika" src="http://vozi.morgensternmarketing.de/storage/upload/users/1545296519.png" alt="..."> -->
      <a href="{{ url('/users/profile/avatar') }}" class="promeni-slika">
        <span class="fa-stack fa-1x">
          <i class="fas fa-circle fa-stack-2x"></i>
          <i class="fas fa-camera fa-stack-1x fa-inverse"></i>
        </span>
      </a>
      <div class="panel-body text-center">
        <p class="lead ime">Ime Prezime</p>
        <p class="small">Член од 02.2019</p>
        <p>mail@mailt.com</p>
        <p>078 123 456</p>
        <a href="{{ url('/users/profile/general') }}" class="btn btn-primary"><i class="fas fa-user-edit"></i> Измени податоци</a>
      </div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-info panel-profil">
          <div class="panel-heading">
            <h3><i class="fas fa-bell"></i> Известувања</h3>
          </div>
          <div class="panel-body">
            <div class="list-group">
              <a href="#" class="list-group-item active">
                <h4 class="list-group-item-heading">List group item heading</h4>
                <p class="list-group-item-text">...</p>
              </a>
            </div>
            <div class="list-group">
              <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">List group item heading</h4>
                <p class="list-group-item-text">...</p>
              </a>
            </div>
            <div class="list-group">
              <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">List group item heading</h4>
                <p class="list-group-item-text">...</p>
              </a>
            </div>
            <a href="#" class="btn btn-link"><i class="fas fa-bell"></i> Види ги сите</a>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="panel panel-info panel-profil">
          <div class="panel-heading">
            <h3><i class="fas fa-map-marked-alt"></i> Мои патувања</h3>
          </div>
          <div class="panel-body">
            <table class="table">
              <tbody>
                <tr>
                  <td class="lead">Скопје</td>
                  <td class="lead"><i class="fas fa-angle-double-right"></i></td>
                  <td class="lead">Струмица</td>
                </tr>
                <tr>
                  <td class="lead">Скопје</td>
                  <td class="lead"><i class="fas fa-angle-double-right"></i></td>
                  <td class="lead">Скопје</td>
                </tr>
                <tr>
                  <td class="lead">Струмица</td>
                  <td class="lead"><i class="fas fa-angle-double-right"></i></td>
                  <td class="lead">Штип</td>
                </tr>
                <tr>
                  <td class="lead">Битола</td>
                  <td class="lead"><i class="fas fa-angle-double-right"></i></td>
                  <td class="lead">Струмица</td>
                </tr>
              </tbody>
            </table>
            <a href="{{ url('/rides') }}" class="btn btn-link"><i class="fas fa-map-marked-alt"></i> Види ги сите</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 p-0">
        <div class="vozime">
          <div class="tekst">
            <h2>Барам патување</h2>
            <p class="lead">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <a href="{{ url('/rides/search-ride') }}" class="btn btn-primary btn-lg">Пребарувај патувања</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 p-0">
        <div class="vozam">
          <div class="tekst">
            <h2>Нудам патување</h2>
            <p class="lead">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <a href="{{ url('/rides/create') }}" class="btn btn-primary btn-lg">Додади патување</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
