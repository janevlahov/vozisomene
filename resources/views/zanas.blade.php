@extends('layouts.app')
@section('content')
<section class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="mt-0 text-center">За нас</h1>
      </div>
    </div>
    <div class="row zanas">
      <div class="col-md-12 bg-white p-2">
        <div class="row mb-4">
          <div class="col-md-4">
            <h2 class="mt-0 text-center">За нас <br>Возисомене</h2>
            <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p> -->
          </div>
          <div class="col-md-8">
            <p>Возисомене е car sharing веб страна која има за цел да помогне на луѓето кои патуваат на иста дестинација да се пронајдат едни со други. Возачите кои имаат празни места во својот автомобил можат да го објават своето патување така што ќе бидат пронајдени од патниците кои патуваат на иста крајна дестинација.</p>
            <p>Со овој начин на превоз возачите нема да патуваат сами, а патниците ќе го избегнат чекањето по станици, долгите редици, доцнењето на јавниот превоз и малтретирањето. Делејќи го превозот заштедувате време и пари, патувате безбедно и удобно, запознавате нови луѓе и се забавувате.</p>
            <p>Нашиот тим се стреми и работи на подобрување на вебстраната затоа секоја критика е додредојдена.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/najava.jpg')}}" class="img-responsive" alt="">
          </div>
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/dashboard123.jpg')}}" class="img-responsive" alt="">
          </div>
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/home-1.jpg')}}" class="img-responsive" alt="">
          </div>
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/bg-kontakt.jpg')}}" class="img-responsive" alt="">
          </div>
        </div>
        <div class="row my-2">
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/dashboard.jpg')}}" class="img-responsive" alt="">
          </div>
          <div class="col-md-6 text-center py-2">
            <p class="lead">Лорем ипсум</p>
            <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/bg-reviews.jpg')}}" class="img-responsive" alt="">
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/bg-ride.jpg')}}" class="img-responsive" alt="">
          </div>
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/bg-dodadi.jpg')}}" class="img-responsive" alt="">
          </div>
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/login.jpg')}}" class="img-responsive" alt="">
          </div>
          <div class="col-md-3 zanas-slika">
            <img src="{{asset('storage/upload/web/najava.jpg')}}" class="img-responsive" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
