<footer class="bg-light">
  <div class="container py-5 pb-xs-0">
    <div class="row">
      <div class="col-md-4">
        <ul class="footer-links fa-ul mb-0">
          <li class="{{ request()->is('kako-raboti') ? 'active' : '' }} mb-1">
            <a href="{{ url('kako-raboti') }}"><span class="fa-li"><i class="fas fa-angle-right text-secondary"></i></span> Како работи</a>
          </li>
          <li class="{{ request()->is('za-nas') ? 'active' : '' }} mb-1">
            <a href="{{ url('za-nas') }}"><span class="fa-li"><i class="fas fa-angle-right text-secondary"></i></span> За нас</a>
          </li>
          <li class="{{ request()->is('najchesto-postavuvani-prasanja') ? 'active' : '' }} mb-1">
            <a href="{{ url('najchesto-postavuvani-prasanja') }}"><span class="fa-li"><i class="fas fa-angle-right text-secondary"></i></span> Најчесто поставувани прашања</a>
          </li>
        </ul>
      </div>
      <div class="col-md-4">
        <ul class="footer-links fa-ul mb-0">
          <li class="{{ request()->is('kontakt') ? 'active' : '' }} mb-1">
            <a href="{{ url('kontakt') }}"><span class="fa-li"><i class="fas fa-angle-right text-secondary"></i></span> Контакт</a>
          </li>
          <li class="{{ request()->is('uslovi-za-koristenje') ? 'active' : '' }} mb-1">
            <a href="{{ url('uslovi-za-koristenje') }}"><span class="fa-li"><i class="fas fa-angle-right text-secondary"></i></span> Услови за користење</a>
          </li>
          <li class="{{ request()->is('politika-za-privatnost') ? 'active' : '' }} mb-1">
            <a href="{{ url('politika-za-privatnost') }}"><span class="fa-li"><i class="fas fa-angle-right text-secondary"></i></span> Политика за приватност</a>
          </li>
        </ul>
      </div>
      <div class="col-md-4 text-rght mt-xs-2">
        <ul class="list-inline">
          <li><a href="#"><i class="fab fa-facebook fa-3x text-facebook"></i></a></li>
          <li><a href="#"><i class="fab fa-instagram fa-3x text-instagram"></i></a></li>
          <li><a href="#"><i class="fab fa-youtube fa-3x text-youtube"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="copyright bg-white py-3 py-xs-2">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          &copy; <?php echo date("Y"); ?> <a href="https://morgenstern.mk" target="_blank">Моргенстерн ДООЕЛ</a>
        </div>
      </div>
    </div>
  </div>
</footer>
