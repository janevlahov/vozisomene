<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">

      <!-- Collapsed Hamburger -->
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="{{ url('/') }}"><img src="{{asset('/storage/upload/web/vozi_so_mene-logo.jpg')}}" alt="{{config('app.name', 'Возисомене')}}"></a>
    </div>

    <div class="collapse navbar-collapse" id="app-navbar-collapse">
      @guest

        @if (!Request::is('admin/*'))
          <ul class="nav navbar-nav navbar-right">
            <li class="{{ request()->is('login') ? 'active' : '' }}"><a href="{{ route('login') }}">Најава</a></li>
            <li class="{{ request()->is('register') ? 'active' : '' }}"><a href="{{ route('register') }}">Регистрација</a></li>
          </ul>
        @endif

      @else
        <ul class="nav navbar-nav navbar-right meni-profil">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle notifikacii" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
              <i class="fas fa-bell fa-2x"></i>
              @if(count(auth()->user()->unreadnotifications) > 0)
                <span id='notificationCounter' class="badge badge-danger">{{count(auth()->user()->unreadnotifications)}}</span>
              @endif
            </a>
            <ul id='notificationsList' class="dropdown-menu">
              @if(count(auth()->user()->unreadnotifications) > 0)
                @foreach(auth()->user()->unreadnotifications as $notification)
                  @if(!empty($notification->data['title']))
                    <li class="notification"><a href="{{url('notifications/show/'.$notification->id)}}">{{$notification->data['title']}}</a></li>
                    <li role="separator" class="divider"></li>
                  @endif
                @endforeach
                <a href="#" id='markAllread' class="text-center btn btn-link" onclick="MarkAllAsRead()">Обележи како прочитани</a>
              @else
                <li><a href="#" class="text-center btn btn-link">Немаш нови известувања</a></li>
              @endif
            </ul>
          </li>
          <li class="dropdown avatar">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
              {{ Auth::user()->firstname }}
              @if(!empty(auth()->user()->Userdetails->avatar))<img src="{{asset('storage/upload/users/'.auth()->user()->Userdetails->avatar)}}" alt="{{auth()->user()->firstname}} {{auth()->user()->lastname}}">
              @else<img src="/storage/upload/web/nouser.jpg" alt="Nema slika">
              @endif
              <i class="fas fa-sort-down"></i>
            </a>
            <ul class="dropdown-menu">
              @if((new \Jenssegers\Agent\Agent())->isMobile())<li>TestMobile</li>@endif
              <li><a href="{{ url('/dashboard') }}"><i class="fas fa-tachometer-alt text-secondary"></i> Преглед</a></li>
              <li><a href="{{ url('/users/profile/general') }}"><i class="fas fa-user text-secondary"></i> Профил</a></li>
              <li><a href="{{ url('/rides') }}"><i class="fas fa-map text-secondary"></i> Мои Патувања</a></li>
              <li><a href="{{ url('/rides/my-booked') }}"><i class="fas fa-map-marked-alt text-secondary"></i> Мои закажени патувања</a></li>
              <li role="separator" class="divider"></li>
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt text-secondary"></i> Одјави се</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
              </li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav meni">
          <li class="{{ request()->is('/rides/search-ride') ? 'active' : '' }}"><a href="{{ url('/rides/search-ride') }}">Барај Патување</a></li>
          <li class="{{ request()->is('/rides/create') ? 'active' : '' }}"><a href="{{ url('/rides/create') }}">Додади Патување</a></li>
        </ul>
      @endguest
    </div>
  </div>
</nav>
@section('includesscripts')

<script>
@auth
$('#markAllread').click(function(){

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
  $.ajax({
    url: "{{ route('markAllRead') }}",
    type: 'post',
    data: {},

    success: function(result){
      $("#notificationCounter").text("");
      $(".notification, #markAllread").remove();
      $("#notificationsList").append("<li><a href='#''>Немате нови известувања</a></li>");
    },
    error: function (err) {
      console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
    }
  });
});
@endauth
</script>

@endsection
