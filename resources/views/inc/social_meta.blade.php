 @if(request()->is('rides/detailed-view/*'))
      @if(!empty($urlId) && !empty($rideFrom) && !empty($rideTo))
        <meta property="og:url" content="{{ url('/rides/detailed-view/') }}/{{$urlId}}"/>
        <meta property="og:type" content="website"/>
        <meta property="og:site_name" content="Возисомене"/>
        <meta property="og:image" content="{{asset('/storage/upload/web/FB_Share_1920x1080.jpg')}}"/>
        <meta property="og:title" content="{{$rideFrom}} >> {{$rideTo}}" />
        <meta property="og:description" content="Имам {{$rideRQtyPlaces}} слободни места од {{$rideFrom}} до {{$rideTo}}. Кликни овде да резервираш."/>
        <meta name="twitter:site" content="{{ url('/rides/detailed-view/') }}/{{$urlId}}" />
        <meta name="twitter:title" content="{{$rideFrom}} >> {{$rideTo}}">
        <meta name="twitter:description" content="Имам {{$rideRQtyPlaces}} слободни места од {{$rideFrom}} до {{$rideTo}}. Кликни овде да резервираш.">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:image" content="{{asset('/storage/upload/web/FB_Share_1920x1080.jpg')}}">
        <meta name="twitter:image:alt" content="Возисомене">
      @endif
    @elseif(request()->is('rides/*'))
      @if(!empty($urlId) && !empty($rideFrom) && !empty($rideTo))
        <meta property="og:url" content="{{ url('/rides') }}/{{$urlId}}"/>
        <meta property="og:type" content="website"/>
        <meta property="og:site_name" content="Возисомене"/>
        <meta property="og:image" content="{{asset('/storage/upload/web/FB_Share_1920x1080.jpg')}}"/>
        <meta property="og:title" content="{{$rideFrom}} >> {{$rideTo}} " />
        <meta property="og:description" content="Имам {{$rideRQtyPlaces}} слободни места од {{$rideFrom}} до {{$rideTo}}. Кликни овде да резервираш."/>
        <meta name="twitter:site" content="{{ url('/rides') }}/{{$urlId}}" />
        <meta name="twitter:title" content="{{$rideFrom}} >> {{$rideTo}}">
        <meta name="twitter:description" content="Имам {{$rideRQtyPlaces}} слободни места од {{$rideFrom}} до {{$rideTo}}. Кликни овде да резервираш.">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:image" content="{{asset('/storage/upload/web/FB_Share_1920x1080.jpg')}}">
        <meta name="twitter:image:alt" content="Возисомене">
      @endif
    @else
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="{{ url('') }}"/>
      <meta property="og:site_name" content="Возисомене"/>
      <meta property="og:image" content="{{asset('/storage/upload/web/fb_share.png')}}"/>
      <meta property="og:title" content="Оди буквално насекаде. Од секаде."/>
      <meta property="og:description" content="Со пристап до многу патувања, можеш да ги пронајдеш луѓето од твоето опкружување што патуваат каде што патуваш и ти."/>
      <meta name="twitter:card" content="summary_large_image">
  	  <meta name="twitter:image" content="{{asset('/storage/upload/web/fb_share.png')}}">
  	  <meta name="twitter:image:alt" content="Возисомене">
  	  <meta name="twitter:title" content="Оди буквално насекаде. Од секаде.">
  	  <meta name="twitter:description" content="Со пристап до многу патувања, можеш да ги пронајдеш луѓето од твоето опкружување што патуваат каде што патуваш и ти.">
  	  <meta name="twitter:site" content="{{ url('') }}" />
    @endif
