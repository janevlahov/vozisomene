@extends('admin.layouts')

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Корисници
        <small>Табеларно</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
        <li class="active">Корисници</li>
      </ol>
    </section>
 <!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Табела на корисници</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="users_table" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Име и Презиме</th>
							<th>Е-маил</th>
							<th>Телефон</th>
							<th>Регистриран на</th>
							<th>Активен</th>
							<th>Причина</th>
							<th>Акција</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
							<tr>
								<td>{{$user->id}}</td>
								<td>{{$user->firstname}} {{$user->lastname}}</td>
								<td>{{$user->email}}</td>
								<td>{{$user->phone}}</td>
								<td>{{$user->created_at}}</td>
								<td>{{Helper::bool_beautify($user->active)}}</td>
								<td>{{$user->reason}}</td>
								<td><a href="view-user/{{$user->id}}"><i class="far fa-eye"></i></a> <a href="edit-user/{{$user->id}}"> <i class="fas fa-edit"></i></a> <a href="mailto:{{$user->email}}" target="_top"> <i class="fas fa-envelope"></i></a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</section>
@endsection

@section('includecss')
	  <link rel="stylesheet" href="{{ asset('css/admin-lte/dataTables.bootstrap.min.css')}}">
@endsection

@section('includesscripts')
<script src="{{ asset('js/admin-lte/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('js/admin-lte/dataTables.bootstrap.min.js') }}"></script>
<script>
	$(document).ready( function () {
    	$('#users_table').DataTable({
    		"order": [[ 4, "desc" ]],
    		"language": {
	          "paginate": {
	            "next": "Следна",
	            "previous": "Претходна"
	          },
          		"search": "Пребарувај:",
          		"lengthMenu": "Прикажи _MENU_ записи",
          		"info": "Прикажани се од _START_ до _END_ од вкупно _TOTAL_ записи",
          		"infoEmpty": "Нема податоци",
         		"emptyTable": "Нема податоци"
	        }
    	});
	} );
</script>
@endsection