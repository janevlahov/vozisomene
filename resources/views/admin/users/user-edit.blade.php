@extends('admin.layouts')

@section('content')
  <section class="content-header">
    <h1>Корисник <small>Промена</small></h1>
    <ol class="breadcrumb">
      <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
      <li class="active">Промена на корисник</li>
    </ol>
  </section>

  <section class="content">
    <form action="{{route('admin.updateUser', $user->id)}}" method="POST" enctype=multipart/form-data>
      <div class="box">
        <div class="box-header with-border">
         <div class="row">
          <div class="col-lg-1 col-xs-6 text-center">
            {!! Helper::userAvatarAdmin($user->avatar, '','',$user->id) !!}
          </div>
          <div class="col-lg-1 col-xs-6">
            <h3 class="box-title">{{$user->firstname}} {{$user->lastname}}</h3>
            @if($user_rating == '') <p>Без оценки</p> @else <div class='user_rating'> {{$user_rating}} </div> @endif
          </div>
          <div class="col-lg-10 col-xs-6 text-right">
            <input type="submit" class="btn btn-primary upload-result" value="Зачувај"> 
            <a href="/admin/view-user/{{$user->id}}" class="btn btn-primary">Назад</a>
          </div>
        </div>
      </div>
      <div class="box-header with-border">
        <div class="row">
            <div class="col-sm-6 col-lg-1"><h4>Активен:</h4></div> 
            <div class="col-sm-6 col-lg-1">
              <div class="checkbox">
                <input type="checkbox" id='active_user' name="active_user" onchange="changeVal(this)" value="{{$user->active}}" @if($user->active == 1) checked="checked" @endif data-toggle="modal" data-target="#modal-danger">
              </div>
            </div>
            <div class="col-sm-12 col-lg-10">
              <div class="form-group"><textarea name="block_reason" id="block_reason" class="form-control" rows="1" placeholder="Причина за деактивација на корисничка сметка">{{$user->reason}}</textarea></div>
            </div>
        </div>
      </div>
      <div class="box-body">
        @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif

        <div class="row">
          <div class="col-lg-4">
            <div class="row">
              <div class="col-lg-12"><h4>Лични податоци</h4></div>	
            </div>

            <div class="row">
              <div class="col-sm-6 col-lg-5">Име:</div>	
              <div class="col-sm-6 col-lg-7"><div class="form-group"><input type="text" name="firstname" class="form-control" value="{{$user->firstname}}"></div></div>	
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-5">Презиме:</div> 
              <div class="col-sm-6 col-lg-7"><div class="form-group"><input type="text" name="lastname" class="form-control" value="{{$user->lastname}}"></div></div> 
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-5">Е-маил:</div> 
              <div class="col-sm-6 col-lg-7"><div class="form-group"><input type="text" name="email" class="form-control" value="{{$user->email}}" required></div></div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-5">Телефон:</div> 
              <div class="col-sm-6 col-lg-7"><div class="form-group"><input type="text" name="phone" class="form-control" value="{{$user->phone}}"></div></div>
            </div>
          </div>
          <div class="col-lg-4">
           <div class="row">
            <div class="col-lg-12"><h4>Адреса</h4></div>	
          </div>
          <div class="row">
            <div class="col-sm-6 col-lg-6">Улица:</div>	
            <div class="col-sm-6 col-lg-6"><div class="form-group"><input type="text" name="address" class="form-control" value="{{$user->address}}"></div></div>	
          </div>
          <div class="row">
            <div class="col-sm-6 col-lg-6">Поштенски Број:</div>	
            <div class="col-sm-6 col-lg-6"><div class="form-group"><input type="text" name="zip" class="form-control" value="{{$user->zip}}"></div></div>	
          </div>

          <div class="row">
            <div class="col-sm-6 col-lg-6">Град:</div>  
            <div class="col-sm-6 col-lg-6"><div class="form-group"><input type="text" name="city" class="form-control" value="{{$user->city}}"></div></div> 
          </div>

          <div class="row">
            <div class="col-sm-6 col-lg-6">Држава:</div>	
            <div class="col-sm-6 col-lg-6">
              <div class="form-group">
                <select class="form-control" id="state" name='state'>
                  @if(count($StatesList) > 0)
                  @if($user->state == false)
                  <option value="" disabled="" selected="">Избери Држава</option>
                  @endif
                  @foreach($StatesList as $State)
                  <option value="{{$State}}" @if($State == $user->state) selected='selected' @endif>{{$State}}</option>
                  @endforeach
                  @endif
                </select>
              </div>
            </div>	
          </div>
        </div>
      <div class="col-lg-4">
        <div class="row">
          <div class="col-lg-12"><h4>Био</h4></div>	
        </div>
        <div class="row">
          <div class="col-lg-12"><div class="form-group"><textarea name="bio" id="" class="form-control" rows="6">{{$user->bio}}</textarea></div></div>	
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-5">Дата на раѓање:</div>  
          <div class="col-sm-6 col-lg-7">
            <div class="form-group">
              <select class="form-control" id="birthday" name='birthday'>
                @if(count($BirthdayDates) > 0)
                  @if($user->birthday == false)
                    <option value="" disabled="" selected="">Одберете година на раѓање</option>
                  @endif
                  @foreach($BirthdayDates as $BirthdayDate)
                    <option value="{{$BirthdayDate}}" @if($BirthdayDate == $user->birthday) selected='selected' @endif>{{$BirthdayDate}}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>  
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-5">Пол:</div> 
          <div class="col-sm-6 col-lg-7">
            <div class="form-group">
              <label class="radio-inline">
                <input type="radio" name="sex" value="male" @if($user->sex == 'male') checked="checked" @endif>Машки
              </label>
              <label class="radio-inline">
                <input type="radio" name="sex" value="female" @if($user->sex == 'female') checked="checked" @endif>Женски
              </label>
              <label class="radio-inline">
                <input type="radio" name="sex" value="" @if($user->sex == '') checked="checked" @endif>n/a
              </label>
            </div>
          </div>
          </div>
      </div>
    </div>

    </div>
    </div>

    <div class="box">
     <div class="box-body">
      <div class="row">
        <div class="col-lg-4">
          <div class="row">
           <div class="col-lg-12"><h4>Автомобил</h4></div>	
         </div>
         <div class="row">
           <div class="col-sm-6 col-lg-5">Број на регистарска табличка:</div>	
           <div class="col-sm-6 col-lg-7"><div class="form-group"><input type="text" name="licence_plate" class="form-control" value="{{$user->licence_plate}}"></div></div>	
         </div>
         <div class="row">
          <div class="col-sm-6 col-lg-5">Држава на регистрација:</div>	
          <div class="col-sm-6 col-lg-7">
            <div class="form-group">
            <select class="form-control" id="country_code" name='licence_state'>
              @if(count($CountryCodes) > 0)
                @if($user->licence_state == false)
                  <option value="" disabled="" selected="">Држава на регистрација</option>
                @endif
                @foreach($CountryCodes as $CountryCode => $Country)
                  <option value="{{$CountryCode}}" @if($CountryCode == $user->licence_state) selected='selected' @endif>{{$Country}}</option>
                @endforeach
              @endif
            </select>
          </div>
          </div>	
         </div>
          <div class="row">
            <div class="col-lg-12"><h4>Тип на автомобил</h4></div> 
          </div>
          <div class="row">
            <div class="col-sm-6 col-lg-5">Година на производтство:</div>  
            <div class="col-sm-6 col-lg-7">
              <div class="form-group">
                <select class="form-control" name="car_years" id="car-years">
                  @php 
                    $YearBegin = 1985;
                    $YearNow = date("Y"); 
                  @endphp
                  <option value="">Избери Година</option>
                  @for($i = intval($YearNow); $i >= $YearBegin; $i--)
                    <option value="{{$i}}" @if($user->year_production == $i) selected @endif>{{$i}}</option>
                  @endfor
                </select>
              </div>
            </div>
          </div>
         <div class="row">
           <div class="col-sm-6 col-lg-5">Марка на автомобил:</div>
           <div class="col-sm-6 col-lg-7">
              <div class="form-group">
                @if(count($CarBrands) > 0)
                  <select class="form-control" name="car_makes" id="car-makes">
                    <option value="">Избери Марка</option>
                    @for($i = 0; $i < count($CarBrands); $i++)
                      <option value="{{$CarBrands[$i]}}" @if($user->car_brand == $CarBrands[$i]) selected @endif>{{$CarBrands[$i]}}</option>
                    @endfor
                  </select>
                @endif
              </div>
            </div>
         </div>
         <div class="row">
           <div class="col-sm-6 col-lg-5">Модел на автомобил:</div> 
           <div class="col-sm-6 col-lg-7">
              <div class="form-group">
                <select class="form-control" name="car_models" id="car-models"></select>
              </div>
            </div>  
         </div>
       </div>

      <div class="col-lg-3">
        <div class="row">
          <div class="col-lg-12"><h4>Тип Автомобил</h4></div> 
        </div>
         <div class="row">
           <div class="col-sm-6 col-lg-7">
            <div class="form-group">
              @if(count($TypeNames) > 0)
                @foreach($TypeNames as $TypeName)
                
                  <div class="radio">
                    <label><input type="radio" name="car_type" value='{{$TypeName}}' @if($TypeName == $user->type) checked="checked" @endif>{{$TypeName}}</label>
                  </div>
               
                @endforeach
              @endif
              </div>
            </div>
         </div>
      </div>

      <div class="col-lg-3">
         <div class="row">
          <div class="col-lg-12"><h4>Боја на автомобил</h4></div> 
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-6">
            <div class="form-group">
            @if(count($ColourNames) > 0)
              @foreach($ColourNames as $ColourName)
                <div class="radio">
                    <label><input type="radio" name="car_color" value='{{$ColourName}}' @if($ColourName == $user->color) checked="checked" @endif>{{$ColourName}}</label>
                </div>
              @endforeach
            @endif
          </div>
          </div>
        </div>
      </div>

      <div class="col-lg-2">
        <div class="row">
          <div class="col-lg-12"><h4>Фотографија</h4></div>	
        </div>
        <div class="row">
          <div class="col-lg-12">
            @if(!empty($user->car_photo))
              <img src="{{asset('storage/upload/cars/'.$user->car_photo)}}" id='car_image' class="slika media-object mr-3" alt="">
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div id="krop" class="krop hidden mb-3">
              <span class="small text-white text-strong">Влечи за подобро позиционирање</span>
              <div id="upload-demo"></div>
              <p class="small text-center">Сечи ја сликата ако е потребно</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
          
            <div class="input-file-container mx-auto">
              <input type="file" id="upload" name='car_img' class="input-file">
              <label tabindex="0" for="upload" class="input-file-trigger btn btn-primary mx-auto">Одбери слика...</label>
              <p class="file-return"></p>
            </div>
            <input type="hidden" name="auto_image" id='auto_image'>
            <input type="hidden" name="auto_image_name" id='auto_image_name'>
            <!--   <button type="button" class="btn btn-primary btn-icon-split upload-result">
              <span class="icon text-white-50"><i class="fas fa-save"></i></span>
              <span class="text">Зачувај Слика</span>
            </button> -->
          </div>
        </div>
      </div>
      </div>

    </div>

    </div>

    <div class="box">
      <div class="box-body">
        <div class="row">
          <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-12"><h4>Однесувања во автомобил</h4></div> 
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Зборлив:</div>	
              <div class="col-sm-6 col-lg-6">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-default @if($user->talkative == 'talk_1') active @endif">
                    <input type="radio" name="talkative" id="talk_1" value="talk_1" @if($user->talkative == 'talk_1') checked="checked" @endif>
                    <span class="fa-stack fa-1x" data-toggle="tooltip" data-placement="top" title="Јас сум тивок човек">
                      <i class="fas fa-comments fa-stack-1x text-muted"></i>
                      <i class="fas fa-ban fa-stack-1x text-danger"></i>
                    </span>
                  </label>
                  <label class="btn btn-default @if($user->talkative == 'talk_2') active @endif">
                    <input type="radio" name="talkative" id="talk_2" value="talk_2" @if($user->talkative == 'talk_2') checked="checked" @endif>
                    <i class="fas fa-comments fa-1x text-muted" data-toggle="tooltip" data-placement="top" title="Разговарам во зависност од расположението"></i>
                  </label>
                  <label class="btn btn-default @if($user->talkative == 'talk_3') active @endif">
                    <input  type="radio" name="talkative" id="talk_3" value="talk_3" @if($user->talkative == 'talk_3') checked="checked" @endif>
                    <i class="fas fa-comments fa-1x text-primary" data-toggle="tooltip" data-placement="top" title="Возењето неможе да помине без раговор"></i>
                  </label>
                </div>
                
              </div>	
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Пушач:</div>	
              <div class="col-sm-6 col-lg-6">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-default @if($user->smoker == 'smoke_1') active @endif">
                    <input type="radio" name="smoker" value="smoke_1" @if($user->smoker == 'smoke_1') checked="checked" @endif>
                    <span class="fa-stack fa-1x" data-toggle="tooltip" data-placement="top" title="Не дозволувам пушење во автомобилот">
                      <i class="fas fa-joint fa-stack-1x text-muted"></i>
                      <i class="fas fa-ban fa-stack-1x text-danger"></i>
                    </span>
                  </label>
                  <label class="btn btn-default @if($user->smoker == 'smoke_2') active @endif">
                    <input type="radio" name="smoker" value="smoke_2"  @if($user->smoker == 'smoke_2') checked="checked" @endif>
                    <i class="fas fa-joint fa-1x text-muted" data-toggle="tooltip" data-placement="top" title="Понекогаш дозволувам пушење во автомобилот"></i>
                  </label>
                  <label class="btn btn-default @if($user->smoker == 'smoke_3') active @endif">
                    <input type="radio" name="smoker" value="smoke_3"  @if($user->smoker == 'smoke_3') checked="checked" @endif>
                    <i class="fas fa-joint fa-1x text-primary" data-toggle="tooltip" data-placement="top" title="Дозволувам пушење во автомобилот"></i>
                  </label>
                </div>

              </div>	
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Миленичиња:</div>	
              <div class="col-sm-6 col-lg-6">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-default @if($user->pets == 'pets_1') active @endif">
                    <input type="radio" name="pets" value="pets_1"  @if($user->pets == 'pets_1') checked="checked" @endif>
                    <span class="fa-stack fa-1x" data-toggle="tooltip" data-placement="top" title="Без миленичиња во автомобилот">
                      <i class="fas fa-paw fa-stack-1x text-muted"></i>
                      <i class="fas fa-ban fa-stack-1x text-danger"></i>
                    </span>
                  </label>
                  <label class="btn btn-default @if($user->pets == 'pets_2') active @endif">
                    <input type="radio" name="pets" value="pets_2" @if($user->pets == 'pets_2') checked="checked" @endif>
                    <i class="fas fa-paw fa-1x text-muted" data-toggle="tooltip" data-placement="top" title="Зависи за какво милениче се работи"></i>
                  </label>
                  <label class="btn btn-default @if($user->pets == 'pets_3') active @endif">
                    <input type="radio" name="pets" value="pets_3" @if($user->pets == 'pets_3') checked="checked" @endif>
                    <i class="fas fa-paw fa-1x text-primary" data-toggle="tooltip" data-placement="top" title="Миленичињата се добредојдени"></i>
                  </label>
                </div>
              </div>	
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Музика:</div>  
              <div class="col-sm-6 col-lg-6">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-default @if($user->music == 'music_1') active @endif">
                    <input type="radio" name="music" value="music_1" @if($user->music == 'music_1') checked="checked" @endif>
                    <span class="fa-stack fa-1x" data-toggle="tooltip" data-placement="top" title="Тишината е закон">
                      <i class="fas fa-music fa-stack-1x text-muted"></i>
                      <i class="fas fa-ban fa-stack-1x text-danger"></i>
                    </span>
                  </label>
                  <label class="btn btn-default @if($user->music == 'music_2') active @endif">
                    <input type="radio" name="music" value="music_2" @if($user->music == 'music_2') checked="checked" @endif>
                    <i class="fas fa-music fa-1x text-muted" data-toggle="tooltip" data-placement="top" title="Слушам во зависност од расположението"></i>
                  </label>
                  <label class="btn btn-default @if($user->music == 'music_3') active @endif">
                    <input type="radio" name="music" value="music_3" @if($user->music == 'music_3') checked="checked" @endif>
                    <i class="fas fa-music fa-1x text-primary" data-toggle="tooltip" data-placement="top" title="Патувањето неможе да помине без музика"></i>
                  </label>
                </div>
              </div> 
            </div>
        </div>
      <div class="col-lg-5">
        <div class="row">
          <div class="col-lg-12"><h4>Е-маил Известувања</h4></div> 
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-6">Ново патување:</div> 
          <div class="col-sm-6 col-lg-6"><input type="checkbox" id='new_ride' name="new_ride" value="{{$user->new_ride}}" @if($user->new_ride == 1) checked="checked" @endif></div> 
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-6">Направена измена на патувањето:</div> 
          <div class="col-sm-6 col-lg-6"><input type="checkbox" name="update_ride" value="{{$user->update_ride}}" @if($user->update_ride == 1) checked="checked" @endif></div> 
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-6">Нова/измена на резервација на патување:</div> 
          <div class="col-sm-6 col-lg-6"><input type="checkbox" name="ride_request" value="{{$user->ride_request}}" @if($user->ride_request == 1) checked="checked" @endif></div> 
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-6">Прифатено/одбиено патување:</div> 
          <div class="col-sm-6 col-lg-6"><input type="checkbox" name="ride_status" value="{{$user->ride_status}}" @if($user->ride_status == 1) checked="checked" @endif></div> 
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-6">Нов/променет коментар:</div> 
          <div class="col-sm-6 col-lg-6"><input type="checkbox" name="comment_added" value="{{$user->comment_added}}" @if($user->comment_added == 1) checked="checked" @endif></div> 
        </div>
        <div class="row">
          <div class="col-sm-6 col-lg-6">Промоции и Новости:</div> 
          <div class="col-sm-6 col-lg-6"><input type="checkbox" name="promotions_news" value="{{$user->promotions_news}}" @if($user->promotions_news == 1) checked="checked" @endif></div> 
        </div>
      </div>
    </div>
  </div>
</div>

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </form>
  </section>
@endsection

@section('includecss')
	  <link rel="stylesheet" href="{{ asset('css/admin-lte/dataTables.bootstrap.min.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.3/croppie.min.css" rel="stylesheet">
@endsection

@section('includesscripts')
<script src="{{ asset('js/admin-lte/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('js/admin-lte/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/starrr.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.3/croppie.min.js"></script>
<script src="{{ asset('js/carmodels.js') }}"></script> 

<script>


  function changeVal(element)
  {
    if(element.checked){
      $(element).val(1);
      document.getElementById("block_reason").required = false;
    }else{
      if (confirm('Дали навистина сакате да ја деактивирате корисничката сметка?')) {
          $(element).val(0);
          document.getElementById("block_reason").required = true;
          alert("Внесете причина за деактивација.");
      }else{
        $(element).val(1);
        document.getElementById('active_user').checked = true;
        document.getElementById("block_reason").required = false;
      }
    }
  }

	$(document).ready( function () {
    	$('#recieved_r_table, #sent_r_table').DataTable({
        "order": [[ 5, "desc" ]],
        "language": {
          "paginate": {
            "next": "Следна",
            "previous": "Претходна"
          }
        }
      });

      $('#booked_rides_table').DataTable({
        "order": [[ 6, "desc" ]],
        "language": {
          "paginate": {
            "next": "Следна",
            "previous": "Претходна"
          }
        }
      });

      $('#user_rides_table').DataTable({
        "order": [[ 5, "desc" ]],
        "language": {
          "paginate": {
            "next": "Следна",
            "previous": "Претходна"
          }
        }
      });
	} );

  var user_rating = "<?php echo $user_rating; ?>";
  if(user_rating != ''){
    $('.user_rating').starrr({rating: user_rating, readOnly: true});
  }


  document.querySelector("html").classList.add('js');

  var fileInput  = document.querySelector( ".input-file" ),
    button     = document.querySelector( ".input-file-trigger" ),
    the_return = document.querySelector(".file-return");

  button.addEventListener( "keydown", function( event ) {
    if ( event.keyCode == 13 || event.keyCode == 32 ) {
        fileInput.focus();
    }
  });
  button.addEventListener( "click", function( event ) {
    fileInput.focus();
    return false;
  });
  fileInput.addEventListener( "change", function( event ) {
    the_return.innerHTML = this.value;
  });

  function resolve_car(){
    var car_makes = document.getElementById("car-makes").value;
      var car_model = "{{$user->car_model}}";

      var car_options_arr = getCarModel(car_makes);
      car_options = '<option value="">Избери Модел</option>';

      for(var i = 0; i < car_options_arr.length; i++){
        if(car_options_arr[i] == car_model){
        car_options += "<option value='"+car_options_arr[i]+"' selected>"+car_options_arr[i]+"</option>";
        }else{
          car_options += "<option value='"+car_options_arr[i]+"'>"+car_options_arr[i]+"</option>";
        }
      }
      
      $("#car-models").find('option').remove().end().append(car_options);
  }

  $( document ).ready(function() {
    resolve_car();
  });

  $( "#car-makes" ).change(function() {
      resolve_car();
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $uploadCrop = $('#upload-demo').croppie({

    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'circle'
    },

    boundary: {
        width: 300,
        height: 300
    }
  });

  $('#upload').on('change', function () {

    var reader = new FileReader();
    reader.onload = function (e) {
      $uploadCrop.croppie('bind', {
        url: e.target.result
      });
    }

    reader.readAsDataURL(this.files[0]);
    if ( /\.(jpe?g|png|gif)$/i.test(this.files[0].name) === false ) { 
      alert("Овој тип на документ не е дозволен. Ве молиме прикачете слика"); 
      $( "#krop" ).addClass( "hidden" ); 
      $("#upload").val('');
      return false;
    }

    $( "#krop" ).removeClass( "hidden" );
      
      console.log(file);
    
  });

  $('.upload-result').on('click', function (ev) {

    $uploadCrop.croppie('result', {
      type: 'canvas',
      size: 'viewport'

    }).then(function (resp) {
      document.getElementById("auto_image").value = resp;
      var auto_name = document.getElementById("upload").value;
      document.getElementById("auto_image_name").value = auto_name;
 
    });
  });
</script>

@endsection