@extends('admin.layouts')

@section('content')
    <section class="content-header">
      <h1>Пријави <small>Табеларно</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
        <li class="active">Пријави</li>
      </ol>
    </section>
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Табела на кориснички пријави</h3>
			</div>
			<div class="box-body">
				<table id="reports_table" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>До Корисник</th>
							<th>Од Корисник</th>
							<th>Наслов</th>
							<th>Опис на проблем</th>
							<th>Статус</th>
							<th>Дата на креирање</th>
							<th>Последна Промена</th>
							<th>Акција</th>
						</tr>
					</thead>
					<tbody>
						@foreach($userReports as $report)
							<tr>
								<td>{{$report->id}}</td>
								<td><a href="/admin/view-user/{{$report->user_id}}">{{$report->userFirstname}} {{$report->userLastname}}</a></td>
								<td><a href="/admin/view-user/{{$report->reported_by}}">{{$report->reportedByFirstname}} {{$report->reportedByLastname}}</a></td>
								<td>{{$report->subject}}</td>
								<td>{{$report->description}}</td>
								<td>{{Helper::reportStatus_trans($report->status)}}</td>
								<td>{{$report->created_at}}</td>
								<td>{{$report->updated_at}}</td>
								<td><a href="reports/view-report/{{$report->id}}"><i class="far fa-eye"></i></a> <a href="reports/edit-report/{{$report->id}}"> <i class="fas fa-edit"></i></a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</section>
@endsection

@section('includecss')
	  <link rel="stylesheet" href="{{ asset('css/admin-lte/dataTables.bootstrap.min.css')}}">
@endsection

@section('includesscripts')
<script src="{{ asset('js/admin-lte/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('js/admin-lte/dataTables.bootstrap.min.js') }}"></script>
<script>
	$(document).ready( function () {
    	$('#reports_table').DataTable({
    		"order": [[ 6, "desc" ]],
    		"language": {
	          "paginate": {
	            "next": "Следна",
	            "previous": "Претходна"
	          },
          		"search": "Пребарувај:",
          		"lengthMenu": "Прикажи _MENU_ записи",
          		"info": "Прикажани се од _START_ до _END_ од вкупно _TOTAL_ записи",
          		"infoEmpty": "Нема податоци",
         		"emptyTable": "Нема податоци"
	        }
    	});
	} );
</script>
@endsection