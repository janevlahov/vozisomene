@extends('admin.layouts')

@section('content')
  @if(!empty($userReport))

    <section class="content-header">
      <h1>Пријавa <small>Промена</small></h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
        <li class="active">Промена на Пријава</li>
      </ol>
    </section>
    
    <form action="{{route('admin.updateUserReport', $userReport->id)}}" method="POST">

      <section class="content">
        <div class="box text-center">
          <div class="box-header with-border">
            <div class="row">
              <div class="col-lg-12 text-right">
                <input type="submit" class="btn btn-primary upload-result" value="Зачувај"> 
                <a href="/admin/users/reports/view-report/{{$userReport->id}}" class="btn btn-primary">Назад</a>
              </div>
            </div>
          </div>
          <div class="box-header with-border">
            <div class="row">
              <div class="col-lg-4">
                <h3>{{$userReport->reportedByFirstname}} {{$userReport->reportedByLastname}}</h3>
              </div>
              <div class="col-lg-4">
                <h3><i class="fas fa-arrow-right"></i></h3>
              </div>
              <div class="col-lg-4">
                <h3>{{$userReport->userFirstname}} {{$userReport->userLastname}}</h3>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-lg-12">
                <p class="lead">{{$userReport->subject}}</p>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <p class="lead">{{$userReport->description}}</p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="content">
        <div class="box">
          <div class="box-body">
            <div class="row">
              <div class="col-md-1">Статус: </div>
              <div class="col-md-3">

                <select class="form-control" id="reportStatus" name='status'>
                  @foreach($reportStatus as $statusKey => $statusValue)
                    <option value="{{$statusKey}}" @if($statusKey == $userReport->status) selected='selected' @endif>{{$statusValue}}</option>
                  @endforeach
                </select>
                
              </div>
              <div class="col-md-1">Акција: </div>
              <div class="col-md-3">
                <div class="form-group">
                  <input type="text" name="action" class="form-control" value="{{$userReport->action}}">
                </div>
              </div>
              <div class="col-md-1">Решение: </div>
              <div class="col-md-3">
                <div class="form-group">
                  <textarea name="solution" class="form-control" cols="10" rows="1">{{$userReport->solution}}</textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
  @else
    <section class="content">
      <div class="box text-center">
        <div class="box-header with-border">
          <h4>Записот не постои</h4>
        </div>
      </div>
    </section>
  @endif
@endsection

@section('includesscripts')


@endsection