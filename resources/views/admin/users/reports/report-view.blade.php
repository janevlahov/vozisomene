@extends('admin.layouts')
@section('content')
  @if(!empty($userReport))

    <section class="content-header">
      <h1>Пријава <small>Преглед</small></h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
        <li class="active">Преглед на Пријава</li>
      </ol>
    </section>

    <section class="content">
      <div class="box text-center">
        <div class="box-header with-border">
          <div class="row">
            <div class="col-lg-12 text-right">
              <a href="/admin/users/reports/edit-report/{{$userReport->id}}" class="btn btn-primary">Промени</a>
            </div>
          </div>
        </div>
        <div class="box-header with-border">
          <div class="row">
            <div class="col-lg-4">
              <h3>{{$userReport->reportedByFirstname}} {{$userReport->reportedByLastname}}</h3>
            </div>
            <div class="col-lg-4">
              <h3><i class="fas fa-arrow-right"></i></h3>
            </div>
            <div class="col-lg-4">
              <h3>{{$userReport->userFirstname}} {{$userReport->userLastname}}</h3>
            </div>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-lg-12">
              <p class="lead">{{$userReport->subject}}</p>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <p class="lead">{{$userReport->description}}</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="box">
        <div class="box-body">
          <div class="row">
            <div class="col-md-1">Статус: </div>
            <div class="col-md-3">{{Helper::reportStatus_trans($userReport->status)}}</div>
            <div class="col-md-1">Акција: </div>
            <div class="col-md-3"><p>{{$userReport->action}}</p></div>
            <div class="col-md-1">Решение: </div>
            <div class="col-md-3"><p>{{$userReport->solution}}</p></div>
          </div>
        </div>
      </div>
    </section>
  @else
    <section class="content">
      <div class="box text-center">
        <div class="box-header with-border">
          <h4>Записот не постои</h4>
        </div>
      </div>
    </section>
  @endif
@endsection
