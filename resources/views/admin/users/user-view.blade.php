@extends('admin.layouts')

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Корисник <small>Преглед</small></h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
        <li class="active">Преглед на Корисник</li>
      </ol>
    </section>

	<section class="content">
		<div class="box">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-lg-1 col-xs-6 text-center">
            {!! Helper::userAvatarAdmin($user->avatar, '','',$user->id) !!}
          </div>
          <div class="col-lg-1 col-xs-6">
            <h3 class="box-title">{{$user->firstname}} {{$user->lastname}}</h3>
            @if($user_rating == '') <p>Без оценки</p> @else <div class='user_rating'> {{$user_rating}} </div> @endif
          </div>
         
          <div class="col-lg-10 col-xs-6 text-right">
            <a href="mailto:{{$user->email}}" target="_top" class="btn btn-primary"> <i class="fas fa-envelope"></i></a>
            <a href="/admin/edit-user/{{$user->id}}" class="btn btn-primary">Промени</a>
          </div>
        </div>
      </div>
      <div class="box-header with-border">
        <div class="row">
          <div class="col-sm-6 col-lg-1"><h4>Активен:</h4></div> 
          <div class="col-sm-6 col-lg-1"><h4>{{Helper::bool_beautify($user->active)}}</h4></div>
          <div class="col-sm-12 col-lg-10"><h4>{{$user->reason}}</h4></div>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-lg-4">
            <div class="row">
              <div class="col-lg-12"><h4>Лични податоци</h4></div>	
            </div>
          
            <div class="row">
              <div class="col-sm-6 col-lg-5">Име и Презиме:</div>	
              <div class="col-sm-6 col-lg-7">{{$user->firstname}} {{$user->lastname}}</div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-5">Е-маил:</div> 
              <div class="col-sm-6 col-lg-7">{{$user->email}}</div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-5">Телефон:</div> 
              <div class="col-sm-6 col-lg-7">{{$user->phone}}</div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-5">Пол:</div>	
              <div class="col-sm-6 col-lg-7">{{Helper::sex_translation($user->sex)}}</div>	
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-5">Дата на раѓање:</div>	
              <div class="col-sm-6 col-lg-7">{{$user->birthday}}</div>	
            </div>
          </div>
          <div class="col-lg-3">
            <div class="row">
              <div class="col-lg-12"><h4>Адреса</h4></div>	
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Улица:</div>	
              <div class="col-sm-6 col-lg-6">{{$user->address}}</div>	
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Поштенски Број:</div>  
              <div class="col-sm-6 col-lg-6">{{$user->zip}}</div> 
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Град:</div>	
              <div class="col-sm-6 col-lg-6">{{$user->city}}</div>	
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Држава:</div>	
              <div class="col-sm-6 col-lg-6">{{$user->state}}</div>	
            </div>
          </div>
          <div class="col-lg-3">
            <div class="row">
              <div class="col-lg-12"><h4>Био</h4></div>	
            </div>
            <div class="row">
              <div class="col-lg-12">{{$user->bio}}</div>	
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="box">
      	<div class="box-body">
      		<div class="row">
      			<div class="col-lg-3">
      				<div class="row">
      					<div class="col-lg-12"><h4>Автомобил</h4></div>	
      				</div>
      				<div class="row">
      					<div class="col-sm-6 col-lg-6">Бр. на регистарска табла:</div>	
      					<div class="col-sm-6 col-lg-6">{{$user->licence_plate}}</div>	
      				</div>
      				<div class="row">
      					<div class="col-sm-6 col-lg-6">Држава на регистрација:</div>	
      					<div class="col-sm-6 col-lg-6">{{$user->licence_state}}</div>	
      				</div>
      			</div>
      			<div class="col-lg-3">
      				<div class="row">
      					<div class="col-lg-12"><h4>Тип на автомобил</h4></div>	
      				</div>
      				<div class="row">
      					<div class="col-sm-6 col-lg-6">Марка на автомобил:</div>	
      					<div class="col-sm-6 col-lg-6">{{$user->car_brand}}</div>	
      				</div>
      				<div class="row">
      					<div class="col-sm-6 col-lg-6">Модел на автомобил:</div>	
      					<div class="col-sm-6 col-lg-6">{{$user->car_model}}</div>	
      				</div>
      				<div class="row">
      					<div class="col-sm-6 col-lg-6">Година на производтство:</div>	
      					<div class="col-sm-6 col-lg-6">{{$user->year_production}}</div>	
      				</div>
      			</div>
      			<div class="col-lg-3">
      				<div class="row">
      					<div class="col-lg-12"><h4>Додатни Информации</h4></div>	
      				</div>
      				<div class="row">
      					<div class="col-sm-6 col-lg-6">Тип:</div>	
      					<div class="col-sm-6 col-lg-6">{{$user->type}}</div>
      				</div>
      				<div class="row">
      					<div class="col-sm-6 col-lg-6">Боја:</div>	
      					<div class="col-sm-6 col-lg-6">{{$user->color}}</div>
      				</div>
      			</div>
      			<div class="col-lg-3">
      				<div class="row">
      					<div class="col-lg-12"><h4>Фотографија</h4></div>	
      				</div>
      				<div class="row">
      					<div class="col-lg-12">
      						<img src="{{asset('storage/upload/cars/'.$user->car_photo)}}" alt="">
      					</div>
      				</div>
      			</div>
      		</div>
      	</div>
      </div>

    <div class="box">
      <div class="box-body">
      	<div class="row">
    			<div class="col-lg-5">
            <div class="row">
              <div class="col-lg-12"><h4>Однесувања во автомобил</h4></div> 
            </div>
    				<div class="row">
    					<div class="col-sm-6 col-lg-6">Зборлив:</div>	
    					<div class="col-sm-6 col-lg-6">{!! Helper::pref_talkative($user->talkative)[1] !!}</div>	
    				</div>
    				<div class="row">
    					<div class="col-sm-6 col-lg-6">Пушач:</div>	
    					<div class="col-sm-6 col-lg-6">{!! Helper::pref_smoker($user->smoker)[1] !!} </div>	
    				</div>
    				<div class="row">
    					<div class="col-sm-6 col-lg-6">Миленичиња:</div>	
    					<div class="col-sm-6 col-lg-6">{!! Helper::pref_pets($user->pets)[1] !!} </div>	
    				</div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Музика:</div>  
              <div class="col-sm-6 col-lg-6">{!! Helper::pref_music($user->music)[1] !!}</div> 
            </div>
    			</div>
          <div class="col-lg-5">
            <div class="row">
              <div class="col-lg-12"><h4>Е-маил Известувања</h4></div> 
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Ново патување:</div> 
              <div class="col-sm-6 col-lg-6">{{Helper::notification_settings($user->new_ride)}}</div> 
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Направена измена на патувањето:</div> 
              <div class="col-sm-6 col-lg-6">{{Helper::notification_settings($user->update_ride)}}</div> 
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Нова/измена на резервација на патување:</div> 
              <div class="col-sm-6 col-lg-6">{{Helper::notification_settings($user->ride_request)}}</div> 
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Прифатено/одбиено патување:</div> 
              <div class="col-sm-6 col-lg-6">{{Helper::notification_settings($user->ride_status)}}</div> 
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Нов/променет коментар:</div> 
              <div class="col-sm-6 col-lg-6">{{Helper::notification_settings($user->comment_added)}}</div> 
            </div>
            <div class="row">
              <div class="col-sm-6 col-lg-6">Промоции и Новости:</div> 
              <div class="col-sm-6 col-lg-6">{{Helper::notification_settings($user->promotions_news)}}</div> 
            </div>
          </div>
      	</div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="box box-primary">
          <div class="box-header">
            <i class="fas fa-comments"></i>

            <h3 class="box-title">Понудени Патувања</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">

            </div>
            <!-- /. tools -->
          </div>
          <div class="box-body">
            <table id="user_rides_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Тргнува Од</th>
                  <th>Крајна дестинација</th>
                  <th>Стопирачка локација 1</th>
                  <th>Стопирачка локација 2</th>
                  <th>Креиранo на</th>
                  <th>Променетo на</th>
                  <th>Акција</th>
                </tr>
              </thead>
              <tbody>
                @foreach($user_rides as $ride)
                  <tr>
                    <td>{{$ride->id}}</td>
                    <td>{{$ride->d_from}}</td>
                    <td>{{$ride->d_to}}</td>
                    <td>{{$ride->d_stop_1}}</td>
                    <td>{{$ride->d_stop_2}}</td>
                    <td>{{$ride->created_at}}</td>
                    <td>{{$ride->updated_at}}</td>
                    <td><a href="/admin/view-ride/{{$ride->id}}"><i class="far fa-eye"></i></a></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="box box-primary">
          <div class="box-header">
            <i class="fas fa-comments"></i>

            <h3 class="box-title">Резервирани Патувања</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">

            </div>
            <!-- /. tools -->
          </div>
          <div class="box-body">
            <table id="booked_rides_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Тргнува Од</th>
                  <th>Крајна дестинација</th>
                  <th>Резервирани места</th>
                  <th>Статус</th>
                  <th>Цена (мкд)</th>
                  <th>Креиранo на</th>
                  <th>Акција</th>
                </tr>
              </thead>
              <tbody>
                @foreach($booked_rides as $b_ride)
                    @php 
                      $from = $b_ride->b_from;
                      $to = $b_ride->b_to;
                      $cost = $b_ride->b_cost;
                    @endphp
                  <tr>
                    <td>{{$b_ride->ride_id}}</td>
                    <td>{{$b_ride->$from}}</td>
                    <td>{{$b_ride->$to}}</td>
                    <td>{{$b_ride->qty_places}}</td>
                    <td>{{Helper::Status_Message($b_ride->book_status)}}</td>
                    <td>{{$b_ride->$cost}}</td>
                    <td>{{$b_ride->created_at}}</td>
                    <td><a href="/admin/view-ride/{{$b_ride->ride_id}}"><i class="far fa-eye"></i></a></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="box box-info">
          <div class="box-header">
            <i class="fas fa-comments"></i>

            <h3 class="box-title">Примени Коментари</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">

            </div>
            <!-- /. tools -->
          </div>
          <div class="box-body">
            <table id="recieved_r_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Примен од</th>
                  <th>ID на патување</th>
                  <th>Оценка</th>
                  <th>Коментар</th>
                  <th>Креиранo на</th>
                  <th>Променетo на</th>
                </tr>
              </thead>
              <tbody>
                @foreach($recieved_r as $r_review)
                  <tr>
                    <td>{{$r_review->id}}</td>
                    <td> <a href="/admin/view-user/{{$r_review->s_user_id}}">{{$r_review->firstname}} {{$r_review->lastname}}</a> </td>
                    <td><a href="/admin/view-ride/{{$r_review->ride_id}}">{{$r_review->ride_id}}</a></td>
                    <td>{{$r_review->rating}}</td>
                    <td>{{$r_review->comment}}</td>
                    <td>{{$r_review->created_at}}</td>
                    <td>{{$r_review->updated_at}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="box box-info">
          <div class="box-header">
            <i class="fas fa-comments"></i>

            <h3 class="box-title">Дадени Коментари</h3>
            <div class="pull-right box-tools">

            </div>
          </div>
          <div class="box-body">
            <table id="sent_r_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Даден на</th>
                  <th>ID на патување</th>
                  <th>Оценка</th>
                  <th>Коментар</th>
                  <th>Креиранo на</th>
                  <th>Променетo на</th>
                </tr>
              </thead>
              <tbody>
                @foreach($sent_r as $s_review)
                  <tr>
                    <td>{{$s_review->id}}</td>
                    <td><a href="/admin/view-user/{{$s_review->r_user_id}}">{{$s_review->firstname}} {{$s_review->lastname}}</a></td>
                    <td><a href="/admin/view-ride/{{$s_review->ride_id}}">{{$s_review->ride_id}}</a></td>
                    <td>{{$s_review->rating}}</td>
                    <td>{{$s_review->comment}}</td>
                    <td>{{$s_review->created_at}}</td>
                    <td>{{$s_review->updated_at}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-lg-12">
        <div class="box box-info">
          <div class="box-header">
            <i class="fas fa-comments"></i>

            <h3 class="box-title">Пријавил Корисници</h3>
            <div class="pull-right box-tools">

            </div>
          </div>
          <div class="box-body">
            <table id="reportedUsers" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Пријавил на</th>
                  <th>Наслов</th>
                  <th>Опис</th>
                  <th>Статус</th>
                  <th>Акција</th>
                  <th>Решение</th>
                  <th>Креиранo на</th>
                  <th>Променетo на</th>
                </tr>
              </thead>
              <tbody>
                @foreach($userReports as $reports)
                  <tr>
                    <td><a href="/admin/users/reports/view-report/{{$reports->id}}">{{$reports->id}}</a></td>
                    <td><a href="/admin/view-user/{{$reports->userid}}">{{$reports->firstname}} {{$reports->lastname}}</a></td>
                    <td>{{$reports->subject}}</td>
                    <td>{{$reports->description}}</td>
                    <td>{{Helper::reportStatus_trans($reports->status)}}</td>
                    <td>{{$reports->action}}</td>
                    <td>{{$reports->solution}}</td>
                    <td>{{$reports->created_at}}</td>
                    <td>{{$reports->updated_at}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="box box-info">
          <div class="box-header">
            <i class="fas fa-comments"></i>

            <h3 class="box-title">Пријавен Од</h3>
            <div class="pull-right box-tools">

            </div>
          </div>
          <div class="box-body">
            <table id="reportedByUsers" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Пријавен Од</th>
                  <th>Наслов</th>
                  <th>Опис</th>
                  <th>Статус</th>
                  <th>Акција</th>
                  <th>Решение</th>
                  <th>Креиранo на</th>
                  <th>Променетo на</th>
                </tr>
              </thead>
              <tbody>
                @foreach($userReportedBy as $reports)
                  <tr>
                    <td><a href="/admin/users/reports/view-report/{{$reports->id}}">{{$reports->id}}</a></td>
                    <td><a href="/admin/view-user/{{$reports->reported_by}}">{{$reports->firstname}} {{$reports->lastname}}</a></td>
                    <td>{{$reports->subject}}</td>
                    <td>{{$reports->description}}</td>
                    <td>{{Helper::reportStatus_trans($reports->status)}}</td>
                    <td>{{$reports->action}}</td>
                    <td>{{$reports->solution}}</td>
                    <td>{{$reports->created_at}}</td>
                    <td>{{$reports->updated_at}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
          
</section>
@endsection

@section('includecss')
	<link rel="stylesheet" href="{{ asset('css/admin-lte/dataTables.bootstrap.min.css')}}">
@endsection

@section('includesscripts')
<script src="{{ asset('js/admin-lte/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('js/admin-lte/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/starrr.js') }}"></script>
<script>
	$(document).ready( function () {
    	$('#recieved_r_table, #sent_r_table').DataTable({
        "order": [[ 5, "desc" ]],
        "language": {
          "paginate": {
            "next": "Следна",
            "previous": "Претходна"
          },
          "search": "Пребарувај:",
          "lengthMenu": "Прикажи _MENU_ записи",
          "info": "Прикажани се од _START_ до _END_ од вкупно _TOTAL_ записи",
          "infoEmpty": "Нема податоци",
          "emptyTable": "Нема податоци"
        }
      });

      $('#booked_rides_table').DataTable({
        "order": [[ 6, "desc" ]],
        "language": {
          "paginate": {
            "next": "Следна",
            "previous": "Претходна"
          },
          "search": "Пребарувај:",
          "lengthMenu": "Прикажи _MENU_ записи",
          "info": "Прикажани се од _START_ до _END_ од вкупно _TOTAL_ записи",
          "infoEmpty": "Нема податоци",
          "emptyTable": "Нема податоци"
        }
      });

      $('#user_rides_table').DataTable({
        "order": [[ 5, "desc" ]],
        "language": {
          "paginate": {
            "next": "Следна",
            "previous": "Претходна"
          },
          "search": "Пребарувај:",
          "lengthMenu": "Прикажи _MENU_ записи",
          "info": "Прикажани се од _START_ до _END_ од вкупно _TOTAL_ записи",
          "infoEmpty": "Нема податоци",
          "emptyTable": "Нема податоци"
        }
      });

      $('#reportedUsers').DataTable({
        "order": [[ 5, "desc" ]],
        "language": {
          "paginate": {
            "next": "Следна",
            "previous": "Претходна"
          },
          "search": "Пребарувај:",
          "lengthMenu": "Прикажи _MENU_ записи",
          "info": "Прикажани се од _START_ до _END_ од вкупно _TOTAL_ записи",
          "infoEmpty": "Нема податоци",
          "emptyTable": "Нема податоци"
        }
      });

      $('#reportedByUsers').DataTable({
        "order": [[ 5, "desc" ]],
        "language": {
          "paginate": {
            "next": "Следна",
            "previous": "Претходна"
          },
          "search": "Пребарувај:",
          "lengthMenu": "Прикажи _MENU_ записи",
          "info": "Прикажани се од _START_ до _END_ од вкупно _TOTAL_ записи",
          "infoEmpty": "Нема податоци",
          "emptyTable": "Нема податоци"
        }
      });

	} );

  var user_rating = "<?php echo $user_rating; ?>";
  if(user_rating != ''){
    $('.user_rating').starrr({rating: user_rating, readOnly: true});
  }
</script>
@endsection