@extends('admin.layouts')

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Администратор<small> Промена на Лозинка</small></h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
        <li class="active">Админ Профил</li>
      </ol>
    </section>
    <!-- Main content -->
    <form action="{{route('admin.updateAdminPassword', Auth::user()->id)}}" method="POST">
		<section class="content">
			<div class="box">
				<div class="box-header">
					<div class="row">
						<div class="col-lg-1 col-xs-6">
							<h3 class="box-title">{{ Auth::user()->name }}</h3>
						</div>
						<div class="col-lg-11 col-xs-6 text-right">	
							<input type="submit" class="btn btn-primary upload-result" value="Зачувај"> 
            				<a href="/admin/viewAdminProfile/{{ Auth::user()->id }}" class="btn btn-primary">Назад</a>
						</div>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-lg-12">	
							@if ($errors->any())
							        {!! implode('', $errors->all('<p>:message</p>')) !!}
							@endif
						</div>
					</div>

	        		<div class="row">
	        			<div class="col-lg-4">
				            <div class="row">
				             	<div class="col-lg-12"><h4>Промена на лозинка</h4></div>	
				            </div>
				            <div class="row">
				             	<div class="col-sm-6 col-lg-5">Нова Лозинка:</div> 
				             	<div class="col-sm-6 col-lg-7"><div class="form-group"><input type="password" class="form-control" name="password"></div></div>
				            </div>
				            <div class="row">
				             	<div class="col-sm-6 col-lg-5">Повторете ја лозинката:</div> 
				             	<div class="col-sm-6 col-lg-7"><div class="form-group"><input type="password" class="form-control" name="password_confirmation"></div></div>
				            </div>
				        </div>
	        		</div>
	        	</div>
			</div>
		</section>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	</form>

@endsection

