@extends('admin.layouts')

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Администратор<small> Преглед</small></h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
        <li class="active">Админ Профил</li>
      </ol>
    </section>
    <!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header">
				<div class="row">
					<div class="col-lg-2 col-xs-6">
						<h3 class="box-title">{{ Auth::user()->name }}</h3>
					</div>
					<div class="col-lg-10 col-xs-6 text-right">	
						<a href="/admin/editAdminProfile/{{ Auth::user()->id }}" class="btn btn-primary">Промени</a>
						<a href="/admin/changeAdminpassword/{{ Auth::user()->id }}" class="btn btn-primary">Промени Лозинка</a>
					</div>
				</div>
			</div>
			<div class="box-body">
        		<div class="row">
        			<div class="col-lg-4">
			            <div class="row">
			              <div class="col-lg-12"><h4>Податоци за администратор</h4></div>	
			            </div>
			            <div class="row">
			              <div class="col-sm-6 col-lg-5">Име и Презиме:</div> 
			              <div class="col-sm-6 col-lg-7">{{ Auth::user()->name }}</div>
			            </div>
			            <div class="row">
			              <div class="col-sm-6 col-lg-5">Е-маил:</div> 
			              <div class="col-sm-6 col-lg-7">{{ Auth::user()->email }}</div>
			            </div>
			            <div class="row">
			              <div class="col-sm-6 col-lg-5">Креиран на:</div>	
			              <div class="col-sm-6 col-lg-7">{{ Auth::user()->created_at }}</div>	
			            </div>
			            <div class="row">
			              <div class="col-sm-6 col-lg-5">Последна промена на:</div>	
			              <div class="col-sm-6 col-lg-7">{{ Auth::user()->updated_at }}</div>	
			            </div>
			        </div>
        		</div>
        	</div>
		</div>
	</section>

@endsection

