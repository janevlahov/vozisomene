<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Возисомене Админ Панел</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="{{ asset('css/admin-lte/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin-lte/font-awesome.min.css') }}">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/v4-shims.css">
  <link rel="stylesheet" href="{{ asset('css/admin-lte/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin-lte/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin-lte/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin-lte/morris.css') }}">
  <link rel="stylesheet" href="{{ asset('css/admin-lte/jquery-jvectormap.css') }}">

  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  @yield('includecss')

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="/admin" class="logo">
      <span class="logo-mini">ВСМ</span>
      <span class="logo-lg">Возисомене Админ Панел</span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
       
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <ul class="menu">
                  <li><a href="#"><i class="fa fa-user text-red"></i> You changed your username</a></li>
                </ul>
              </li>
              <li class="footer"><a href="#">Види ги сите</a></li>
            </ul>
          </li>
       
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header"><p>{{ Auth::user()->name }}</p></li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="/admin/viewAdminProfile/{{ Auth::user()->id }}" class="btn btn-default btn-flat">Профил</a>
                </div>
                <div class="pull-right">
                  <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Излез</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left info">
          <p >{{ Auth::user()->name }}</p>
        </div>
      </div>

      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
          </span>
        </div>
      </form>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Корисници</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="/admin/users"><i class="fa fa-circle-o"></i> Корисници</a></li>
            <li><a href="/admin/users/reports/"><i class="fa fa-circle-o"></i> Пријави</a></li>
          </ul>
        </li>
        <li class="header">Патувања</li>
        <li><a href="/admin/rides"><i class="fa fa-circle-o text-red"></i> <span>Патувања</span></a></li>
        <li class="header">DOCUMENTATION</li>
        <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
      </ul>
    </section>
  </aside>

  <div class="content-wrapper">

    @yield('content')
  
  </div>

  <footer class="main-footer">
    <strong>&copy; <script type="text/javascript">document.write(new Date().getFullYear());</script> <a href="https://morgenstern.mk">Моргенстерн Маркетинг</a>.</strong> Сите права се задржани.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Последни Активности</h3>
        <ul class="control-sidebar-menu">
         
        </ul>

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<script src="{{ asset('js/admin-lte/jquery.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/jquery-ui.min.js') }}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{ asset('js/admin-lte/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('js/admin-lte/jquery.knob.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/moment.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/fastclick.js') }}"></script>
<script src="{{ asset('js/admin-lte/adminlte.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/raphael.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/morris.min.js') }}"></script>
<script src="{{ asset('js/admin-lte/demo.js') }}"></script> 
<script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.6.3/js/v4-shims.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="{{ asset('js/bootstrap-datepicker.mk.js') }}"></script>
<script src="{{ asset('js/bootstrap-clockpicker.min.js') }}"></script>

@yield('includesscripts')
</body>
</html>
