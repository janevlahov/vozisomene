@extends('admin.layouts')

@section('content')
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Патувања
        <small>Табеларно</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
 <!-- Main content -->
	<section class="content">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Табела на патувања</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table id="rides_table" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Тргнува Од</th>
							<th>Крајна дестинација</th>
							<th>Стопирачка локација 1</th>
							<th>Стопирачка локација 2</th>
							<th>Статус</th>
							<th>Креиранo на</th>
							<th>Променетo на</th>
							<th>Акција</th>
						</tr>
					</thead>
					<tbody>
						@foreach($rides as $ride)
							<tr>
							  	<td>{{$ride->id}}</td>
				                <td>{{$ride->d_from}}</td>
				                <td>{{$ride->d_to}}</td>
				                <td>{{$ride->d_stop_1}}</td>
				                <td>{{$ride->d_stop_2}}</td>
				                <td>{{Helper::Ride_Status($ride->status)}}</td>
				                <td>{{$ride->created_at}}</td>
				                <td>{{$ride->updated_at}}</td>
								<td><a href="view-ride/{{$ride->id}}"><i class="far fa-eye"></i></a> <a href="edit-ride/{{$ride->id}}"><i class="fas fa-edit"></i></a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</section>
@endsection

@section('includecss')
	  <link rel="stylesheet" href="{{ asset('css/admin-lte/dataTables.bootstrap.min.css')}}">
@endsection

@section('includesscripts')
<script src="{{ asset('js/admin-lte/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('js/admin-lte/dataTables.bootstrap.min.js') }}"></script>
<script>
	$(document).ready( function () {
    	$('#rides_table').DataTable({
    		"order": [[ 6, "desc" ]],
    		"language": {
	          	"paginate": {
		            "next": "Следна",
		            "previous": "Претходна"
	          	},
          		"search": "Пребарувај:",
          		"lengthMenu": "Прикажи _MENU_ записи",
          		"info": "Прикажани се од _START_ до _END_ од вкупно _TOTAL_ записи",
          		"infoEmpty": "Нема податоци",
          		"emptyTable": "Нема податоци"
        	}
    	});
	} );
</script>
@endsection