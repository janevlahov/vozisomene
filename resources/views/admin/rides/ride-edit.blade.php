@extends('admin.layouts')

@section('content')
	<section class="content-header">
	    <h1>Патување <small>Промена</small></h1>
	    <ol class="breadcrumb">
	      <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
	      <li class="active">Промена на Патување</li>
	    </ol>
	</section>
	<form action="{{route('admin.updateRide', $ride->id)}}" method="POST" id="msform">
		<section class="content">
		    <div class="box box-info">
			    <div class="box-header with-border">
			        <div class="row">
				        <div class="col-lg-12 col-xs-12 text-right">
				            <input type="submit" class="btn btn-primary upload-result" value="Зачувај"> 
	            			<a href="/admin/view-ride/{{$ride->id}}" class="btn btn-primary">Назад</a>
				        </div>
			        </div>
			    </div>

			    <div class="box-body">
			    	<div class="row">
			    		<div class="col-lg-12">
			    			<div id="map"></div>
			    		</div>
			    	</div>
			    </div>
		    </div>
	  	</section>
	  	<section class="content">
	  		<div class="box box-info">
	  			<div class="box-body">
	  				<div class="row">
	  					<div class="col-lg-6">
	  						<div class="row">
	  							<div class="col-lg-6"><p>Локација на поаѓање:</p></div>
	  							<div class="col-lg-6"><div class="form-group"><input id="origin-input" name="d_from" class="controls form-control" value="{{$ride->d_from}}"></div></div>
	  						</div>

	  						<div class="row">
	  							<div class="col-lg-6"><p>Локација на пристигнување:</p></div>
	  							<div class="col-lg-6"><div class="form-group"><input id="destination-input" name="d_to" class="controls form-control" value="{{$ride->d_to}}"></div></div>
	  						</div>

	  						<div class="row">
	  							<div class="col-lg-6"><p>Дата/Време на поаѓање:</p></div>
	  							<div class="col-lg-3">
	  								<div class="input-group">
				                      	<div class="input-group-addon"><i class="fas fa-calendar-alt"></i></div>
				                      	<input type="text" id="datepicker1" name="d_date" class="form-control" value="{{ Carbon\Carbon::parse($ride->d_date)->format('d.m.Y') }}" autocomplete="off">
				                    </div>
	  							</div>
	  							<div class="col-lg-3">
	  								<div class="input-group clockpicker1" data-align="top" data-autoclose="true">
				                      	<div class="input-group-addon"><i class="fas fa-clock"></i></div>
				                     	<input type="text" class="form-control" name="d_time" id='clockpicker1I' value="{{$ride->d_time}}" autocomplete="off">
				                    </div>
	  							</div>
	  						</div>

	  						<button type="button" id="get_directions" style="height: 1px; width:1px; visibility: hidden;"></button>

	  					</div>
	  					<div class="col-lg-6">
	  						<div class="row">
	  							<div class="col-lg-6"><p>Стопирачка локација 1:</p></div>
	  							<div class="col-lg-6"><div class="form-group"><input id="waypoints1" name="d_stop_1" class="controls waypoints form-control" value="{{$ride->d_stop_1}}"></div></div>
	  						</div>

	  						<div class="row">
	  							<div class="col-lg-6"><p>Стопирачка локација 2:</p></div>
	  							<div class="col-lg-6"><div class="form-group"><input id="waypoints2" name="d_stop_2" class="controls waypoints form-control" value="{{$ride->d_stop_2}}"></div></div>
	  						</div>

	  						<div class="row">
	  							<div class="col-lg-6"><p>Повратно патување: <input type="checkbox" name="roundtrip" id="roundtrip" onchange="toggleCheckbox(this)" @if($ride->roundtrip) checked @endif></p> </div>
	  							<div class="col-lg-3">
	  								<div class="input-group">
			                          <div class="input-group-addon"><i class="fas fa-calendar-alt"></i></div>
			                          <input type="text" id="datepicker2" name="d_rdate" class="form-control" value="@if($ride->d_rdate != '') {{ Carbon\Carbon::parse($ride->d_rdate)->format('d.m.Y') }} @endif" autocomplete="off">
			                        </div>
	  							</div>
	  							<div class="col-lg-3">
	  								<div class="input-group clockpicker2" data-align="top" data-autoclose="true">
			                          <div class="input-group-addon"><i class="fas fa-clock"></i></div>
			                          <input type="text" class="form-control" name="d_rtime" id='clockpicker2I' value="{{$ride->d_rtime}}" autocomplete="off">
			                        </div>
	  							</div>
	  						</div>
	  					</div>
	  				</div>
	  			</div>
	  		</div>

	  		<div class="box box-info">
	  			<div class="box-body">
	  				<div class="row">
	  					<div class="col-lg-4">
	  						<div class="row">
	  							<div class="col-lg-6">
	  								<p>Слободни места:</p>
	  							</div>
	  							<div class="col-lg-6">
	  								<div class="form-group">
	  									<input type="number" id="free_places" name="free_places" class="controls form-control" min="1" max="6" value="{{$ride->free_places}}" >
	  								</div>
	  							</div>
	  						</div>

	  						<div class="row">
	  							<div class="col-lg-6">
	  								<p>Реални Слободни места:</p>
	  							</div>
	  							<div class="col-lg-6">
	  								{{$r_qty_places}}
	  							</div>
	  						</div>

	  						<div class="row">
	  							<div class="col-lg-6">
	  								<p>Гаранција за задни седишта:</p>
	  							</div>
	  							<div class="col-lg-6">
	  								<div class="form-group">
	  									<input type="checkbox" name="back_s_guarantee" id="back_s_guarantee" onchange="changeVal(this)" @if($ride->back_s_guarantee) checked @endif>
	  								</div>
	  							</div>
	  						</div>
	  					</div>

	  					<div class="col-lg-4">
	  						<div class="row">
	  							<div class="col-lg-6">
	  								<p>Оддалеченост::</p>
	  							</div>
	  							<div class="col-lg-6">
	  								{{$ride->distance}} км
	  							</div>
	  						</div>
	  						<div class="row">
	  							<div class="col-lg-6">
	  								<p>Време на патување:</p>
	  							</div>
	  							<div class="col-lg-6">
	  								{{$ride->est_d_time}}
	  							</div>
	  						</div>
	  						<div class="row">
	  							<div class="col-lg-6">
	  								<p>Услови за користење:</p>
	  							</div>
	  							<div class="col-lg-6">
	  								{{Helper::bool_beautify($ride->terms_cond)}}
	  							</div>
	  						</div>
	  					</div>

	  					<div class="col-lg-4">
	  						<div class="row">
	  							<div class="col-lg-6">
	  								<p>Статус:</p>
	  							</div>
	  							<div class="col-lg-6">
	  								{{Helper::Ride_Status($ride->status)}}
	  							</div>
	  						</div>
	  						<div class="row">
	  							<div class="col-lg-6">
	  								<p>Креирано на:</p>
	  							</div>
	  							<div class="col-lg-6">
	  								{{$ride->created_at}}
	  							</div>
	  						</div>
	  						<div class="row">
	  							<div class="col-lg-6">
	  								<p>Променето на:</p>
	  							</div>
	  							<div class="col-lg-6">
	  								{{$ride->updated_at}}
	  							</div>
	  						</div>
	  					</div>

	  				</div>
	  			</div>
	  		</div>

	  		<div class="box box-info">
	  			<div class="box-header with-border">
        			<div class="row">
        				<div class="col-lg-12">
        					<h4>Инфо за Локација</h4>
        				</div>
        			</div>
        		</div>
	  			<div class="box-body">
	  				<div class="row">
	  					<div id="directions-panel"></div>
	  				</div>
	  			</div>
	  		</div>

	  		<div class="box box-info">
	  			<div class="box-header with-border">
        			<div class="row">
        				<div class="col-lg-12">
        					<h4>Цена на чинење</h4>
        				</div>
        			</div>
        		</div>
	  			<div class="box-body">
	  				<div class="row">
	  					<div id='costsharing'></div>
	  				</div>
	  			</div>
	  		</div>

	  		<div class="box box-info">
	  			<div class="box-body">
	  				<div class="row">
	  					<div class="col-lg-2">
	  						<p>Детали за патувањето:</p>
	  					</div>
	  					<div class="col-lg-10">
							<div class="form-group">
	  							<textarea name="ride_comment" id="ride_comment" class="form-control">{{$ride->d_description}}</textarea>
	  						</div>
	  					</div>
	  				</div>
	  			</div>
	  		</div>

	  		<input type='hidden' id='d_stop1_value'>
		  	<input type='hidden' id='d_stop1_time' name="d_stop1_time">
		  	<input type='hidden' id='d_stop2_value'>
		  	<input type='hidden' id='d_stop2_time' name="d_stop2_time">
		  	<input type='hidden' id='d_stop3_value'>
		  	<input type='hidden' id='d_stop3_time' name="d_stop3_time">
		  	<input type='hidden' id='d_enddest_value'>
		  	<input type='hidden' id='d_enddest_time' name="d_enddest_time">
		  	<input type='hidden' id='distance' name='distance'>
		  	<input type='hidden' id='est_d_time' name='est_d_time'>

			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
		</section>
	</form>
	  	
	<section class="content">
		<div class="box box-info">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-lg-6 col-xs-6">
						<h4>Возач: <a href='/admin/view-user/{{$driver_info->id}}'>{!! Helper::userAvatarAdmin($driver_info->avatar,$driver_info->firstname,$driver_info->lastname,$driver_info->id) !!}</a></h4>
					</div>
				</div>
			</div>
			<div class="box-header with-border">
				<div class="row">
					<div class="col-lg-1 col-xs-6"><h4>Сопатници</h4></div>
				</div>
			</div>
			<div class="box-body">
				@if(count($booked_users) > 0)
					<table id="rides_table" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Име и Презиме</th>
								<th>Тргнува Од</th>
								<th>До дестинација</th>
								<th>Број на седишта</th>
								<th>Статус</th>
								<th>Цена (мкд)</th>
								<th>Резервирано на</th>
							</tr>
						</thead>
						<tbody>
							@foreach($booked_users as $b_user)
								@php 
									$fromPlace = $b_user->b_from;
									$toPlace = $b_user->b_to;
									$cena = $b_user->b_cost;
								@endphp
								<tr>
									<td><a href='/admin/view-user/{{$b_user->user_id}}'>{!! Helper::userAvatarAdmin($b_user->avatar,$b_user->firstname,$b_user->lastname,$b_user->user_id) !!}</a></td>
									<td>{{$ride->$fromPlace}}</td>
									<td>{{$ride->$toPlace}}</td>
									<td>{{$b_user->qty_places}}</td>
									<td>{{Helper::Status_Message($b_user->status)}}</td>
									<td>{{$ride->$cena}}</td>
									<td>{{$b_user->created_at}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				@else
				<p>Нема сопатници</p>
				@endif
			</div>
		</div>
	</section>
		

@endsection
@section('includecss')
    <link rel="stylesheet" href="{{ asset('css/admin-lte/admin.css')}}">
@endsection
@section('includesscripts')
<script>
	window.onload = function() {
	    var orig = document.getElementById('origin-input');
	    var dest = document.getElementById('destination-input');
	    var wp1 = document.getElementById('waypoints1');
	    var wp2 = document.getElementById('waypoints2');

	    orig.onpaste = function(e) {
	      e.preventDefault();
	    }
	    dest.onpaste = function(e) {
	      e.preventDefault();
	    }
	    wp1.onpaste = function(e) {
	      e.preventDefault();
	    }
	    wp2.onpaste = function(e) {
	      e.preventDefault();
	    }
  	}

  	back_s_guarantee = document.getElementById('back_s_guarantee');
  	if(back_s_guarantee.checked){
  	 	$(back_s_guarantee).val(1);
  	 	$(back_s_guarantee).prop('checked',true);

  	}else{
  	 	$(back_s_guarantee).val(0);
  	 	$(back_s_guarantee).prop('checked',false);
  	}

	$(function() {
    	$('#get_directions').click();
  	});

  	// Prevent Submit on Enter
  	$('#msform').on('keyup keypress', function(e) {
	    var keyCode = e.keyCode || e.which;
	    if(keyCode === 13){
	      	e.preventDefault();
	      	return false;
	    }
  	});

  	function changeVal(element){
  		if(element.checked){
  			$(element).val(1);
  		}else{
  			$(element).val(0);
  		}
  	}

  	function toggleCheckbox(element){
	    if(element.checked){
	      $(element).val(1);
	      $(element).prop('checked',true);
	    
	    }else{
	      $(element).val(0);
	      $(element).prop('checked',false);
	      $('#clockpicker2I, #datepicker2').val('');
	    }
  	}

  	$('.clockpicker1, .clockpicker2').clockpicker().find('input').focusout(function(){
	    // regular expression to match required time format
	    re = /^\d{1,2}:\d{2}([ap]m)?$/;

	    if(!this.value.match(re)){
	      this.value = '';
	    }
  	});

  	var date = new Date();
  	$('#datepicker1,#datepicker2').datepicker({
  		format: 'dd.mm.yyyy',
  		startDate: date,
  		endDate: '+365d',
  		autoclose: true,
  		language: 'mk',
  	});

  	var datepicker1 = $("#datepicker1").datepicker('getDate');
  	$("#datepicker2").datepicker('setStartDate',datepicker1);

  	$("#datepicker1").change(function(){
  		var datepicker1 = $("#datepicker1").datepicker('getDate');
  		var datepicker2 = $("#datepicker2").datepicker('getDate');
  		$("#datepicker2").datepicker('setStartDate',datepicker1);
  		if(datepicker2 < datepicker1){
  			$("#datepicker2").datepicker('clearDates');
  		}
  	});

  	$('#free_places').on('keydown keyup', function(e){
  		if ($(this).val() > 6
        && e.keyCode !== 46 // keycode for delete
        && e.keyCode !== 8 // keycode for backspace
        ) {
  			e.preventDefault();
  			$(this).val(3);
  		}

  		if ($(this).val() <= 0
	      && e.keyCode !== 46 // keycode for delete
	      && e.keyCode !== 8 // keycode for backspace
	      ) {
	  		e.preventDefault();
	  		$(this).val(3);
	  	}

		if(event.keyCode == 69){
		  	e.preventDefault();
		}
	});


  	function initMap() {
	    var directionsService = new google.maps.DirectionsService;
	    var directionsDisplay = new google.maps.DirectionsRenderer;
	    var map = new google.maps.Map(document.getElementById('map'), {
	      zoom: 8,
	      center: {lat: 41.71556, lng: 21.77556},
	      streetViewControl: false,
	      fullscreenControl: false
	    });
	    directionsDisplay.setMap(map);

	    document.getElementById('get_directions').addEventListener('click', function() {
	      calculateAndDisplayRoute(directionsService, directionsDisplay);
	    });
	    new AutocompleteDirectionsHandler(map);

	   	RideChangeUpdate();
  	}

  	function calculateAndDisplayRoute(directionsService, directionsDisplay) {
	    var waypts = [];
	    var checkboxArray1 = document.getElementById('waypoints1');
	    var checkboxArray2 = document.getElementById('waypoints2');

	    if(checkboxArray1.value != ""){
	      waypts.push({
	        location: checkboxArray1.value,
	        stopover: true
	      });
	    }

	    if(checkboxArray2.value != ""){
	      waypts.push({
	        location: checkboxArray2.value,
	        stopover: true
	      });
	    }

	    directionsService.route({
	      origin: document.getElementById('origin-input').value,
	      destination: document.getElementById('destination-input').value,
	      waypoints: waypts,
	      optimizeWaypoints: true,
	      travelMode: 'DRIVING'
	    }, function(response, status) {
	      if (status === 'OK') {
	        directionsDisplay.setDirections(response);
	        var route = response.routes[0];
	        var summaryPanel = document.getElementById('directions-panel');
	        summaryPanel.innerHTML = '';
	        var totalDistance = 0;
	        var totalDuration = 0;
	        var segmentCostshare = [];

	        $("#costsharing").empty();

	        // For each route, display summary information.
	        for (var i = 0; i < route.legs.length; i++) {
		          var routeSegment = i + 1;

		          //Format Strings for Part 2
		          var startAddress = route.legs[i].start_address.split(',');
		          var endAddress = route.legs[i].end_address.split(',');

		          if(startAddress.length >= 3){
		            startAddress_f = startAddress[0] + ',' + startAddress[startAddress.length - 2];
		          }else{
		            startAddress_f = startAddress[0];
		          }

		          if(endAddress.length >= 3){
		            endAddress_f = endAddress[0] + ',' + endAddress[endAddress.length - 2];
		          }else{
		            endAddress_f = endAddress[0];
		          }
		          var elememID = 'costShare' + routeSegment;
		          var appendInputs = "<div class='col-lg-3'><div class='form-group'><label class='costShareP'>" + startAddress_f + " <i class='fas fa-angle-double-right'></i> " + endAddress_f + "</label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' max='99999' step='10' /></div></div>";
		          $("#costsharing").append(appendInputs);

		          if(route.legs.length > 1){
		            summaryPanel.innerHTML += "<div class='col-lg-3'><p><span class='preseci'>" + route.legs[i].start_address + "</span> <span class='preseci'> <i class='fas fa-angle-double-right'></i></span> <span class='preseci'> " + route.legs[i].end_address + "</span></p><p><span class='dolzina'><i class='fas fa-road'></i> " + parseInt(route.legs[i].distance.text) + " км </span><span class='saat'><i class='far fa-clock'></i> " + secondsToHms(route.legs[i].duration.value) + "</p></div>";
		          }
		          totalDistance = totalDistance + route.legs[i].distance.value;
		          totalDuration = totalDuration + route.legs[i].duration.value;

		          $('#d_stop' + routeSegment + '_value').val(route.legs[i].duration.value);

		          if(route.legs.length >= 2 && i == 1){
		            document.getElementById('waypoints1').value = route.legs[1].start_address;
		          }

		          if(route.legs.length == 3 && i == 2){
		            document.getElementById('waypoints2').value = route.legs[2].start_address;
		          }

        	}

	        if(route.legs.length >= 1){

	          //Format Strings for Part 2
	          var startAddress = route.legs[0].start_address.split(',');
	          var endAddress = route.legs[route.legs.length - 1].end_address.split(',');

	          if(startAddress.length >= 3){
	            startAddress_f = startAddress[0] + ',' + startAddress[startAddress.length - 2];
	          }else{
	            startAddress_f = startAddress[0];
	          }

	          if(endAddress.length >= 3){
	            endAddress_f = endAddress[0] + ',' + endAddress[endAddress.length - 2];
	          }else{
	            endAddress_f = endAddress[0];
	          }

	          if(route.legs.length == 3){
	            //First Middle Address
	            var middleAddress = route.legs[1].start_address.split(',');
	            if(middleAddress.length >= 3){
	              middleAddress_f = middleAddress[0] + ',' + middleAddress[middleAddress.length - 2];
	            }else{
	              middleAddress_f = middleAddress[0];
	            }
	            //Second Middle Address
	            var middleAddress2 = route.legs[2].start_address.split(',');
	            if(middleAddress2.length >= 3){
	              middleAddress_f2 = middleAddress2[0] + ',' + middleAddress2[middleAddress2.length - 2];
	            }else{
	              middleAddress_f2 = middleAddress2[0];
	            }

	            routeSegment = routeSegment + 1;
	            var elememID = 'costShare' + routeSegment;

	            var appendInputs = "<div class='col-lg-3'><div class='form-group'><label class='costShareP'>" + startAddress_f + " <i class='fas fa-angle-double-right'></i> " + middleAddress_f2 + "</label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' min='10' max='99999' step='10' value='10' /></div></div>";
	            $("#costsharing").append(appendInputs);

	            routeSegment = routeSegment + 1;
	            var elememID = 'costShare' + routeSegment;

	            var appendInputs = "<div class='col-lg-3'><div class='form-group'><label class='costShareP'>" + middleAddress_f + " <i class='fas fa-angle-double-right'></i> " + endAddress_f + "</label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' max='99999' step='10' /></div></div>";
	            $("#costsharing").append(appendInputs);
	          }

	          if(route.legs.length > 1){

	            routeSegment = routeSegment + 1;
	            var elememID = 'costShare' + routeSegment;

	            var appendInputs = "<div class='col-lg-3'><div class='form-group'><label class='costShareP'>" + startAddress_f + " <i class='fas fa-angle-double-right'></i> " + endAddress_f + "</label><input type='number' id='" + elememID + "' name='" + elememID + "' class='costShareI form-control' max='99999' step='10' /></div></div>";
	            $("#costsharing").append(appendInputs);
	          }

	          document.getElementById('origin-input').value = route.legs[0].start_address;
	          document.getElementById('destination-input').value = route.legs[route.legs.length - 1].end_address;


	          totalDistance = totalDistance / 1000;

	          summaryPanel.innerHTML += "<div class='col-lg-3'><p><span class='preseci'>" + startAddress + "</span> <span class='preseci'><i class='fas fa-angle-double-right'></i></span> <span class='preseci'>" + endAddress + "</span></p><p><span class='dolzina'><i class='fas fa-road'></i> "+ parseInt(totalDistance) + " км </span><span class='saat'><i class='fas fa-clock'></i> "+ secondsToHms(totalDuration) + "</span></p></div>";


	          document.getElementById('distance').value = parseInt(totalDistance);
	          document.getElementById('est_d_time').value = secondsToHms(totalDuration);
	        }

	        var d_from = {!! json_encode($ride->d_from); !!};
	        var d_to = {!! json_encode($ride->d_to); !!};
	        var d_from_Input = document.getElementById('origin-input').value;
	        var d_to_Input = document.getElementById('destination-input').value;

	        if(d_from == d_from_Input && d_to == d_to_Input){

	          if(document.getElementById('costShare1')){
	            var d_cost1 = {!! json_encode($ride->d_cost1); !!};
	            document.getElementById('costShare1').value = d_cost1;
	          }
	          if(document.getElementById('costShare2')){
	            var d_cost2 = {!! json_encode($ride->d_cost2); !!};
	            if(d_cost2){
	              document.getElementById('costShare2').value = d_cost2;
	            }else{
	              document.getElementById('costShare2').value = 1;
	            }
	          }
	          if(document.getElementById('costShare3')){
	            var d_cost3 = {!! json_encode($ride->d_cost3); !!};
	            if(d_cost3){
	              document.getElementById('costShare3').value = d_cost3;
	            }else{
	              document.getElementById('costShare3').value = 1;
	            }
	          }
	          if(document.getElementById('costShare4')){
	            var d_cost4 = {!! json_encode($ride->d_cost4); !!};
	            if(d_cost4){
	              document.getElementById('costShare4').value = d_cost4;
	            }else{
	              document.getElementById('costShare4').value = 1;
	            }
	          }
	          if(document.getElementById('costShare5')){
	            var d_cost5 = {!! json_encode($ride->d_cost5); !!};
	            if(d_cost5){
	              document.getElementById('costShare5').value = d_cost5;
	            }else{
	              document.getElementById('costShare5').value = 1;
	            }
	          }
	          if(document.getElementById('costShare6')){
	            var d_cost6 = {!! json_encode($ride->d_cost6); !!};
	            if(d_cost6){
	              document.getElementById('costShare6').value = d_cost6;
	            }else{
	              document.getElementById('costShare6').value = 1;
	            }
	          }

	        }

	        RideChangeUpdate();

	    } else {
	        window.alert('Directions request failed due to ' + status);
	    }
    });
  }

  function AutocompleteDirectionsHandler(map) {
    this.map = map;
    this.originPlaceId = null;
    this.destinationPlaceId = null;
    this.travelMode = 'DRIVING';
    var originInput = document.getElementById('origin-input');
    var destinationInput = document.getElementById('destination-input');
    var waypointinput1 = document.getElementById('waypoints1');
    var waypointinput2 = document.getElementById('waypoints2');

    var modeSelector = document.getElementById('mode-selector');
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.directionsDisplay.setMap(map);

    var originAutocomplete = new google.maps.places.Autocomplete(
      	originInput, {fields: ['place_id']});
    var destinationAutocomplete = new google.maps.places.Autocomplete(
      	destinationInput, {fields: ['place_id']});
    var waypointinputcomplete1 = new google.maps.places.Autocomplete(
      	waypointinput1, {fields: ['place_id']});
    var waypointinputcomplete2 = new google.maps.places.Autocomplete(
      	waypointinput2, {fields: ['place_id']});

            this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
            this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');
            this.setupPlaceChangedListener(waypointinputcomplete1, 'WAYP1');
            this.setupPlaceChangedListener(waypointinputcomplete2, 'WAYP2');
  }

  AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
  	var me = this;
  	autocomplete.bindTo('bounds', this.map);

  	autocomplete.addListener('place_changed', function() {
  		var place = autocomplete.getPlace();
  		var originInput = document.getElementById('origin-input').value;
  		var destinationInput = document.getElementById('destination-input').value;

  		if (!place.place_id) {
  			window.alert("Ве молиме одберете некоја од понудените опции.");
  			return;
  		}
  		if (mode === 'ORIG') {
  			me.originPlaceId = place.place_id;

  			if(destinationInput != ''){
  				document.getElementById('get_directions').click();
  			}
  		}else if(mode === 'DEST') {
  			me.destinationPlaceId = place.place_id;

  			if(originInput != ''){
  				document.getElementById('get_directions').click();
  			}
  		}else if(mode === 'WAYP1'){

  			if(originInput != '' && destinationInput != ''){
  				document.getElementById('get_directions').click();
  			}
  		}else{

  			if(originInput != '' && destinationInput != ''){
  				document.getElementById('get_directions').click();
  			}
  		}

  		me.route();
  	});
  };

  AutocompleteDirectionsHandler.prototype.route = function() {
    if (!this.originPlaceId || !this.destinationPlaceId) {
      return;
    }
    var me = this;
  };

  $("#waypoints1").focusout(function(){
    if(this.value == ''){
      originI = document.getElementById('origin-input').value;
      destI = document.getElementById('destination-input').value;

      if(originI != '' && destI != ''){
        document.getElementById('get_directions').click();
      }else{
        initMap();
      }
    }
  });

  $("#waypoints2").focusout(function(){
    originI = document.getElementById('origin-input').value;
    destI = document.getElementById('destination-input').value;
    if(this.value == ''){
    
      if(originI != '' && destI != ''){
        document.getElementById('get_directions').click();
      }else{
        initMap();
      }
    }
  });

  $("#destination-input").focusout(function(){
    originI = document.getElementById('origin-input').value;

    if(this.value == '' && originI != ''){
      initMap();
      var summaryPanel = document.getElementById('directions-panel');
      summaryPanel.innerHTML = '';
    }
  });

  $("#origin-input").focusout(function(){
    destI = document.getElementById('destination-input').value;
    if(this.value == '' && destI != ''){
      initMap();
      var summaryPanel = document.getElementById('directions-panel');
      summaryPanel.innerHTML = '';
    }
  });
  
function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " час и " : " часа и ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " минута " : " минути ") : "";

    return hDisplay + mDisplay;
  }

  function timeToSeconds(time) {
    time = time.split(/:/);
    return time[0] * 3600 + time[1] * 60;
  }

  function format_two_digits(n) {
    return n < 10 ? '0' + n : n;
  }

function RideChangeUpdate() { 

     var clockpicker1 = $('#clockpicker1I').val();
        var d_stop1_value = Number(document.getElementById('d_stop1_value').value);

        var h = Math.floor(d_stop1_value / 3600);
        var m = Math.floor(d_stop1_value % 3600 / 60);

        var date = new Date("October 13, 2014 "+clockpicker1);

        if(m > 0){
          date.setMinutes(date.getMinutes()+m);
        }
        if(h > 0){
          date.setHours(date.getHours()+h);
        }

        hours = format_two_digits(date.getHours());
        minutes = format_two_digits(date.getMinutes());
        document.getElementById('d_stop1_time').value = hours + ":" + minutes;

        if(document.getElementById('d_stop2_value').value != ''){

          var d_stop1_time = $('#d_stop1_time').val();
          var d_stop2_value = Number(document.getElementById('d_stop2_value').value);

          var h = Math.floor(d_stop2_value / 3600);
          var m = Math.floor(d_stop2_value % 3600 / 60);

          var date = new Date("October 13, 2014 "+d_stop1_time);

          if(m > 0){
            date.setMinutes(date.getMinutes()+m);
          }
          if(h > 0){
            date.setHours(date.getHours()+h);
          }

          hours = format_two_digits(date.getHours());
          minutes = format_two_digits(date.getMinutes());
          document.getElementById('d_stop2_time').value = hours + ":" + minutes;

        }

        if(document.getElementById('d_stop3_value').value != ''){

          var d_stop2_time = $('#d_stop2_time').val();
          var d_stop3_value = Number(document.getElementById('d_stop3_value').value);

          var h = Math.floor(d_stop3_value / 3600);
          var m = Math.floor(d_stop3_value % 3600 / 60);

          var date = new Date("October 13, 2014 "+d_stop2_time);

          if(m > 0){
            date.setMinutes(date.getMinutes()+m);
          }
          if(h > 0){
            date.setHours(date.getHours()+h);
          }

          hours = format_two_digits(date.getHours());
          minutes = format_two_digits(date.getMinutes());
          document.getElementById('d_stop3_time').value = hours + ":" + minutes;
        }     
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuvTR6aAp_y_rcc_w1m087uc9jh84V3Qk&libraries=places&callback=initMap&language=mk&region=mk"></script>
@endsection