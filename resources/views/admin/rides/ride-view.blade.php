@extends('admin.layouts')

@section('content')
  @if(!empty($ride))
  
    <section class="content-header">
      <h1>Патување <small>Преглед</small></h1>
      <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Почетна</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section class="content">
      <div class="box box-info">
        <div class="box-header with-border">
           <div class="row">
            <div class="col-lg-12 col-xs-12 text-right">
              <a href="/admin/edit-ride/{{$ride->id}}" class="btn btn-primary">Промени</a>
            </div>
          </div>
        </div>

        <div class="box-body">
          <div class="row">
            <div class="col-lg-12">
              <div id="map"></div>
            </div>
          </div>
        </div>
      </div>
    </section>

   <section class="content">
    <div class="box box-info">
      <div class="box-body">
        <div class="row">
          <div class="col-lg-6">

            <input id="origin-input" name="d_from" class="controls form-control" type="hidden" value="{{$ride->d_from}}">
            <input id="destination-input" name="d_to" class="controls form-control" type="hidden" value="{{$ride->d_to}}">
            <input id="waypoints1" name="d_stop_1" class="controls waypoints form-control" type="hidden" value="{{$ride->d_stop_1}}">
            <input id="waypoints2" name="d_stop_2" class="controls waypoints form-control" type="hidden" value="{{$ride->d_stop_2}}">

            <div class="row">
              <div class="col-lg-6"><p>Локација на поаѓање:</p></div>
              <div class="col-lg-6">{{ $ride->d_from }}</div>
            </div>

            <div class="row">
              <div class="col-lg-6"><p>Локација на пристигнување:</p></div>
              <div class="col-lg-6">{{ $ride->d_to }}</div>
            </div>

            <div class="row">
              <div class="col-lg-6"><p>Дата/Време на поаѓање:</p></div>
              <div class="col-lg-6">{{ Carbon\Carbon::parse($ride->d_date)->format('d-m-Y') }} - {{$ride->d_time}}</div>
            </div>

            <button type="button" id="get_directions" style="height: 1px; width:1px; visibility: hidden;"></button>

          </div>
          <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-6"><p>Стопирачка локација 1:</p></div>
              <div class="col-lg-6"><p>{{ $ride->d_stop_1 }}</p></div>
            </div>

            <div class="row">
              <div class="col-lg-6"><p>Стопирачка локација 2:</p></div>
              <div class="col-lg-6"><p>{{ $ride->d_stop_2}}</p></div>
            </div>

            <div class="row">
              <div class="col-lg-6"><p>Повратно патување: @if($ride->roundtrip) Да @else Не @endif</p></div>
              <div class="col-lg-6"><p>@if($ride->roundtrip) {{ Carbon\Carbon::parse($ride->d_rdate)->format('d-m-Y') }} - {{$ride->d_rtime}} @endif</p></div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="box box-info">
      <div class="box-body">
        <div class="row">
          <div class="col-lg-4">
            <div class="row">
                <div class="col-lg-6"><p>Слободни места:</p></div>
                <div class="col-lg-6">{{$ride->free_places}}</div>
            </div>

            <div class="row">
              <div class="col-lg-6"><p>Реални Слободни места:</p></div>
              <div class="col-lg-6">{{$r_qty_places}}</div>
            </div>

            <div class="row">
              <div class="col-lg-6"><p>Гаранција за задни седишта:</p></div>
              <div class="col-lg-6">{{Helper::bool_beautify($ride->back_s_guarantee)}}</div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="row">
              <div class="col-lg-6"><p>Оддалеченост:</p></div>
              <div class="col-lg-6">{{$ride->distance}} км</div>
            </div>
            <div class="row">
              <div class="col-lg-6"><p>Време на патување:</p></div>
              <div class="col-lg-6">{{$ride->est_d_time}}</div>
            </div>
            <div class="row">
              <div class="col-lg-6"><p>Услови за користење:</p></div>
              <div class="col-lg-6">{{Helper::bool_beautify($ride->terms_cond)}}</div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="row">
              <div class="col-lg-6"><p>Статус:</p></div>
              <div class="col-lg-6">{{Helper::Ride_Status($ride->status)}}</div>
            </div>
            <div class="row">
              <div class="col-lg-6"><p>Креирано на:</p></div>
              <div class="col-lg-6">{{$ride->created_at}}</div>
            </div>
            <div class="row">
              <div class="col-lg-6"><p>Променето на:</p></div>
              <div class="col-lg-6">{{$ride->updated_at}}</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="box box-info">
      <div class="box-body">
        <div class="row">
          <div class="col-lg-2"><p>Детали за патувањето:</p></div>
          <div class="col-lg-10">{{$ride->d_description}}</div>
        </div>
      </div>
    </div>

    <div class="box box-info">
      <div class="box-body">
        <div class="row">

          @if($ride->d_stop_1 == '' && $ride->d_stop_2 == '')
            <div class="col-lg-3">
              <p><i class="fas fa-map-marker-alt text-success"></i> {{ Helper::Short_Destination($ride->d_from)}}</p>
              <p><i class="fas fa-map-marker-alt text-danger"></i> {{ Helper::Short_Destination($ride->d_to)}}</p>
              <p>{{$ride->d_cost1}} мкд</p>
            </div>
          @endif

          @if($ride->d_stop_1 != '')
            <div class="col-lg-3">
              <p><i class="fas fa-map-marker-alt text-success"></i> {{ Helper::Short_Destination($ride->d_from)}}</p>
              <p><i class="fas fa-map-marker-alt text-danger"></i> {{ Helper::Short_Destination($ride->d_stop_1)}}</p>
              <p>{{$ride->d_cost1}} мкд</p>
            </div>
          @endif
          
          @if($ride->d_stop_2 != '')
            <div class="col-lg-3">
              <p><i class="fas fa-map-marker-alt text-success"></i> {{ Helper::Short_Destination($ride->d_stop_1)}}</p>
              <p><i class="fas fa-map-marker-alt text-danger"></i> {{ Helper::Short_Destination($ride->d_stop_2)}}</p>
              <p>{{$ride->d_cost2}} мкд</p>
            </div>
          @endif
          
          @if($ride->d_stop_2 != '')
            <div class="col-lg-3"> 
              <p><i class="fas fa-map-marker-alt text-success"></i> {{ Helper::Short_Destination($ride->d_stop_2)}}</p>
              <p><i class="fas fa-map-marker-alt text-danger"></i> {{ Helper::Short_Destination($ride->d_to)}}</p>
              <p>{{$ride->d_cost3}} мкд</p>
            </div>
          @endif
          
          @if($ride->d_stop_1 != '' && $ride->d_stop_2 == '')
            <div class="col-lg-3">
              <p><i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_stop_1)}}</p>
              <p><i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_to)}}</p>
              <p>{{$ride->d_cost2}} мкд</p>
            </div>
          @endif
          
          @if($ride->d_stop_1 != '' && $ride->d_stop_2 == '')
            <div class="col-lg-3">
              <p><i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_from)}}</p>
              <p><i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_to)}}</p>
              <p>{{$ride->d_cost3}} мкд</p>
            </div>
          @endif

          @if($ride->d_stop_1 != '' && $ride->d_stop_2 != '')
            <div class="col-lg-3">
              <p><i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_from)}}</p>
              <p><i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_stop_2)}}</p>
              <p>{{$ride->d_cost4}} мкд</p>
            </div>
          @endif

          @if($ride->d_stop_1 != '' && $ride->d_stop_2 != '')
            <div class="col-lg-3">
              <p><i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_stop_1)}}</p>
              <p><i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_to)}}</p>
              <p>{{$ride->d_cost5}} мкд</p>
            </div>
          @endif
          
          

          @if($ride->d_stop_1 != '' && $ride->d_stop_2 != '')
            <div class="col-lg-3">
              <p><i class="fas fa-map-marker-alt text-success"></i> {{Helper::Short_Destination($ride->d_from)}}</p>
              <p><i class="fas fa-map-marker-alt text-danger"></i> {{Helper::Short_Destination($ride->d_to)}}</p>
              <p>{{$ride->d_cost6}} мкд</p>
            </div>
          @endif

        </div>
      </div>
    </div>

    <div class="box box-info">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-lg-6 col-xs-6">
            <h4>Возач: <a href='/admin/view-user/{{$driver_info->id}}'>{!! Helper::userAvatarAdmin($driver_info->avatar,$driver_info->firstname,$driver_info->lastname,$driver_info->id) !!}</a></h4>
          </div>
        </div>
      </div>
      <div class="box-header with-border">
        <div class="row">
          <div class="col-lg-1 col-xs-6"><h4>Сопатници</h4></div>
        </div>
      </div>
      <div class="box-body">
        @if(count($booked_users) > 0)
          <table id="rides_table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Име и Презиме</th>
                <th>Тргнува Од</th>
                <th>До дестинација</th>
                <th>Број на седишта</th>
                <th>Статус</th>
                <th>Цена (мкд)</th>
                <th>Резервирано на</th>
              </tr>
            </thead>
            <tbody>
              @foreach($booked_users as $b_user)
                @php 
                  $fromPlace = $b_user->b_from;
                  $toPlace = $b_user->b_to;
                  $cena = $b_user->b_cost;
                @endphp
                  <tr>
                    <td><a href='/admin/view-user/{{$b_user->user_id}}'>{!! Helper::userAvatarAdmin($b_user->avatar,$b_user->firstname,$b_user->lastname,$b_user->user_id) !!}</a></td>
                    <td>{{$ride->$fromPlace}}</td>
                    <td>{{$ride->$toPlace}}</td>
                    <td>{{$b_user->qty_places}}</td>
                    <td>{{Helper::Status_Message($b_user->status)}}</td>
                    <td>{{$ride->$cena}}</td>
                    <td>{{$b_user->created_at}}</td>
                  </tr>
              @endforeach
            </tbody>
          </table>
        @else
          <p>Нема сопатници</p>
        @endif
      </div>
    </div>
  </section>

@else
  <section class="content">
    <div class="box text-center">
      <div class="box-header with-border">
        <h4>Записот не постои</h4>
      </div>
    </div>
  </section>
@endif

@endsection
@section('includecss')
  <link rel="stylesheet" href="{{ asset('css/admin-lte/admin.css')}}">
@endsection
@section('includesscripts')
<script>
	$(function() {
    $('#get_directions').click();
  });

  function initMap() {
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: {lat: 41.71556, lng: 21.77556},
      streetViewControl: false,
      fullscreenControl: false
    });
    directionsDisplay.setMap(map);

    document.getElementById('get_directions').addEventListener('click', function() {
      calculateAndDisplayRoute(directionsService, directionsDisplay);
    });
    new AutocompleteDirectionsHandler(map);
  }

  function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    var waypts = [];
    var checkboxArray1 = document.getElementById('waypoints1');
    var checkboxArray2 = document.getElementById('waypoints2');

    if(checkboxArray1.value != ""){
      waypts.push({
        location: checkboxArray1.value,
        stopover: true
      });
    }

    if(checkboxArray2.value != ""){
      waypts.push({
        location: checkboxArray2.value,
        stopover: true
      });
    }

    directionsService.route({
      origin: document.getElementById('origin-input').value,
      destination: document.getElementById('destination-input').value,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
        var route = response.routes[0];

      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  function AutocompleteDirectionsHandler(map) {
    this.map = map;
    this.originPlaceId = null;
    this.destinationPlaceId = null;
    this.travelMode = 'DRIVING';
    var originInput = document.getElementById('origin-input');
    var destinationInput = document.getElementById('destination-input');
    var waypointinput1 = document.getElementById('waypoints1');
    var waypointinput2 = document.getElementById('waypoints2');

    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.directionsDisplay.setMap(map);

    var originAutocomplete = new google.maps.places.Autocomplete(
      originInput, {fields: ['place_id']});
    var destinationAutocomplete = new google.maps.places.Autocomplete(
      destinationInput, {fields: ['place_id']});
    var waypointinputcomplete1 = new google.maps.places.Autocomplete(
      waypointinput1, {fields: ['place_id']});
    var waypointinputcomplete2 = new google.maps.places.Autocomplete(
      waypointinput2, {fields: ['place_id']});
  }

  AutocompleteDirectionsHandler.prototype.route = function() {
    if (!this.originPlaceId || !this.destinationPlaceId) {
      return;
    }
    var me = this;
  };
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuvTR6aAp_y_rcc_w1m087uc9jh84V3Qk&libraries=places&callback=initMap&language=mk&region=mk"></script>
@endsection