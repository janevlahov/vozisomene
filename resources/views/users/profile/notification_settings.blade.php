@extends('layouts.profile')

@section('profile_content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3><i class="fas fa-bell"></i> Известувања</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <form action="" method="POST">
          <p class="lead text-secondary">Те молам избери за што сакаш да добиваш Е-маил известувања!</p>
          <div class="checkbox checkbox-primary">
            <input type="checkbox" id='new_ride' name="new_ride" value="{{$UserData->NotificationSettings->new_ride}}" @if($UserData->NotificationSettings->new_ride == 1) checked="checked" @endif>
            <label for="new_ride">Твое ново патување</label>
          </div>
          <div class="checkbox checkbox-primary">
            <input type="checkbox" name="update_ride" value="{{$UserData->NotificationSettings->update_ride}}" @if($UserData->NotificationSettings->update_ride == 1) checked="checked" @endif>
            <label for="update_ride">Направена измена на патувањето</label>
          </div>
          <div class="checkbox checkbox-primary">
            <input type="checkbox" name="ride_request" value="{{$UserData->NotificationSettings->ride_request}}" @if($UserData->NotificationSettings->ride_request == 1) checked="checked" @endif>
            <label for="ride_request">Нова/измена на резервација на патување</label>
          </div>
          <div class="checkbox checkbox-primary">
            <input type="checkbox" name="ride_status" value="{{$UserData->NotificationSettings->ride_status}}" @if($UserData->NotificationSettings->ride_status == 1) checked="checked" @endif>
            <label for="ride_status">Прифатено/одбиено патување</label>
          </div>
          <div class="checkbox checkbox-primary">
            <input type="checkbox" name="comment_added" value="{{$UserData->NotificationSettings->comment_added}}" @if($UserData->NotificationSettings->comment_added == 1) checked="checked" @endif>
            <label for="new_ride">Нов/променет коментар</label>
          </div>
          <div class="checkbox checkbox-primary">
            <input type="checkbox" name="promotions_news" value="{{$UserData->NotificationSettings->promotions_news}}" @if($UserData->NotificationSettings->promotions_news == 1) checked="checked" @endif>
            <label for="new_ride">Промоции и Новости</label>
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('includesscripts')
 <script>
$('input[type="checkbox"]').change(function(){
    cb = $(this);
    cb.val(cb.prop('checked'));

   $.ajaxSetup({
 		headers: {
    		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	}
	})
    $.ajax({
      	url: "/users/profile/actions/update_notification_settings",
      	type: 'post',
      	data: {
        	name: cb.attr('name'),
         	value: cb.val(),
    	},
     	success: function(result){

    }});

 });
</script>
@endsection
