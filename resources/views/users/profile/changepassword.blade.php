@extends('layouts.profile')
@section('profile_content')
<div class="panel panel-info">
	<div class="panel-heading">
		<h3><i class="fas fa-user"></i> Промени Лозинка</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<form action="{{route('users.updatepassword', $currentuserid)}}" method="POST">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="password">Нова Лозинка:</label>
								<input type="password" class="form-control" id="password" name="password">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="password_confirmation">Потврди Лозинка:</label>
								<input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
							</div>
						</div>
					</div>
					
					<input type="hidden" name="_method" value="PUT">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-icon-split">
							<span class="icon text-white-50"><i class="fas fa-save"></i></span>
							<span class="text">Зачувај</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
