@extends('layouts.profile')

@section('profile_content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3><i class="fas fa-map-marker-alt"></i> Твојата адреса</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <form action="{{route('users.update_address_details')}}" method="POST">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label" for="firstname">Адреса на живеење:</label>
                <input type="text" class="form-control" id="address" name="address" value="{{$UserDataDetails->address}}">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="lastname">Поштенски Број:</label>
                <input type="text" class="form-control" id="zip" name="zip" value="{{$UserDataDetails->zip}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="city">Град:</label>
                <input type="text" class="form-control" id="city" name="city" value="{{$UserDataDetails->city}}">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="state">Држава:</label>
                <select class="form-control" id="state" name='state'>
                  @if(count($StatesList) > 0)
                  @if($UserDataDetails->state == false)
                  <option value="" disabled="" selected="">Избери Држава</option>
                  @endif
                  @foreach($StatesList as $State)
                  <option value="{{$State}}" @if($State == $UserDataDetails->state) selected='selected' @endif>{{$State}}</option>
                  @endforeach
                  @endif
                </select>
              </div>
            </div>
          </div>

          <input type="hidden" name="_method" value="PUT">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          <div class="form-group">
              <button type="submit" class="btn btn-primary btn-icon-split btn-lg">
  							<span class="icon text-white-50"><i class="fas fa-save"></i></span>
  							<span class="text">Зачувај</span>
  						</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
