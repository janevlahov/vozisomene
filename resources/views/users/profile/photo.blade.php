@extends('layouts.profile')

@section('profile_content')
<div class="panel panel-info">
	<div class="panel-heading">
		<h3><i class="fas fa-image"></i> Твојата профилна слика</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6">
				@if(empty($UserData->Userdetails->avatar))
					<p class="lead text-secondary">Додади профилна слика!</p>
					<p>Останатите корисници ќе имаат повеќе доверба доколку ја видат личноста со која ќе патуваат, а воедно полесно може да се препознаете едни со други. Така многу полесно ќе пронајдеш сопатници.</p>
				@else
				<p class="lead text-primary text-center">{{$UserData->firstname}} {{$UserData->lastname}}</p>

					<img src="/storage/upload/users/{{$UserData->Userdetails->avatar}}" class="slika img-responsive mx-auto mb-3" id="avatarImg">

				<p class="lead text-secondary text-center">Промени ја твојата профилна слика!</p>
				@endif
					<div id="krop" class="krop hidden mb-3">
						<span class="small text-white text-strong">Влечи за подобро позиционирање</span>
						<div id="upload-demo"></div>
						<p class="small text-center">Сечи ја сликата ако е потребно</p>
					</div>
					<div class="input-file-container mx-auto">
						<input type="file" id="upload" name='avatar_img' class="input-file">
						<label tabindex="0" for="upload" class="input-file-trigger btn btn-primary btn-lg mx-auto">Одбери слика...</label>
						<p class="file-return"></p>
					</div>
			</div>
			<div class="col-xs-12 col-md-6">
					<p class="lead text-secondary text-xs-center mt-xs-3">Каква треба профилната слика?</p>
					<p>Сликата треба да биде јасна и светла, да се гледа вашето лице, да не носите очила за сонце и да гледате право.</p>
					<div class="row">
						<div class="col-xs-6 col-md-6 text-center">
							<img src="/storage/upload/web/profil_pic_4.png" class="img-responsive mx-auto" alt="">
							<p>Добра слика</p>
						</div>
						<div class="col-xs-6 col-md-6 text-center">
							<img src="/storage/upload/web/profil_pic_3.png" class="img-responsive mx-auto" alt="">
							<p>Лоша слика</p>
						</div>
					</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-primary btn-icon-split btn-lg upload-result hidden">
					<span class="icon text-white-50"><i class="fas fa-save"></i></span>
					<span class="text">Зачувај</span>
				</button>
			</div>
		</div>
		@if (!empty($message))
	{{$message}}
	@endif
	</div>

	</div>

@endsection
@section('includecss')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.3/croppie.min.css" rel="stylesheet">
@endsection

@section('includesscripts')
<script>
document.querySelector("html").classList.add('js');

	var fileInput  = document.querySelector( ".input-file" ),
		button     = document.querySelector( ".input-file-trigger" ),
		the_return = document.querySelector(".file-return");

	button.addEventListener( "keydown", function( event ) {
		if ( event.keyCode == 13 || event.keyCode == 32 ) {
				fileInput.focus();
		}
	});
	button.addEventListener( "click", function( event ) {
		fileInput.focus();
		return false;
	});
	fileInput.addEventListener( "change", function( event ) {
		the_return.innerHTML = this.value;
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.3/croppie.min.js"></script>

<script type="text/javascript">

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$uploadCrop = $('#upload-demo').croppie({

    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'circle'
    },

    boundary: {
        width: 300,
        height: 300
    }
});

$('#upload').on('change', function () {

  var reader = new FileReader();
    reader.onload = function (e) {
      $uploadCrop.croppie('bind', {
        url: e.target.result
      });
    }

    reader.readAsDataURL(this.files[0]);
    if ( /\.(jpe?g|png|gif)$/i.test(this.files[0].name) === false ) {
    	alert("Овој тип на документ не е дозволен. Ве молиме прикачете слика");
    	$( "#krop" ).addClass( "hidden" );
    	$("#upload").val('');
    	return false;
    }

	$( "#krop, .upload-result" ).removeClass( "hidden" );
});

$('.upload-result').on('click', function (ev) {

  $uploadCrop.croppie('result', {
    type: 'canvas',
    size: 'viewport'

  }).then(function (resp) {

    var file = document.getElementById("upload").value;

    $.ajax({
      url: "/users/profile/avatar",
      type: "POST",

      data: {"image":resp, "avatar_img":file},

      success: function (data) {
        $('#avatarImg').attr("src",resp);
       	location.reload();
      },
      error: function (err) {
        console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
      }
    });
  });
});
</script>
@endsection
