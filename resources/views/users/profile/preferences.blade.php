@extends('layouts.profile')

@section('profile_content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3><i class="fas fa-user-circle"></i> Твоите поставки</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <p class="lead text-secondary">Твоите поставки поврзани со споделувањето на превозот.</p>
        <p>Те молам одбери ги точно твоите поставки. Така другите корисници ќе имаат претстава за тебе и многу полесно ќе ги пронајдеш идеалните сопатници.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <form class="" action="{{route('users.update_preferences')}}" method="POST">
          <div class="row">
            <div class="col-xs-12 col-md-3">
              <p class="lead text-secondary text-center">Зборлив</p>
              <div class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default @if($UserData->Userpreferences->talkative == 'talk_1') active @endif">
                  <input type="radio" name="talkative" id="talk_1" value="talk_1" @if($UserData->Userpreferences->talkative == 'talk_1') checked="checked" @endif>
                  <span class="fa-stack fa-2x" data-toggle="tooltip" data-placement="top" title="Јас сум тивок човек">
                    <i class="fas fa-comments fa-stack-1x text-muted"></i>
                    <i class="fas fa-ban fa-stack-2x text-danger"></i>
                  </span>
                </label>
                <label class="btn btn-default @if($UserData->Userpreferences->talkative == 'talk_2') active @endif">
                  <input type="radio" name="talkative" id="talk_2" value="talk_2" @if($UserData->Userpreferences->talkative == 'talk_2') checked="checked" @endif>
                  <i class="fas fa-comments fa-2x text-muted" data-toggle="tooltip" data-placement="top" title="Разговарам во зависност од расположението"></i>
                </label>
                <label class="btn btn-default @if($UserData->Userpreferences->talkative == 'talk_3') active @endif">
                  <input  type="radio" name="talkative" id="talk_3" value="talk_3" @if($UserData->Userpreferences->talkative == 'talk_3') checked="checked" @endif>
                  <i class="fas fa-comments fa-2x text-primary" data-toggle="tooltip" data-placement="top" title="Возењето неможе да помине без раговор"></i>
                </label>
              </div>
            </div>
            <div class="col-xs-12 col-md-3">
              <p class="lead text-secondary text-center mt-xs-3">Пушач</p>
              <div class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default @if($UserData->Userpreferences->smoker == 'smoke_1') active @endif">
                  <input type="radio" name="smoker" value="smoke_1" @if($UserData->Userpreferences->smoker == 'smoke_1') checked="checked" @endif>
                  <span class="fa-stack fa-2x" data-toggle="tooltip" data-placement="top" title="Не дозволувам пушење во автомобилот">
                    <i class="fas fa-joint fa-stack-1x text-muted"></i>
                    <i class="fas fa-ban fa-stack-2x text-danger"></i>
                  </span>
                </label>
                <label class="btn btn-default @if($UserData->Userpreferences->smoker == 'smoke_2') active @endif">
                  <input type="radio" name="smoker" value="smoke_2"  @if($UserData->Userpreferences->smoker == 'smoke_2') checked="checked" @endif>
                  <i class="fas fa-joint fa-2x text-muted" data-toggle="tooltip" data-placement="top" title="Понекогаш дозволувам пушење во автомобилот"></i>
                </label>
                <label class="btn btn-default @if($UserData->Userpreferences->smoker == 'smoke_3') active @endif">
                  <input type="radio" name="smoker" value="smoke_3"  @if($UserData->Userpreferences->smoker == 'smoke_3') checked="checked" @endif>
                  <i class="fas fa-joint fa-2x text-primary" data-toggle="tooltip" data-placement="top" title="Дозволувам пушење во автомобилот"></i>
                </label>
              </div>
            </div>
            <div class="col-xs-12 col-md-3">
              <p class="lead text-secondary text-center mt-xs-3">Миленичиња</p>
              <div class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default @if($UserData->Userpreferences->pets == 'pets_1') active @endif">
                  <input type="radio" name="pets" value="pets_1"  @if($UserData->Userpreferences->pets == 'pets_1') checked="checked" @endif>
                  <span class="fa-stack fa-2x" data-toggle="tooltip" data-placement="top" title="Без миленичиња во автомобилот">
                    <i class="fas fa-paw fa-stack-1x text-muted"></i>
                    <i class="fas fa-ban fa-stack-2x text-danger"></i>
                  </span>
                </label>
                <label class="btn btn-default @if($UserData->Userpreferences->pets == 'pets_2') active @endif">
                  <input type="radio" name="pets" value="pets_2" @if($UserData->Userpreferences->pets == 'pets_2') checked="checked" @endif>
                  <i class="fas fa-paw fa-2x text-muted" data-toggle="tooltip" data-placement="top" title="Зависи за какво милениче се работи"></i>
                </label>
                <label class="btn btn-default @if($UserData->Userpreferences->pets == 'pets_3') active @endif">
                  <input type="radio" name="pets" value="pets_3" @if($UserData->Userpreferences->pets == 'pets_3') checked="checked" @endif>
                  <i class="fas fa-paw fa-2x text-primary" data-toggle="tooltip" data-placement="top" title="Миленичињата се добредојдени"></i>
                </label>
              </div>
            </div>
            <div class="col-xs-12 col-md-3">
              <p class="lead text-secondary text-center mt-xs-3">Музика</p>
              <div class="btn-group btn-group-justified" data-toggle="buttons">
                <label class="btn btn-default @if($UserData->Userpreferences->music == 'music_1') active @endif">
                  <input type="radio" name="music" value="music_1" @if($UserData->Userpreferences->music == 'music_1') checked="checked" @endif>
                  <span class="fa-stack fa-2x" data-toggle="tooltip" data-placement="top" title="Тишината е закон">
                    <i class="fas fa-music fa-stack-1x text-muted"></i>
                    <i class="fas fa-ban fa-stack-2x text-danger"></i>
                  </span>
                </label>
                <label class="btn btn-default @if($UserData->Userpreferences->music == 'music_2') active @endif">
                  <input type="radio" name="music" value="music_2" @if($UserData->Userpreferences->music == 'music_2') checked="checked" @endif>
                  <i class="fas fa-music fa-2x text-muted" data-toggle="tooltip" data-placement="top" title="Слушам во зависност од расположението"></i>
                </label>
                <label class="btn btn-default @if($UserData->Userpreferences->music == 'music_3') active @endif">
                  <input type="radio" name="music" value="music_3" @if($UserData->Userpreferences->music == 'music_3') checked="checked" @endif>
                  <i class="fas fa-music fa-2x text-primary" data-toggle="tooltip" data-placement="top" title="Патувањето неможе да помине без музика"></i>
                </label>
              </div>
            </div>
          </div>
          <input type="hidden" name="_method" value="PUT">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-icon-split btn-lg upload-result mt-4">
                  <span class="icon text-white-50"><i class="fas fa-save"></i></span>
                  <span class="text">Зачувај</span>
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
@section('includesscripts')
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
@endsection
