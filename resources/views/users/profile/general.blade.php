@extends('layouts.profile')
@section('profile_content')
<div class="panel panel-info">
	<div class="panel-heading">
		<h3><i class="fas fa-user"></i> Твоите лични податоци</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				@if(Session::has('success'))
					<div class="row">
						<div class="col-md-12">
							<div class="alert alert-success text-center">
								<p class="lead text-strong">{{Session::get('success')}}</p>
							</div>
						</div>
					</div>
				@endif
				<form action="{{route('users.update_general_details')}}" method="POST">
					<div class="row">
						<div class="col-xs-12 col-md-3 col-md-push-9">
							<a href="{{ url('/users/profile/changepassword') }}" class="btn btn-primary btn-block mb-xs-2">Промени Лозинка</a>
						</div>
						<div class="col-xs-12 col-md-3 col-md-pull-3">
							<div class="form-group">
								<div class="btn-group btn-group-justified" data-toggle="buttons">
									<label class="btn btn-default @if($UserData->Userdetails->sex == 'male') active @endif">
										<input type="radio" name="sex" value="male" @if($UserData->Userdetails->sex == 'male') checked="checked" @endif>
										<i class="fas fa-male fa-2x text-primary" data-toggle="tooltip" data-placement="top" title="Јас сум машко"></i>
									</label>
									<label class="btn btn-default @if($UserData->Userdetails->sex == 'female') active @endif">
										<input type="radio" name="sex" value="female" @if($UserData->Userdetails->sex == 'female') checked="checked" @endif>
										<i class="fas fa-female fa-2x text-primary" data-toggle="tooltip" data-placement="top" title="Јас сум женско"></i>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="firstname">Име:</label>
								<input type="text" class="form-control" id="firstname" name="firstname" placeholder="Име" value="{{$UserData->firstname}}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="lastname">Презиме:</label>
								<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Презиме" value="{{$UserData->lastname}}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="email">Е-Маил:</label>
								<input type="email" class="form-control" id="email" name="email" placeholder="Е-Маил Адреса" value="{{$UserData->email}}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="phone">Телефон:</label>
								<input type="text" class="form-control" id="phone" name="phone" placeholder="Телефон" value="{{$UserData->phone}}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label" for="birthday">Година на раѓање:</label>
								<select class="form-control" id="birthday" name='birthday'>
									@if(count($BirthdayDates) > 0)
										@if($UserData->Userdetails->birthday == false)
											<option value="" disabled="" selected="">Одберете година на раѓање</option>
										@endif
										@foreach($BirthdayDates as $BirthdayDate)
											<option value="{{$BirthdayDate}}" @if($BirthdayDate == $UserData->Userdetails->birthday) selected='selected' @endif>{{$BirthdayDate}}</option>
										@endforeach
									@endif
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label" for="bio">Напиши нешто за тебе:</label>
								<textarea name="bio" id="bio" class="form-control" cols="55" rows="5">{{$UserData->Userdetails->bio}}</textarea>
								 <p class="help-block small">Те молам овде да не го напишеш твојот телефонски број, податоци поврзани со твојот Фејсбук профил или други детали поврзани со некое специфично патување.</p>
							</div>
						</div>
					</div>

					<input type="hidden" name="_method" value="PUT">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-icon-split btn-lg">
							<span class="icon text-white-50"><i class="fas fa-save"></i></span>
							<span class="text">Зачувај</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('includesscripts')
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
@endsection
