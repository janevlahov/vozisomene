
@extends('layouts.profile')

@section('profile_content')
<div class="panel panel-info">
	<div class="panel-heading">
		<h3><i class="fas fa-car"></i> Твојот автомобил</h3>
	</div>
	<div class="panel-body">
		@if($AutoDetails->car_brand != '' || $AutoDetails->car_model != '' || $AutoDetails->year_production != '' || $AutoDetails->car_photo != '')
			<div class="row">
				<div class="col-md-6">
					<div class="media mb-3">
						<div class="media-left">
							@if(!empty($AutoDetails->car_photo))
								<img src="/storage/upload/cars/{{$AutoDetails->car_photo}}" id='car_image' class="slika media-object mr-3" alt="">
							@else
								@if($AutoDetails->car_brand != '' || $AutoDetails->car_model != '' || $AutoDetails->year_production != '' || $AutoDetails->type != '' || $AutoDetails->color != '')
									<img src="/storage/upload/web/nocar.jpg" id='car_image' class="slika media-object mr-3" alt="">
								@endif
							@endif
						</div>
						<div class="media-body">
							<p class="lead text-primary" id='show_car'>{{$AutoDetails->car_brand}} {{$AutoDetails->car_model}}</p>
							<p id="show_year">@if($AutoDetails->year_production != '') {{$AutoDetails->year_production}} година @endif </p>
							<p id="show_type">{{$AutoDetails->type}}</p>
							<p id="show_color">@if($AutoDetails->color != '') {{$AutoDetails->color}} боја @endif</p>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<a class="btn btn-danger" role='button' href="{{ URL::route('users.remove_auto') }}">Избриши автомобил</a>
			</div>

			<div class="row">
				<div class="col-md-12">
					<h3 class="text-primary">Или измени ги информациите за твојот автомобил <i class="fas fa-chevron-down"></i></h3>
				</div>
			</div>

		@endif

		@if(empty($AutoDetails->car_brand))
		<div class="row">
			<div class="col-md-12">
				<p>Те молам внеси ги податоците за твојот авотмобил. Така другите корисници ќе каков автомобил возиш и многу полесно ќе ги пронајдеш идеалните сопатници.</p>
			</div>
		</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<div class="row">
					<div class="col-md-12">
						<p class="lead text-secondary">Општи Податоци</p>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label" for="country_code">Држава на регистрација:</label>
									<select class="form-control" id="country_code" name='state'>
										@if(count($CountryCodes) > 0)
											@if($AutoDetails->country_code == false)
												<option value="" disabled="" selected="">Држава на регистрација</option>
											@endif
											@foreach($CountryCodes as $CountryCode => $Country)
												<option value="{{$CountryCode}}" @if($CountryCode == $AutoDetails->licence_state) selected='selected' @endif>{{$Country}}</option>
											@endforeach
										@endif
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label" for="licence-plate">Број регистарска табличка:</label>
									<input type="text" class="form-control" id="licence-plate" name="licence_plate" placeholder="Број регистарска табличка" value="{{$AutoDetails->licence_plate}}">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label>Година на производство</label>
								<div class="form-group">
									<select class="form-control" name="car_years" id="car-years">
										@php
										$YearBegin = 1985; $YearNow = date("Y");
										@endphp

										<option value="">Избери Година</option>
										@for($i = intval($YearNow); $i >= $YearBegin; $i--)
											<option value="{{$i}}" @if($AutoDetails->year_production == $i) selected @endif>{{$i}}</option>
										@endfor
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<label>Марка</label>
								<div class="form-group">
									@if(count($CarBrands) > 0)
									<select class="form-control" name="car_makes" id="car-makes">
										<option value="">Избери Марка</option>
										@for($i = 0; $i < count($CarBrands); $i++)
											<option value="{{$CarBrands[$i]}}" @if($AutoDetails->car_brand == $CarBrands[$i]) selected @endif>{{$CarBrands[$i]}}</option>
										@endfor
									</select>
									@endif
								</div>
							</div>
							<div class="col-md-4">
								<label>Модел</label>
								<div class="form-group">
									<select class="form-control" name="car_models" id="car-models"></select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6 mb-xs-15">
								<label>Тип</label><br>
								@if(count($TypeNames) > 0)
									@foreach($TypeNames as $TypeName)
										<div class="radio radio-inline radio-primary">
											<input type="radio" name="car_type" id="tip-{{$TypeName}}" value='{{$TypeName}}' @if($TypeName == $AutoDetails->type) checked="checked" @endif>
											<label for="tip-{{$TypeName}}">{{$TypeName}}</label>
										</div>
									@endforeach
								@endif
							</div>
							<div class="col-xs-12 col-md-6">
								<label>Боја</label><br>
								@if(count($ColourNames) > 0)
									@foreach($ColourNames as $ColourName)
										<div class="radio radio-inline radio-secondary">
											<input type="radio" name="car_color" id="boja-{{$ColourName}}" value='{{$ColourName}}' @if($ColourName == $AutoDetails->color) checked="checked" @endif>
											<label for="boja-{{$ColourName}}">{{$ColourName}}</label>
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 pt-2 pb-3">
						@if(empty($AutoDetails->car_photo))
							<p class="lead text-secondary">Додади слика од твојот автомобил!</p>
						@else
							<p class="lead text-secondary text-center">Промени ја сликата од твојот автомобил!</p>
						@endif
						<div id="krop" class="krop hidden mb-3">
							<span class="small text-white text-strong">Влечи за подобро позиционирање</span>
							<div id="upload-demo"></div>
							<p class="small text-center">Сечи ја сликата ако е потребно</p>
						</div>
						<div class="input-file-container mx-auto">
							<input type="file" id="upload" name='car_img' class="input-file">
							<label tabindex="0" for="upload" class="input-file-trigger btn btn-primary btn-lg mx-auto">Одбери слика...</label>
							<p class="file-return"></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-sm-10">
								<button type="submit" class="btn btn-primary btn-icon-split btn-lg upload-result">
									<span class="icon text-white-50"><i class="fas fa-save"></i></span>
									<span class="text">Зачувај</span>
								</button>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection

@section('includecss')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.3/croppie.min.css" rel="stylesheet">
@endsection

@section('includesscripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.3/croppie.min.js"></script>
<script src="{{ asset('js/carmodels.js') }}"></script>
<script type="text/javascript">

	document.querySelector("html").classList.add('js');

	var fileInput  = document.querySelector( ".input-file" ),
		button     = document.querySelector( ".input-file-trigger" ),
		the_return = document.querySelector(".file-return");

	button.addEventListener( "keydown", function( event ) {
		if ( event.keyCode == 13 || event.keyCode == 32 ) {
				fileInput.focus();
		}
	});
	button.addEventListener( "click", function( event ) {
		fileInput.focus();
		return false;
	});
	fileInput.addEventListener( "change", function( event ) {
		the_return.innerHTML = this.value;
	});

	function resolve_car(){
		var car_makes = document.getElementById("car-makes").value;
	    var car_model = "{{$AutoDetails->car_model}}";

	    var car_options_arr = getCarModel(car_makes);
	    car_options = '<option value="">Избери Модел</option>';

	    for(var i = 0; i < car_options_arr.length; i++){
	    	if(car_options_arr[i] == car_model){
				car_options += "<option value='"+car_options_arr[i]+"' selected>"+car_options_arr[i]+"</option>";
	    	}else{
	    		car_options += "<option value='"+car_options_arr[i]+"'>"+car_options_arr[i]+"</option>";
	    	}
	    }

	    $("#car-models").find('option').remove().end().append(car_options);
	}

	$( document ).ready(function() {
	   	resolve_car();
	});

	$( "#car-makes" ).change(function() {
	  	resolve_car();
	});

	$.ajaxSetup({
		headers: {
		  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$uploadCrop = $('#upload-demo').croppie({

	    enableExif: true,
	    viewport: {
	        width: 200,
	        height: 200,
	        type: 'circle'
	    },

	    boundary: {
	        width: 300,
	        height: 300
	    }
	});

	$('#upload').on('change', function () {

	  	var reader = new FileReader();
	    reader.onload = function (e) {
	      $uploadCrop.croppie('bind', {
	        url: e.target.result
	      });
	    }

	    reader.readAsDataURL(this.files[0]);
	    if ( /\.(jpe?g|png|gif)$/i.test(this.files[0].name) === false ) {
	    	alert("Овој тип на документ не е дозволен. Ве молиме прикачете слика");
	    	$( "#krop" ).addClass( "hidden" );
	    	$("#upload").val('');
	    	return false;
	    }

		$( "#krop" ).removeClass( "hidden" );
	});

	$('.upload-result').on('click', function (ev) {

	  $uploadCrop.croppie('result', {
	    type: 'canvas',
	    size: 'viewport'

	  }).then(function (resp) {

	    var file = document.getElementById("upload").value;
	    var car_years = document.getElementById("car-years").value;
	    var car_makes = document.getElementById("car-makes").value;
	    var car_models = document.getElementById("car-models").value;
	    var country_code = document.getElementById("country_code").value;
	    var licence_plate = document.getElementById("licence-plate").value;
	    var car_type = $('input[name=car_type]:checked').val();
	    var car_color = $('input[name=car_color]:checked').val();

	    $.ajax({
	      url: "/users/profile/auto",
	      type: "POST",

	      data: {"image":resp, "car_img":file, "car_years":car_years, "car_brand":car_makes, "car_model":car_models, "type":car_type, "color":car_color,"licence_state":country_code,"licence_plate":licence_plate},

	      success: function (data) {
	      	if(resp.includes('image/png')){$('#car_image').attr("src",resp);}
	      	location.reload();
	        //console.log(resp);
	      },
	      error: function (err) {
	        console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
	      }
	    });
	  });
	});
</script>
@endsection
