@extends('layouts.app')
@section('content')
	<div class="container py-5 py-xs-0">
		<div class="row mb-3">
			<div class="col-xs-6 col-md-6">
				<a href="{{ URL::previous() }}" class="btn btn-default"><i class="fas fa-angle-double-left"></i> Назад</a>
			</div>
			@if(!$authUserData)
				@if($user->active)
					<div class="col-xs-6 col-md-6">
						<a href="" class="btn btn-default pull-right" data-toggle="modal" data-target="#report">Пријави Корисник</a>
					</div>
				@endif
			@else
				<div class="col-xs-6 col-md-2 alert alert-success pull-right">
					<p>Пријавата е примена</p>
				</div>
			@endif
		@if(!$user->active)
		<div class="col-xs-12 col-md-12">
			<div class="alert alert-danger">
				<p class="error lead text-center" role="alert">Корисничкиот профил е деактивиран</p>
			</div>
		</div>
		@endif
	</div>
		@if(!empty($user))

			<div class="row">
				<div class="col-xs-12 col-md-4 sofer">
					<div class="panel panel-primary panel-profil">
						@if(!empty($user->avatar))<img class="slika" src="{{asset('storage/upload/users/'.$user->avatar)}}" alt="{{$user->firstname}} {{$user->lastname}}">
			      @else<img class="slika" src="/storage/upload/web/nouser.jpg" alt="Nema slika">
			      @endif
						<div class="panel-body text-center">
							<p class="lead ime mb-1">{{$user->firstname}} {{$user->lastname}}</p>
							<p class="small">Член од {{ Carbon\Carbon::parse($user->created_at)->format('m.Y') }}</p>

							@if(!empty($user->birthday))
								<p class="small">Година на раѓање: {{$user->birthday}}</p>
							@endif
							@if(!empty($avg_rating))
								<div class="rejting text-center mb-1">
									<div class='driver_rating'></div>
									<span class="text-primary text-strong">{{$avg_rating}}</span>
								</div>
							@else
								<p>Корисникот не е оценет</p>
							@endif

							@if(Helper::count_numas_driver($user->id) == 0)
								<p class="text-left ml-2">Нема споделено патувања</p>
							@else
								<p class="text-left ml-2"><span class="text-strong">{{Helper::count_numas_driver($user->id)}}</span> @if(Helper::count_numas_driver($user->id) > 1) споделени патувања
							@else споделено патување @endif</p>
							@endif

							@if(Helper::count_numas_passanger($user->id) == 0)
							<p class="text-left ml-2">Нема резервирано патувања</p>
							@else
							<p class="text-left ml-2"><span class="text-strong">{{Helper::count_numas_passanger($user->id)}}</span> @if(Helper::count_numas_passanger($user->id) > 1) резервирани патувања @else резервирано патување @endif</p>
							@endif
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-4 preferenci">
					<div class="panel panel-primary panel-profil mt-xs-0">
						<div class="panel-body">
							<div class="media">
								<div class="media-left media-middle">
									{!! Helper::pref_talkative($user->talkative)[0] !!}
								</div>
								<div class="media-body media-middle">
									{{ Helper::pref_talkative($user->talkative)[1] }}
								</div>
							</div>
							<div class="media">
								<div class="media-left media-middle">
									{!! Helper::pref_smoker($user->smoker)[0] !!}
								</div>
								<div class="media-body media-middle">
									{{ Helper::pref_smoker($user->smoker)[1] }}
								</div>
							</div>
							<div class="media">
								<div class="media-left media-middle">
									{!! Helper::pref_pets($user->pets)[0] !!}
								</div>
								<div class="media-body media-middle">
									{{ Helper::pref_pets($user->pets)[1] }}
								</div>
							</div>
							<div class="media">
								<div class="media-left media-middle">
									{!! Helper::pref_music($user->music)[0] !!}
								</div>
								<div class="media-body media-middle">
									{{ Helper::pref_music($user->music)[1] }}
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-4 kola">
					<div class="panel panel-primary panel-profil">
						@if(!empty($user->car_photo))<img class="slika" src="{{asset('storage/upload/cars/'.$user->car_photo)}}" alt="{{$user->car_brand}} {{$user->car_model}}">
			      		@else<img class="slika" src="/storage/upload/web/nocar.jpg" alt="Nema slika">
			      		@endif
						<div class="panel-body text-center">
							@if(!empty($user->car_brand) || !empty($user->car_model) || !empty($user->year_production) || !empty($user->year_production) || !empty($user->type) || !empty($user->color))
								@if(!empty($user->car_brand))<p class="lead ime mb-1"> {{$user->car_brand}} {{$user->car_model}}</p>@endif
								@if(!empty($user->year_production))<p class="small"> Година {{$user->year_production}}</p>@endif
								@if(!empty($user->type))<p>Тип: {{$user->type}}</p>@endif
								@if(!empty($user->color))<p>Боја: {{$user->color}}</p>@endif
							@else
								<p class="lead ime text-danger">Нема податоци за автомобилот</p>
							@endif
						</div>
					</div>
				</div>
			</div>
			@if(!empty($user->bio))
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info alert-bio">
	          <div class="tekst">
	            <p><strong>Нешто за {{$user->firstname}}:</strong><br>
	              {{$user->bio}}
	            </p>
	          </div>
	        </div>
				</div>
			</div>
			@endif

			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3><i class="fas fa-comments"></i> Коментари</h3>
						</div>
						<div class="panel-body">
							@if(count($user_reviews))
								@foreach ($user_reviews as $review)
									<div class="row komentar">
										<div class='col-xs-4 col-md-2'>
											@if(!empty($review->avatar))<img src="/storage/upload/users/{{$review->avatar}}" class="img-responsive" alt="{{$review->firstname}}">
			    	     				@else<img class="img-responsive" src="/storage/upload/web/nouser.jpg" alt="Nema slika">
			    	      		@endif
											<p class="text-center text-primary text-strong mt-1 mb-4 mb-xs-0">{{$review->firstname}}</p>
										</div>
										<div class="col-xs-8 col-md-10">
											<p class="mb-0"><i class="fas fa-calendar-alt text-primary"></i> {{ Carbon\Carbon::parse($review->created_at)->format('d.m.Y') }} <span class="ml-1 ml-xs-0 d-xs-block"><i class="fas fa-map-marker-alt text-primary"></i> {!! Helper::ride_place_name($review->s_user_id, $review->ride_id)[0] !!} <i class="fas fa-angle-double-right"></i> {!! Helper::ride_place_name($review->s_user_id, $review->ride_id)[1] !!}</span></p>
											<div class='starrr mt-xs-1'>
												@for($i = 1; $i<= 5; $i++)
													@if($i <= $review->rating)
														<a style="cursor: default;" class="fa-star fa"></a>
													@else
														<a style="cursor: default;" class="fa-star-o fa"></a>
													@endif
												@endfor
											</div>
										</div>
										<div class="col-xs-12 col-md-10">
											<div class="panel panel-default mt-1 mb-4">
												<div class="panel-body">
													<p class="mb-0">{{$review->comment}}</p>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							@else
								<div class="alert alert-danger">
									<p class="lead">Нема коментaри за корисникот</p>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div id="report" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Пријави Корисник</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-12">
									<form action="{{route('users.report_user', $user->id)}}" method="POST">
										<div class="form-group">
											<input type="text" class="form-control" id="reportSubject" name="subject" autocomplete="off" placeholder="Наслов на пријава">
										</div>
										<div class="form-group">
											<textarea rows="4" cols="50" class="form-control" name="description" placeholder="Опишете ни го проблемот"></textarea>
										</div>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="submit" value="Пријави" class="btn btn-default">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		@endif
	</div>
	@if(empty($avg_rating))
		@php $avg_rating = ''; @endphp
	@endif
@endsection
@section('includesscripts')
	<script src="{{ asset('js/starrr.js') }}"></script>
	<script>
	var avg_rating = "<?php echo $avg_rating ?>";
	if(avg_rating != ''){
		$('.driver_rating').starrr({rating: avg_rating, readOnly: true});
	}
	</script>
@endsection
