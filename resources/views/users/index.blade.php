@extends('layouts.app')

@section('content')
	<section class="bg-home">
		<div class="container">
			<div class="home-content">
				<h1>Вози со мене и пристигни опуштено</h1>
				<h2>Да ја оствариме целта заедно</h2>
				<form class="form-inline" id="msform" action="{{action('UsersController@index_search')}}" method="get">
				  <div class="form-group form-group-lg">
				    <input type="text" class="form-control" name="fp" id="autocomplete" placeholder="Од">
				  </div>
				  <div class="form-group form-group-lg">
				    <input type="text" class="form-control" name="tp" id="autocomplete2" placeholder="До">
				  </div>
				  <div class="form-group form-group-lg">
				    <input type="text" class="form-control" name="rd" id="startDate" placeholder="Датум" autocomplete="off">
				  </div>
				  <button type="submit" class="btn btn-primary btn-lg">Барај</button>
				</form>
			</div>
		</div>
	</section>
	<section class="bg-white pocetna">
		<div class="tekst-pocetna">
			<h2 class="mt-0">Каде сакаш да возиш?</h2>
			<p class="lead text-secondary">Ајде да го направиме ова најевтиното патување досега.</p>
			<a href="{{ url('/rides/create') }}" class="btn btn-primary btn-lg mt-1">Понуди патување</a>
		</div>
		<img src="/storage/upload/web/pocetna.jpg" class="slika1-pocetna hidden-xs" alt="">
	</section>
	<section class="bg-primary popularno py-5">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="mt-0">Каде сакаш да се возиш?</h2>
				</div>
			</div>
			<div class="row">
				@if(count($getFrequnetRides))
					@foreach($getFrequnetRides as $frequentRide)
						<div class="col-md-4">
							<div class="panel panel-default panel-popularno">
								<div class="panel-body">
									<a href="/rides/search-ride?fp={{$frequentRide->d_from}}&tp={{$frequentRide->d_to}}" class="text-center"><p class="lead">{{Helper::rmCountry($frequentRide->d_from)}} <i class="fas fa-angle-double-right"></i> {{Helper::rmCountry($frequentRide->d_to)}}</p></a>
								</div>
							</div>
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</section>
	<section class="bg-white py-5">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5">
					<h2 class="mt-0 mb-5">Оди буквално насекаде. <br>Од секаде.</h2>
					<div class="row">
						<div class="col-xs-4 col-md-4 text-center">
							<i class="fas fa-users fa-4x text-primary"></i>
		 					<h3>Ти бираш</h3>
						</div>
						<div class="col-xs-4 col-md-4 text-center">
							<i class="fas fa-hands-helping fa-4x text-primary"></i>
							<h3>Ниски цени</h3>
						</div>
						<div class="col-xs-4 col-md-4 text-center">
							<i class="fas fa-smile-wink fa-4x text-primary"></i>
							<h3>Без стрес</h3>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-xs-offset-0 col-md-6 col-md-offset-1">
					<p class="lead text-secondary mt-0 mb-1">Паметно</p>
					<p>Со пристап до многу патувања, можеш да ги пронајдеш луѓето од твоето опкружување што патуваат каде што патуваш и ти.</p>
					<p class="lead text-secondary mt-2 mb-1">Брзо</p>
					<p>Внеси ја твојата дестинација за да го најдеш совршеното возење. Избери со кого сакаш да патуваш. И резервирај!</p>
					<p class="lead text-secondary mt-2 mb-1">Беспрекорно</p>
					<p>Без стрес директно до целта. Без долги редици, без чекање во станица. Патувај заедно и оцени го возењето по завршувањето.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="bg-white py-5 py-xs-0">
		<div class="container">
			<div class="row row-eq-height">
				<div class="col-xs-12 col-xs-offset-0 col-md-5 col-md-offset-1">
					<div class="panel panel-vozime text-center">
						<div class="panel-body p-3">
							<i class="fas fa-suitcase fa-5x"></i>
							<h3 class="text-white text-strong mb-2">Сакаш да се возиш?</h3>
							<p>Сакаш да се возиш удобно и да заштедиш време и пари?</p>
							<p class="mb-0">Пронајди го твојот идеален сопатник.</p>
						</div>
						<div class="panel-footer">
							<a href="{{ url('/rides/search-ride') }}" class="btn btn-default btn-icon-split btn-lg">
                <span class="icon"><i class="fas fa-search-location"></i></span>
                <span class="text">Пребарувај патувања</span>
              </a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-5">
					<div class="panel panel-vozam text-center">
						<div class="panel-body p-3">
							<i class="fas fa-car-side fa-5x"></i>
							<h3 class="text-white text-strong mb-2">Сакаш да возиш?</h3>
							<p>Возиш некаде и имаш празни места?</p>
							<p class="mb-0">Сподели го твоето возење и заштеди пари.</p>
						</div>
						<div class="panel-footer">
							<a href="{{ url('/rides/create') }}" class="btn btn-default btn-icon-split btn-lg">
                <span class="icon"><i class="fas fa-share"></i></span>
                <span class="text">Понуди патување</span>
              </a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bg-white pt-5">
		<div class="container-fluid">
			<div class="col-md-12">
				<div class="owl-carousel owl-vozisomene">
					<a href="/rides/search-ride?dest=скопје">
						<div class="slika skopje"></div>
						<div class="tekst"><h4>Барам превоз до <span>Скопје</span></h4></div>
					</a>
					<a href="/rides/search-ride?dest=струмица">
						<div class="slika strumica"></div>
						<div class="tekst"><h4>Барам превоз до <span>Струмица</span></h4></div>
					</a>
					<a href="/rides/search-ride?dest=охрид">
						<div class="slika ohrid"></div>
						<div class="tekst"><h4>Барам превоз до <span>Охрид</span></h4></div>
					</a>
					<a href="/rides/search-ride?dest=штип">
						<div class="slika stip"></div>
						<div class="tekst"><h4>Барам превоз до <span>Штип</span></h4></div>
					</a>
					<a href="/rides/search-ride?dest=велес">
						<div class="slika veles"></div>
						<div class="tekst"><h4>Барам превоз до <span>Велес</span></h4></div>
					</a>
				</div>
			</div>
		</div>
	</section>


@endsection

@section('includecss')
  <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
@endsection

@section('includesscripts')

<script>

$('#msform').on('keyup keypress', function(e) {
	var keyCode = e.keyCode || e.which;
	if (keyCode === 13) {
		if(document.getElementById("autocomplete2").value == 'undefined'){
			document.getElementById("autocomplete2").value = '';
		}
		if(document.getElementById("autocomplete").value == 'undefined'){
			document.getElementById("autocomplete").value = '';
		}
		e.preventDefault();
		return false;
	}
});

var date = new Date();
$('#startDate').datepicker({
	format: 'dd.mm.yyyy',
	startDate: date,
	endDate: '+365d',
	autoclose: true,
	language: 'mk',
});

var placeSearch, autocomplete, geocoder;

function initAutocomplete() {

	geocoder = new google.maps.Geocoder();

	autocomplete = new google.maps.places.Autocomplete(
		(document.getElementById('autocomplete')), {
			types: ['geocode']
		});

		autocomplete.addListener('place_changed', fillInAddress);

		autocomplete2 = new google.maps.places.Autocomplete(
			(document.getElementById('autocomplete2')), {
				types: ['geocode']
			});

			autocomplete2.addListener('place_changed', fillInAddress2);
		}

		function fillInAddress() {
			var place = autocomplete.getPlace();
			document.getElementById("autocomplete").value = place.formatted_address;
		}
		function fillInAddress2() {
			var place = autocomplete2.getPlace();
			document.getElementById("autocomplete2").value = place.formatted_address;
		}
		$('.owl-carousel').owlCarousel({
    loop:true,
    nav:true,
    navText:["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
    dots:false,
    rewind:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:4
        }
      }
    })
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuvTR6aAp_y_rcc_w1m087uc9jh84V3Qk&libraries=places&callback=initAutocomplete&language=mk&region=MKD"></script>
		@endsection
