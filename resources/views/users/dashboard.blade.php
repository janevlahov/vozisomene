@extends('layouts.app')

@section('content')
<section class="bg-light-gradient py-5 py-xs-0">
<div class="container">
  <div class="row">
    <div class="col-md-7 vcenter">
      <h1 class="mt-0 mt-xs-2">Здраво {{ Auth::user()->firstname }}</h1>
    </div><!--
    --><div class="col-md-5 vcenter">
      <p class="text-muted text-right">Како слушна и што мислиш за Возисомене? <a href="{{ url('/kontakt') }}" class="btn btn-default">Пиши ни</a></p>
    </div>
  </div>
  <div class="row mb-4 hidden-xs">
    <div class="col-md-3">
      <div class="card-counter bg-primary text-white">
        <i class="fas fa-car-side"></i>
        <span class="count-numbers">{{Helper::count_numas_driver(Auth::user()->id)}} <i class="fas fa-times"></i></span>
        <span class="count-name">си бил возач</span>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card-counter bg-secondary text-white">
        <i class="fas fa-suitcase"></i>
        <span class="count-numbers">{{Helper::count_numas_passanger(Auth::user()->id)}} <i class="fas fa-times"></i></span>
        <span class="count-name">си бил патник</span>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card-counter bg-primary text-white">
        <i class="fas fa-map-pin"></i>
        <span class="count-numbers" data-toggle="tooltip" data-placement="top">@if($UserInfo['UserCity'] != '' ){{$UserInfo['UserCity']}} @else <a href="{{ url('/users/profile/address') }}" class="btn btn-link">Додади град</a> @endif</span>
        <span class="count-name">твојот град</span>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card-counter bg-secondary text-white">
        <i class="fas fa-car"></i>
        <span class="count-numbers" data-toggle="tooltip" data-placement="top">@if($UserInfo['UserCarBrand'] != '' || $UserInfo['UserCarModel'] != '') {{$UserInfo['UserCarBrand']}} {{$UserInfo['UserCarModel']}} @else <a href="{{ url('/users/profile/auto') }}" class="btn btn-link" >Додади автомобил</a> @endif</span>
        <span class="count-name">твојот автомобил</span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-md-4">
      <div class="panel panel-primary panel-profil">
        @if(!empty(auth()->user()->Userdetails->avatar))<img class="slika" src="{{asset('storage/upload/users/'.auth()->user()->Userdetails->avatar)}}" alt="{{auth()->user()->firstname}} {{auth()->user()->lastname}}">
        @else<img class="slika" src="/storage/upload/web/nouser.jpg" alt="Nema slika">
        @endif
        <a href="{{ url('/users/profile/avatar') }}" class="promeni-slika">
          <span class="fa-stack fa-1x">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-camera fa-stack-1x fa-inverse"></i>
          </span>
        </a>
        <div class="panel-body text-center">
          <p class="lead ime">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
          @if(!empty(Helper::user_rating(Auth::user()->id)))
            <div class="rejting text-center mb-1">
              <div class='driver_rating'></div>
              <span class="text-primary text-strong" id='user_rating'>{{Helper::user_rating(Auth::user()->id)}}</span>
            </div>
          @endif

          <p class="small">Член од {{ Carbon\Carbon::parse(Auth::user()->created_at)->format('m.Y') }}</p>
          <p>{{ Auth::user()->email }}</p>
          @if(empty(Auth::user()->phone))
           <a href="{{ url('/users/profile/general') }}" class="link-danger">Додади телефон</a>
          @else <p>{{ Auth::user()->phone }}</p>
          @endif
          <span class="kopce">
            <a href="{{ url('/users/profile/general') }}" class="btn btn-primary btn-block"><i class="fas fa-user-edit"></i> Измени податоци</a>
          </span>
        </div>
      </div>
    </div>

  <div class="col-xs-12 col-md-8">
    <div class="row">
      <div class="col-xs-12 col-md-6">
        <div class="panel panel-info panel-profil panel-izvestuvanja mt-xs-0">
          <div class="panel-heading">
            <h3><i class="fas fa-bell"></i> Известувања</h3>
          </div>
          <div class="panel-body" id="notifications_data">
          	@if(count(auth()->user()->unreadnotifications) > 0)
	            @foreach(auth()->user()->unreadnotifications as $notification)
		            @if(!empty($notification->data['title']))
  				        <div class="list-group">
  				          <a href="{{url('notifications/show/'.$notification->id)}}" class="list-group-item">
  				            <h4 class="list-group-item-heading">{{$notification->data['title']}}</h4>
  				          </a>
  				        </div>
		            @endif
	            @endforeach
           	@endif
             @if(count(auth()->user()->unreadnotifications) > 0) <span class="kopce text-center"><a href="#" onclick="MarkAllAsRead()" id='markAllread' class="btn btn-link"><i class="fas fa-highlighter"></i> Обележи како прочитани</a></span>
             @else <p class="lead text-danger">Немаш нови известувања</p>
             @endif
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="panel panel-info panel-profil panel-moipatuvanja mt-xs-0">
          <div class="panel-heading">
            <h3><i class="fas fa-map-marked-alt"></i> Мои патувања</h3>
          </div>
          <div class="panel-body">
            <table class="table">
              <tbody>
                @php $count = 0; @endphp
              	@if(count($userRides))
					        @foreach($userRides as $ride)
  	                <tr class="dash-tr-vozac">

  	                  <td><a href="/rides/{{$ride->id}}"><span class="lead preseci" data-toggle="tooltip" data-placement="top">{{Helper::rmCountry($ride->d_from)}}</span> </a></td>
  	                  <td><i class="fas fa-angle-double-right lead"></i></td>
  	                  <td><a href="/rides/{{$ride->id}}"><span class="lead preseci" data-toggle="tooltip" data-placement="top">{{Helper::rmCountry($ride->d_to)}}</span></a></td>

  	                </tr>
                    @php $count++; @endphp
                    @if($count == 2 && count($booked_rides))
                      @break
                    @endif
                 @endforeach
                 @else <p class="lead text-danger">Немаш објавено патувања</p>
               @endif

               @php $count2 = 0; @endphp

               @if(count($booked_rides))
                  @foreach($booked_rides as $ride)

                    @php $bfrom = $ride->b_from; $bto = $ride->b_to; @endphp

                    <tr class="dash-tr-patnik">
                      <td><a href="/rides/detailed-view/{{$ride->ride_id}}"><span class="lead preseci" data-toggle="tooltip" data-placement="top">{{Helper::rmCountry($ride->$bfrom)}}</span></a></td>
                      <td><i class="fas fa-angle-double-right lead"></i></td>
                      <td><a href="/rides/detailed-view/{{$ride->ride_id}}"><span class="lead preseci" data-toggle="tooltip" data-placement="top">{{Helper::rmCountry($ride->$bto)}}</span></a></td>
                    </tr>
                    @if($count == 2)
                      @php $count2++; @endphp
                      @if($count2 == 2)
                        @break
                      @endif
                    @endif
                  @endforeach
               @endif
              </tbody>
            </table>
            <span class="kopce text-center">
              <a href="{{ url('/rides/search-ride') }}" class="btn btn-link"><i class="fas fa-suitcase"></i> Барај патување</a>
              <a href="{{ url('/rides/create') }}" class="btn btn-link"><i class="fas fa-car"></i> Додади патување</a>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 p-0">
        <div class="vozime">
          <div class="tekst">
            <h2>Барам патување</h2>
            <p class="lead text-strong mb-0">Сакаш да се возиш удобно и да заштедиш време и пари?</p>
            <p class="lead text-strong">Пронајди го твојот идеален сопатник.</p>
            <a href="{{ url('/rides/search-ride') }}" class="btn btn-primary btn-lg">Пребарувај патувања</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 p-0">
        <div class="vozam">
          <div class="tekst">
            <h2>Нудам патување</h2>
            <p class="lead text-strong mb-0">Возиш некаде и имаш празни места?</p>
            <p class="lead text-strong">Сподели го твоето возење и заштеди пари.</p>
            <a href="{{ url('/rides/create') }}" class="btn btn-primary btn-lg">Сподели патување</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('includesscripts')
@if(!empty(Helper::user_rating(Auth::user()->id)))
<script src="{{ asset('js/starrr.js') }}"></script>
<script>
var driver_rating = document.getElementById('user_rating').innerHTML;
if(driver_rating != ''){
  $('.driver_rating').starrr({rating: driver_rating, readOnly: true});
}
</script>
@endif
<script>
$(function() {
  function isEllipsisActive(element) {
    return element.offsetWidth < element.scrollWidth;
  }
  Array.from(document.querySelectorAll('.count-numbers, .preseci'))
  .forEach(element => {
    if (isEllipsisActive(element)) {
      element.title = element.innerText;
    }
  });
  $('[data-toggle="tooltip"]').tooltip()
});
</script>
@endsection
