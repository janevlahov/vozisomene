<form action="{{action('ReviewsController@store')}}" method="POST" id='comment_form'>
	<h3 class="mt-0 text-primary">@if(!empty($review) ) Измени коментар @else Оцени го возењето @endif</h3>
	<div class="row">
		<div class="col-md-3 col-xs-12">
			<div class='starrr'></div>
		</div>
		<input type='hidden' id='old_riderating' value="{{ old('ride_rating') }}" />
		@if ($errors->has('ride_rating'))
			<div class="col-md-9 col-xs-12">
				<span class="text-danger"><i class="fas fa-hand-point-left hidden-xs"></i> Избери оценка од 1 до 5</span>
			</div>
		@endif
	</div>
	<input type='hidden' name='ride_rating' id='ride_rating' @if(isset($review->rating)) value='{{$review->rating}}' @endif />
	<div class="mt-1 form-group{{ $errors->has('user_comment') ? ' has-error' : '' }}">
		<label class="control-label" for="user_comment">Дади краток опис за твоето искуство</label>
		<textarea name="user_comment" id="user_comment" class="form-control">@if(isset($review->comment)) {{$review->comment}} @else {{ old('user_comment') }} @endif</textarea>
	</div>
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="ride_user_id" value="{{$ride->user_id}}">
	<input type="hidden" name="ride_id" value="{{$ride->id}}">
	@if(!empty($review) > 0)
		<input type="hidden" name="review_id" value="{{$review->id}}">
	@endif
		<input type="submit" class="btn btn-primary btn-lg" @if(!empty($review) > 0) value="Измени" @else value="Додади коментар" @endif >
	@if(!empty($review) > 0) <a class="btn btn-danger" onclick="hide_edit_comment()">Откажи</a> @endif

</form>
