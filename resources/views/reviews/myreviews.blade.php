@extends('layouts.profile')

@section('profile_content')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3><i class="fas fa-comments"></i> Дадени коментари</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        @if(count($MyReviews) > 0 )
          @foreach($MyReviews as $review)
            <div class="row komentar">
              <div class='col-xs-4 col-md-2'>
                @if(!empty($review->avatar))<img src="/storage/upload/users/{{$review->avatar}}" class="img-responsive" alt="{{$review->firstname}}">
                  @else<img class="img-responsive" src="/storage/upload/web/nouser.jpg" alt="Nema slika">
                @endif
                <p class="text-center text-primary text-strong mt-1 mb-4 mb-xs-0">{{$review->firstname}}</p>
              </div>
              <div class="col-xs-8 col-md-10">
                <p class="mb-0"><i class="fas fa-calendar-alt text-primary"></i> {{ Carbon\Carbon::parse($review->created_at)->format('d.m.Y') }} <span class="ml-1 ml-xs-0 d-xs-block"><i class="fas fa-map-marker-alt text-primary"></i> {!! Helper::ride_place_name($review->s_user_id, $review->ride_id)[0] !!} <i class="fas fa-angle-double-right"></i> {!! Helper::ride_place_name($review->s_user_id, $review->ride_id)[1] !!}</span></p>
                <div class='starrr mt-xs-1'>
                  @for($i = 1; $i<= 5; $i++)
                    @if($i <= $review->rating)
                      <a style="cursor: default;" class="fa-star fa"></a>
                    @else
                      <a style="cursor: default;" class="fa-star-o fa"></a>
                    @endif
                  @endfor
                </div>
              </div>
              <div class="col-xs-12 col-md-10">
                <div class="panel panel-default mt-1 mb-4">
                  <div class="panel-body">
                    <p class="mb-0">{{$review->comment}}</p>
                  </div>
                </div>
              </div>
            </div>
          @endforeach

          {{ $MyReviews->links() }}

        @else
          <div class="col-md-12">
            <p class="lead text-danger">Нема коментари</p>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>

@endsection
