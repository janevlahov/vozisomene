<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Лозинката треба да има најмалку 6 карактери и да се совпаѓа со лозинката за потврда',
    'reset' => 'Вашата лозинка и ресетирана!',
    'sent' => 'Линкот за ресетирање на лозинката е испратен на твојата Е-Маил адреса!',
    'token' => 'Токенот за ресетирање на лозинката е невалиден.',
    'user' => "Не можеме да пронајдеме корисник со оваа Е-Маил адреса.",

];
