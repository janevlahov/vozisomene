<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Е-маил адресата или лозинката не се точни',
    'throttle' => 'Премногу обиди за најава. Те молиме, обиди се повторно за :seconds секунди.',

];
