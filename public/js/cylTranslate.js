/*lookUpObject = { A: "А", a: "а", B: "Б", b: "б", C: "Ц", c: "ц", V: "В", v: "в", W: "Њ", w: "њ", Y: "Ѕ", y: "ѕ", G: "Г", g: "г", D: "Д", d: "д", E: "Е", e: "е", "ЗХ": "Ж", "Зх": "Ж", "зх": "ж", Z: "З", z: "з", I: "И", i: "и", J: "Ј", j: "ј", K: "К", k: "к", L: "Л", l: "л", M: "М", m: "м", N: "Н", n: "н", O: "О", o: "о", P: "П", p: "п", R: "Р", r: "р", S: "С", s: "с", T: "Т", t: "т", U: "У", u: "у", F: "Ф", f: "ф", H: "Х", h: "х", "Цх": "Ч", "цх": "ч", "Сх": "Ш", "сх": "ш", "Нј": "њ", "нј": "њ", "Гј": "Ѓ", "гј": "ѓ", Q: "Љ", q: "љ", "Лј": "Љ", "лј": "љ", "}": "Ѓ", "]": "ѓ", ":": "Ч", ";": "ч", "\"": "Ќ", "\'": "ќ", "Кј": "Ќ", "кј": "ќ", "X": "Џ", "x": "џ", "Ѕх": "Џ", "ѕх": "џ", "Дз": "Ѕ", "дз": "ѕ", "|": "Ж", "\\": "ж", "{": "Ш", "[": "ш" };

function transformTypedChar(charStr) {
	var result = lookUpObject [charStr];
	if(result){
	    return lookUpObject[charStr];
	}else{
		return charStr;
	}
}

function getInputSelection(el) {
    var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;

    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        range = document.selection.createRange();

        if (range && range.parentElement() == el) {
            len = el.value.length;
            normalizedValue = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            textInputRange = el.createTextRange();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            endRange = el.createTextRange();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {
        start: start,
        end: end
    };
}

function offsetToRangeCharacterMove(el, offset) {
    return offset - (el.value.slice(0, offset).split("\r\n").length - 1);
}

function setInputSelection(el, startOffset, endOffset) {
    el.focus();
    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        el.selectionStart = startOffset;
        el.selectionEnd = endOffset;
    } else {
        var range = el.createTextRange();
        var startCharMove = offsetToRangeCharacterMove(el, startOffset);
        range.collapse(true);
        if (startOffset == endOffset) {
            range.move("character", startCharMove);
        } else {
            range.moveEnd("character", offsetToRangeCharacterMove(el, endOffset));
            range.moveStart("character", startCharMove);
        }
        range.select();
    }
}

 $('input:not(.noCyrillic,[type="checkbox"]),textarea').keypress(function(evt) {
    if (evt.which) {
        var charStr = String.fromCharCode(evt.which);
        var transformedChar = transformTypedChar(charStr);
        if (transformedChar != charStr) {
            var sel = getInputSelection(this), val = this.value;
            this.value = val.slice(0, sel.start) + transformedChar + val.slice(sel.end);

            // Move the caret
            setInputSelection(this, sel.start + 1, sel.start + 1);
            return false;
        }
    }
});*/