function getCarModel(carModel){
	switch(carModel) {
		case 'Abarth':
		   	return ["124 Spider", "500", "500C", "595", "695", "Grande Punto", "Punto EVO", "Punto Supersport", "Друг модел"];
		break;
		case 'Acura':
	    	return ["RL", "TL", "TSX", "ZDX", "RDX", "MDX", "Друг модел"];
	    break;
	    case 'Aixam':
	    	return ["571", "721", "741", "Scouty", "Друг модел"];
	    break;
	    case 'Alfa Romeo':
	    	return ["8 c", "Alfa 145", "Alfa 146", "Alfa 147", "Alfa 149", "Alfa 155", "Alfa 156", "Alfa 159", "Alfa 164", "Alfa 166", "Alfa 1750", "Alfa 2000", "Alfa 33", "Alfa 4C", "Alfa 75", "Alfa 8C", "Alfa 90", "Alfasud", "Alfetta", "Brera", "Crosswagon", "Giulia", "Giulietta", "GT", "GTV", "GTV6", "MiTo", "RZ/SZ", "Spider", "Sportwagon", "Sprint", "SZ", "Друг модел"];
	    break;
	    case 'Aston Martin':
	    	return ["AR1", "Cygnet", "DB", "DB 7", " DB 9","DB 11", "Lagonda", "Rapide", "V 8", "Vantage", "Vanquish", "Virage", "Volante", "Друг модел"];
	    break;
	    case 'Audi':
	    	return ["100", "200", "80",  "90", "А1", "A2", "A3", "A4", "A5", "A6", "А7", "A8", "Allroad", "Coupé", "Cabriolet", "Q2", "Q3", "Q5", "Q7", "Quattro", "R8",  "RS 2", "RS 4", "RS 5", "RS 6", "RS 7", "S1", "S2",  "S3",  "S4", "S5", "S6", "S8", "SQ5", "SQ7", "TT", "V8", "Друг модел"];
	    break;
	    case 'Bentley':
	    	return ["Arnage", "Azure", "Brooklands", "Continental", "4Eghit08", "Mulsanne", "Turbo R", "Turbo RT", "Turbo S", "Друг модел"];
	    break;
	    case 'Buick':
	    	return ["Enclave", "LaCrosse", "Lacerene", "Друг модел"];
	    break;
	    case 'BMW':
	    	return ["114", "116", "118", "120", "123", "125", "130", "135", "140", "214", "216", "218", "220", "225", "228", "230", "235", "240", "315", "316", "318", "320", "323", "324", "325", "328", "330", "335", "418", "420", "425", "428", "430", "435", "440", "518", "520", "523", "524", "525", "528", "530", "535", "540", "545", "550", "628", "630", "633", "635", "645", "650", "725", "728", "730", "732", "735", "740", "745", "750", "760", "830", "840", "850", "M1", "M3", "M5", "M6", "X1", "X2", "X3", "X4", "X5", "X6", "Z1", "Z3", "Z4", "Z8", "Друг модел"];
	    break;
	    case 'Cadillac':
	    	return ["Allante", "ATS", "BIS", "Brougham", "CT", "CTS", "Deville", "DTS", "Eldorado", "Escalade", "Fleetwood", "La Salle", "Series", "Seville", "SRX", "STS", "STX", "XLR", "XT5", "Друг модел"];
	    break;
	    case 'Chevrolet':
	    	return ["2500", "Alero", "Astro", "Avalanche", "Aveo", "Beretta", "Blazer", "C1500", "Camaro", "Caprice", "Captiva", "Cavalier", "Chevelle", "Chevy Van", "Citation", "Colorado", "Corsica", "Corvette", "Cruze", "El Camino", "Epica", "Equinox", "Evada", "G", "G 20", "HHR", "Impala", "5K 150012", "K 30", "Kalos", "Lacetti", "Lanos", "Lumina", "Malibu", "Matiz", "Monte Carlo", "Nubira", "Orlando", "Rezzo", "S-10", "Silverado", "Spark", "SSR", "Suburban", "Tacuma", "Tahoe", "Trailblazer", "Trans Sport", "Trax", "Uplander", "Viva", "Volt", "Venture", "Друг модел"];
	    break;
	    case 'Chrysler':
	    	return ["300C", "300 M", "Aspen", "Crossfire", "Daytona", "ES", "Grand Voyager", "GS", "GTS", "Imperial", "Le Baron", "Neon", "New Yorker", "Pacifica", "Prowler", "PT Cruiser", "Ram Van", "Saratoga", "Sebring", "Stratus", "Town &amp; Country", "Valiant", "Viper", "Vision", "Voyager", "Друг модел"];
	    break;
	    case 'Citroen':
	    	return ["2 CV", "Acadiane", "AX", "Axel", "Berlingo", "BX", "C-Crosser", "C-Elysee", "C-Zero", "C1", "C15", "C2", "C25", "C3", "C3 Picasso", "C35", "C4", "C4 Picasso", "C4 Cactus", "C4 Aircross", "C5", "C6", "C8", "C-Crosse", "CX", "DS", "DS3", "DS4", "DS5", "Dyane", "Evasion", "Grand C4 Picasso", "GSA", "Jumper", "Jumpy", "LNA", "Mehari", "Nemo", "SAXO", "SM", "Spacetourer", "Visa", "Xantia", "XM", "Xsara", "Xsara Picasso", "ZX", "Друг модел"];
	    break;
	    case 'Dacia':
	    	return ["1310", "Berlina", "Break", "Dokker", "Double Cab", "Drop Side", "Lodgy", "Logan", "Nova", "Pick Up", "Sandero", "Stepway", "Друг модел"];
	    break;
	    case 'Daewoo':
	    	return ["Espero", "Evanda", "Kalos", "Korando", "Lacetti", "Lanos", "Leganza", "Lublin", "Matiz", "Musso", "Nexia", "Nubira", "Rexton", "Rezzo", "Tacuma", "Tico", "Друг модел"];
	    break;
	    case 'Daihatsu':
	    	return ["Applause", "Charade", "Charmant", "Copen", "Cuore", "Domino", "Feroza/Sportrak", "Freeclimber", "Gran Move", "Hijet", "MATERIA", "Move", "Rocky/Fourtrak", "Sirion", "Taf", "Terios", "TREVIS", "YRV", "Друг модел"];
	    break;
	    case 'Dodge':
	    	return ["Avenger", "Caliber", "Caravan", "Challenger", "Charger", "Coronet", "Dakota", "Dart", "Demon", "Durango", "Grand Caravan", "Intrepid", "Hornet", "Journey", "Magnum", "Neon", "Nitro", "RAM", "Stealth", "Startus", "Van", "Viper", "Друг модел"];
	    break;
	    case 'DS Automobiles':
	    	return ["DS 3", "DS 4", "DS 5", "Друг модел"];
	    break;
	    case 'Ferrari':
	    	return ["208", "246", "250", "275", "649288", "308", "328", "330", "348", "360", "365", "400", "412", "456", "458", "550", "575", "599 GTB", "612", "750", "California", "Daytona", "Dino GT4", "Enzo Ferrari", "F12", "F355", "F40", "F430", "F50", "512", "FF", "FXX", "GTC4 Lusso", "LaFerrari", "Mondial", "Superamerica", "Testarossa", "Друг модел"];
	    break;
	    case 'Fiat':
	    	return ["124", "126", "127", "130", "131", "132", "133", "242", "500", "500 Abarth", "595", "600", "850", "900", "Albea", "Argenta", "Barchetta", "Brava", "Bravo", "Campagnola", "Cinquecento", "Coupe", "Croma", "Dino", "Doblo", "Ducato", "Duna", "Fiorino", "Freemont", "Fullback", "Grande Punto", "Grande Punto Abarth", "Idea", "Linea", "Marea", "Marengo", "Maxi", "Multipla", "New Panda", "Penny", "Pinifarina", "Palio", "Panda", "Punto", "Qubo", "Regata", "Ritmo", "Scudo", "Sedici", "Seicento", "Spider Europa", "Stilo", "Strada", "Tempra", "Tipo", "Ulysse", "Uno", "X 1/9", "Друг модел"];
	    break;
	    case 'Ford':
	    	return ["Aerostar", "B-Max", "Bronco", "C-Max", "Capri", "Connekt Elekreo", "Cougar", "Courier", "Crown", "Econoline", "Econovan", "EcoSport", "Edge", "Escape", "Escort", "Excursion", "Expedition", "Explorer", "Express", "F 1", "F 100", "F 150", "F 250", "F 350", "F 360", "F 450", "F 550", "F 650", "Super Duty", "Fairlane", "Falcon", "Fiesta", "Flex", "Focus", "Focus C-Max", "Focus CC", "Freestar", "FreeStyle", "Fusion", "Galaxy", "Gran Torino", "Granada", "Grand C-Max", "GT", "Ka", "Kuga", "M", "Maverick", "Mercury", "Mondeo", "Mustang", "Orion", "Probe", "Puma", "Ranger", "Scorpio", "Sierra", "S-Max", "Sportka", "Streetka", "Taunus", "Taurus", "Torino", "Thunderbird", "Tourneo", "Transit", "Windstar", "Друг модел"];
	    break;
	    case 'Honda':
	    	return ["Accord", "Ascot", "Avancier", "Aerodeck", "Capa", "City", "Civic", "Concerto", "CR-V", "CR-Z", "Crosstour", "CRX", "Element", "Fit", "FR-V", "HR-V", "Insight", "Inspire", "Integra", "Jazz", "Legend", "Life", "Mobilio", "Logo", "NSX", "Odyssey", "Orthia", "Partner", "Pilot", "Prelude", "Quintet", "Ridgeline", "S2000", "Saber", "Shuttle", "Sm-x", "Stepwgn", "Stream", "Torneo", "Друг модел"];
	    break;
	    case 'Hummer':
	    	return ["H1", "H2", "H3", "Друг модел"];
	    break;
	    case 'Hyundai':
	    	return ["Accent", "Atos", "Avante", "Azera", "Coupe", "Elantra", "Equus", "Excel", "Galloper", "Genesis", "Getz", "Grace", "Grand Santa Fe", "Grand Starex H1", "Grandeur", "H-1", "H 100", "H-1 Starex", "H 200", "H 300", "H 350", "H-1", "H-1 Cargo", "H-1 Starex", "H-1 Travel", "Highway", "i10", "i20", "i30", "i40", "i50", "i800", "loniq", "ix20", "ix35", "ix55", "Lantra", "Matrix", "NF", "Pony", "Porter", "Santa Fe", "Santamo", "Solaris", "S-Coupe", "Sonata", "Starex", "Stellar", "Terracan", "Tiburon", "Trajet", "Tucson", "Verna", "XG 30", "XG 350", "Друг модел"];
	    break;
	    case 'Infiniti':
	    	return ["EX25", "EX30", "EX35", "EX37", "FX", "FX30", "FX50", "FX60", "G25", "G37", "10106", "I35", "JX35", "M30", "M35h", "M37", "M45", "Q30", "Q45", "Q50", "Q60", "Q70", "QX30", "QX50", "QX56", "QX60", "QX70", "QX80", "Друг модел"];
	    break;
	    case 'Isuzu':
	    	return ["Axiom", "Bighorn", "Campo", "D-Max", "Gemini", "Midi", "NKR", "NNR", "NPR", "PICK UP", "Rodeo", "Trooper", "WFR", "Друг модел"];
	    break;
	    case 'Jaguar':
	    	return ["420", "D-Type", "Daimler", "E-Type", "MK II", "S-Type", "Sovereign", "X300", "XE", "XF", "XJ", "XJ12", "XJ40", "XJ6", "XJ8", "XJR", "XJS", "XJSC", "XK", "XK8", "XKR", "X-Type", "Друг модел"];
	    break;
	    case 'Jeep':
	    	return ["Cherokee", "CJ", "Comanche", "Commander", "Compass", "Grand Cherokee", "Liberty", "Patriot", "Renegade", "Wagoneer", "Willys", "Wrangler", "Друг модел"];
	    break;
	    case 'Kia':
	    	return ["Besta", "Borrego", "Carens", "Carnival", "Ceed", "Ceed Sporty Wagon", "Cerato", "Clarus", "Elan", "Joice", "K2500", "K2700", "K2900", "Leo", "Magentis", "Mentor", "Mohave / Borrego", "Mini", "Niro", "Opirus", "Picanto", "Pregio", "Pride", "ProCeed", "Retona", "Rio", "Roadster", "Rocsta", "Sephia", "Shuma", "Sorento", "Soul", "Sportage", "Venga", "Друг модел"];
	    break;
	    case 'Lada':
	    	return ["110", "111", "112", "1200", "1300 / 1500", "2107", "2110", "2111", "2112", "4x4", "Aleko", "C-Cross", "Carlota", "Forma", "Granta", "Kalina", "Largus", "Natacha", "Niva", "Nova", "Priora", "Sagona", "Samara", "Sprint", "Taiga", "Universal", "VAZ 215", "Vesta", "Друг модел"];
	    break;
	    case 'Lamborghini':
	    	return ["Aventador", "Countach", "Diablo", "Espada", "Gallardo", "Huracan", "Jalpa", "LM", "Miura", "Murciélago", "Reventon", "Urraco", "Друг модел"];
	    break;
	    case 'Lancia':
	    	return ["A112", "Beta", "Dedra", "Delta", "Flaminia", "Flavia", "Fulvia", "Gamma", "HPE", "K", "Kappa", "Lybra", "MUSA", "Phedra", "Prisma", "Stratos", "Thema", "Thesis", "Trevi", "Voyager", "Ypsilon", "Zeta", "Друг модел"];
	    break;
	    case 'Land Rover':
	    	return ["Defender", "Discovery", "Discovery Sport", "Freelander", "LRX", "Range Rover", "Range Rover Evoque", "Range Rover Sport", "Serie I", "Serie II", "Serie III", "Друг модел"];
	    break;
	    case 'Lexus':
	    	return ["10355", "ES 300", "ES 330", "ES 350", "GS 250", "GS 300", "GS 350", "GS 430", "GS 450", "GS 460", "GX 470", "IS 200", "IS 220", "IS 250", "IS 300", "IS 350", "IS-F", "LC 500", "LS 400", "LS 430", "LS 460", "LX 470", "LX 570", "LS 600", "NX 200", "NX 300", "RC 200", "RC 300", "RC 350", "RC F", "RX 200", "RX 300", "RX 330", "RX 350", "RX 400", "RX 450", "SC 400", "SC 430", "Друг модел" ];
	    break;
	    case 'Liger':
	    	return ["Ambra", "Nova", "Optima", "Too", "Друг модел"];
	    break;
	    case 'Lotus':
	    	return ["2-Eleven", "340 R", "Cortina", "Elan", "Elise", "Elite", "Esprit", "Europa", "Evora", "Excel", "Exige", "Omega", "Super Seven", "V8", "Venturi", "Друг модел"];
	    break;
	    case 'Maserati':
	    	return ["222", "224", "228", "3200", "418", "420", "4200", "422", "424", "430", "Biturbo", "Ghibli", "Gransport", "Granturismo", "Indy", "Karif", "Levante", "MC12", "Merak", "Quattroporte", "Racing", "Shamal", "Spyder", "TC", "Друг модел"];
	    break;
	    case 'Maybach':
	    	return ["57", "62", "Друг модел"];
	    break;
	    case 'Mazda':
	    	return ["121", "2", "3", "323", "5", "6", "626", "929", "Atenza", "Axela", "B2500", "B2600", "Bongo", "B series", "BT-50", "Capella", "CX-3", "CX-5", "CX-7", "CX-9", "Demio", "E series", "Familia", "Millenia", "MPV", "MX-3", "MX-5", "MX-6", "Pick Up", "Premacy", "Protege", "RX-6", "RX-7", "RX-8", "Tribute", "Xedos", "Друг модел"];
	    break;
	    case 'Mercedes-Benz':
	    	return ["190", "200", "220", "230", "240", "250", "260", "270", "280", "290", "300", "320", "350", "380", "400", "416", "420", "450", "500", "560", "600", "A 140", "A 150", "A 160", "A 170", "A 180", "A 190", "A 200", "A 210", "A 220", "A 250", "Actros", "AMG GT", "Atego", "B 150", "B 160", "B 170", "B 180", "B 200", "B 220", "B 250", "B Electric Drive", "C 160", "C 180", "C 200", "C 220", "C 230", "C 240", "C 250", "C 270", "C 280", "C 300", "C 30 AMG", "C 320", "C 32 AMG", "C 350", "C 36 AMG", "C 400", "C 43 AMG", "C 450", "C 55 AMG", "C 63 AMG", "CE 200", "CE 220", "CE 230", "CE 280", "CE 300", "Citan", "CL 160", "CL 180", "CL 200", "CL 220", "CL 230", "CL 420", "CL 500", "CL 55 AMG", "CL 600", "CL 63 AMG", "CL 65 AMG", "CLA  180", "CLA  200", "CLA  220", "CLA  250", "CLA  45 AMG", "CLC 160", "CLC 180", "CLC 200", "CLC 220", "CLC 230", "CLC 250", "CLC 350", "CLK 200", "CLK 220", "CLK 230", "CLK 240", "CLK 270", "CLK 280", "CLK 320", "CLK 350", "CLK 430", "CLK 500", "CLK 55 AMG", "CLK 63 AMG", "CLS 220", "CLS 250", "CLS 280", "CLS 300", "CLS 320", "CLS 350", "CLS 350", "CLS 400", "CLS 500", "CLS 55 AMG", "CLS 63 AMG", "E 200", "E 220", "E 230", "E 240", "E 250", "E 260", "E 270", "E 280", "E 290", "E 300", "E 320", "E 350", "E 36 AMG", "E 400", "E 420", "E 430", "E 50", "E 500", "E 55", "E 60 AMG", "E 63 AMG", "G 230", "G 240", "G 250", "G 270", "G 280", "G 290", "G 300", "G 320", "G 350", "G 400", "G 500", "G 55 AMG", "GL 320", "GL 350", "GL 420", "GL 450", "GL 500", "GL 55 AMG", "GL 63 AMG", "GLA 180", "GLA 200", "GLA 250", "GLA 45 AMG", "GLC 220", "GLC 250", "GLC 350", "GLC 43 AMG", "GLE 250", "GLE 350", "GLE 400", "GLE 43 AMG", "GLE 450", "GLE 500", "GLE 63 AMG", "GLK 200", "GLK 220", "GLK 250", "GLK 280", "GLK 300", "GLK 320", "GLK 350", "GLS 350", "GLS 400", "GLS 500", "GLS 63 AMG", "MB 100", "ML 230", "ML 250", "ML 270", "ML 280", "ML 300", "ML 320", "ML 350", "ML 400", "ML 420", "ML 430", "ML 450", "ML 500", "ML 55 AMG", "ML 63 AMG", "R 280", "R 300", "R 320", "R 350", "R 500", "R 63 AMG", "S 250", "S 260", "S 280", "S 300", "S 320", "S 350", "S 400", "S 420", "S 430", "S 450", "S 500", "S 55", "S 550", "S 600", "S 63 AMG", "S 65 AMG", "SL 230", "SL 280", "SL 300", "SL 320", "SL 350", "SL 380", "SL 420", "SL 450", "SL 500", "SL 55 AMG", "SL 560", "SL 600", "SL 60 AMG", "SL 63 AMG", "SL 65 AMG", "SL 70 AMG", "SL 73 AMG", "SLC 180", "SLC 200", "SLC 250", "SLC 300", "SLC 43 AMG", "SLK 200", "SLK 230", "SLK 250", "SLK 280", "SLK 300", "SLK 320", "SLK 32 AMG", "SLK 350", "SLK 55 AMG", "SLR", "SLS AMG", "Sprinter", "T1", "T2", "V 200", "V 220", "V 230", "V 280", "Vaneo", "Vario", "Viano", "Vito", "Друг модел"];
	    break;
	    case 'MG':
	    	return ["MGA", "MGB", "MGF", "Midget", "Montego", "TD", "TF", "ZR", "ZS", "ZT", "Друг модел"];
	    break;
	    case 'Mini':
	    	return ["1000", "1300", "Cabrio", "Clubman", "Clubvan", "Coope", "Cooper S", "Countryman", "John Cooper Works", "ONE", "Paceman", "Roadster", "Друг модел"];
	    break;
	    case 'Mitsubishi':
	    	return ["3000 GT", "400", "Airtrek", "ASX", "Attrage", "Canter", "Carisma", "Colt", "Cordia", "Cosmos", "Delica", "Diamante", "Dingo", "Dion", "Eclipse", "Galant", "Galloper", "Grandis", "I-MiEV", "L200", "L300", "L400", "Lancer", "Legnum", "Mirage", "Montero", "Outlander", "Pajero", "Pajero Pinin", "Pick-up", "RVR", "Santamo", "Sapporo", "Sigma", "Space Gear", "Space Runner", "Space Star", "Space Wagon", "Starion", "Tredia", "Друг модел"];
	    break;
	    case 'Nissan':
	    	return ["100 NX", "200 SX", "240 SX", "280 ZX", "300 ZX", "350Z", "370Z", "AD", "Almera", "Almera Tino", "Altima", "Armada", "Avenir", "Bassara", "Bluebird", "Cabstar", "Cargo", "Cedric", "Cefiro", "Cherry", "Cube", "Datsun", "E-NV200", "Elgrand", "Evalia", "Figaro", "Frontier", "Gloria", "GT-R", "Interstar", "Juke", "King Cab", "Kubistar", "Laurel", "Leaf", "Liberty", "March", "Maxima", "Micra", "Murano", "Navara", "Note", "NP 300", "NV 200", "NV 300", "NV 400", "Pathfinder", "Patrol", "Pick Up", "Pixo", "Prairie", "Presage", "Presea", "Primastar", "Primera", "Pulsar", "Qashqai", "Qashqai+2", "Quest", "R Nessa", "Rogue", "Safari", "Sentra", "Serena", "Silvia", "Skyline", "Stagea", "Sunny", "Teana", "Terrano", "Tiida", "Tino", "Titan", "Trade", "Urvan", "Vanette", "Wingroad", "X-Trail", "Друг модел"];
	    break;
	    case 'Opel':
	    	return ["Adam", "Agila", "Ampera", "Antara", "Arena", "Ascona", "Astra", "Calibra", "Campo", "Cavalier", "Combo", "Cascada", "Commodore", "Corsa", "Diplomat", "Frontera", "GT", "Insignia", "Kadett", "Karl", "Manta", "Meriva", "Mokka", "Monterey", "Monza", "Movano", "Nova", "Omega", "Pick Up Sportscap", "Rekord", "Senator", "Signum", "Sintra", "Speedster", "Tigra", "Vectra", "Vivaro", "Zafira", "Друг модел"];
	    break;
	    case 'Peugeot':
	    	return ["1007", "104", "106", "107", "108", "2008", "204", "205", "206", "207", "208", "3008", "301", "304", "305", "1306425", "307", "308", "309", "4007", "4008", "404", "405", "406", "407", "5008", "504", "505", "604", "1436057", "607", "806", "807", "Bipper", "Boxer", "Campert", "Expert", "iOn", "J5", "J9", "144Partner5", "Ranch", "RCZ", "Traveller", "TePee", "Друг модел"];
	    break;
	    case 'Pontiac':
	    	return ["6000", "Bonneville", "Fiero", "Firebird", "G6", "Grand-Am", "Grand-Prix", "GTO", "Montana", "Solstice", "Sunbird", "Sunfire", "Targa", "Trans Am", "Trans Sport", "Vibe", "Друг модел"];
	    break;
	    case 'Porsche':
	    	return ["550", "356", "911", "912", "914", "924", "928", "944", "959", "962", "968", "Boxster", "Carrera GT", "Cayenne", "Cayman", "Macan", "Panamera", "Targa", "Друг модел"];
	    break;
	    case 'Proton':
	    	return ["300 Serie", "400 Serie", "Друг модел"];
	    break;
	    case 'Renault':
	    	return ["Alpine A110", "Alpine V6", "Avantime", "Captur", "Clio", "Coupe", "Duster", "Espace", "Express", "Fluence", "Fuego", "Grand Espace", "Grand Modus", "Grand Scenic", "Kadjar", "Kangoo", "Koleos", "Laguna", "Latitude", "Logan", "Mascott", "Master", "Megane", "Modus", "P 1400", "R 11", "R 14", "R 18", "R 19", "R 20", "R 21", "R 25", "R 30", "R 4", "R 5", "R 6", "R 9", "Rapid", "Safrane", "Sandero", "Scenic", "Spider", "Super 5", "Symbol", "Talisman", "Trafic", "Twingo", "Twizy", "Vel Satis", "Wind", "ZOE", "Друг модел"];
	    break;
	    case 'Rolls-Royce':
	    	return ["Corniche", "Flying Spur", "Ghost", "Park Ward", "Phantom", "Silver Cloud", "Silver Dawn", "Silver Seraph", "Silver Shadow", "Silver Spirit", "Silver Spur", "Друг модел"];
	    break;
	    case 'Rover':
	    	return ["100", "111", "114", "115", "200", "213", "214", "216", "218", "220", "25", "400", "414", "416", "418", "420", "45", "600", "618", "620", "623", "75", "800", "820", "825", "827", "City Rover", "Metro", "Estate", "Metro", "MINI", "Montego", "Rover", "SD", "Streetwise", "Tourer", "Друг модел"];
	    break;
	    case 'Saab':
	    	return ["9", "90", "900", "9000", "9-3", "9-4X", "9-5", "96", "9-7X", "99", "Друг модел"];
	    break;
	    case 'Seat':
	    	return ["Alhambra", "Altea", "Arosa", "Ateca", "Cordoba", "Exeo", "Fura", "Ibiza", "Inca", "Leon", "Malaga", "Marbella", "Mii", "Panda", "Ronda", "Terra", "Toledo", "Друг модел"];
	    break;
	    case 'Skoda':
	    	return ["105", "120", "130", "135", "Citigo", "Fabia", "Favorit", "Felicia", "Forman", "Kodiaq", "Octavia", "Pick-up", "Praktik", "Rapid / Spaceback", "Roomster", "Snowman", "Superb", "Yeti", "Друг модел"];
	    break;
	    case 'Smart':
	    	return ["Barbus", "Ciy - coupe / cabrio", "Crossblade", "ForFour", "ForTwo", "Roadster", "Друг модел"];
	    break;
	    case 'Ssangyong':
	    	return ["Actyon", "Family", "Kallista", "Korando", "Kyron", "MUSSO", "REXTON", "Rodius", "Tivoli", "XLV", "Друг модел"];
	    break;
	    case 'Subaru':
	    	return ["1200", "1800", "B9 Tribeca", "Baja", "BRZ", "E10", "E12", "Forester", "Impreza", "Justy", "Legacy", "Leone", "Levorg", "Libero", "M60", "M70", "M80", "Mini", "OUTBACK", "16SVX37", "Trezia", "Tribeca", "Vanille", "Vivio", "WRX", "XT", "XV", "Друг модел"];
	    break;
	    case 'Suzuki':
	    	return ["Alto", "Baleno", "Cappuccino", "Carry", "Celerio", "Escudo", "Grand Vitara", "Ignis", "Jimny", "Kizashi", "Liana", "LJ", "Maruti", "SA 310", "SJ Samurai", "Santana", "SJ 410", "SJ 413", "Splash", "Super-Carry", "Swift", "SX4", "Vitara", "Wagon R+", "X-90", "XL-7", "Друг модел"];
	    break;
	    case 'Toyota':
	    	return ["4-Runner", "Allion", "Alphard", "Aristo", "Auris", "Avalon", "Avensis", "Avensis Verso", "Aygo", "BB", "Belta", "Caldina", "Cami", "Camry", "Carina", "Celica", "Chaser", "Corolla", "Corolla Verso", "Corona", "Corsa", "Cressida", "Crown", "Duet", "Dyna", "Estima", "F", "FJ", "FJ 40", "Fortuner", "Fun Cruiser", "Funcargo", "Gaia", "GT86", "Harrier", "HDJ", "Hiace", "Highlander", "Hilux", "Ipsum", "IQ", "Ist", "KJ", "Land Cruiser", "LC", "Lite-Ace", "Mark II", "Matrix", "Mirai", "MR 2", "Nadia", "Noah", "Opa", "Paseo", "Passo", "Pick up", "Picnic", "Platz", "Premio", "Previa", "Prius", "Pro Acev", "Ractis", "Raum", "RAV 4", "Sequoia", "Sienna", "Solara", "Sprinter", "Starlet", "Supra", "Tacoma", "Tercel", "Town Ace", "Tundra", "Urban Cruiser", "Venza", "Verossa", "Verso", "Vista", "Vitz", "Voxy", "Will", "Wish", "Yaris", "Друг модел"];
	    break;
	    case 'Volkswagen':
	    	return ["181", "Amarok", "Anfibio", "Beetle", "Bora", "Buggy", "Bus", "Caddy", "CC", "Coccinelle", "Corrado", "Crafter", "Cros Touran", "Derby", "Eos", "Escarabajo", "Fox", "Golf", "Golf Plus", "Iltis", "Jetta", "Kafer", "Karmann Ghia", "L 80", "LT", "Lupo", "Maggiolino", "New Beetle", "Passat", "Phaeton", "Polo", "Routan", "Santana", "Scirocco", "Sharan", "T1", "T2", "T3 Други", "T3 Caravelle", "T3 Multivan", "T4 Други", "T4 Caravelle", "T4 Multivan", "T5 Други", "T5 Caravelle", "T5 Multivan", "T5 Shuttle", "T6 California", "T6 Caravelle", "T6 Multivan", "T6 Tranporter", "Taro", "Tiguan", "Touareg", "Touran", "Transporter", "Up", "Vento", "XL1", "Друг модел"];
	    break;
	    case 'Volvo':
	    	return ["240", "244", "245", "262", "264", "265", "340", "360", "440", "460", "480", "740", "744", "745", "760", "780", "850", "855", "940", "944", "945", "960", "965", "Amazon", "C30", "C70", "Polar", "S40", "S60", "S70", "S80", "S90", "V40", "V50", "V70", "V90", "XC 60", "XC 70", "XC 90", "Друг модел"];
	    break;
	    case 'Wartburg':
	    	return ["311", "353", "Друг модел"];
	    break;
	    case 'Yugo':
	    	return ["45", "55", "60", "65", "Skala", "Florida", "Poly", "Друг модел"];
	    break;
	    case 'Zastava':
	    	return ["101", "128", "750", "850", "Yugo", "Poly", "Друг модел"];
	    break;
	 	default:
	    return ["Нема Информации"];
	}
}
