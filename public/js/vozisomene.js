 $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    function MarkAllAsRead() {
      $.ajax({
        type:'POST',
        url:'/notifications/markAllRead',
        data:'',
          success:function(data) {
      
            $('#notificationsList').empty();
            $('#notificationCounter').remove();
            $('#notificationsList').append("<li><a href='#' class='text-center btn btn-link'>Немаш нови известувања</a></li>");
            
            if($('#notifications_data').length){
              $('#notifications_data').empty();
              $('#notifications_data').append("<p class='lead text-danger'>Немаш нови известувања</p>");
            }

          },
          error: function (err) {
            //console.log("AJAX error in request: " + JSON.stringify(err, null, 2));
          }
      });
    }